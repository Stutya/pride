angular.module('UB_PRIDE_APP.controllers', [])
    .run(function($ionicPlatform, $state, $log, $window, $ionicLoading, $rootScope, $ionicHistory) {
        $ionicPlatform.ready(function() {
            document.addEventListener("resume", function() {
                $ionicLoading.hide();
                $log.debug("The application is resuming from the background");
            }, false);
        });
        $ionicPlatform.registerBackButtonAction(function() {
            $log.debug('mypage' + JSON.stringify($rootScope.processFlowLineItems));
            if ($state.current.name == "app.today") {
                navigator.app.exitApp();
            } else if ($rootScope.NonPerfectCallControllers.indexOf($rootScope.name) != -1) {
                navigator.app.backHistory();
            } else if ($rootScope.controllersList.indexOf($rootScope.name) != -1) {
                if ($rootScope.params.order != undefined || $rootScope.name == 'CheckoutCtrl') {
                    if ($rootScope.params.order != undefined && $rootScope.params.order == 0) {
                        window.location.href = '#/app/lastVistSummary/' + $rootScope.params.AccountId;
                        //$location.path('app/lastVistSummary/' + $rootScope.params.AccountId);
                    } else {
                        var pageOrder;
                        if ($rootScope.name == 'CheckoutCtrl') {
                            pageOrder = $rootScope.processFlowLineItems.length - 1;
                        } else {
                            pageOrder = parseInt($rootScope.params.order) - 1;
                        }

                        if ($rootScope.processFlowLineItems[pageOrder] != undefined) {
                            var nextPageId = $rootScope.processFlowLineItems[pageOrder].Page__c;
                            var nextPage;
                            angular.forEach($rootScope.pages, function(record, key) {
                                if (record.pageDescription.Id == nextPageId) {
                                    nextPage = record;
                                }
                            });
                            if (nextPage.pageDescription.Template_Name__c == 'Template1') {
                                if (pageOrder == 3) {
                                    window.location.href = '#/app/stockDetails/' + $rootScope.params.AccountId + '/' + $rootScope.params.visitId + '/' + pageOrder + '/' + new Date();
                                    //$location.path('app/stockDetails/' + $rootScope.params.AccountId + '/' + $rootScope.params.visitId + '/' + pageOrder + '/' + new Date());
                                } else {
                                    window.location.href = '#/app/stocks/' + $rootScope.params.AccountId + '/' + $rootScope.params.visitId + '/' + pageOrder + '/' + new Date();
                                    //$location.path('app/stocks/' + $rootScope.params.AccountId + '/' + $rootScope.params.visitId + '/' + pageOrder + '/' + new Date());
                                }
                            } else if (nextPage.pageDescription.Template_Name__c == 'Template2') {
                                $rootScope.resentlyAdded = true;
                                window.location.href = '#/app/container2/' + $rootScope.params.AccountId + '/' + $rootScope.params.visitId + '/' + pageOrder + '/' + new Date();
                                //$location.path('app/container2/' + $rootScope.params.AccountId + '/' + $rootScope.params.visitId + '/' + pageOrder + '/' + new Date());
                            } else {
                                $rootScope.resentlyAdded2 = true;
                                window.location.href = '#/app/container3/' + $rootScope.params.AccountId + '/' + $rootScope.params.visitId + '/' + pageOrder + '/' + new Date();
                                //$location.path('app/container3/' + $rootScope.params.AccountId + '/' + $rootScope.params.visitId + '/' + pageOrder + '/' + new Date());
                            }
                        } else {
                            window.location.href = '#/app/checkout/' + $rootScope.params.AccountId + '/' + $rootScope.params.visitId;
                            //$location.path('app/checkout/' + $rootScope.params.AccountId + '/' + $rootScope.params.visitId);
                        }
                    }
                } else {
                    if ($rootScope.name == 'OverviewCtrl') {
                        //$rootScope.goToHome();
                        $ionicHistory.nextViewOptions({
                            disableBack: true
                        });
                        window.location.href = '#/app/today/' + new Date();
                    } else if ($rootScope.name == 'BusinessObjectivesCtrl') {
                        window.location.href = '#/app/overview/' + $rootScope.params.accountId;
                        //$location.path('app/overview/' + $rootScope.params.accountId);
                    } else if ($rootScope.name == 'LastVisitSummaryCtrl') {
                        window.location.href = '#/app/overview/' + $rootScope.params.accountId;
                        //$location.path('app/overview/' + $rootScope.params.accountId);
                    }
                }
            } else {
                if ($rootScope.name == 'OverviewCtrl') {
                    $ionicHistory.nextViewOptions({
                        disableBack: true
                    });
                    window.location.href = '#/app/today/' + new Date();
                } else if ($rootScope.name == 'BusinessObjectivesCtrl' || $rootScope.name == 'MarketShareCtrl') {
                    window.location.href = '#/app/overview/' + $rootScope.params.accountId;
                    //$location.path('app/overview/' + $rootScope.params.accountId);
                } else if ($rootScope.name == 'LastVisitSummaryCtrl') {
                    window.location.href = '#/app/overview/' + $rootScope.params.accountId;
                    //$location.path('app/overview/' + $rootScope.params.accountId);
                }
            }
        }, 100);
    })
    .config(function($ionicConfigProvider) {
        $ionicConfigProvider.views.maxCache(0);
        // note that you can also chain configs
        $ionicConfigProvider.backButton.text('Back').icon('ion-chevron-left');
    })
    .filter('cut', function() {
        return function(value, wordwise, max, tail) {
            if (!value) return '';

            max = parseInt(max, 10);
            if (!max) return value;
            if (value.length <= max) return value;

            value = value.substr(0, max);
            if (wordwise) {
                var lastspace = value.lastIndexOf(' ');
                if (lastspace != -1) {
                    value = value.substr(0, lastspace);
                }
            }

            return value + (tail || ' …');
        };
    })
    .directive('myTouchstart', [function() {
        return function(scope, element, attr) {

            element.on('touchstart', function(event) {
                scope.$apply(function() { 
                    scope.$eval(attr.myTouchstart); 
                });
            });
        };
    }])
    .controller('AppCtrl', function($scope, $ionicHistory, $rootScope, $ionicPopup, $window, $q, $ionicModal, $timeout, $interval, $location, $ionicLoading, $log, $filter, $state, SalesforceSession, fetchRecordTypes, SYNC_SFDC, IMG_SOUP, FETCH_MOBILE_SETTINGS, FETCH_OBJECT_RECORDS, FETCH_DATA_LOCAL, FETCH_DAILY_PLAN_ACCOUNTS, FETCH_DATA, SOUP_EXISTS, WEBSERVICE_ERROR) {
        try {
            var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
            var cordovaLog = cordova.require("salesforce/plugin/oauth");
            $rootScope.MonthsList = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

            $scope.checkConnection = function() {
                if (navigator && navigator.connection && navigator.connection.type != 'none') {
                    $scope.online = true;
                } else {
                    $scope.online = false;
                }
            };
            $rootScope.appVersion = navigator.appVersion;
            $rootScope.appVersion = $rootScope.appVersion.substr($rootScope.appVersion.indexOf("PRIDE/") + 6, 5);
            $log.debug("Version" + $rootScope.appVersion);
            if ($rootScope.myUserName == undefined || $rootScope.myUserName == '') {
                var userDetails = FETCH_DATA_LOCAL('User', sfSmartstore.buildSmartQuerySpec("SELECT  {User:_soup} FROM {User} ", 100));
                angular.forEach(userDetails.currentPageOrderedEntries, function(record, key) {

                    $rootScope.myUserName = record[0].Name;
                });
            }
            $rootScope.noScreen = function(screen) {
                Splash.ShowToast('Help is not currently available for this screen', 'long', 'center', function(a) {
                    console.log(a)
                });
            }
            $rootScope.showLoadingScreen=function(){
                $ionicLoading.show({
                    template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/>'
                });
            }
            $rootScope.goToHome = function() {
                $ionicHistory.nextViewOptions({
                    disableBack: true
                });
                $location.path('app/today/' + new Date());
            }
            $rootScope.backText = 'Back';
            $rootScope.showBack = true;
            $rootScope.params = {};
            $rootScope.name = 'AppCtrl';
            $rootScope.controllersList = ["PRIDECallCtrl", "PRIDECallCtrl2", "CheckoutCtrl", "StockCtrl", "StockDetailsCtrl", "ChataiCtrl", "RetailCtrl"];
            $rootScope.NonPerfectCallControllers = ["AddNewCtrl", "ReasonCtrl", "RemoveReasonCtrl", "MapCtrl", "productOrderCtrl"]
            $scope.myGoBack = function() {
                if ($rootScope.NonPerfectCallControllers.indexOf($rootScope.name) != -1) {
                    $window.history.back();
                } else if ($rootScope.controllersList.indexOf($rootScope.name) != -1) {
                    if ($rootScope.params.order != undefined || $rootScope.name == 'CheckoutCtrl') {
                        if ($rootScope.params.order != undefined && $rootScope.params.order == 0) {
                            $location.path('app/lastVistSummary/' + $rootScope.params.AccountId);
                        } else {
                            var pageOrder;
                            if ($rootScope.name == 'CheckoutCtrl') {
                                pageOrder = $rootScope.processFlowLineItems.length - 1;
                            } else {
                                pageOrder = parseInt($rootScope.params.order) - 1;
                            }

                            if ($rootScope.processFlowLineItems[pageOrder] != undefined) {
                                var nextPageId = $rootScope.processFlowLineItems[pageOrder].Page__c;
                                var nextPage;
                                angular.forEach($rootScope.pages, function(record, key) {
                                    if (record.pageDescription.Id == nextPageId) {
                                        nextPage = record;
                                    }
                                });
                                if (nextPage.pageDescription.Template_Name__c == 'Template1') {
                                    if (pageOrder == 3) {
                                        $location.path('app/stockDetails/' + $rootScope.params.AccountId + '/' + $rootScope.params.visitId + '/' + pageOrder + '/' + new Date());
                                    } else {
                                        $location.path('app/stocks/' + $rootScope.params.AccountId + '/' + $rootScope.params.visitId + '/' + pageOrder + '/' + new Date());
                                    }
                                } else if (nextPage.pageDescription.Template_Name__c == 'Template2') {
                                    $rootScope.resentlyAdded = true;
                                    $location.path('app/container2/' + $rootScope.params.AccountId + '/' + $rootScope.params.visitId + '/' + pageOrder + '/' + new Date());
                                } else {
                                    $rootScope.resentlyAdded2 = true;
                                    $location.path('app/container3/' + $rootScope.params.AccountId + '/' + $rootScope.params.visitId + '/' + pageOrder + '/' + new Date());
                                }
                            } else {
                                $location.path('app/checkout/' + $rootScope.params.AccountId + '/' + $rootScope.params.visitId);
                            }
                        }
                    } else {
                        if ($rootScope.name == 'OverviewCtrl') {
                            $rootScope.goToHome();
                        } else if ($rootScope.name == 'BusinessObjectivesCtrl') {
                            $location.path('app/overview/' + $rootScope.params.accountId);
                        } else if ($rootScope.name == 'LastVisitSummaryCtrl') {
                            $location.path('app/overview/' + $rootScope.params.accountId);
                        }
                    }
                } else {
                    if ($rootScope.name == 'OverviewCtrl') {
                        $rootScope.goToHome();
                    } else if ($rootScope.name == 'BusinessObjectivesCtrl' || $rootScope.name == 'MarketShareCtrl') {
                        $location.path('app/overview/' + $rootScope.params.accountId);
                    } else if ($rootScope.name == 'LastVisitSummaryCtrl') {
                        $location.path('app/overview/' + $rootScope.params.accountId);
                    }
                }
            }
            $rootScope.salestrackingList = [];
            //it will store the today's accounts with route plan
            $rootScope.Outlets = [];
            // $rootScope.TomorrowPlan = [];
            //$rootScope.DayAfterPlan = [];
            $scope.totalNotifications = 0;
            $rootScope.showLogo = true;
            $rootScope.completedAccountIds = [];
            $rootScope.unsyncedAccounts = [];
            $rootScope.uploadfields = [];
            $rootScope.slidesList = [];
            $rootScope.slideFields = [];
            $rootScope.StockAndSalesList = [];
            $rootScope.retailSalesList = [];
            $rootScope.chataiSales = [];
            $rootScope.audit = {};
            $rootScope.PrideImages = [];
            $rootScope.Month = {};
            $rootScope.ChataiMonth = {};
            $rootScope.visitrecord = {};
            $scope.myplan = [];
            $rootScope.resentlyAdded = false;
            $rootScope.resentlyAdded2 = false;
            if (SOUP_EXISTS.soupExists('Db_transaction')) {
                var completedTasks = FETCH_DATA.querySoup('Db_transaction', 'status', 'saved');
                if (completedTasks.currentPageOrderedEntries != undefined && completedTasks.currentPageOrderedEntries.length != 0) {
                    angular.forEach(completedTasks.currentPageOrderedEntries, function(record, key) {
                        $rootScope.completedAccountIds.push(record.Account);
                    });
                }
                if (SOUP_EXISTS.soupExists('Visit__c')) {
                    var myVisits = FETCH_DATA.querySoup('Visit__c', 'IsDirty', true);
                    if (myVisits.currentPageOrderedEntries != undefined && myVisits.currentPageOrderedEntries.length != 0) {
                        angular.forEach(myVisits.currentPageOrderedEntries, function(record, key) {
                            $rootScope.unsyncedAccounts[record.Account__c] = 'done';
                        });
                    }
                }

            }
            //function to calculate the distance between two geolocations
            $scope.distance = function(lat1, lon1, lat2, lon2, unit) {
                var radlat1 = Math.PI * lat1 / 180;
                var radlat2 = Math.PI * lat2 / 180;
                var radlon1 = Math.PI * lon1 / 180;
                var radlon2 = Math.PI * lon2 / 180;
                var theta = lon1 - lon2;
                var radtheta = Math.PI * theta / 180;
                var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
                dist = Math.acos(dist);
                dist = dist * 180 / Math.PI;
                dist = dist * 60 * 1.1515;
                if (unit == "K") {
                    dist = dist * 1.609344;
                } else if (unit == "N") {
                    dist = dist * 0.8684;
                }
                return dist
            };
            //  IMG_SOUP();
            // document.addEventListener("deviceready", function(){
            //     $timeout(function() {
            //     if (navigator && navigator.connection && navigator.connection.type != 'none' && typeof Splash !== "undefined" && Splash != undefined && Splash.isActive() == 1) {
            //         $q.all(
            //             fetchRecordTypes()
            //         ).then(function(response) {
            //             $q.all({
            //                 SETTINGS_STATUS: FETCH_MOBILE_SETTINGS()
            //             }).then(function(response1) {
            //                 $q.all({
            //                     OBJECT_STATUS: FETCH_OBJECT_RECORDS()
            //                 }).then(function(response2) {
            //                     if (response2.OBJECT_STATUS == 'Success' || response2.OBJECT_STATUS == 'Failure') {
            //                         setTimeout(function() {
            //                             if (typeof Splash !== "undefined" && Splash != undefined && Splash.isActive() == 1)
            //                                 Splash.Complete();
            //                         }, 2000);
            //                     }

            //                 }, function(error2) {
            //                     setTimeout(function() {
            //                         if (typeof Splash !== "undefined" && Splash != undefined && Splash.isActive() == 1)
            //                             Splash.Complete();
            //                     }, 2000);
            //                 });
            //             }, function(error1) {
            //                 $q.all({
            //                     OBJECT_STATUS: FETCH_OBJECT_RECORDS()
            //                 }).then(function(response2) {
            //                     if (response2.OBJECT_STATUS == 'Success' || response2.OBJECT_STATUS == 'Failure') {
            //                         setTimeout(function() {
            //                             if (typeof Splash !== "undefined" && Splash != undefined && Splash.isActive() == 1)
            //                                 Splash.Complete();
            //                         }, 2000);
            //                     }
            //                 }, function(error2) {
            //                     setTimeout(function() {
            //                         if (typeof Splash !== "undefined" && Splash != undefined && Splash.isActive() == 1)
            //                             Splash.Complete();
            //                     }, 2000);
            //                 });
            //             })
            //         }, function(error) {
            //             $q.all({
            //                 SETTINGS_STATUS: FETCH_MOBILE_SETTINGS()
            //             }).then(function(response1) {
            //                 $q.all({
            //                     OBJECT_STATUS: FETCH_OBJECT_RECORDS()
            //                 }).then(function(response2) {
            //                     if (response2.OBJECT_STATUS == 'Success' || response2.OBJECT_STATUS == 'Failure') {
            //                         setTimeout(function() {
            //                             if (typeof Splash !== "undefined" && Splash != undefined && Splash.isActive() == 1)
            //                                 Splash.Complete();
            //                         }, 2000);
            //                     }
            //                 }, function(error2) {
            //                     setTimeout(function() {
            //                         if (typeof Splash !== "undefined" && Splash != undefined && Splash.isActive() == 1)
            //                             Splash.Complete();
            //                     }, 2000);
            //                 });
            //             }, function(error1) {
            //                 $q.all({
            //                     OBJECT_STATUS: FETCH_OBJECT_RECORDS()
            //                 }).then(function(response2) {
            //                     if (response2.OBJECT_STATUS == 'Success' || response2.OBJECT_STATUS == 'Failure') {
            //                         setTimeout(function() {
            //                             if (typeof Splash !== "undefined" && Splash != undefined && Splash.isActive() == 1)
            //                                 Splash.Complete();
            //                         }, 2000);
            //                     }
            //                 }, function(error2) {
            //                     setTimeout(function() {
            //                         if (typeof Splash !== "undefined" && Splash != undefined && Splash.isActive() == 1)
            //                             Splash.Complete();
            //                     }, 2000);
            //                 });
            //             });
            //         });
            //     } else {
            //         $log.debug('No connection');
            //         setTimeout(function() {
            //             if (typeof Splash !== "undefined" && Splash != undefined && Splash.isActive() == 1)
            //                 Splash.Complete();
            //         }, 2000);
            //     }
            // }, 4000);
            // }, false);
            $rootScope.processFlow = {};
            var confirmReumePopup = null;
            $rootScope.cancelResumeSuccess = function() {
                confirmReumePopup.close();
            }
            $rootScope.confirmResumeSuccess = function() {
                var pendingTasks = FETCH_DATA.querySoup('Db_transaction', 'status', 'pending');
                $rootScope.processFlowLineItems = [];
                $rootScope.pages = [];
                $rootScope.processFlow = {};
                var currentCustomer = FETCH_DATA.querySoup('Account', 'Id', pendingTasks.currentPageOrderedEntries[0].Account);
                $scope.currentOutlet = currentCustomer.currentPageOrderedEntries[0];
                var flow = FETCH_DATA_LOCAL('Db_process_flow', sfSmartstore.buildSmartQuerySpec("SELECT  {Db_process_flow:_soup} FROM {Db_process_flow} WHERE  {Db_process_flow:Outlet_Type__c}='" + currentCustomer.currentPageOrderedEntries[0].RecordType.Name + "'", 1));
                $rootScope.processFlow = flow.currentPageOrderedEntries[0][0];
                var flowLineItems = FETCH_DATA.querySoup('Db_process_flow_line_item', "Process_Flow__c", flow.currentPageOrderedEntries[0][0].Id);
                var queryspec = '';
                var queryspecForSection = '';
                angular.forEach(flowLineItems.currentPageOrderedEntries, function(record, key) {
                    $rootScope.processFlowLineItems[record.Order__c - 1] = record;
                    if (queryspec == '' && queryspecForSection == '') {
                        queryspec = "{DB_page:Id}='" + record.Page__c + "'";
                        queryspecForSection = "{DB_section:Page__c}='" + record.Page__c + "'";
                    } else {
                        queryspec = queryspec + " OR {DB_page:Id}='" + record.Page__c + "'";
                        queryspecForSection = queryspecForSection + " OR {DB_section:Page__c}='" + record.Page__c + "'";
                    }
                });
                if (queryspec != '' && queryspecForSection != '') {
                    var pages = FETCH_DATA_LOCAL('DB_page', sfSmartstore.buildSmartQuerySpec("SELECT  {DB_page:_soup} FROM {DB_page} WHERE " + queryspec, 20));
                    var sections = FETCH_DATA_LOCAL('DB_section', sfSmartstore.buildSmartQuerySpec("SELECT  {DB_section:_soup} FROM {DB_section} WHERE " + queryspecForSection + " order by {DB_section:Order__c}", 20));
                    queryspec = '';
                    angular.forEach(sections.currentPageOrderedEntries, function(record, key) {
                        if (queryspec == '') {
                            queryspec = "{DB_fields:Section__c}='" + record[0].Id + "'";
                        } else {
                            queryspec = queryspec + " OR {DB_fields:Section__c}='" + record[0].Id + "'";
                        }
                    });
                    var fields = [];
                    if (queryspec != '') {
                        fields = FETCH_DATA_LOCAL('DB_fields', sfSmartstore.buildSmartQuerySpec("SELECT  {DB_fields:_soup} FROM {DB_fields} WHERE " + queryspec + " order by {DB_fields:Order__c}", 200));
                    }
                    $log.debug('fields' + JSON.stringify(fields.currentPageOrderedEntries));
                    angular.forEach(pages.currentPageOrderedEntries, function(page, pageKey) {
                        var pagedec = {};
                        pagedec.pageDescription = page[0];
                        pagedec.sections = [];
                        angular.forEach(sections.currentPageOrderedEntries, function(section, sectionKey) {
                            if (page[0].Id == section[0].Page__c) {
                                var sectiondec = {};
                                sectiondec.sectionDescription = section[0];
                                sectiondec.fields = [];
                                angular.forEach(fields.currentPageOrderedEntries, function(field, fieldKey) {
                                    if (field[0].Section__c == section[0].Id) {
                                        var fieldDescription = field[0];
                                        if (field[0].Picklist_Values__c != undefined) {
                                            fieldDescription.pickListValues = field[0].Picklist_Values__c.split(';');
                                        }
                                        sectiondec.fields.push(fieldDescription);
                                    }
                                });
                                pagedec.sections.push(sectiondec);
                            }
                        });
                        $rootScope.pages.push(pagedec);
                    });
                }

                if (pendingTasks.currentPageOrderedEntries[0].stageValue == 'chatai') {
                    $location.path('app/chataiSales/' + pendingTasks.currentPageOrderedEntries[0].Account + '/' + pendingTasks.currentPageOrderedEntries[0].Visit + '/' + pendingTasks.currentPageOrderedEntries[0].stage + '/' + new Date());
                } else if (pendingTasks.currentPageOrderedEntries[0].stageValue == 'Retail') {
                    $location.path('app/retailSales/' + pendingTasks.currentPageOrderedEntries[0].Account + '/' + pendingTasks.currentPageOrderedEntries[0].Visit + '/' + pendingTasks.currentPageOrderedEntries[0].stage + '/' + new Date());
                } else if (pendingTasks.currentPageOrderedEntries[0].stage == 'checkout') {
                    $location.path('app/checkout/' + pendingTasks.currentPageOrderedEntries[0].Account + '/' + pendingTasks.currentPageOrderedEntries[0].Visit);
                } else {
                    var nextPage = {};
                    //$log.debug('processFlow == '+JSON.stringify($rootScope.processFlowLineItems));
                    angular.forEach($rootScope.pages, function(record, key) {
                        $log.debug(record.pageDescription.Id + "====" + pendingTasks.currentPageOrderedEntries[0].stage + "====" + $rootScope.processFlowLineItems[pendingTasks.currentPageOrderedEntries[0].stage].Page__c);
                        if (record.pageDescription.Id == $rootScope.processFlowLineItems[pendingTasks.currentPageOrderedEntries[0].stage].Page__c) {
                            $log.debug(record.pageDescription.Id + "====" + pendingTasks.currentPageOrderedEntries[0].stage + "====" + $rootScope.processFlowLineItems[pendingTasks.currentPageOrderedEntries[0].stage].Page__c);
                            nextPage = record;
                        }
                    });
                    $log.debug('next page === ' + JSON.stringify(nextPage));
                    if (nextPage.pageDescription.Template_Name__c == 'Template1') {
                        if (pendingTasks.currentPageOrderedEntries[0].stageValue == 'stocks') {
                            $location.path('app/stocks/' + pendingTasks.currentPageOrderedEntries[0].Account + '/' + pendingTasks.currentPageOrderedEntries[0].Visit + '/' + pendingTasks.currentPageOrderedEntries[0].stage + '/' + new Date());
                        } else {
                            $location.path('app/stockDetails/' + pendingTasks.currentPageOrderedEntries[0].Account + '/' + pendingTasks.currentPageOrderedEntries[0].Visit + '/' + pendingTasks.currentPageOrderedEntries[0].stage + '/' + new Date());
                        }

                    } else if (nextPage.pageDescription.Template_Name__c == 'Template2') {
                        $location.path('app/container2/' + pendingTasks.currentPageOrderedEntries[0].Account + '/' + pendingTasks.currentPageOrderedEntries[0].Visit + '/' + pendingTasks.currentPageOrderedEntries[0].stage + '/' + new Date());
                    } else {
                        $location.path('app/container3/' + pendingTasks.currentPageOrderedEntries[0].Account + '/' + pendingTasks.currentPageOrderedEntries[0].Visit + '/' + pendingTasks.currentPageOrderedEntries[0].stage + '/' + new Date());
                    }
                }
                confirmReumePopup.close();


            }
            $rootScope.resumeCall = function() {
                $ionicLoading.show({
                    template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/>'
                });
                if (SOUP_EXISTS.soupExists('Db_transaction')) {
                    var pendingTasks = FETCH_DATA.querySoup('Db_transaction', 'status', 'pending');
                    if (pendingTasks.currentPageOrderedEntries != undefined && pendingTasks.currentPageOrderedEntries.length != 0) {
                        var currentCustomer = FETCH_DATA.querySoup('Account', 'Id', pendingTasks.currentPageOrderedEntries[0].Account);
                        $scope.currentOutlet = currentCustomer.currentPageOrderedEntries[0];

                        confirmReumePopup = $ionicPopup.confirm({
                            cssClass: 'resumeConfirm',
                            title: '',
                            scope: $rootScope,
                            template: 'You are about to resume the call at ' + $scope.currentOutlet.Name + '.<br/><br/><br/><div style="text-align:right; color:#38B6CB; margin-right: 18px; font-size: larger; font-weight: bold;"><span ng-click="cancelResumeSuccess()" style="padding-right: 35px;">NO</span> &nbsp;<span ng-click="confirmResumeSuccess()">YES</span> </div>',
                            buttons: [

                            ]
                        });
                        confirmReumePopup.then(function(res) {

                        });


                    } else {
                        Splash.ShowToast('You have no pending calls.', 'long', 'bottom', function(a) {
                            console.log(a)
                        });
                    }
                }

                $ionicLoading.hide();
            }
            $rootScope.pendingTask = false;
            $rootScope.day = 'today';
            $rootScope.Uploads = [];
            $ionicModal.fromTemplateUrl('./templates/notifications.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function(modal) {
                $scope.modal = modal;
            });

            $rootScope.processFlow = [];
            $rootScope.processFlowLineItems = [];
            $rootScope.pages = [];
            $rootScope.sections = [];
            $rootScope.fields = [];
            $scope.Notifications = [];
            $scope.getUploads = function() {
                $ionicLoading.show({
                    template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/>'
                });
                $scope.myplan = $rootScope.Outlets;
                if ($scope.myplan.length != 0) {
                    var today = $filter('date')(new Date(), 'yyyy-MM-dd');
                    $scope.AccountsWithIds = {};
                    var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
                    var queryVariable = '';
                    angular.forEach($scope.myplan, function(record, key) {
                        $scope.AccountsWithIds[record.Id] = record;
                        if (queryVariable == '') {
                            queryVariable = queryVariable + "{Upload__c:Account__c} = '" + record.Id + "'";
                        } else {
                            queryVariable = queryVariable + " OR {Upload__c:Account__c} = '" + record.Id + "'";
                        }
                    });
                    $scope.promos = [];
                    $scope.pops = [];
                    $scope.Notifications = [];
                    $scope.complaints = [];
                    $rootScope.Uploads = [];
                    var rtList = FETCH_DATA_LOCAL('DB_RecordTypes', sfSmartstore.buildSmartQuerySpec("SELECT  {DB_RecordTypes:_soup} FROM {DB_RecordTypes} WHERE {DB_RecordTypes:SobjectType}='Upload__c'", 15));
                    var recordTypesList = [];
                    angular.forEach(rtList.currentPageOrderedEntries, function(record, key) {
                        recordTypesList[record[0].Name] = record[0].Id;
                    });
                    var DATA = FETCH_DATA_LOCAL('Upload__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Upload__c:_soup} FROM {Upload__c} WHERE ({Upload__c:RecordTypeId}='" + recordTypesList['Cooler'] + "' OR {Upload__c:RecordTypeId}='" + recordTypesList['POP'] + "' OR {Upload__c:RecordTypeId}='" + recordTypesList['Promotions'] + "') AND " + queryVariable, 1000));
                    angular.forEach(DATA.currentPageOrderedEntries, function(record, key) {
                        if (record[0] != undefined && record[0] != '' && record[0].Status__c != 'Inactive')
                            $rootScope.Uploads.push(record[0]);
                        if (record[0].Start_Date__c != undefined && record[0] != '' && (record[0].RecordType.Name === 'POP' || (record[0].RecordType.Name === 'Promotions' && record[0].Status__c!='Completed'))) {
                            var startDate = $filter('date')(new Date(record[0].Start_Date__c), 'yyyy-MM-dd');
                            var endDate = $filter('date')(new Date(record[0].End_Date__c), 'yyyy-MM-dd');
                            if (today >= startDate && today <= endDate) {
                                var notification = record[0];
                                notification.RoutePlanOrder = $scope.AccountsWithIds[record[0].Account__c].RoutePlan.Order__c;
                                $scope.Notifications.push(notification);
                            }
                        }

                    });

                }

                $ionicLoading.hide();
            }

            $rootScope.currentOutlet = {};
            $rootScope.selectedAccounts = [];
            $scope.superSync= function(){
                $ionicLoading.show({
                    template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/>'
                });
                $scope.checkConnection();
                if ($scope.online == true) {
                        $scope.SyncSFDC(true);                    
                } else {
                    $ionicLoading.hide();
                    Splash.ShowToast('No internet. please try again later.', 'long', 'bottom', function(a) {
                        console.log(a)
                    });
                }
            }
            $scope.SyncSFDC = function(ifLast) {
                
                    $q.all(
                        SYNC_SFDC()
                    ).then(function(response) {
                        if (response != undefined)
                            $q.all({
                                SETTINGS_STATUS: FETCH_MOBILE_SETTINGS()
                            }).then(function(response1) {
                                if (response1 != undefined)
                                    $q.all({
                                        OBJECT_STATUS: FETCH_OBJECT_RECORDS()
                                    }).then(function(response2) {
                                        //$log.debug('result' + JSON.stringify(response2));
                                        if (response2.OBJECT_STATUS == 'Success' || response2.OBJECT_STATUS == 'Failure') {
                                            if(ifLast){
                                                $timeout(function(){
                                        if($rootScope.name == "TodayCtrl" && $rootScope.day=="today"){
                                            $rootScope.unsyncedAccounts = [];
                                            $rootScope.goToHome();

                                        }
                                        $ionicLoading.hide();
                                    },2000);
                                            }

                                        }

                                    }, function(error2) {
                                        if(ifLast){
                                           $timeout(function(){
                                        if($rootScope.name == "TodayCtrl" && $rootScope.day=="today"){
                                            $rootScope.unsyncedAccounts = [];
                                            $rootScope.goToHome();

                                        }
                                        $ionicLoading.hide();
                                    },2000);
                                        }
                                    });
                            }, function(error1) {
                                $q.all({
                                    OBJECT_STATUS: FETCH_OBJECT_RECORDS()
                                }).then(function(response2) {
                                    if (response2.OBJECT_STATUS == 'Success' || response2.OBJECT_STATUS == 'Failure') {
                                        if(ifLast){
                                            $timeout(function(){
                                        if($rootScope.name == "TodayCtrl" && $rootScope.day=="today"){
                                            $rootScope.unsyncedAccounts = [];
                                            $rootScope.goToHome();

                                        }
                                        $ionicLoading.hide();
                                    },2000);
                                        }
                                    }
                                }, function(error2) {
                                    if(ifLast){
                                        $timeout(function(){
                                        if($rootScope.name == "TodayCtrl" && $rootScope.day=="today"){
                                            $rootScope.unsyncedAccounts = [];
                                            $rootScope.goToHome();

                                        }
                                        $ionicLoading.hide();
                                    },2000);
                                    }
                                });
                            })
                    }, function(error) {
                        $q.all({
                            SETTINGS_STATUS: FETCH_MOBILE_SETTINGS()
                        }).then(function(response1) {
                            if (response1 != undefined)
                                $q.all({
                                    OBJECT_STATUS: FETCH_OBJECT_RECORDS()
                                }).then(function(response2) {
                                    if (response2.OBJECT_STATUS == 'Success' || response2.OBJECT_STATUS == 'Failure') {
                                        if(ifLast){
                                            $timeout(function(){
                                        if($rootScope.name == "TodayCtrl" && $rootScope.day=="today"){
                                            $rootScope.unsyncedAccounts = [];
                                            $rootScope.goToHome();

                                        }
                                        $ionicLoading.hide();
                                    },2000);
                                        }
                                    }
                                }, function(error2) {
                                    if(ifLast){
                                        $timeout(function(){
                                        if($rootScope.name == "TodayCtrl" && $rootScope.day=="today"){
                                            $rootScope.unsyncedAccounts = [];
                                            $rootScope.goToHome();

                                        }
                                        $ionicLoading.hide();
                                    },2000);
                                    }
                                });
                        }, function(error1) {
                            $q.all({
                                OBJECT_STATUS: FETCH_OBJECT_RECORDS()
                            }).then(function(response2) {
                                if (response2.OBJECT_STATUS == 'Success' || response2.OBJECT_STATUS == 'Failure') {
                                    if(ifLast){
                                        $timeout(function(){
                                        if($rootScope.name == "TodayCtrl" && $rootScope.day=="today"){
                                            $rootScope.unsyncedAccounts = [];
                                            $rootScope.goToHome();

                                        }
                                        $ionicLoading.hide();
                                    },2000);
                                    }
                                }
                            }, function(error2) {
                                if(ifLast){
                                    $timeout(function(){
                                        if($rootScope.name == "TodayCtrl" && $rootScope.day=="today"){
                                            $rootScope.unsyncedAccounts = [];
                                            $rootScope.goToHome();

                                        }
                                        $ionicLoading.hide();
                                    },2000);
                                    
                                }

                            });
                        });
                    });

                
            }


            if (SOUP_EXISTS.soupExists('Db_transaction') == true) {
                var pendingTasks = FETCH_DATA.querySoup('Db_transaction', 'status', 'pending');
                if (pendingTasks.currentPageOrderedEntries != undefined && pendingTasks.currentPageOrderedEntries.length != 0) {
                    $rootScope.pendingTask = true;
                } else {
                    $rootScope.pendingTask = false;
                }
            }
            var transaction = function() {
                //  $log.debug('soupkk' + SOUP_EXISTS.soupExists('Db_transaction'));
                if (SOUP_EXISTS.soupExists('Db_transaction') == true) {
                    var pendingTasks = FETCH_DATA.querySoup('Db_transaction', 'status', 'pending');
                    if (pendingTasks.currentPageOrderedEntries != undefined && pendingTasks.currentPageOrderedEntries.length != 0) {
                        $rootScope.pendingTask = true;
                    } else {
                        $rootScope.pendingTask = false;
                    }
                }
            };

            /*var syncfunc = $interval(function() {
                $scope.checkConnection();
                //$log.debug('soupkkAccount' + SOUP_EXISTS.soupExists('Account'));
                if ($scope.online == true && SOUP_EXISTS.soupExists('Account') == true) {
                    //Data sync between mobile and salesforce

                    SYNC_SFDC();
                }
            }, 500000);*/
            $rootScope.logOut = function() {
                if (navigator && navigator.connection && navigator.connection.type != 'none') {
                    $ionicLoading.show({
                        template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/>'
                    });
                    if (angular.isDefined(transaction)) {
                        $interval.cancel(transaction);
                    }
                    $q.all(
                        SYNC_SFDC()
                    ).then(function(response) {
                        //$log.debug('sync response' + JSON.stringify(response));
                        //sfSmartstore.removeAllSoups();
                        sfSmartstore.removeSoup('Db_transaction', function(response) {}, function(error) {});
                        sfSmartstore.removeSoup('Account', function(response) {}, function(error) {});
                        $ionicLoading.show({
                            template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/>'
                        });
                        $timeout(function() {
                            $ionicLoading.hide();
                            cordovaLog.logout();
                            navigator.app.exitApp();

                        }, 2000);

                    });
                } else {
                    Splash.ShowToast('Internet connectivity is required to logout.', 'long', 'center', function(a) {
                        console.log(a)
                    });
                }
            }
            $ionicLoading.hide();
        } catch (err) {
            $ionicLoading.hide();
            WEBSERVICE_ERROR('AppCtrlError', err.name + '::' + err.message);
        }
    })
    .controller('AddNewCtrl', function($scope, $stateParams, $rootScope, $q, $location, $timeout, $ionicLoading, $log, $filter, FETCH_LOCAL_DATA, WEBSERVICE_ERROR) {
        try {
            $scope.AccountList = [];
            var queryVariable = '';
            $rootScope.backText = 'Back';
            $rootScope.showBack = true;
            $scope.selectAll = false;
            $rootScope.params = $stateParams;
            $scope.searchAccount = '';
            $rootScope.name = "AddNewCtrl";
            $scope.selectedAccountsDisplay = [];
            $ionicLoading.show({
                template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/>'
            });
            $timeout(function() {
                var outletIds = [];
                angular.forEach($rootScope.Outlets, function(record, key) {
                    outletIds.push(record.Id);
                });
                var outlets = FETCH_LOCAL_DATA('Account', 'Id', null);
                for (var i = 0; i < outlets.length; i++) {
                    if (outletIds==undefined || outletIds.length==0 || outletIds.indexOf(outlets[i].Id) == -1) {
                        $scope.AccountList.push({
                            Id: outlets[i].Id,
                            Name: outlets[i].Name,
                            Outlet_ID__c: outlets[i].Outlet_ID__c,
                            Route__c: outlets[i].Route__c,
                            Area_Outlet_Location__c: outlets[i].Area_Outlet_Location__c
                        });
                    }
                }
                $ionicLoading.hide();
            }, 500);

            $scope.ifCalled=false;
            $scope.justSelectAllOutlets = function() {
                //$scope.selectAll = !$scope.selectAll;
                if(!$scope.ifCalled){
                $scope.ifCalled=true;
                $scope.selectedAccountsDisplay = [];
                angular.forEach($scope.AccountList, function(record, key) {
                    if ($scope.selectAll == false) {
                        record.selected = false;
                    } else {
                        record.selected = true;
                        $scope.selectedAccountsDisplay.push(record);
                    }
                });
                $timeout(function(){$scope.ifCalled=false},2000);
                }
            }
            $scope.selectAllOutlets = function() {
                if(!$scope.ifCalled){
                $scope.ifCalled=true;
                $scope.selectAll = !$scope.selectAll;
                
                $scope.selectedAccountsDisplay = [];
                angular.forEach($scope.AccountList, function(record, key) {
                    if ($scope.selectAll == false) {
                        record.selected = false;
                    } else {
                        record.selected = true;
                        $scope.selectedAccountsDisplay.push(record);
                    }
                });
                $timeout(function(){$scope.ifCalled=false},2000);
                }
            }

            $scope.selectRow = function(kk) {
                var checkAllSelcted = true;
                $scope.selectedAccountsDisplay = [];
                angular.forEach($scope.AccountList, function(record, key) {
                    if (record.Id == kk) {
                        record.clicked = true;
                        record.selected = !record.selected;
                    } else {
                        record.clicked = false;
                    }
                    if (record.selected == true) {
                        $scope.selectedAccountsDisplay.push(record);
                    } else {
                        $scope.selectAll = false;
                        checkAllSelcted = false;
                    }
                });
                if (checkAllSelcted) {
                    $scope.selectAll = true;
                }

            }

            $scope.justSelect = function(kk) {
                var checkAllSelcted = true;
                $scope.selectedAccountsDisplay = [];
                angular.forEach($scope.AccountList, function(record, key) {
                    if (record.Id == kk) {
                        record.clicked = true;

                    } else {
                        record.clicked = false;
                    }
                    if (record.selected == true) {
                        $scope.selectedAccountsDisplay.push(record);
                    } else {
                        $scope.selectAll = false;
                        checkAllSelcted = false;
                    }
                });
                if (checkAllSelcted) {
                    $scope.selectAll = true;
                }
            }
            $scope.AddRoutes = function() {
                var isSelected = false;
                $rootScope.selectedAccounts = [];
                angular.forEach($scope.AccountList, function(record, key) {
                    if (record.selected == true) {
                        $rootScope.selectedAccounts.push(record);
                        isSelected = true;
                    }
                });
                if (isSelected == true)
                    $location.path('app/reason/' + new Date());
                else
                    Splash.ShowToast('You must select at least one outlet to be added.', 'long', 'bottom', function(a) {
                        console.log(a)
                    });
            }
            $scope.CancelAddRoutes = function() {
                $rootScope.selectedAccounts = [];
                //$location.path('app/today/' + new Date());
                $rootScope.goToHome();
            }
        } catch (err) {
            $ionicLoading.hide();
            WEBSERVICE_ERROR('AddNewCtrlError', err.name + '::' + err.message);
        }
    })
    .controller('ReasonCtrl', function($scope, $stateParams, $ionicPopup, $ionicSideMenuDelegate, $rootScope, $q, $window, $ionicModal, $location, $state, $timeout, $interval, $ionicLoading, $log, $filter, $cordovaCamera, SalesforceSession, FETCH_DATA, FETCH_DAILY_PLAN_ACCOUNTS, MOBILEDATABASE_ADD, FETCH_DATA_LOCAL, WEBSERVICE_ERROR) {
        try {
            $scope.addAccounts = {};
            $rootScope.backText = 'Back';
            $rootScope.showBack = true;
            $rootScope.params = $stateParams;
            $rootScope.name = "ReasonCtrl";
            var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
            $scope.showAccounts = true;
            $scope.Accounts = [];
            $scope.Accounts = $rootScope.selectedAccounts;
            $scope.confirmOutletAdd = function() {
                if ($scope.addAccounts.Reason != undefined && $scope.addAccounts.Reason != '') {
                    $ionicLoading.show({
                        template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/>'
                    });

                    var routeplans = [];
                    var routeAssignments = [];
                    var today = $filter('date')(new Date(), 'yyyy-MM-dd');
                    var queryVariable = '';
                    angular.forEach($scope.Accounts, function(record, key) {
                        if (queryVariable == '') {
                            queryVariable = queryVariable + "{Route_Assignment__c:Account__c} = '" + record.Id + "'";
                        } else {
                            queryVariable = queryVariable + " OR {Route_Assignment__c:Account__c} = '" + record.Id + "'";
                        }
                    });
                    if (queryVariable != '') {
                        var routeOrder = 0;
                        if ($rootScope.Outlets.length > 0)
                            routeOrder = $rootScope.Outlets[($rootScope.Outlets.length - 1)].RoutePlan.Order__c;
                        var d = new Date();
                        var currentMonth = $rootScope.MonthsList[d.getMonth()];
                        var currentYear = d.getFullYear();
                        var rtList = FETCH_DATA_LOCAL('DB_RecordTypes', sfSmartstore.buildSmartQuerySpec("SELECT  {DB_RecordTypes:_soup} FROM {DB_RecordTypes} WHERE {DB_RecordTypes:SobjectType}='Route_Plan__c' AND {DB_RecordTypes:Name}='Route Plan Line Item'", 1));
                        var routePlanRTId = FETCH_DATA_LOCAL('DB_RecordTypes', sfSmartstore.buildSmartQuerySpec("SELECT  {DB_RecordTypes:_soup} FROM {DB_RecordTypes} WHERE {DB_RecordTypes:SobjectType}='Route_Plan__c' AND {DB_RecordTypes:Name}='Route Plan'", 1));
                        var routeAssignment = FETCH_DATA_LOCAL('Route_Assignment__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Route_Assignment__c:_soup} FROM {Route_Assignment__c} WHERE " + queryVariable, 100));
                        var routeplanRecord = FETCH_DATA_LOCAL('Route_Plan__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Route_Plan__c:_soup} FROM {Route_Plan__c} WHERE  {Route_Plan__c:Month__c}='" + currentMonth + "' AND {Route_Plan__c:Year__c}='" + currentYear + "' AND {Route_Plan__c:RecordTypeId}='" + routePlanRTId.currentPageOrderedEntries[0][0].Id + "'", 1));
                        var routeAssignmentList = [];
                        angular.forEach(routeAssignment.currentPageOrderedEntries, function(record, key) {
                            angular.forEach($rootScope.selectedAccounts, function(rec, ke) {
                                if (record[0].Account__c == rec.Id && routeAssignmentList.indexOf(rec.Id) == -1) {
                                    routeAssignmentList.push(rec.Id);
                                    var routePlan = {
                                        Route_Outlet__c: record[0].Id,
                                        Id: record[0].Account__c + record[0].Id,
                                        External_Id__c: $window.Math.random() * 10000000,
                                        Planned_Date__c: today,
                                        Visit_Type__c: 'Market visit',
                                        Status__c: 'New',
                                        Reason__c: $scope.addAccounts.Reason,
                                        Order__c: ++routeOrder,
                                        IsDirty: true
                                    };
                                    routePlan.Id=routePlan.External_Id__c;
                                    if (routeplanRecord != undefined && routeplanRecord.currentPageOrderedEntries != undefined && routeplanRecord.currentPageOrderedEntries.length > 0 && routeplanRecord.currentPageOrderedEntries[0][0].Id != undefined) {
                                        routePlan['Route_Plan__c'] = routeplanRecord.currentPageOrderedEntries[0][0].Id;
                                    }

                                    if (rtList != undefined && rtList.currentPageOrderedEntries != undefined && rtList.currentPageOrderedEntries[0][0].Id != undefined) {
                                        routePlan['RecordTypeId'] = rtList.currentPageOrderedEntries[0][0].Id;
                                    }
                                    $log.debug('routePlan' + JSON.stringify(routePlan));
                                    routeplans.push(routePlan);
                                }
                            });
                        });
                        if (routeplans.length > 0) {
                            MOBILEDATABASE_ADD('Route_Plan__c', routeplans, 'External_Id__c');
                        }
                    }
                    $timeout(function() {
                        $ionicLoading.hide();
                        $rootScope.goToHome();
                    }, 3000);
                } else {
                    Splash.ShowToast('Please enter a reason for adding outlets to today\'s visit.', 'long', 'bottom', function(a) {
                        console.log(a)
                    });

                }


            }
            $scope.CancelAddRoutes = function() {
                $rootScope.selectedAccounts = [];
                $location.path('app/AddOutlets/' + $stateParams.refresh);
            }

        } catch (err) {
            $ionicLoading.hide();
            WEBSERVICE_ERROR('ReasonCtrlError', err.name + '::' + err.message);
        }
    })

.controller('RemoveReasonCtrl', function($scope, $stateParams, $rootScope, $window, $location, $timeout, $ionicLoading, $log, $filter, SalesforceSession, MOBILEDATABASE_ADD, WEBSERVICE_ERROR) {
        try {
            $scope.remove = {};
            $scope.selectedAccountToRemove = {};
            angular.forEach($rootScope.Outlets, function(record, key) {
                if (record.Id == $stateParams.accountId) {
                    $scope.selectedAccountToRemove = record;
                }
            });
            $rootScope.params = $stateParams;
            $rootScope.name = "RemoveReasonCtrl";
            $scope.confirmOutletRemove = function() {
                if ($scope.remove.Reason != undefined && $scope.remove.Reason != '') {
                    $ionicLoading.show({
                        template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/>'
                    });
                    var deleteRouteplan = [];
                    if($scope.selectedAccountToRemove.RoutePlan!=undefined){
                        var routeplanRecord = $scope.selectedAccountToRemove.RoutePlan;
                        routeplanRecord.Status__c = 'Removed';
                        routeplanRecord.Reason__c = $scope.remove.Reason;
                        routeplanRecord.IsDirty = true;
                        if(routeplanRecord.External_Id__c==undefined)
                            routeplanRecord.External_Id__c = $window.Math.random() * 100000000;
                        deleteRouteplan.push(routeplanRecord);
                        MOBILEDATABASE_ADD('Route_Plan__c', deleteRouteplan, 'External_Id__c');
                    }
                    $timeout(function() {
                        $ionicLoading.hide();
                        $rootScope.goToHome();
                    }, 3000);

                } else {
                    Splash.ShowToast('Please Specify reason for removing outlet.', 'long', 'bottom', function(a) {
                        console.log(a);
                    });
                }
            }
            $scope.CancelAddRoutes = function() {
                $rootScope.selectedAccounts = [];
                $rootScope.goToHome();
            }
        } catch (err) {
            $ionicLoading.hide();
            WEBSERVICE_ERROR('RemoveReasonCtrlError', err.name + '::' + err.message);
        }
    })
    .controller('TodayCtrl', function($scope, $stateParams, $ionicSlideBoxDelegate, $ionicPopup, $ionicSideMenuDelegate, $rootScope, $q, $window, $ionicModal, $location, $state, $timeout, $interval, $ionicLoading, $log, $filter, $cordovaCamera, SalesforceSession, FETCH_DATA, FETCH_DAILY_PLAN_ACCOUNTS, MOBILEDATABASE_ADD, FETCH_DATA_LOCAL, SOUP_EXISTS, FETCH_REMOTE_DATA, WEBSERVICE_ERROR) {
        try {
            $ionicLoading.show({
                template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
            });
            $scope.showMessage = '';
            $rootScope.params = $stateParams;
            $rootScope.name = "TodayCtrl";
            var todayDateVal = new Date();
            $scope.TodayDate = $filter('date')(todayDateVal, 'EEE, dd MMM yy');
            todayDateVal.setDate(todayDateVal.getDate() + 1);
            $scope.TomorrowDate = $filter('date')(todayDateVal, 'EEE, dd MMM yy');
            todayDateVal.setDate(todayDateVal.getDate() + 1);
            $scope.DayAfterDate = $filter('date')(todayDateVal, 'EEE, dd MMM yy');
            $scope.AccountImages = [];
            $scope.SlidesList = [];
            $scope.showNoOutletsMessage = false;
            var sfSmartstore = cordova.require("salesforce/plugin/smartstore");


            $scope.goToMap = function(outletId) {
                $log.debug('outletId == ' + outletId);
                if (navigator && navigator.connection && navigator.connection.type != 'none') {
                    if (outletId == 'All')
                        $location.path('app/map/');
                    else
                        $location.path('app/map/' + outletId);
                } else {
                    Splash.ShowToast('Internet connectivity is required for this feature.', 'long', 'center', function(a) {
                        console.log(a)
                    });
                }

            }


            $scope.getPlan = function(day) {
            $timeout(function(){

                $ionicLoading.show({
                    template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
                });
                $rootScope.day = day;
                // $log.debug('debug;;;;;;'+day);          
                $rootScope.Outlets = [];
                if (day == 'today') {

                    var todayRoutePlan = FETCH_DAILY_PLAN_ACCOUNTS(day);
                    var slideNotoShow =0;
                    if (todayRoutePlan.length > 0) {
                        var today = $filter('date')(new Date(), 'dd/MM/yyyy');
                        var transactionsList = [];
                        if (SOUP_EXISTS.soupExists('Db_transaction')) {
                            var completedTasks = FETCH_DATA.querySoup('Db_transaction', 'entryDate', today);
                            if (completedTasks.currentPageOrderedEntries != undefined && completedTasks.currentPageOrderedEntries.length != 0) {
                                angular.forEach(completedTasks.currentPageOrderedEntries, function(record, key) {
                                    transactionsList[record.Account] = record;
                                });
                            }
                        }
                        var todayRoutePlan = FETCH_DAILY_PLAN_ACCOUNTS(day);
                        var todayRoutePlanwithStatus = [];
                        angular.forEach(todayRoutePlan, function(record, key) {
                            var accountRecord = record;
                            accountRecord.isNext = false;
                            if (transactionsList[accountRecord.Id] == undefined) {
                                accountRecord.isCompleted = 'Not started';
                            } else {
                                accountRecord.isCompleted = transactionsList[accountRecord.Id].status;
                            }
                            todayRoutePlanwithStatus.push(accountRecord);
                        });
                        var isAllCompleted =true;
                        for (var i = 0; i < todayRoutePlanwithStatus.length; i++) {
                            if (todayRoutePlanwithStatus[i].isCompleted == 'Not started') {
                                todayRoutePlanwithStatus[i].isNext = true;
                                slideNotoShow = i;
                                isAllCompleted =false;
                                break;
                            }
                        }
                        for (var i = 0; i < todayRoutePlanwithStatus.length; i++) {
                            $rootScope.Outlets.push(todayRoutePlanwithStatus[i]);
                        }

                        var completedTasks = FETCH_DATA.querySoup('Db_transaction', 'entryDate', null);
                        $rootScope.unsyncedAccounts = [];
                        var myVisits = FETCH_DATA.querySoup('Visit__c', 'IsDirty', true);
                        if (myVisits.currentPageOrderedEntries != undefined && myVisits.currentPageOrderedEntries.length != 0) {
                            angular.forEach(myVisits.currentPageOrderedEntries, function(record, key) {
                                $rootScope.unsyncedAccounts[record.Account__c] = 'done';
                            });
                        }
                    }
                } else {
                    $rootScope.Outlets = FETCH_DAILY_PLAN_ACCOUNTS(day);

                }
                $scope.SlidesList = [];
                if ($rootScope.Outlets.length > 0) {
                    var slidesLength = parseInt($rootScope.Outlets.length / 4);
                    if ($rootScope.Outlets.length % 4 != 0) {
                        slidesLength = slidesLength + 1;
                    }
                    for (var i = 0; i < slidesLength; i++) {
                        $scope.SlidesList.push(i);
                    }
                    $ionicSlideBoxDelegate.slide(0, 200);
                    var queryVariable = '';
                    angular.forEach($rootScope.Outlets, function(record, key) {
                        $scope.AccountImages[record.Id] = './img/capture.png';
                        if (queryVariable == '') {
                            queryVariable = queryVariable + "{Db_images:ParentId} = '" + record.Id + "'";
                        } else {
                            queryVariable = queryVariable + " OR {Db_images:ParentId} = '" + record.Id + "'";
                        }
                    });
                    var imagesLocal = FETCH_DATA_LOCAL('Db_images', sfSmartstore.buildSmartQuerySpec("SELECT  {Db_images:_soup} FROM {Db_images} WHERE " + queryVariable, 100));
                    angular.forEach(imagesLocal.currentPageOrderedEntries, function(record, key) {
                        $scope.AccountImages[record[0].ParentId] = "data:image/jpeg;base64," + record[0].Body;
                    });
                    if(day=='today')
                        $scope.getUploads();
                }
                $timeout(function() {
                    $ionicSlideBoxDelegate.update();
                    // var outletsDone = 0;
                    // if ($rootScope.Outlets.length > 1)
                    //     angular.forEach($rootScope.Outlets, function(record, key) {
                    //         if (record.isCompleted == 'saved')
                    //             outletsDone++;

                    //     });
                    if (slideNotoShow != 0 && day=="today")
                        $ionicSlideBoxDelegate.slide(parseInt(slideNotoShow / 4), 500)
                    $ionicLoading.hide();
                }, 100);
                var pendingTasks = FETCH_DATA.querySoup('Db_transaction', 'status', 'pending');

                 if (day == 'today' && isAllCompleted ==true && pendingTasks.currentPageOrderedEntries.length == 0) {
                    Splash.ShowToast('You have completed all calls for the day.', 'long', 'bottom', function(a) {
                        console.log(a)
                    });
                 }

                if ($rootScope.Outlets.length < 1) {
                    $scope.showNoOutletsMessage = true;
                    Splash.ShowToast('You have no outlet visits scheduled.', 'long', 'bottom', function(a) {
                        console.log(a)
                    });
                } else{
                    $scope.showNoOutletsMessage = false;
                }
            },1000);
            }

            if (typeof Splash !== "undefined" && Splash != undefined && Splash.isActive() == 1) {
                $timeout(function() {
                    if (navigator && navigator.connection && navigator.connection.type != 'none') {
                        $q.all({
                            Status: FETCH_REMOTE_DATA()
                        }).then(function(response) {
                            $log.debug('response===' + JSON.stringify(response));
                            if (response.Status != undefined) {
                                $scope.getPlan('today');
                                $timeout(function() {
                                    if (typeof Splash !== "undefined" && Splash != undefined && Splash.isActive() == 1)
                                        Splash.Complete();
                                    var userDetails = FETCH_DATA_LOCAL('User', sfSmartstore.buildSmartQuerySpec("SELECT  {User:_soup} FROM {User} ", 100));
                                    angular.forEach(userDetails.currentPageOrderedEntries, function(record, key) {

                                        $rootScope.myUserName = record[0].Name;
                                    });
                                }, 2000);
                            }
                        }, function(error) {
                            $timeout(function() {
                                if (typeof Splash !== "undefined" && Splash != undefined && Splash.isActive() == 1)
                                    Splash.Complete();
                            }, 2000);
                        });
                    } else {
                        if (SOUP_EXISTS.soupExists('Account') == true && SOUP_EXISTS.soupExists('Route_Plan__c') == true && SOUP_EXISTS.soupExists('Route_Assignment__c') == true && SOUP_EXISTS.soupExists('Db_process_flow') == true) {
                            $scope.getPlan('today');
                            $scope.showMessage = false;
                        } else {
                            Splash.ShowToast('Internet connection is required to download user details.', 'long', 'bottom', function(a) {
                                console.log(a)
                            });
                            $scope.showMessage = true;
                            $ionicLoading.hide();
                        }
                        $timeout(function() {
                            if (typeof Splash !== "undefined" && Splash != undefined && Splash.isActive() == 1)
                                Splash.Complete();
                        }, 500);
                    }
                }, 2000);
            } else {
                if (SOUP_EXISTS.soupExists('Account') == true && SOUP_EXISTS.soupExists('Route_Plan__c') == true && SOUP_EXISTS.soupExists('Route_Assignment__c') == true && SOUP_EXISTS.soupExists('Db_process_flow') == true) {
                    $scope.getPlan('today');
                    $scope.showMessage = false;
                } else {
                    $scope.showMessage = true;
                }
            }

            $scope.takePicture = function(accId) {
                var options = {
                    quality: 75,
                    destinationType: Camera.DestinationType.DATA_URL,
                    sourceType: Camera.PictureSourceType.CAMERA,
                    allowEdit: true,
                    encodingType: Camera.EncodingType.JPEG,
                    targetWidth: 300,
                    targetHeight: 300,
                    saveToPhotoAlbum: false
                };
                $cordovaCamera.getPicture(options).then(function(imageData) {
                    $scope.attachment = [{
                        Id: accId,
                        ParentId: accId,
                        Name: new Date(),
                        Body: imageData,
                        IsDirty: false
                    }];
                    MOBILEDATABASE_ADD('Db_images', $scope.attachment, 'Id');
                    $scope.AccountImages[accId] = "data:image/jpeg;base64," + imageData;
                }, function(err) {

                });

            }

            $scope.onSwipeup = function(outletId) {
                if ($rootScope.day == 'today') {
                    var canDelete = false;
                    angular.forEach($rootScope.Outlets, function(record, key) {
                        if (record.Id == outletId && record.isCompleted == 'Not started') {
                            canDelete = true;
                        }
                    });
                    if (canDelete == true) {
                        var confirmDeletePopup = $ionicPopup.confirm({
                            title: 'Confirm Delete',
                            template: 'Are you sure to remove outlet from plan?'
                        });
                        confirmDeletePopup.then(function(res) {
                            if (res) {
                                $location.path('app/removeReason/' + outletId);
                            }
                        });
                    } else {
                        var pendingTasks = FETCH_DATA.querySoup('Db_transaction', 'status', 'pending');
                        if (pendingTasks.currentPageOrderedEntries != undefined && pendingTasks.currentPageOrderedEntries.length > 0) {
                            if (pendingTasks.currentPageOrderedEntries[0].Account == outletId) {
                                Splash.ShowToast('You have already started the call for this outlet. It cannot be removed from today\'s plan', 'long', 'bottom', function(a) {
                                    console.log(a)
                                });
                            } else {
                                Splash.ShowToast('You have already visited this outlet. It cannot be removed from today\'s plan', 'long', 'bottom', function(a) {
                                    console.log(a)
                                });
                            }
                        } else {
                            Splash.ShowToast('You have already visited this outlet. It cannot be removed from today\'s plan', 'long', 'bottom', function(a) {
                                console.log(a)
                            });
                        }

                    }
                } else {
                    Splash.ShowToast('Outlets can only be de-selected from today\'s plan.', 'long', 'center', function(a) {
                        console.log(a)
                    });
                }
            }

            $scope.addNewOutlets = function() {
                if ($rootScope.day == 'today')
                    $location.path('app/AddOutlets/' + new Date());
                else
                    Splash.ShowToast('Outlets can only be added to today\'s plan.', 'long', 'center', function(a) {
                        console.log(a)
                    });
            }
            $scope.prevSlide = function() {
                $ionicSlideBoxDelegate.previous();
            }
            $scope.nextSlide = function() {
                $ionicSlideBoxDelegate.next();
            }

        } catch (err) {
            $ionicLoading.hide();
            WEBSERVICE_ERROR('TodayCtrlError', err.name + '::' + err.message);
        }

    })



.controller('OverviewCtrl', function($scope, $stateParams, $window, $location, $rootScope, $q, $ionicModal, $timeout, $interval, $ionicLoading, $log, $filter, FETCH_LOCAL_DATA, WEBSERVICE_ERROR) {
            try {
                $ionicLoading.show({
                    template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
                });
                $rootScope.params = $stateParams;
                $rootScope.name = "OverviewCtrl";
                $scope.breadList = [{
                    name: 'Outlet Summary '
                }, {
                    name: 'Last Visit Summary'
                }];
                var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
                $rootScope.backText = 'Back';
                $rootScope.showBack = true;
                $scope.currentMonthSales = 0;
                $scope.avgSales = 0;
                $scope.promotions = [];
                $scope.lastThreeMonth = [];
                $scope.TotalMonthWiseSales = [];
                $scope.CategoryWiseMs = {
                    Overall: 0,
                    Mild: 0,
                    Strong: 0
                };
                var customer = FETCH_LOCAL_DATA('Account', 'Id', $stateParams.accountId);
                if (customer.length != 0 && customer[0] != undefined) {
                    $scope.currentCustomer = customer[0];
                }
                $scope.AccountImage = './img/capture.png';
                var images = FETCH_LOCAL_DATA('Db_images', 'ParentId', $stateParams.accountId);

                if (images.length > 0) {
                    $scope.AccountImage = "data:image/jpeg;base64," + images[0].Body;
                }
                var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                var d = new Date();
                var monthVal = d.getMonth();
                var yearVal = d.getFullYear();
                var monthAndYearList = [];
                var MonthAndCategoryWiseSales = [],
                    industryMonthAndCategoryWiseSales = [];
                for (var i = 0; i < 3; i++) {
                    monthVal = monthVal - 1;
                    if (monthVal < 0) {
                        monthVal = 11;
                        yearVal = currentYear - 1;
                    }
                    $scope.TotalMonthWiseSales[months[monthVal]] = 0;
                    $scope.lastThreeMonth.push({
                        Month: months[monthVal],
                        Year: yearVal
                    });
                    MonthAndCategoryWiseSales[months[monthVal]] = {
                        Overall: 0,
                        Mild: 0,
                        Strong: 0
                    };
                    industryMonthAndCategoryWiseSales[months[monthVal]] = {
                        Overall: 0,
                        Mild: 0,
                        Strong: 0
                    };
                    monthAndYearList.push(months[monthVal] + yearVal);

                }
                var rtList = FETCH_LOCAL_DATA('DB_RecordTypes', 'SobjectType', 'Upload__c');
                var recordtypesWithIds = [];
                angular.forEach(rtList, function(record, key) {
                    recordtypesWithIds[record.Name] = record.Id;
                });
                var uploads = FETCH_LOCAL_DATA('Upload__c', 'Account__c', $stateParams.accountId);
                var todayDate = $filter('date')(new Date(), 'yyyy-MM-dd');
                for (var i = 0; i < uploads.length; i++) {
                    if (uploads[i].RecordTypeId == recordtypesWithIds['Promotions'] && uploads[i].Status__c!='Completed') {
                        var startDate = $filter('date')(new Date(uploads[i].Start_Date__c), 'yyyy-MM-dd');
                        var endDate = $filter('date')(new Date(uploads[i].End_Date__c), 'yyyy-MM-dd');
                        if (todayDate >= startDate && todayDate <= endDate) {
                            $scope.promotions.push(uploads[i]);
                        }
                    } else if (uploads[i].RecordTypeId == recordtypesWithIds['Sales'] && uploads[i].Sales__c != undefined && uploads[i].Sales_Month__c != undefined && monthAndYearList.indexOf(uploads[i].Sales_Month__c) != -1) {
                        if (uploads[i].Company__c == 'UB') {
                            if (uploads[i].Month__c == months[d.getMonth()] && uploads[i].Year__c == d.getFullYear()) {
                                $scope.currentMonthSales = $scope.currentMonthSales + uploads[i].Sales__c;
                            } else {
                                MonthAndCategoryWiseSales[uploads[i].Month__c].Overall = MonthAndCategoryWiseSales[uploads[i].Month__c].Overall + parseInt(uploads[i].Sales__c);
                                if (uploads[i].Category__c == 'Mild' || uploads[i].Category__c == 'MILD' || uploads[i].Category__c === 'Mild Beer' || uploads[i].Category__c == 'MILD BEER') {
                                    MonthAndCategoryWiseSales[uploads[i].Month__c].Mild = MonthAndCategoryWiseSales[uploads[i].Month__c].Mild +parseInt(uploads[i].Sales__c);
                                }
                                if (uploads[i].Category__c == 'Strong' || uploads[i].Category__c == 'STRONG' || uploads[i].Category__c == 'Strong Beer' || uploads[i].Category__c == 'STRONG BEER') {
                                    MonthAndCategoryWiseSales[uploads[i].Month__c].Strong = MonthAndCategoryWiseSales[uploads[i].Month__c].Strong + parseInt(uploads[i].Sales__c);
                                }
                                $scope.TotalMonthWiseSales[uploads[i].Month__c] = $scope.TotalMonthWiseSales[uploads[i].Month__c] + parseInt(uploads[i].Sales__c);
                            }
                        }

                        if (uploads[i].Category__c == 'Mild' || uploads[i].Category__c == 'MILD' || uploads[i].Category__c == 'Mild Beer' || uploads[i].Category__c == 'MILD BEER') {
                            industryMonthAndCategoryWiseSales[uploads[i].Month__c].Mild = industryMonthAndCategoryWiseSales[uploads[i].Month__c].Mild + parseInt(uploads[i].Sales__c);
                        }
                        if (uploads[i].Category__c == 'Strong' || uploads[i].Category__c == 'STRONG' || uploads[i].Category__c == 'Strong Beer' || uploads[i].Category__c == 'STRONG BEER') {
                            industryMonthAndCategoryWiseSales[uploads[i].Month__c].Strong = industryMonthAndCategoryWiseSales[uploads[i].Month__c].Strong + parseInt(uploads[i].Sales__c);
                        }
                        industryMonthAndCategoryWiseSales[uploads[i].Month__c].Overall = industryMonthAndCategoryWiseSales[uploads[i].Month__c].Overall + parseInt(uploads[i].Sales__c);


                    }
                }
                if(MonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Overall != 0){
                    $scope.avgSales = MonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Overall;
                }
                if(MonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Overall != 0){
                    $scope.avgSales = $scope.avgSales + MonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Overall;
                }
                if(MonthAndCategoryWiseSales[$scope.lastThreeMonth[2].Month].Overall != 0){
                    $scope.avgSales = $scope.avgSales + MonthAndCategoryWiseSales[$scope.lastThreeMonth[2].Month].Overall;
                }
                $scope.avgSales =Math.round($scope.avgSales/3); 
                // $log.debug(JSON.stringify(MonthAndCategoryWiseSales['Mar']));
                // $log.debug(JSON.stringify(industryMonthAndCategoryWiseSales['Mar']));  
                if (MonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Overall != 0) {
                    $scope.CategoryWiseMs.Overall = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Overall / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Overall)*100));
                    $scope.CategoryWiseMs.Mild = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Mild / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Mild)*100));
                    $scope.CategoryWiseMs.Strong = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Strong / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Strong)*100));
                } else if (MonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Overall != 0) {
                    $scope.CategoryWiseMs.Overall = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Overall / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Overall)*100));
                    $scope.CategoryWiseMs.Mild = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Mild / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Mild)*100));
                    $scope.CategoryWiseMs.Strong = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Strong / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Strong)*100));
                } else {
                    $scope.CategoryWiseMs.Overall = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[2].Month].Overall / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[2].Month].Overall)*100));
                    $scope.CategoryWiseMs.Mild = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[2].Month].Mild / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[2].Month].Mild)*100));
                    $scope.CategoryWiseMs.Strong = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[2].Month].Strong / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[2].Month].Strong)*100));
                }
                if(isNaN($scope.CategoryWiseMs.Overall)){
                    $scope.CategoryWiseMs.Overall=0;
                }
                if(isNaN($scope.CategoryWiseMs.Mild)){
                    $scope.CategoryWiseMs.Mild=0;
                }
                if(isNaN($scope.CategoryWiseMs.Strong)){
                    $scope.CategoryWiseMs.Strong=0;
                }

                $ionicLoading.hide();
                $scope.gotoObjectives = function() {
                    $scope.backText = 'Back';
                    $location.path('app/lastVistSummary/' + $stateParams.accountId);
                }
                } catch (err) {
                    $ionicLoading.hide();
                    WEBSERVICE_ERROR('OverviewCtrlError', err.name + '::' + err.message);
                }

            })
        .controller('MarketShareCtrl', function($scope, $stateParams, $window, $location, $rootScope, $ionicLoading, $log, $filter, $ionicSlideBoxDelegate, FETCH_LOCAL_DATA, FETCH_DATA_LOCAL, WEBSERVICE_ERROR) {
            try {
                $ionicLoading.show({
                    template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
                });
                $rootScope.params = $stateParams;
                $rootScope.name = "MarketShareCtrl";
                $scope.MSbreadList = [{
                    name: 'Mild'
                }, {
                    name: 'Strong'
                }];
                $rootScope.backText = 'Back';
                $rootScope.showBack = true;
                $scope.lastThreeMonth = [];
                var customer = FETCH_LOCAL_DATA('Account', 'Id', $stateParams.accountId);
                if (customer.length != 0 && customer[0] != undefined) {
                    $scope.currentCustomer = customer[0];
                }
                var MonthAndCategoryWiseSales = [],
                    industryMonthAndCategoryWiseSales = [];
                var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                var d = new Date();
                var monthVal = d.getMonth();
                var yearVal = d.getFullYear();
                var monthAndYearList = [];
                industryMonthAndCategoryWiseSales[months[monthVal]] = {
                        Overall: 0,
                        Mild: 0,
                        Strong: 0
                    };
                for (var i = 0; i < 3; i++) {
                    monthVal = monthVal - 1;
                    if (monthVal < 0) {
                        monthVal = 11;
                        yearVal = currentYear - 1;
                    }
                    $scope.lastThreeMonth.push({
                        Month: months[monthVal],
                        Year: yearVal
                    });
                    industryMonthAndCategoryWiseSales[months[monthVal]] = {
                        Overall: 0,
                        Mild: 0,
                        Strong: 0
                    };
                    monthAndYearList.push(months[monthVal] + yearVal);
                }
                var rtList = FETCH_LOCAL_DATA('DB_RecordTypes', 'SobjectType', 'Upload__c');
                var recordtypesWithIds = [];
                angular.forEach(rtList, function(record, key) {
                    recordtypesWithIds[record.Name] = record.Id;
                });
                $scope.historicalSalesCategoryWise = [];
                $scope.historicalSalesCategoryWise['Mild'] = [];
                $scope.historicalSalesCategoryWise['Strong'] = [];
                var SalesMonthsAvailable = [];
                var CategoryWiseSales = [];
                var uploads = FETCH_LOCAL_DATA('Upload__c', 'Account__c', $stateParams.accountId);
                var salesRecords = [];
                var products = [];
                $scope.categoryList = ["Mild", "Strong"];
                for (var i = 0; i < uploads.length; i++) {
                   if (uploads[i].RecordTypeId == recordtypesWithIds['Sales'] && uploads[i].Sales__c != undefined && uploads[i].Sales_Month__c != undefined && monthAndYearList.indexOf(uploads[i].Sales_Month__c) != -1) {
                        if (uploads[i].Category__c == 'Mild' || uploads[i].Category__c == 'MILD' || uploads[i].Category__c == 'Mild Beer' || uploads[i].Category__c == 'MILD BEER') {
                            industryMonthAndCategoryWiseSales[uploads[i].Month__c].Mild = industryMonthAndCategoryWiseSales[uploads[i].Month__c].Mild + parseInt(uploads[i].Sales__c);
                        }
                        if (uploads[i].Category__c == 'Strong' || uploads[i].Category__c == 'STRONG' || uploads[i].Category__c == 'Strong Beer' || uploads[i].Category__c == 'STRONG BEER') {
                            industryMonthAndCategoryWiseSales[uploads[i].Month__c].Strong = industryMonthAndCategoryWiseSales[uploads[i].Month__c].Strong + parseInt(uploads[i].Sales__c);
                        }
                        industryMonthAndCategoryWiseSales[uploads[i].Month__c].Overall = industryMonthAndCategoryWiseSales[uploads[i].Month__c].Overall + parseInt(uploads[i].Sales__c);
                        if (products.indexOf(uploads[i].Brands_and_Products__c) == -1) {
                            products.push(uploads[i].Brands_and_Products__c);
                        }
                        var record = {};
                        if (salesRecords[uploads[i].Brands_and_Products__c] != undefined) {
                            record = salesRecords[uploads[i].Brands_and_Products__c];
                        } else {
                            salesRecords[uploads[i].Brands_and_Products__c] = [];
                            record['productName'] = uploads[i].SKU__c;
                            record['category'] = uploads[i].Category__c;
                            record[$scope.lastThreeMonth[0].Month] = {
                                Sales: '-',
                                CurrentMs: 0,
                                LastMs: 0,
                                avgSales: '-',
                                Color: 'black'
                            };
                            record[$scope.lastThreeMonth[1].Month] = {
                                Sales: '-',
                                CurrentMs: 0,
                                LastMs: 0,
                                avgSales: '-',
                                Color: 'black'
                            };
                            record[$scope.lastThreeMonth[2].Month] = {
                                Sales: '-',
                                CurrentMs: 0,
                                LastMs: 0,
                                avgSales: '-',
                                Color: 'black'
                            };
                        }
                        if (SalesMonthsAvailable.indexOf(uploads[i].Month__c) != -1) {
                            SalesMonthsAvailable.push(uploads[i].Month__c);
                        }
                        if(uploads[i].Month__c==$scope.lastThreeMonth[2].Month)
                            record['LatestSales'] = uploads[i].Sales__c;
                        else if(uploads[i].Month__c==$scope.lastThreeMonth[1].Month)
                            record['LatestSales'] = uploads[i].Sales__c;
                        else if(uploads[i].Month__c==$scope.lastThreeMonth[0].Month)
                            record['LatestSales'] = uploads[i].Sales__c;
                        record[uploads[i].Month__c] = {
                            Sales: uploads[i].Sales__c,
                            CurrentMs: Math.round(parseFloat(uploads[i].Current_MS__c)),
                            LastMs: Math.round(parseFloat(uploads[i].Last_MS__c)),
                            avgSales: Math.round(parseFloat(uploads[i].Average_Sales__c)),
                            Color: uploads[i].Color__c
                        };
                        salesRecords[uploads[i].Brands_and_Products__c] = record;
                    }
                }
                angular.forEach(products, function(sku, key) {
                    //$log.debug('SalesRecord'+sku);
                    var salesRec = salesRecords[sku];

                    if (SalesMonthsAvailable.indexOf($scope.lastThreeMonth[0].Month) != -1 && salesRec[$scope.lastThreeMonth[0].Month].Sales == '-') {
                        salesRec[$scope.lastThreeMonth[0].Month].Sales = 0;
                    }else{
                        if(salesRec[$scope.lastThreeMonth[0].Month].Sales!= '-' && salesRec[$scope.lastThreeMonth[0].Month].Sales>0 ){
                           salesRec[$scope.lastThreeMonth[0].Month].CurrentMs = Math.round((salesRec[$scope.lastThreeMonth[0].Month].Sales/industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month][salesRec.category])*100); 
                        }
                       
                    }
                    if (SalesMonthsAvailable.indexOf($scope.lastThreeMonth[1].Month) != -1 && salesRec[$scope.lastThreeMonth[1].Month].Sales == '-') {
                        salesRec[$scope.lastThreeMonth[1].Month].Sales = 0;
                    }else{
                        if(salesRec[$scope.lastThreeMonth[1].Month].Sales!= '-' && salesRec[$scope.lastThreeMonth[1].Month].Sales>0 ){
                            salesRec[$scope.lastThreeMonth[1].Month].CurrentMs = Math.round((salesRec[$scope.lastThreeMonth[1].Month].Sales/industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month][salesRec.category])*100);
                        }
                    }
                    if (SalesMonthsAvailable.indexOf($scope.lastThreeMonth[2].Month) != -1 && salesRec[$scope.lastThreeMonth[2].Month].Sales == '-') {
                        salesRec[$scope.lastThreeMonth[2].Month].Sales = 0;
                    }else{
                        if(salesRec[$scope.lastThreeMonth[2].Month].Sales!= '-' && salesRec[$scope.lastThreeMonth[2].Month].Sales>0 ){
                            salesRec[$scope.lastThreeMonth[2].Month].CurrentMs = Math.round((salesRec[$scope.lastThreeMonth[2].Month].Sales/industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[2].Month][salesRec.category])*100);
                        }
                    }
                    var avgsales = 0,
                        sales1 = salesRec[$scope.lastThreeMonth[0].Month].Sales,
                        sales2 = salesRec[$scope.lastThreeMonth[1].Month].Sales,
                        sales3 = salesRec[$scope.lastThreeMonth[2].Month].Sales;
                    if (sales1 != '-') {
                        avgsales = avgsales + parseInt(sales1);
                    }
                    if (sales2 != '-') {
                        avgsales = avgsales + parseInt(sales2);
                    }
                    if (sales3 != '-') {
                        avgsales = avgsales + parseInt(sales3);
                    }
                    /*
                     *Description : now average is not calculated but is taken from most recent sales average field.
                     *Date : 28-05-2015
                     *Author : Kunal Bindal
                     */
                    if(salesRec[$scope.lastThreeMonth[0].Month].avgSales!= '-'){
                        salesRec['AvgSales'] = salesRec[$scope.lastThreeMonth[0].Month].avgSales;
                    }else if(salesRec[$scope.lastThreeMonth[1].Month].avgSales!= '-'){
                        salesRec['AvgSales'] = salesRec[$scope.lastThreeMonth[1].Month].avgSales;
                    }else if(salesRec[$scope.lastThreeMonth[2].Month].avgSales!= '-'){
                        salesRec['AvgSales'] = salesRec[$scope.lastThreeMonth[2].Month].avgSales;
                    }
                    $log.debug('Avg Sales ===' + avgsales);

                    $log.debug($scope.lastThreeMonth[0].Month + '===' + sales1);
                    $log.debug($scope.lastThreeMonth[1].Month + '===' + sales2);
                    $log.debug($scope.lastThreeMonth[2].Month + '===' + sales3);
                    avgsales = Math.round(parseFloat(avgsales / 3));
                    
                    //salesRec['AvgSales'] = avgsales;
                    if ($scope.historicalSalesCategoryWise[salesRecords[sku].category] == undefined) {
                        $scope.historicalSalesCategoryWise[salesRecords[sku].category] = [];
                    }
                    $scope.historicalSalesCategoryWise[salesRecords[sku].category].push(salesRecords[sku]);

                });
                if ($scope.historicalSalesCategoryWise['Mild'].length < 1) {
                    Splash.ShowToast('No sales found for Mild.', 'long', 'bottom', function(a) {
                        console.log(a)
                    });
                }
                $scope.prevSlide = function() {
                    $ionicSlideBoxDelegate.previous();
                    if ($scope.historicalSalesCategoryWise['Mild'].length < 1) {
                        Splash.ShowToast('No sales found for Mild.', 'long', 'bottom', function(a) {
                            console.log(a)
                        });
                    }

                }
                $scope.nextSlide = function() {
                    $ionicSlideBoxDelegate.next();
                    if ($scope.historicalSalesCategoryWise['Strong'].length < 1) {
                        Splash.ShowToast('No sales found for Strong.', 'long', 'bottom', function(a) {
                            console.log(a)
                        });
                    }
                }
                $ionicLoading.hide();
            } catch (err) {
                $ionicLoading.hide();
                WEBSERVICE_ERROR('MarketShareCtrlError', err.name + '::' + err.message);
            }
        })
        .controller('BusinessObjectivesCtrl', function($scope, $stateParams, $location, $rootScope, $ionicLoading, $log, $filter, SalesforceSession, FETCH_DATA_LOCAL, FETCH_DATA, WEBSERVICE_ERROR) {
            try {
                $ionicLoading.show({
                    template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
                });
                $rootScope.params = $stateParams;
                $rootScope.name = "BusinessObjectivesCtrl";
                $rootScope.backText = 'Back';
                $scope.breadList = [{
                    name: 'Outlet Summary '
                }, {
                    name: 'Last Visit Summary'
                }];
                $scope.currentAccount = {};
                var currentAccRecord = FETCH_DATA.querySoup('Account', 'Id', $stateParams.accountId);
                $scope.currentAccount = currentAccRecord.currentPageOrderedEntries[0];
                $scope.gotoLastvistSummary = function() {
                    $location.path('app/lastVistSummary/' + $scope.currentAccount.Id);
                }
                var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
                var rtList = FETCH_DATA_LOCAL('DB_RecordTypes', sfSmartstore.buildSmartQuerySpec("SELECT  {DB_RecordTypes:_soup} FROM {DB_RecordTypes} WHERE {DB_RecordTypes:SobjectType}='Upload__c' AND {DB_RecordTypes:Name}='Business Objectives'", 1));
                var businessObjectivesRecords = FETCH_DATA_LOCAL('Upload__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Upload__c:_soup} FROM {Upload__c} WHERE {Upload__c:Account__c}='" + $stateParams.accountId + "' AND {Upload__c:RecordTypeId}='" + rtList.currentPageOrderedEntries[0][0].Id + "'", 1000));
                var date = new Date();
                var firstDay = $filter('date')(new Date(date.getFullYear(), date.getMonth(), 1), 'yyyy-MM-dd');
                var lastDay = $filter('date')(new Date(date.getFullYear(), date.getMonth() + 1, 0), 'yyyy-MM-dd');
                $scope.businessObjectives = [];
                angular.forEach(businessObjectivesRecords.currentPageOrderedEntries, function(record, key) {
                    var startDate = $filter('date')(new Date(record[0].Start_Date__c), 'yyyy-MM-dd');
                    if (firstDay >= startDate && startDate <= lastDay) {
                        $scope.businessObjectives.push(record[0]);
                    }
                });

                $ionicLoading.hide();
            } catch (err) {
                $ionicLoading.hide();
                WEBSERVICE_ERROR('BusinessObjectivesCtrlError', err.name + '::' + err.message);
            }
        })
        .controller('LastVisitSummaryCtrl', function($scope, $stateParams, $ionicSlideBoxDelegate, $cordovaCamera, $rootScope, $window, $state, $location, $q, $ionicModal, $timeout, $interval, $ionicLoading, $log, $filter, SalesforceSession, FETCH_DATA_LOCAL, SOUP_EXISTS, FETCH_DAILY_PLAN_ACCOUNTS, MOBILEDATABASE_ADD, FETCH_DATA, WEBSERVICE_ERROR,GEO_LOCATION) {
            try {
                $ionicLoading.show({
                    template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
                });
                $rootScope.params = $stateParams;
                $rootScope.name = "LastVisitSummaryCtrl";
                $scope.breadList = [{
                    name: 'Outlet Summary '
                }, {
                    name: 'Last Visit Summary'
                }];
                $rootScope.backText = 'Back';
                $scope.currentAccount = {};
                var currentAccRecord = FETCH_DATA.querySoup('Account', 'Id', $stateParams.accountId);
                $scope.currentAccount = currentAccRecord.currentPageOrderedEntries[0];
                var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
                var d = new Date();
                var today = d.getTime().toString();
                var todayRoutePlan = FETCH_DAILY_PLAN_ACCOUNTS('today');
                angular.forEach(todayRoutePlan, function(acc, accKey) {
                    if(acc.Id==$scope.currentAccount.Id)
                      $scope.currentAccount.RoutePlanId=acc.RoutePlanId;
                });
                $scope.openComplaints = [];
                var rtList = FETCH_DATA_LOCAL('DB_RecordTypes', sfSmartstore.buildSmartQuerySpec("SELECT  {DB_RecordTypes:_soup} FROM {DB_RecordTypes} WHERE {DB_RecordTypes:SobjectType}='Visit_Summary__c' AND {DB_RecordTypes:Name}='Feedback and Complaints'", 1));
                var lastVisit = FETCH_DATA_LOCAL('Visit__c', sfSmartstore.buildSmartQuerySpec("SELECT {Visit__c:_soup} FROM {Visit__c} WHERE {Visit__c:Account__c}='" + $scope.currentAccount.Id + "' ORDER BY {Visit__c:Check_In_DateTime__c} DESC", 20));
                $scope.lastVisitRecord = {};
                if (lastVisit.currentPageOrderedEntries != undefined && lastVisit.currentPageOrderedEntries.length > 0 && lastVisit.currentPageOrderedEntries[0][0] != undefined) {
                    $scope.lastVisitRecord = lastVisit.currentPageOrderedEntries[0][0];
                    if($scope.lastVisitRecord.Check_Out_DateTime__c==undefined || $scope.lastVisitRecord.Check_Out_DateTime__c==''){
                        if(lastVisit.currentPageOrderedEntries[1] !=undefined && lastVisit.currentPageOrderedEntries[1][0]!=undefined)
                            $scope.lastVisitRecord = lastVisit.currentPageOrderedEntries[1][0];
                        else
                            $scope.lastVisitRecord = {};
                    }
                    if ($scope.lastVisitRecord != undefined && $scope.lastVisitRecord.Check_In_DateTime__c != undefined) {
                        $scope.lastVisitRecord.Check_In_DateTime__c = $filter('date')($scope.lastVisitRecord.Check_In_DateTime__c, 'dd MMM yyyy');
                    }

                    var query = '';
                    angular.forEach(lastVisit.currentPageOrderedEntries, function(record, key) {
                        var visitId = '';
                        var extId = record[0].External_Id__c;
                        if (record[0].Id != undefined) {
                            visitId = record[0].Id;
                        }
                        if (query == '') {
                            if (visitId != '') {
                                query = "{Visit_Summary__c:Visit__c}='" + visitId + "' OR {Visit_Summary__c:Visit__c}='" + extId + "'";
                            } else {
                                query = "{Visit_Summary__c:Visit__c}='" + extId + "'";
                            }

                        } else {
                            if (visitId != '') {
                                query = query + " OR {Visit_Summary__c:Visit__c}='" + visitId + "' OR {Visit_Summary__c:Visit__c}='" + extId + "'";
                            } else {
                                query = query + " OR {Visit_Summary__c:Visit__c}='" + extId + "'";
                            }

                        }
                    });
                    $log.debug('query' + query);
                    // var openComplaints = FETCH_DATA_LOCAL('Visit_Summary__c', sfSmartstore.buildSmartQuerySpec("SELECT {Visit_Summary__c:_soup} FROM {Visit_Summary__c} WHERE {Visit_Summary__c:Status__c}='Logged' AND {Visit_Summary__c:RecordTypeId}='" + rtList.currentPageOrderedEntries[0][0].Id + "' AND (" + query + ")", 50));
                    var openComplaints = FETCH_DATA_LOCAL('Visit_Summary__c', sfSmartstore.buildSmartQuerySpec("SELECT {Visit_Summary__c:_soup} FROM {Visit_Summary__c} WHERE ({Visit_Summary__c:Status__c}='Logged' or {Visit_Summary__c:Status__c}='logged') AND {Visit_Summary__c:RecordTypeId}='" + rtList.currentPageOrderedEntries[0][0].Id + "' and ({Visit_Summary__c:AccountId__c}='"+$stateParams.accountId+"' or (" + query + ")) ", 50));
                    //var openComplaints = FETCH_DATA_LOCAL('Visit_Summary__c', sfSmartstore.buildSmartQuerySpec("SELECT {Visit_Summary__c:_soup} FROM {Visit_Summary__c} WHERE {Visit_Summary__c:RecordTypeId}='" + rtList.currentPageOrderedEntries[0][0].Id + "' and ((({Visit_Summary__c:Status__c}='Logged' or {Visit_Summary__c:Status__c}='logged') AND {Visit_Summary__c:AccountId__c}='"+$stateParams.accountId+"') or (" + query + "))  ", 50));
                    angular.forEach(openComplaints.currentPageOrderedEntries, function(record, key) {
                        if(record[0]['Status__c']=='logged')
                            record[0]['Status__c']='Logged';
                        if(record[0]['Source__c']=='Consumer complaint')
                            record[0]['Source__c']='Consumer Complaint';
                        if(record[0]['Source__c']=='consumer complaint')
                            record[0]['Source__c']='Consumer Complaint';
                        if(record[0]['Source__c']=='Trade complaint')
                            record[0]['Source__c']='Trade Complaint';
                        if(record[0]['Source__c']=='Trade complaint')
                            record[0]['Source__c']='Trade Complaint';
                        $scope.openComplaints.push(record[0]);
                    });
                }
                $log.debug('Complaints' + JSON.stringify($scope.openComplaints));
                $rootScope.processFlowLineItems = [];
                $rootScope.pages = [];
                var flow = FETCH_DATA_LOCAL('Db_process_flow', sfSmartstore.buildSmartQuerySpec("SELECT  {Db_process_flow:_soup} FROM {Db_process_flow} WHERE {Db_process_flow:Outlet_Type__c}='" + $scope.currentAccount.RecordType.Name + "'", 1));
                $rootScope.processFlow = flow.currentPageOrderedEntries[0][0];
                var flowLineItems = FETCH_DATA.querySoup('Db_process_flow_line_item', "Process_Flow__c", flow.currentPageOrderedEntries[0][0].Id);
                var queryspec = '';
                var queryspecForSection = '';
                angular.forEach(flowLineItems.currentPageOrderedEntries, function(record, key) {
                    $rootScope.processFlowLineItems[record.Order__c - 1] = record;
                    if (queryspec == '' && queryspecForSection == '') {
                        queryspec = "{DB_page:Id}='" + record.Page__c + "'";
                        queryspecForSection = "{DB_section:Page__c}='" + record.Page__c + "'";
                    } else {
                        queryspec = queryspec + " OR {DB_page:Id}='" + record.Page__c + "'";
                        queryspecForSection = queryspecForSection + " OR {DB_section:Page__c}='" + record.Page__c + "'";
                    }
                });
                if (queryspec != '' && queryspecForSection != '') {
                    var pages = FETCH_DATA_LOCAL('DB_page', sfSmartstore.buildSmartQuerySpec("SELECT  {DB_page:_soup} FROM {DB_page} WHERE " + queryspec, 20));
                    var sections = FETCH_DATA_LOCAL('DB_section', sfSmartstore.buildSmartQuerySpec("SELECT  {DB_section:_soup} FROM {DB_section} WHERE " + queryspecForSection + " order by {DB_section:Order__c}", 20));
                    queryspec = '';
                    angular.forEach(sections.currentPageOrderedEntries, function(record, key) {
                        if (queryspec == '') {
                            queryspec = "{DB_fields:Section__c}='" + record[0].Id + "'";
                        } else {
                            queryspec = queryspec + " OR {DB_fields:Section__c}='" + record[0].Id + "'";
                        }
                    });
                    var fields = [];
                    if (queryspec != '') {
                        fields = FETCH_DATA_LOCAL('DB_fields', sfSmartstore.buildSmartQuerySpec("SELECT  {DB_fields:_soup} FROM {DB_fields} WHERE " + queryspec + " order by {DB_fields:Order__c}", 200));
                    }
                    //$log.debug('fields' + JSON.stringify(fields.currentPageOrderedEntries));
                    var pagesWithIds = [];
                    angular.forEach(pages.currentPageOrderedEntries, function(page, pageKey) {
                        var pagedec = {};
                        pagedec.pageDescription = page[0];
                        pagesWithIds[page[0].Id] = page[0];
                        pagedec.sections = [];
                        angular.forEach(sections.currentPageOrderedEntries, function(section, sectionKey) {
                            if (page[0].Id == section[0].Page__c) {
                                var sectiondec = {};
                                sectiondec.sectionDescription = section[0];
                                sectiondec.fields = [];
                                angular.forEach(fields.currentPageOrderedEntries, function(field, fieldKey) {
                                    if (field[0].Section__c == section[0].Id) {
                                        var fieldDescription = field[0];
                                        if (field[0].Picklist_Values__c != undefined) {
                                            fieldDescription.pickListValues = field[0].Picklist_Values__c.split(';');
                                        }
                                        sectiondec.fields.push(fieldDescription);
                                    }
                                });
                                pagedec.sections.push(sectiondec);
                            }
                        });
                        $rootScope.pages.push(pagedec);
                    });
                }
                $ionicLoading.hide();
                $scope.checkIn = function() {


                    var pendingTasks = FETCH_DATA.querySoup('Db_transaction', 'status', 'pending');
                    if ($rootScope.day == 'today') {
                        if (pendingTasks.currentPageOrderedEntries.length == 0) {
                            if ($rootScope.day == 'today') {
                                $ionicLoading.show({
                                    template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
                                });
                                $rootScope.StockAndSalesList = [];
                                $rootScope.retailSalesList = [];
                                $rootScope.uploadfields = [];
                                $rootScope.slidesList = [];
                                $rootScope.slideFields = [];
                                $rootScope.PrideImages = [];
                                $rootScope.CompeSales = [];
                                $rootScope.UblSales = [];
                                $rootScope.chataiSales = [];
                                $rootScope.audit = {};
                                $rootScope.tempvisitType = 'false';
                                $rootScope.pendingTask = true;
                                $rootScope.visitrecord = {};
                                $rootScope.Month = {};
                                //sfSmartstore.removeSoup('Db_transaction', function(response) {}, function(error) {});
                                if (SOUP_EXISTS.soupExists('Db_transaction') == false) {

                                    var INDEXES_TRANSACTION = [{
                                        "path": "Account",
                                        "type": "string"
                                    }, {
                                        "path": "Visit",
                                        "type": "string"
                                    }, {
                                        "path": "stage",
                                        "type": "string"
                                    }, {
                                        "path": "stageValue",
                                        "type": "string"
                                    }, {
                                        "path": "status",
                                        "type": "string"
                                    }, {
                                        "path": "entryDate",
                                        "type": "string"
                                    }];
                                    SOUP_REGISTER('Db_transaction', INDEXES_TRANSACTION);
                                }
                                $scope.currentLocation = {};
                                GEO_LOCATION.getCurrentPosition().then(function(position){
                                    $scope.currentLocation.latitude = position.latitude;
                                    $scope.currentLocation.longitude = position.longitude;
                                    $scope.Visit = [{
                                        Account__c: $scope.currentAccount.Id,
                                        Check_In_DateTime__c: today,
                                        Check_In_Location__Latitude__s: $scope.currentLocation.latitude,
                                        Check_In_Location__Longitude__s: $scope.currentLocation.longitude,
                                        External_Id__c: $window.Math.random() * 10000000,
                                        Route_Plan__c: $scope.currentAccount.RoutePlanId,
                                        IsDirty: true
                                    }];

                                    if ($scope.currentAccount.Location__Latitude__s == undefined || $scope.currentAccount.Location__Latitude__s == null) {
                                        $scope.currentAccount.Location__Latitude__s = $scope.currentLocation.latitude;
                                        $scope.currentAccount.Location__Longitude__s = $scope.currentLocation.longitude;
                                        $scope.currentAccount.IsDirty = true;
                                        var accountToUpate = [];
                                        accountToUpate.push($scope.currentAccount);
                                        MOBILEDATABASE_ADD('Account', accountToUpate, 'Id');
                                        $log.debug('accountToUpate' + JSON.stringify(accountToUpate));
                                    } else {
                                        if ($scope.distance($scope.currentOutlet.Location__Latitude__s, $scope.currentOutlet.Location__Longitude__s, $scope.currentLocation.latitude, $scope.currentLocation.longitude, 'K') > 1) {
                                            $scope.Visit[0].Deviation_Reason__c = 'location is different from existing location of the outlet';
                                        }
                                    }
                                    MOBILEDATABASE_ADD('Visit__c', $scope.Visit, 'External_Id__c');
                                    $ionicLoading.hide();
                                    var page = pagesWithIds[$rootScope.processFlowLineItems[0].Page__c];
                                    if (page.Template_Name__c == 'Template1') {
                                        $location.path('app/stocks/' + $scope.currentAccount.Id + '/' + $scope.Visit[0].External_Id__c + '/0/' + new Date());
                                    } else if (page.Template_Name__c == 'Template2') {
                                        $location.path('app/container2/' + $scope.currentAccount.Id + '/' + $scope.Visit[0].External_Id__c + '/0/' + new Date());
                                    } else {
                                        $location.path('app/container3/' + $scope.currentAccount.Id + '/' + $scope.Visit[0].External_Id__c + '/0/' + new Date());
                                    }
                                    $log.info('LOCATION' + JSON.stringify(position));
                                },function(error){
                                    $log.error('ERROR' + JSON.stringify(error));
                                });
                                
                            }
                        } else {
                            Splash.ShowToast('A previous call has not been completed. Tap Resume icon to complete the open call.', 'long', 'bottom', function(a) {
                                console.log(a)
                            });
                        }
                    } else {
                        Splash.ShowToast('You can only start a call for outlets in today\'s route plan.', 'long', 'bottom', function(a) {
                            console.log(a)
                        });
                    }

                };
            } catch (err) {
                $ionicLoading.hide();
                WEBSERVICE_ERROR('LastVisitSummaryCtrlError', err.name + '::' + err.message);
            }
        })
        .controller('PRIDECallCtrl', function($scope, $stateParams, $ionicScrollDelegate, $ionicSlideBoxDelegate, $cordovaCamera, $rootScope, $window, $state, $location, $q, $ionicModal, $ionicPopup, $timeout, $interval, $ionicLoading, $log, $filter, SalesforceSession, FETCH_DATA_LOCAL, SOUP_EXISTS, FETCH_DAILY_PLAN_ACCOUNTS, MOBILEDATABASE_ADD, FETCH_DATA, WEBSERVICE_ERROR) {
            try {
                $rootScope.backText = 'Back';
                $rootScope.showBack = true;
                $rootScope.params = $stateParams;
                $rootScope.name = "PRIDECallCtrl";
                $scope.isFieldVisible = function(x, y) {
                    // $log.debug('x=='+x +'y==='+y);
                    if (x != undefined && y != undefined)
                        return x.indexOf(y) == -1;
                    else
                        return false;
                }

                $scope.updateSlide = function(hand) {
                    $timeout(function() {
                        $log.debug('testing');
                        $ionicScrollDelegate.$getByHandle('small-' + hand).resize();
                        $log.debug(hand);
                    }, 1000);
                }
                $scope.AccountId = $stateParams.AccountId;
                $scope.visitId = $stateParams.visitId;
                $scope.uploadId = '';
                if ($stateParams.uploadId != undefined && $stateParams.uploadId != '')
                    $scope.uploadId = $stateParams.uploadId;
                $scope.remove = [];
                var today = $filter('date')(new Date(), 'dd/MM/yyyy');
                $scope.breadActive = 'Cooler';
                $scope.uploadslist = [];
                var transaction = [{
                    Account: $stateParams.AccountId,
                    stage: $stateParams.order,
                    stageValue: 'pridecall',
                    Visit: $stateParams.visitId,
                    status: 'pending',
                    entryDate: today
                }];

                MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
                $scope.myplan = $rootScope.Outlets;
                // if ($rootScope.day == 'today') {
                //     $scope.myplan = $rootScope.TodaysPlan;
                // } else if ($rootScope.day == 'tomorrow') {
                //     $scope.myplan = $rootScope.TomorrowPlan;
                // } else {
                //     $scope.myplan = $rootScope.DayAfterPlan;
                // }
                var currentAccountLocal = FETCH_DATA.querySoup('Account', 'Id', $stateParams.AccountId);
                if (currentAccountLocal != undefined && currentAccountLocal.currentPageOrderedEntries != undefined && currentAccountLocal.currentPageOrderedEntries.length > 0) {
                    $scope.currentOutlet = currentAccountLocal.currentPageOrderedEntries[0];
                    $scope.currentCustomer = currentAccountLocal.currentPageOrderedEntries[0];;
                }

                var sfSmartstore = cordova.require("salesforce/plugin/smartstore");

                $scope.order = $stateParams.order;
                $scope.removeUpload = function(removeUploadId) {
                    var confirmUploadDeletePopup = $ionicPopup.confirm({
                        title: 'Confirm Delete',
                        template: 'Are you sure to remove this ' + $scope.breadActive + '?'
                    });
                    confirmUploadDeletePopup.then(function(res) {
                        if (res) {
                            $location.path('app/removeUploadReason/' + removeUploadId + '/' + $scope.AccountId + '/' + $scope.visitId + '/' + $scope.order);
                            console.log('You are sure');
                        } else {
                            console.log('You are not sure');
                        }
                    });

                }
                $scope.changeValue = function(a, b, c, d) {
                   // $log.debug($rootScope.uploadfields[a][b][c]);
                   // $log.debug(a + b + c + d);
                    //  $log.debug(JSON.stringify($rootScope.uploadfields));
                    //  if($rootScope.uploadfields[a]==undefined)
                    //      $rootScope.uploadfields[a]=[];
                    //  $log.debug(JSON.stringify($rootScope.uploadfields[a]));
                    //  if($rootScope.uploadfields[a][b]==undefined)
                    //      $rootScope.uploadfields[a][b]=[];
                    //  $log.debug(JSON.stringify($rootScope.uploadfields[a][b]));
                    // $rootScope.uploadfields[a][b][c]=d;
                    // $log.debug($rootScope.uploadfields[a][b][c]);
                }
                $scope.confirmRemoveUpload = function() {
                    if ($scope.remove.Reason != undefined && $scope.remove.Reason != '') {
                        $ionicLoading.show({
                            template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
                        });
                        var removeUploadId = $scope.uploadId;
                        //var delIndex=-1;
                        angular.forEach($scope.uploadslist, function(record, key) {
                            var uploadsToDelete = [];
                            if (record.External_Id__c == removeUploadId) {
                                delIndex = key;
                                record.Status__c = 'Inactive';
                                record.Reason__c = $scope.remove.Reason;
                                record.IsDirty = true;
                                uploadsToDelete.push(record);
                                MOBILEDATABASE_ADD('Upload__c', uploadsToDelete, 'External_Id__c');
                            }

                        });
                        $ionicLoading.hide();

                        $location.path('app/container2/' + $scope.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date() + 'save');
                    } else {
                        Splash.ShowToast('Please Specify reason for removing ' + $scope.breadActive + '.', 'long', 'bottom', function(a) {
                            console.log(a);
                        });
                    }
                }
                $scope.prevSlide = function() {
                    $ionicSlideBoxDelegate.previous();
                }
                $scope.nextSlide = function() {
                    $ionicSlideBoxDelegate.next();
                }
                var pageId = $rootScope.processFlowLineItems[$stateParams.order].Page__c;
                $scope.breadActive = $rootScope.processFlowLineItems[$stateParams.order].Name;
                var currentPage = {};
                angular.forEach($rootScope.pages, function(record, key) {
                    if (record.pageDescription.Id == pageId) {
                        currentPage = record;
                    }
                });
                if (currentPage.sections.length > 1) {
                    $scope.addNewSectionFields = currentPage.sections[1].fields;
                } else {
                    $scope.addNewSectionFields = currentPage.sections[0].fields;
                }
                $scope.currentPageTitle = currentPage.pageDescription.Title__c;
                $scope.addNewButton = currentPage.pageDescription.Add_New__c;
                $scope.addNewLabel = currentPage.pageDescription.Add_New_Label__c;
                $scope.showPlanogram = currentPage.pageDescription.Is_Planogram_Show__c;
                $scope.showCaptureSales = currentPage.pageDescription.Capture_Sales__c;
                //$scope.uploadfields = [];
                var coolerList = FETCH_DATA.querySoup('Upload__c','Account__c',$scope.AccountId);
                angular.forEach(coolerList.currentPageOrderedEntries, function(record, key) {
                    //$log.debug('cout' + JSON.stringify($scope.AccountId));
                    if ($scope.AccountId == record.Account__c && record.RecordTypeId == currentPage.sections[0].sectionDescription.Input_Record_Type_Id__c) {
                        //$log.debug('uploadssss111' + JSON.stringify($rootScope.Uploads));
                        var upload = {};
                        upload = record;
                        if (upload.External_Id__c == undefined) {
                            upload.External_Id__c = $window.Math.random() * 10000000;
                        }
                        if (upload.Company__c == undefined && upload.Company__c == '') {
                            upload.Company__c = 'UBL';
                        }
                        upload.fields = currentPage.sections[0].fields;
                        if (upload.Status__c == undefined || upload.Status__c != 'Inactive')
                            $scope.uploadslist.push(upload);
                    }
                });
                $scope.titlefields = [];
                var fieldsCounts = 0;
                angular.forEach(currentPage.sections[0].fields, function(field, key) {
                    if (field.Type__c == 'title') {
                        $scope.titlefields.push(field.Field_API__c);
                    } else if (field.Type__c != 'textarea' && field.Type__c != 'output') {
                        fieldsCounts++;
                    }
                });
                /*Last Visit start*/
                $scope.lastCoolers = [];
                var lastVisit = FETCH_DATA_LOCAL('Visit__c', sfSmartstore.buildSmartQuerySpec("SELECT {Visit__c:_soup} FROM {Visit__c} WHERE {Visit__c:Account__c}='" + $stateParams.AccountId + "' ORDER BY {Visit__c:Check_In_DateTime__c} DESC", 2));
                $log.debug('last visits' + JSON.stringify(lastVisit));
                var rtList1 = FETCH_DATA_LOCAL('DB_RecordTypes', sfSmartstore.buildSmartQuerySpec("SELECT  {DB_RecordTypes:_soup} FROM {DB_RecordTypes} WHERE {DB_RecordTypes:SobjectType}='Visit_Summary__c' AND {DB_RecordTypes:Name}!='Feedback and Complaints'", 50));
                $log.debug('visit coolers' + JSON.stringify(rtList1));
                var query = '';
                if (lastVisit.currentPageOrderedEntries != undefined && lastVisit.currentPageOrderedEntries.length > 0 && lastVisit.currentPageOrderedEntries[0][0] != undefined) {

                    angular.forEach(lastVisit.currentPageOrderedEntries, function(record, key) {
                        if (record[0].External_Id__c != $stateParams.visitId) {
                            var visitId = '';
                            var extId = record[0].External_Id__c;

                            if (record[0].Id != undefined) {
                                visitId = record[0].Id;
                            }

                            if (query == '') {
                                if (visitId != '') {
                                    query = " {Visit_Summary__c:Visit__c}='" + visitId + "' OR {Visit_Summary__c:Visit__c}='" + extId + "'";
                                } else {
                                    query = " {Visit_Summary__c:Visit__c}='" + extId + "'";
                                }

                            } else {
                                if (visitId != '') {
                                    query = query + " OR {Visit_Summary__c:Visit__c}='" + visitId + "' OR {Visit_Summary__c:Visit__c}='" + extId + "'";
                                } else {
                                    query = query + " OR {Visit_Summary__c:Visit__c}='" + extId + "'";
                                }

                            }
                            $log.debug('query==' + query);
                        }


                    });
                }
                $log.debug("myquery SELECT {Visit_Summary__c:_soup} FROM {Visit_Summary__c} " + query + " ");
                if (query != '') {
                    var lastCoolers = FETCH_DATA_LOCAL('Visit_Summary__c', sfSmartstore.buildSmartQuerySpec("SELECT {Visit_Summary__c:_soup} FROM {Visit_Summary__c} where {Visit_Summary__c:RecordTypeId}='" + currentPage.sections[0].sectionDescription.Record_Type_Id__c + "' AND (" + query + ")", 50));
                    //$log.debug('lastCooler records' + JSON.stringify(lastCoolers));
                    var img_query = '';
                    angular.forEach(lastCoolers.currentPageOrderedEntries, function(record, key) {
                        if (record[0].External_Id__c != undefined && record[0].External_Id__c != '') {
                            $log.debug('External_Id__c==' + record[0].External_Id__c);
                            $scope.lastCoolers.push(record[0]);
                            if (img_query == '') {
                                if(record[0].Id!=undefined)
                                    img_query = " WHERE {Db_images:ParentId}='" + record[0].External_Id__c + "' OR {Db_images:ParentId}='" + record[0].Id + "'";
                                else
                                    img_query = " WHERE {Db_images:ParentId}='" + record[0].External_Id__c + "' ";

                            } else {
                                if(record[0].Id!=undefined)
                                    img_query = img_query + " OR {Db_images:ParentId}='" + record[0].External_Id__c + "' OR {Db_images:ParentId}='" + record[0].Id + "'";
                                else
                                    img_query = img_query + " OR {Db_images:ParentId}='" + record[0].External_Id__c + "' ";


                            }
                        }
                    });
                    $log.debug('lastCoolers' + JSON.stringify($scope.lastCoolers));
                    $log.debug("query lastCooler SELECT {Db_images:_soup} FROM {Db_images} " + img_query);
                    $scope.lastCoolers = [];
                    if (img_query != '') {
                        var lastCoolerImages = FETCH_DATA_LOCAL('Db_images', sfSmartstore.buildSmartQuerySpec("SELECT {Db_images:_soup} FROM {Db_images} " + img_query, 50));
                        //$log.debug('lastCoolerImages' + JSON.stringify(lastCoolerImages));

                        angular.forEach(lastCoolerImages.currentPageOrderedEntries, function(record, key) {
                            $scope.lastCoolers.push(record[0]);
                        });
                    }

                    //$log.debug('lastCoolerImages' + JSON.stringify($scope.lastCoolers));
                }
                if ($rootScope.resentlyAdded == true) {
                    $timeout(function() {
                        $log.debug($ionicSlideBoxDelegate.slidesCount());
                        $log.debug($rootScope.resentlyAdded);
                        $log.debug($scope.uploadslist.length);
                        $ionicSlideBoxDelegate.slide($ionicSlideBoxDelegate.slidesCount() - 1, 500);
                        $rootScope.resentlyAdded = false;
                    }, 1500)

                }


                if ($scope.uploadslist.length > 0 && ($rootScope.uploadfields[$scope.order] == undefined || $rootScope.uploadfields[$scope.order].length == 0)) {
                    var visitRecord = FETCH_DATA.querySoup('Visit__c', 'External_Id__c', $stateParams.visitId);
                    var query = "{Visit_Summary__c:Visit__c} = '" + $stateParams.visitId + "'";
                    if (visitRecord.currentPageOrderedEntries != undefined && visitRecord.currentPageOrderedEntries[0] != undefined && visitRecord.currentPageOrderedEntries[0].Id != undefined) {
                        query = query + " OR {Visit_Summary__c:Visit__c} = '" + visitRecord.currentPageOrderedEntries[0].Id + "'";
                    }
                    var visitSummaryrecords = FETCH_DATA_LOCAL('Visit_Summary__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Visit_Summary__c:_soup} FROM {Visit_Summary__c} WHERE " + query, 500));
                    if (visitSummaryrecords.currentPageOrderedEntries != undefined && visitSummaryrecords.currentPageOrderedEntries.length > 0) {
                        var summaryRecords = [];
                        angular.forEach(visitSummaryrecords.currentPageOrderedEntries, function(record, key) {
                            if (record[0].Upload__c != undefined) {
                                summaryRecords.push(record[0]);
                            }
                        });
                        $rootScope.uploadfields[$scope.order] = summaryRecords;
                    }
                }

                /*Last Visit end*/
                //  $log.debug('uploadssss' + JSON.stringify($rootScope.Uploads));
                $scope.saveAndNext = function() {
                    $timeout(function(){
                        var visitSummaryRecords = [];
                        var attachments = [];
                        angular.forEach($rootScope.uploadfields[$scope.order], function(record, key) {
                            var visitSummary = {};
                            visitSummary = record;
                            var salestrackingRecord = $rootScope.salestrackingList[$scope.uploadslist[key].Id];
                            // $log.debug('salestrackingRecord'+JSON.stringify(salestrackingRecord));
                            if ($scope.showCaptureSales && salestrackingRecord != undefined) {
                                visitSummary.During_Promotion_Brand_Sale__c = salestrackingRecord.During_Promotion_Brand_Sale__c;
                                visitSummary.During_Promotion_End_Date__c = $filter('date')(salestrackingRecord.During_Promotion_End_Date__c, 'yyyy-MM-dd');
                                visitSummary.During_Promotion_Industry_Sale__c = salestrackingRecord.During_Promotion_Industry_Sale__c;
                                visitSummary.During_Promotion_Start_Date__c = $filter('date')(salestrackingRecord.During_Promotion_Start_Date__c, 'yyyy-MM-dd');
                                visitSummary.Post_Promotion_Brand_Sale__c = salestrackingRecord.Post_Promotion_Brand_Sale__c;
                                visitSummary.Post_Promotion_End_Date__c = $filter('date')(salestrackingRecord.Post_Promotion_End_Date__c, 'yyyy-MM-dd');
                                visitSummary.Post_Promotion_Industry_Sale__c = salestrackingRecord.Post_Promotion_Industry_Sale__c;
                                visitSummary.Post_Promotion_Start_Date__c = $filter('date')(salestrackingRecord.Post_Promotion_Start_Date__c, 'yyyy-MM-dd');
                                visitSummary.Pre_Promotion_Brand_Sale__c = salestrackingRecord.Pre_Promotion_Brand_Sale__c;
                                visitSummary.Pre_Promotion_End_Date__c = $filter('date')(salestrackingRecord.Pre_Promotion_End_Date__c, 'yyyy-MM-dd');
                                visitSummary.Pre_Promotion_Industry_Sale__c = salestrackingRecord.Pre_Promotion_Industry_Sale__c;
                                visitSummary.Pre_Promotion_Start_Date__c = $filter('date')(salestrackingRecord.Pre_Promotion_Start_Date__c, 'yyyy-MM-dd');
                            }
                            if (record.Working_Condition__c == 'No' && record.External_Id__c == undefined) {
                                var rtList = FETCH_DATA_LOCAL('DB_RecordTypes', sfSmartstore.buildSmartQuerySpec("SELECT  {DB_RecordTypes:_soup} FROM {DB_RecordTypes} WHERE {DB_RecordTypes:SobjectType}='Visit_Summary__c' AND {DB_RecordTypes:Name}='Feedback and Complaints'", 50));
                                $scope.complaintRtId = rtList.currentPageOrderedEntries[0][0].Id;
                                var complaint = {
                                    Response_Type__c: 'Complaint',
                                    Source__c: 'Trade Complaint',
                                    Category__c: 'Cooler Complaint',
                                    Status__c: 'Logged',
                                    Description__c: 'Cooler Not Working',
                                    Visit__c: $stateParams.visitId,
                                    RecordTypeId: $scope.complaintRtId,
                                    External_Id__c: $stateParams.visitId + $window.Math.random() * 10000000
                                };
                                if ($scope.uploadslist[key].Id != undefined) {
                                    complaint['Upload__c'] = $scope.uploadslist[key].Id;
                                } else {
                                    complaint['Upload__c'] = $scope.uploadslist[key].External_Id__c;
                                }
                                visitSummaryRecords.push(complaint);
                            }
                            visitSummary['RecordTypeId'] = currentPage.sections[0].sectionDescription.Record_Type_Id__c;
                            if (record.External_Id__c == undefined) {
                                visitSummary['External_Id__c'] = $stateParams.visitId + '-' + $window.Math.random() * 10000000;
                            }
                            if (record.Visit__c == undefined) {
                                visitSummary['Visit__c'] = $stateParams.visitId;
                            }
                            if ($scope.uploadslist[key].Id != undefined) {
                                visitSummary['Upload__c'] = $scope.uploadslist[key].Id;
                            } else {
                                visitSummary['Upload__c'] = $scope.uploadslist[key].External_Id__c;
                            }
                            visitSummary['IsDirty'] = true;
                            $log.debug('mycooler' + JSON.stringify(visitSummary));
                            visitSummaryRecords.push(visitSummary);
                            angular.forEach($rootScope.PrideImages[$scope.uploadslist[key].External_Id__c], function(imageData, key) {
                                $log.debug('image parent id' + visitSummary['External_Id__c']);
                                attachments.push({
                                    ParentId: visitSummary['External_Id__c'],
                                    Name: visitSummary['Upload__c'] + '.jpg',
                                    Body: imageData,
                                    IsDirty: true,
                                    External_Id__c: visitSummary['Upload__c'] + '--' + visitSummary['External_Id__c'] + key
                                });
                            });
                        });

                        if (visitSummaryRecords.length > 0) {
                            MOBILEDATABASE_ADD(currentPage.pageDescription.Master_Object__c, visitSummaryRecords, 'External_Id__c');
                        }
                        if (attachments.length > 0) {
                            MOBILEDATABASE_ADD('Db_images', attachments, 'External_Id__c');
                        }

                        $log.debug('attachments' + JSON.stringify(attachments));
                        $rootScope.resentlyAdded = true;
                        var transaction = [{
                            Account: $stateParams.AccountId,
                            stage: $stateParams.order,
                            stageValue: 'pridecall',
                            Visit: $stateParams.visitId,
                            status: 'saved',
                            entryDate: today
                        }];
                        MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
                        var pageOrder = parseInt($stateParams.order) + 1;
                        $ionicLoading.hide();

                        if ($rootScope.processFlowLineItems[pageOrder] != undefined) {
                            var nextPageId = $rootScope.processFlowLineItems[pageOrder].Page__c;
                            var nextPage;
                            angular.forEach($rootScope.pages, function(record, key) {
                                if (record.pageDescription.Id == nextPageId) {
                                    nextPage = record;
                                }
                            });

                            if (nextPage.pageDescription.Template_Name__c == 'Template1') {
                                $location.path('app/stocks/' + $scope.AccountId + '/' + $stateParams.visitId + '/' + pageOrder + '/' + new Date());
                            } else if (nextPage.pageDescription.Template_Name__c == 'Template2') {
                                $location.path('app/container2/' + $scope.AccountId + '/' + $stateParams.visitId + '/' + pageOrder + '/' + new Date());
                            } else {
                                $location.path('app/container3/' + $scope.AccountId + '/' + $stateParams.visitId + '/' + pageOrder + '/' + new Date());
                            }
                        } else {
                            $location.path('app/checkout/' + $stateParams.AccountId + '/' + $stateParams.visitId);
                        }
                    },100);
                }
                $scope.takePicture = function(uploadId) {
                    var options = {
                        quality: 75,
                        destinationType: Camera.DestinationType.DATA_URL,
                        sourceType: Camera.PictureSourceType.CAMERA,
                        allowEdit: true,
                        encodingType: Camera.EncodingType.JPEG,
                        targetWidth: 990,
                        targetHeight: 360,
                        popoverOptions: CameraPopoverOptions,
                        saveToPhotoAlbum: false
                    };
                    $cordovaCamera.getPicture(options).then(function(imageData) {
                        var imageslist = [];
                        if ($rootScope.PrideImages[uploadId] != undefined) {
                            imageslist = $rootScope.PrideImages[uploadId];
                        }
                        imageslist[0] = imageData;
                        $rootScope.PrideImages[uploadId] = imageslist;
                    }, function(err) {

                    });

                }
                $scope.saveNew = function() {
                    $timeout(function(){

                        var uploadsToInsert = [];
                        var record = {};
                        var ifcheck = true;
                        angular.forEach($scope.addNewSectionFields, function(field, key) {
                            if ((field.fieldvalue == undefined || field.fieldvalue == '') && ifcheck) {
                                Splash.ShowToast('Please enter ' + field.Name + '.', 'long', 'bottom', function(a) {
                                    console.log(a)
                                });
                                ifcheck = false;
                                $ionicLoading.hide();
                            }
                            record[field.Field_API__c] = field.fieldvalue;
                        });
                        if ($scope.showAdd) {
                            record.Account__c = $scope.AccountId;
                            record.RecordTypeId = currentPage.sections[0].sectionDescription.Input_Record_Type_Id__c;
                            record.External_Id__c = $stateParams.visitId + $window.Math.random() * 10000000;
                            record.IsDirty = true;
                            $rootScope.Uploads.push(record);
                            uploadsToInsert.push(record);
                            MOBILEDATABASE_ADD('Upload__c', uploadsToInsert, 'External_Id__c');
                            $ionicLoading.hide();
                            angular.forEach($scope.addNewSectionFields, function(field, key) {
                                field.fieldvalue = '';
                            });
                            Splash.ShowToast('The cooler has been added.', 'long', 'bottom', function(a) {
                                console.log(a)
                            });
                            $rootScope.resentlyAdded = true;
                            $location.path('app/container2/' + $scope.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date() + 'save');
                        }
                    },100);
                }
                $scope.showAdd = false;
                $scope.checkValues = function() {
                    $scope.showAdd = true;
                    angular.forEach($scope.addNewSectionFields, function(field, key) {
                        if (field.fieldvalue == undefined || field.fieldvalue == '') {
                            $scope.showAdd = false;
                        }

                    });
                }
                $scope.cancelAddNew = function() {
                    angular.forEach($scope.addNewSectionFields, function(field, key) {
                        field.fieldvalue = '';
                    });
                    $location.path('app/container2/' + $scope.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date() + 'save');
                }
                $scope.CancelUploadRemove = function() {
                    $scope.remove.Reason = '';
                    $location.path('app/container2/' + $scope.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date() + 'save');
                }
                $scope.capturesales = function(uploadId) {
                    $location.path('app/capturesales/' + uploadId);
                }
                $scope.addNew = function() {
                    $location.path('app/AddNew/' + $scope.AccountId + '/' + $scope.visitId + '/' + $scope.order);
                }
            } catch (err) {
                $ionicLoading.hide();
                WEBSERVICE_ERROR('PRIDECallCtrlError', err.name + '::' + err.message);
            }

        })

        .controller('PRIDECallCtrl2', function($scope, $ionicScrollDelegate, $stateParams, $ionicSlideBoxDelegate, $cordovaCamera, $rootScope, $window, $state, $location, $q, $ionicModal, $timeout, $interval, $ionicLoading, $log, $filter, SalesforceSession, FETCH_DATA_LOCAL, SOUP_EXISTS, FETCH_DAILY_PLAN_ACCOUNTS, MOBILEDATABASE_ADD, FETCH_DATA, WEBSERVICE_ERROR) {
            try {
                $ionicLoading.show({
                    template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
                });
                $rootScope.params = $stateParams;
                $rootScope.name = "PRIDECallCtrl2";
                $rootScope.backText = 'Back';
                $scope.isFieldVisible = function(x, y) {
                    if (x != undefined && y != undefined)
                        return x.indexOf(y) == -1;
                    else
                        return false;
                }
                var today = $filter('date')(new Date(), 'dd/MM/yyyy');
                var transaction = [{
                    Account: $stateParams.AccountId,
                    stage: $stateParams.order,
                    stageValue: 'pridecall2',
                    Visit: $stateParams.visitId,
                    status: 'pending',
                    entryDate: today
                }];

                $scope.updateSlide = function(hand) {
                    $timeout(function() {
                        $log.debug('testing');
                        $ionicScrollDelegate.$getByHandle('small-' + hand).resize();
                        $log.debug(hand);
                    }, 1000);
                }
                $scope.myplan = $rootScope.Outlets;
                // if ($rootScope.day == 'today') {
                //     $scope.myplan = $rootScope.TodaysPlan;
                // } else if ($rootScope.day == 'tomorrow') {
                //     $scope.myplan = $rootScope.TomorrowPlan;
                // } else {
                //     $scope.myplan = $rootScope.DayAfterPlan;
                // }
                var currentAccountLocal = FETCH_DATA.querySoup('Account', 'Id', $stateParams.AccountId);
                if (currentAccountLocal != undefined && currentAccountLocal.currentPageOrderedEntries != undefined && currentAccountLocal.currentPageOrderedEntries.length > 0) {
                    $scope.currentOutlet = currentAccountLocal.currentPageOrderedEntries[0];
                    $scope.currentCustomer = currentAccountLocal.currentPageOrderedEntries[0];;
                }

                MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
                var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
                $scope.breadActive = 'Cooler';
                $scope.order = $stateParams.order;
                $scope.pagesList = [];
                $scope.pagesList = $rootScope.slidesList[$stateParams.order];
                var pageId = $rootScope.processFlowLineItems[$stateParams.order].Page__c;
                $scope.breadActive = $rootScope.processFlowLineItems[$stateParams.order].Name;
                var currentPage = {};
                angular.forEach($rootScope.pages, function(record, key) {
                    if (record.pageDescription.Id == pageId) {
                        currentPage = record;
                    }
                });
                $scope.addNewButton = currentPage.pageDescription.Add_New__c;
                $scope.addNewLabel = currentPage.pageDescription.Add_New_Label__c;

                if ($rootScope.slidesList[$stateParams.order] == undefined && currentPage.pageDescription.Title__c.indexOf('Complaint') != -1 || currentPage.pageDescription.Title__c.indexOf('complaint') != -1) {
                    var lastVisit = FETCH_DATA_LOCAL('Visit__c', sfSmartstore.buildSmartQuerySpec("SELECT {Visit__c:_soup} FROM {Visit__c} WHERE {Visit__c:Account__c}='" + $stateParams.AccountId + "' ORDER BY {Visit__c:Check_In_DateTime__c} DESC", 20));
                    var rtList = FETCH_DATA_LOCAL('DB_RecordTypes', sfSmartstore.buildSmartQuerySpec("SELECT  {DB_RecordTypes:_soup} FROM {DB_RecordTypes} WHERE {DB_RecordTypes:SobjectType}='Visit_Summary__c' AND {DB_RecordTypes:Name}='Feedback and Complaints'", 50));
                    $scope.openComplaints = [];
                    var query = '';
                    if (lastVisit.currentPageOrderedEntries != undefined && lastVisit.currentPageOrderedEntries.length > 0 && lastVisit.currentPageOrderedEntries[0][0] != undefined) {

                        angular.forEach(lastVisit.currentPageOrderedEntries, function(record, key) {
                            var visitId = '';
                            var extId = record[0].External_Id__c;
                            if (record[0].Id != undefined) {
                                visitId = record[0].Id;
                            }
                            if (query == '') {
                                if (visitId != '') {
                                    query = "{Visit_Summary__c:Visit__c}='" + visitId + "' OR {Visit_Summary__c:Visit__c}='" + extId + "'";
                                } else {
                                    query = "{Visit_Summary__c:Visit__c}='" + extId + "'";
                                }

                            } else {
                                if (visitId != '') {
                                    query = query + " OR {Visit_Summary__c:Visit__c}='" + visitId + "' OR {Visit_Summary__c:Visit__c}='" + extId + "'";
                                } else {
                                    query = query + " OR {Visit_Summary__c:Visit__c}='" + extId + "'";
                                }

                            }
                        });
                        if (query == '') {
                            query = "{Visit_Summary__c:Visit__c}='" + $stateParams.visitId + "'";
                        } else {
                            query = query + " OR {Visit_Summary__c:Visit__c}='" + $stateParams.visitId + "'";
                        }
                        // var openComplaints = FETCH_DATA_LOCAL('Visit_Summary__c', sfSmartstore.buildSmartQuerySpec("SELECT {Visit_Summary__c:_soup} FROM {Visit_Summary__c} WHERE {Visit_Summary__c:Status__c}='Logged' AND {Visit_Summary__c:RecordTypeId}='" + rtList.currentPageOrderedEntries[0][0].Id + "' AND (" + query + ")", 50));
                        var openComplaints = FETCH_DATA_LOCAL('Visit_Summary__c', sfSmartstore.buildSmartQuerySpec("SELECT {Visit_Summary__c:_soup} FROM {Visit_Summary__c} WHERE ({Visit_Summary__c:Status__c}='Logged' or {Visit_Summary__c:Status__c}='logged') AND {Visit_Summary__c:RecordTypeId}='" + rtList.currentPageOrderedEntries[0][0].Id + "' and ({Visit_Summary__c:AccountId__c}='"+$stateParams.AccountId+"' or (" + query + ")) ", 50));
                        // $log.debug('openComplaints' + JSON.stringify(openComplaints));
                        angular.forEach(openComplaints.currentPageOrderedEntries, function(record, key) {
                            //$scope.openComplaints.push(record[0]);
                            var page = {};
                            page.External_Id__c = record[0].External_Id__c;
                            page.fields = currentPage.sections[0].fields;
                            $log.debug('My complaints'+JSON.stringify(record[0]));
                            if(record[0].Status__c=='logged')
                                record[0].Status__c='Logged';
                            if(record[0].Source__c!=undefined && record[0].Source__c.toLowerCase()=='trade complaint')
                                record[0].Source__c='Trade Complaint';
                            if(record[0].Source__c!=undefined && record[0].Source__c.toLowerCase()=='consumer complaint')
                                record[0].Source__c='Consumer Complaint';
                            var uploadfield = {
                                Response_Type__c: record[0].Response_Type__c,
                                Source__c: record[0].Source__c,
                                Category__c: record[0].Category__c,
                                Status__c: record[0].Status__c,
                                Notify_Supervisor__c: record[0].Notify_Supervisor__c,
                                Brand__c: record[0].Brand__c,
                                Description__c: record[0].Description__c,
                                Visit__c: record[0].Visit__c,
                                RecordTypeId: record[0].RecordTypeId,
                                External_Id__c: record[0].External_Id__c,
                                AccountId__c :  $stateParams.AccountId
                            };
                            $rootScope.slideFields[record[0].External_Id__c] = uploadfield;
                            if ($rootScope.slidesList[$stateParams.order] == undefined) {
                                $rootScope.slidesList[$stateParams.order] = [];
                                $scope.pagesList = [];
                            }
                            $scope.pagesList.push(page);
                            $rootScope.slidesList[$stateParams.order].push(page);
                        });
                    }
                }
                $ionicLoading.hide();
                $scope.addNewPage = function() {
                    $ionicLoading.show({
                        template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
                    });
                    var firstPage = {};
                    firstPage.External_Id__c = $stateParams.visitId + $window.Math.random() * 10000000;
                    firstPage.fields = currentPage.sections[0].fields;
                    if ($rootScope.slidesList[$stateParams.order] == undefined) {
                        $rootScope.slidesList[$stateParams.order] = [];
                        $scope.pagesList = [];
                    }
                    //  alert($stateParams.order+'before slidesList'+$rootScope.slidesList[$stateParams.order].length+'before '+$scope.pagesList.length);
                    $rootScope.slidesList[$stateParams.order].push(firstPage);
                    $scope.pagesList = $rootScope.slidesList[$stateParams.order];
                    if($scope.breadActive=='Complaints'){
                        $rootScope.slideFields[firstPage.External_Id__c] = {};
                        $rootScope.slideFields[firstPage.External_Id__c]['Status__c'] = 'Logged';
                    }
                    // alert($stateParams.order+'after slidesList'+$rootScope.slidesList[$stateParams.order].length+'after '+$scope.pagesList.length);
                    $ionicSlideBoxDelegate.update();
                    $timeout(function() {
                        $ionicSlideBoxDelegate.slide($ionicSlideBoxDelegate.slidesCount() - 1, 500);
                        $ionicLoading.hide();
                    }, 500);

                }
                if ($scope.pagesList != undefined && $scope.pagesList.length > 0 && $rootScope.slideFields != undefined && $rootScope.slideFields.length == 0) {

                    var visitRecord = FETCH_DATA.querySoup('Visit__c', 'External_Id__c', $stateParams.visitId);
                    var query = "{Visit_Summary__c:Visit__c} = '" + $stateParams.visitId + "'";
                    if (visitRecord.currentPageOrderedEntries != undefined && visitRecord.currentPageOrderedEntries[0] != undefined && visitRecord.currentPageOrderedEntries[0].Id != undefined) {
                        query = query + " OR {Visit_Summary__c:Visit__c} = '" + visitRecord.currentPageOrderedEntries[0].Id + "'";
                    }
                    var visitSummaryrecords = FETCH_DATA_LOCAL('Visit_Summary__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Visit_Summary__c:_soup} FROM {Visit_Summary__c} WHERE " + query, 500));
                    if (visitSummaryrecords.currentPageOrderedEntries != undefined && visitSummaryrecords.currentPageOrderedEntries.length > 0) {
                        angular.forEach(visitSummaryrecords.currentPageOrderedEntries, function(record, key) {
                            if (record[0].External_Id__c != undefined) {
                                $rootScope.slideFields[record[0].External_Id__c] = record[0];
                            }
                        });
                    }

                }

                $scope.saveAndNext = function() {
                    $timeout(function(){
                    var todayDate = $filter('date')(new Date(), 'dd/MM/yyyy');
                    if ($rootScope.slidesList[$stateParams.order] != undefined && $rootScope.slidesList[$stateParams.order].length > 0) {
                        var visitSummaryRecords = [];
                        angular.forEach($rootScope.slidesList[$stateParams.order], function(record, key) {
                            var visitSummary = {};
                            $log.debug('bbb'+$rootScope.slideFields[record.External_Id__c]);
                            visitSummary = $rootScope.slideFields[record.External_Id__c];
                            if (visitSummary != undefined) {
                                if (visitSummary.Source__c != undefined && visitSummary.Source__c.indexOf('Feedback') != -1)
                                    visitSummary.Response_Type__c = 'Feedback';
                                else
                                    visitSummary.Response_Type__c = 'Complaint';
                                if (visitSummary.Status__c != undefined && visitSummary.Status__c == 'Resolved')
                                    visitSummary.Resolution_Date__c = todayDate;
                                $log.debug('visit feedback and complaints' + JSON.stringify(visitSummary));
                                visitSummary['RecordTypeId'] = currentPage.sections[0].sectionDescription.Record_Type_Id__c;
                                if (visitSummary.External_Id__c == undefined) {
                                    visitSummary['External_Id__c'] = $stateParams.visitId + $window.Math.random() * 10000000;
                                    $rootScope.slideFields[record.External_Id__c].External_Id__c = visitSummary['External_Id__c'];
                                }
                                if (visitSummary.Visit__c == undefined) {
                                    visitSummary['Visit__c'] = $stateParams.visitId;
                                }
                                visitSummary['IsDirty'] = true;
                                visitSummaryRecords.push(visitSummary);
                            }

                        });
                        $log.debug('visitSummaryRecords' + JSON.stringify(visitSummaryRecords));
                        if (visitSummaryRecords.length > 0) {
                            MOBILEDATABASE_ADD(currentPage.pageDescription.Master_Object__c, visitSummaryRecords, 'External_Id__c');
                        }



                    } else {

                    }
                    var pageOrder = parseInt($stateParams.order) + 1;
                    var transaction = [{
                        Account: $stateParams.AccountId,
                        stage: $stateParams.order,
                        stageValue: 'pridecall2',
                        Visit: $stateParams.visitId,
                        status: 'saved',
                        entryDate: today
                    }];
                    MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
                    $ionicLoading.hide();
                    if ($rootScope.processFlowLineItems[pageOrder] != undefined) {
                        var nextPageId = $rootScope.processFlowLineItems[pageOrder].Page__c;
                        var nextPage;
                        angular.forEach($rootScope.pages, function(record, key) {
                            if (record.pageDescription.Id == nextPageId) {
                                nextPage = record;
                            }
                        });

                        if (nextPage.pageDescription.Template_Name__c == 'Template1') {
                            $location.path('app/stocks/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + pageOrder + '/' + new Date());
                        } else if (nextPage.pageDescription.Template_Name__c == 'Template2') {
                            $location.path('app/container2/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + pageOrder + '/' + new Date());
                        } else {
                            $location.path('app/container3/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + pageOrder + '/' + new Date());
                        }
                    } else {
                        $location.path('app/checkout/' + $stateParams.AccountId + '/' + $stateParams.visitId);
                    }
                },100);
                }
                $scope.prevSlide = function() {
                    $ionicSlideBoxDelegate.previous();
                }
                $scope.nextSlide = function() {
                    $ionicSlideBoxDelegate.next();
                }
                if ($rootScope.resentlyAdded2 == true) {
                    $timeout(function() {
                        $ionicSlideBoxDelegate.slide($ionicSlideBoxDelegate.slidesCount() - 1, 500);
                        $rootScope.resentlyAdded2 = false;
                    }, 1500)
                }

            } catch (err) {
                $ionicLoading.hide();
                WEBSERVICE_ERROR('PRIDECallCtrl2Error', err.name + '::' + err.message);
            }
        })
        .controller('SalesTrackingCtrl', function($scope, $stateParams, $rootScope, $window, $state, $location, $q, $ionicModal, $timeout, $interval, $ionicLoading, $log, $filter, SalesforceSession, FETCH_DATA_LOCAL, SOUP_EXISTS, FETCH_DAILY_PLAN_ACCOUNTS, MOBILEDATABASE_ADD, FETCH_DATA, WEBSERVICE_ERROR) {
            try {
                $scope.salestracking = {};
                $rootScope.backText = 'Back';
                $rootScope.showBack = true;
                $rootScope.showBack = true;
                $rootScope.params = $stateParams;
                $rootScope.name = "SalesTrackingCtrl";
                $scope.saveSales = function() {
                    // $log.debug('salescapture' + JSON.stringify($scope.salestracking));
                    // $log.debug('$stateParams.refresh' + JSON.stringify($stateParams.uploadId));
                    $rootScope.salestrackingList[$stateParams.uploadId] = $scope.salestracking;
                    $window.history.back();
                }
            } catch (err) {
                $ionicLoading.hide();
                WEBSERVICE_ERROR('SalesTrackingCtrlError', err.name + '::' + err.message);
            }
        })
        .controller('CheckoutCtrl', function($scope, $stateParams, $ionicPopup, $rootScope, $window, $state, $location, $q, $ionicModal, $timeout, $interval, $ionicLoading, $log, $filter, SalesforceSession, FETCH_DATA_LOCAL, SOUP_EXISTS, FETCH_DAILY_PLAN_ACCOUNTS, MOBILEDATABASE_ADD, FETCH_DATA, WEBSERVICE_ERROR,GEO_LOCATION) {
            try {
                // $scope.visitrecord = {};
                $ionicLoading.show({
                    template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
                });
                if ($rootScope.visitrecord.Visit_Summary__c == undefined) {
                    $rootScope.visitrecord = {};
                }
                $rootScope.params = $stateParams;
                $rootScope.name = "CheckoutCtrl";
                $rootScope.backText = 'Back';
                $rootScope.showBack = true;
                $scope.currentOutlet = {};
                $scope.currentCustomer = {};
                var today = $filter('date')(new Date(), 'dd/MM/yyyy');
                var transaction = [{
                    Account: $stateParams.AccountId,
                    stage: 'checkout',
                    stageValue: 'checkout',
                    Visit: $stateParams.visitId,
                    status: 'pending',
                    entryDate: today
                }];

                MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
                var currentAccountLocal = FETCH_DATA.querySoup('Account', 'Id', $stateParams.AccountId);
                if (currentAccountLocal != undefined && currentAccountLocal.currentPageOrderedEntries != undefined && currentAccountLocal.currentPageOrderedEntries.length > 0) {
                    $scope.currentOutlet = currentAccountLocal.currentPageOrderedEntries[0];
                    $scope.currentCustomer = currentAccountLocal.currentPageOrderedEntries[0];;
                }
                var d = new Date();
                var today = d.getTime().toString();
                var todayRoutePlan = FETCH_DAILY_PLAN_ACCOUNTS('today');
                angular.forEach(todayRoutePlan, function(acc, accKey) {
                    if(acc.Id==$scope.currentCustomer.Id)
                      $scope.currentCustomer.RoutePlanId=acc.RoutePlanId;
                });
                $ionicLoading.hide();
                $scope.noCheckoutPopup = function() {
                    confirmcheckoutPopup.close();
                }
                $scope.yesCheckoutPopup = function() {
                    $scope.checkout();
                    confirmcheckoutPopup.close();
                }
                $scope.confirmCheckout = function() {
                    confirmcheckoutPopup = $ionicPopup.confirm({
                        template: 'Once ended, a call cannot be modified. Do you want to continue?<br/><br/><br/><div style="text-align:right; color:#38B6CB;margin-right: 18px; text-align:right; color:#38B6CB;margin-right: 32px; font-size: 18px;font-weight: bold;"><span ng-click="noCheckoutPopup()" style="padding-right: 35px;">NO</span> &nbsp;<span ng-click="yesCheckoutPopup()">YES</span> </div>',
                        cssClass: 'checkoutConfirm',
                        title: '',
                        scope: $scope,
                        buttons: [

                        ]
                    });
                    confirmcheckoutPopup.then(function(res) {

                    });
                }

                $scope.checkout = function() {
                    $ionicLoading.show({
                        template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
                    });
                    $scope.currentLocation = {};
                    GEO_LOCATION.getCurrentPosition().then(function(position){
                        $scope.currentLocation.latitude = position.latitude;
                        $scope.currentLocation.longitude = position.longitude;
                        var today = $filter('date')(new Date(), 'dd/MM/yyyy hh:mm a');
                        var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
                        var lastVisit = FETCH_DATA_LOCAL('Visit__c', sfSmartstore.buildSmartQuerySpec("SELECT {Visit__c:_soup} FROM {Visit__c} WHERE {Visit__c:External_Id__c}='" + $stateParams.visitId + "' OR {Visit__c:Id}='" + $stateParams.visitId + "'", 1));
                        $scope.Visit = [{
                            Account__c: $stateParams.AccountId,
                            Check_In_Location__Latitude__s: lastVisit.currentPageOrderedEntries[0][0].Check_In_Location__Latitude__s,
                            Check_In_Location__Longitude__s: lastVisit.currentPageOrderedEntries[0][0].Check_In_Location__Longitude__s,
                            Check_In_DateTime__c: lastVisit.currentPageOrderedEntries[0][0].Check_In_DateTime__c,
                            Route_Plan__c: $scope.currentCustomer.RoutePlanId,
                            Check_Out_DateTime__c: today,
                            Check_Out_Location__Latitude__s: $scope.currentLocation.latitude,
                            Check_Out_Location__Longitude__s: $scope.currentLocation.longitude,
                            External_Id__c: $stateParams.visitId,
                            Visit_Summary__c: $rootScope.visitrecord.Visit_Summary__c,
                            IsDirty: true
                        }];
                        //$log.debug('checkout visitrecord' + JSON.stringify($scope.Visit))
                        MOBILEDATABASE_ADD('Visit__c', $scope.Visit, 'External_Id__c');
                        today = $filter('date')(new Date(), 'dd/MM/yyyy');
                        var transaction = [{
                            Account: $stateParams.AccountId,
                            stage: 'checkout',
                            Visit: $stateParams.visitId,
                            status: 'saved',
                            entryDate: today
                        }];
                        MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
                        $rootScope.resentlyAdded = false;
                        $rootScope.resentlyAdded2 = false;
                        $rootScope.pendingTask = false;
                        $ionicLoading.hide();
                        $rootScope.goToHome();
                        //$location.path('app/today/');
                        $log.info('LOCATION' + JSON.stringify(position));

                    },function(error){
                        $log.error('ERROR' + JSON.stringify(error));
                    });
                }
            } catch (err) {
                $ionicLoading.hide();
                WEBSERVICE_ERROR('CheckoutCtrlError', err.name + '::' + err.message);
            }
        })
        .controller('StockCtrl', function($scope, $stateParams, $ionicScrollDelegate, $rootScope, $window, $state, $location, $q, $ionicModal, $timeout, $interval, $ionicLoading, $log, $filter, SalesforceSession, FETCH_DATA_LOCAL, SOUP_EXISTS, FETCH_DAILY_PLAN_ACCOUNTS, MOBILEDATABASE_ADD, FETCH_DATA, WEBSERVICE_ERROR) {
            try {
                $ionicLoading.show({
                    template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
                });
                $rootScope.backText = 'Back';
                var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
                $rootScope.params = $stateParams;
                $rootScope.name = "StockCtrl";
                var oldvalues = [];
                angular.forEach($rootScope.StockAndSalesList, function(record, key) {
                    oldvalues[record.Product__c] = record;
                });
                if (oldvalues != undefined && oldvalues.length == 0) {
                    var visitRecord = FETCH_DATA.querySoup('Visit__c', 'External_Id__c', $stateParams.visitId);
                    var query = "{Visit_Summary__c:Visit__c} = '" + $stateParams.visitId + "'";
                    if (visitRecord.currentPageOrderedEntries != undefined && visitRecord.currentPageOrderedEntries[0] != undefined && visitRecord.currentPageOrderedEntries[0].Id != undefined) {
                        query = query + " OR {Visit_Summary__c:Visit__c} = '" + visitRecord.currentPageOrderedEntries[0].Id + "'";
                    }
                    var visitSummaryrecords = FETCH_DATA_LOCAL('Visit_Summary__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Visit_Summary__c:_soup} FROM {Visit_Summary__c} WHERE " + query, 500));
                    if (visitSummaryrecords.currentPageOrderedEntries != undefined && visitSummaryrecords.currentPageOrderedEntries.length > 0) {
                        angular.forEach(visitSummaryrecords.currentPageOrderedEntries, function(record, key) {
                            if (record[0].Product__c != undefined) {
                                oldvalues[record[0].Product__c] = record[0];
                            }
                        });
                    }
                }

                var today = $filter('date')(new Date(), 'dd/MM/yyyy');
                var transaction = [{
                    Account: $stateParams.AccountId,
                    stage: $stateParams.order,
                    stageValue: 'stocks',
                    Visit: $stateParams.visitId,
                    status: 'pending',
                    entryDate: today
                }];
                MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
                if($rootScope.StockAndSalesList.length<1)
                    $rootScope.StockAndSalesList = [];
                $scope.auditTypeValues = [];
                $scope.showAuditType = false;
                if ($rootScope.processFlow.Retail__c == true) {
                    $scope.showAuditType = true;
                    $scope.auditTypeValues.push('Retail Audit');
                }
                if ($rootScope.processFlow.Chatai__c == true) {
                    $scope.showAuditType = true;
                    $scope.auditTypeValues.push('Chatai');
                }

                var currentAccountLocal = FETCH_DATA.querySoup('Account', 'Id', $stateParams.AccountId);
                if (currentAccountLocal != undefined && currentAccountLocal.currentPageOrderedEntries != undefined && currentAccountLocal.currentPageOrderedEntries.length > 0) {
                    $scope.currentOutlet = currentAccountLocal.currentPageOrderedEntries[0];
                    var SKURecordTypes = FETCH_DATA.querySoup('DB_RecordTypes', 'SobjectType', 'Brands_and_Products__c');
                    var SKURecordTypeIds = [];
                    angular.forEach(SKURecordTypes.currentPageOrderedEntries, function(record, key) {
                        SKURecordTypeIds[record.Name] = record.Id;
                    });
                    var visitSummaryRtListLocal = FETCH_DATA.querySoup('DB_RecordTypes', 'SobjectType', 'Visit_Summary__c');
                    var visitSummaryRtList = [];
                    angular.forEach(visitSummaryRtListLocal.currentPageOrderedEntries, function(record, key) {
                        visitSummaryRtList[record.Name] = record.Id;
                    });
                    var outletProducts = FETCH_DATA_LOCAL('Product_Assignment__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Product_Assignment__c:_soup} FROM {Product_Assignment__c} WHERE  {Product_Assignment__c:Outlet_Id__c}='" + $stateParams.AccountId + "' AND {Product_Assignment__c:Added__c}='true' order by {Product_Assignment__c:Order__c} ", 500));
                    if (outletProducts == undefined || outletProducts.currentPageOrderedEntries == undefined || outletProducts.currentPageOrderedEntries.length == 0 || outletProducts.currentPageOrderedEntries[0][0] == undefined) {
                        var stateRecord = FETCH_DATA_LOCAL('Brands_and_Products__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Brands_and_Products__c:_soup} FROM {Brands_and_Products__c} WHERE {Brands_and_Products__c:RecordTypeId}='" + SKURecordTypeIds['State'] + "' ", 1));
                        if (stateRecord != undefined && stateRecord.currentPageOrderedEntries != undefined && stateRecord.currentPageOrderedEntries.length > 0) {
                            var stateActiveProducts = FETCH_DATA_LOCAL('Brands_and_Products__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Brands_and_Products__c:_soup} FROM {Brands_and_Products__c} WHERE   {Brands_and_Products__c:Related_to_State__c}='" + stateRecord.currentPageOrderedEntries[0][0].Id + "' AND {Brands_and_Products__c:RecordTypeId}='" + SKURecordTypeIds['Product'] + "' and {Brands_and_Products__c:Competitor_Product__c}='false' and {Brands_and_Products__c:Used_in_Chatai__c}='false'", 500));
                            if (stateActiveProducts != undefined && stateActiveProducts.currentPageOrderedEntries != undefined && stateActiveProducts.currentPageOrderedEntries.length > 0) {
                                var productOrderList = [];
                                angular.forEach(stateActiveProducts.currentPageOrderedEntries, function(record, key) {
                                    productOrderList.push({
                                        Id: ($stateParams.AccountId + record[0]['Id']),
                                        Outlet_Id__c: $stateParams.AccountId,
                                        Product_Id__c: record[0]['Id'],
                                        Product_Name__c: record[0]['Sku_Code__c'],
                                        Order__c: key + '',
                                        Added__c: 'true'
                                    });
                                    $rootScope.StockAndSalesList[key + ''] = {
                                        ProductName: record[0]['Sku_Code__c'],
                                        Product__c: record[0]['Id'],
                                        Visit__c: $stateParams.visitId,
                                        External_Id__c: $window.Math.random() * 10000000,
                                        RecordTypeId: visitSummaryRtList['Stock and Sales'],
                                        IsDirty: true
                                    };
                                });
                                MOBILEDATABASE_ADD('Product_Assignment__c', productOrderList, 'Id');
                            }
                        }

                    } else {
                        angular.forEach(outletProducts.currentPageOrderedEntries, function(record, key) {
                            var stock = {
                                ProductName: record[0].Product_Name__c,
                                Product__c: record[0].Product_Id__c,
                                Visit__c: $stateParams.visitId,
                                RecordTypeId: visitSummaryRtList['Stock and Sales'],
                                IsDirty: true
                            };
                            var productOldvalues = oldvalues[record[0]['Product_Id__c']];
                            if (productOldvalues != undefined) {
                                stock.Total_Stock__c = productOldvalues.Total_Stock__c;
                                stock.Stock_Available__c = productOldvalues.Stock_Available__c;
                                stock.Chilled_Stock__c = productOldvalues.Chilled_Stock__c;
                                stock.Sales__c = productOldvalues.Sales__c;
                                stock.Stock_Ageing__c = productOldvalues.Stock_Ageing__c;
                                stock.External_Id__c = productOldvalues.External_Id__c;
                            } else {
                                stock.External_Id__c = $window.Math.random() * 10000000;
                            }
                            $rootScope.StockAndSalesList[record[0]['Order__c']] = stock;
                        });
                    }
                }
                $ionicLoading.hide();
                $scope.selectRow = function(kk) {
                        // $rootScope.StockAndSalesList
                        $log.debug(kk + JSON.stringify($rootScope.StockAndSalesList));
                        angular.forEach($rootScope.StockAndSalesList, function(record, key) {
                            if (key == kk) {
                                record.clicked = true;
                            } else {
                                record.clicked = false;
                            }
                        });

                    }
                    // var scrollPosition = 0;
                $scope.focusCheckForStock = function(index) {
                    // scrollPosition =scrollPosition+43;
                    // $scope.selectRow(index);
                    if (index % 8 == 0) {
                        $ionicScrollDelegate.scrollTo(0, index * 43, true);
                    }
                    for(var i=0;i<$rootScope.StockAndSalesList.length;i++){
                        document.getElementById('mySelect_'+(i)).style.opacity = '0';
                        document.getElementById('mySelect1_'+(i)).style.opacity = '0';
                    }
                    document.getElementById('mySelect_'+(index)).style.opacity = '1';            
                    document.getElementById('mySelect1_'+(index)).style.opacity = '1';  

                }
                $scope.changeStock = function(st, ind) {
                    $log.debug(st + ind);
                    if (st == false)
                        $rootScope.StockAndSalesList[ind].Total_Stock__c = 0;
                    else if ($rootScope.StockAndSalesList[ind].Total_Stock__c == '0')
                        $rootScope.StockAndSalesList[ind].Total_Stock__c = '';

                };
                $scope.checkedStock = function(st, ind) {
                    //$log.debug(st + ind);
                    if (st == true)
                        document.getElementById("myinput_" + ind).focus();

                };
                 $scope.selectRow_display = function(kk, event, index){
                    for(var i=0;i<$rootScope.StockAndSalesList.length;i++){
                        document.getElementById('mySelect_'+(i)).style.opacity = '0';
                        document.getElementById('mySelect1_'+(i)).style.opacity = '0';
                    }
                    document.getElementById('mySelect_'+(index)).style.opacity = '1';            
                    document.getElementById('mySelect1_'+(index)).style.opacity = '1';            
                }
                $scope.restrictKeyup = function(_event, _index) {
                    $log.debug(_event.keyCode);
                    
                    var max = 4;
                    var mytext = document.getElementById("myinput_" + (parseInt(_index))).value;
                    if (mytext.length >= max) {
                        mytext=mytext.substr(0, max);
                        document.getElementById("myinput_" + (parseInt(_index))).value = parseInt(mytext);
                        $rootScope.StockAndSalesList[_index].Total_Stock__c=parseInt(mytext);
                        $scope.changeTotalStock(parseInt(mytext),_index);
                    }
                    
                }
                $scope.restrictKeydown = function(_event, _index) {
                    $log.debug(_event.keyCode);
                    if (_event.which == 9) {
                        document.getElementById("myinput_" + (parseInt(_index) + 1)).focus();
                        _event.preventDefault();
                    }
                }
                $scope.changeTotalStock = function(st, ind) {
                    $log.debug(st + ind);
                    if (st > 0 && st != '')
                        $rootScope.StockAndSalesList[ind].Stock_Available__c = true;
                    else
                        $rootScope.StockAndSalesList[ind].Stock_Available__c = false;

                };
                $scope.gotoProductOrder = function() {
                    $location.path('app/productOrder/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $scope.ProductStage + '/' + $stateParams.order + '/' + new Date());
                }
                $scope.selectAll = false;
                $scope.isRunning=false;
                $scope.selectAllAsAvailable = function() {
                    if(!$scope.isRunning){
                        $scope.isRunning=true;
                        $log.debug('$scope.selectAll'+$scope.selectAll+'select');
                        $scope.selectAll = !$scope.selectAll;
                        angular.forEach($rootScope.StockAndSalesList, function(record, key) {
                            $log.debug('$scope.selectAll'+$scope.selectAll+'record.Total_Stock__c'+record.Total_Stock__c);
                            if ($scope.selectAll == false && (record.Total_Stock__c == undefined || record.Total_Stock__c == 0)) {
                                record.Stock_Available__c = false;
                            } else {
                                record.Stock_Available__c = true;
                            }
                            if ($rootScope.StockAndSalesList.length==key+1) {
                                $timeout(function(){
                                    $scope.isRunning=false;
                                },2000);
                            }
                        });
                    }
                }
                $scope.justSelectAllAsAvailable = function() {
                    if(!$scope.isRunning){
                        $scope.isRunning=true;
                        $log.debug('$scope.selectAll'+$scope.selectAll+'justSelect');
                        angular.forEach($rootScope.StockAndSalesList, function(record, key) {
                            $log.debug('$scope.selectAll'+$scope.selectAll+'record.Total_Stock__c'+record.Total_Stock__c);
                            if ($scope.selectAll == false && (record.Total_Stock__c == undefined || record.Total_Stock__c == 0)) {
                                record.Stock_Available__c = false;
                            } else {
                                record.Stock_Available__c = true;
                            }
                            if ($rootScope.StockAndSalesList.length==key+1) {
                                $timeout(function(){
                                    $scope.isRunning=false;
                                },2000);
                            }
                        });
                    }
                }
                $scope.SaveStocks = function() {
                    $timeout(function(){
                        var stcokRecords = [];
                        angular.forEach($rootScope.StockAndSalesList, function(record, key) {
                            if (record.Stock_Available__c != undefined || record.Total_Stock__c != undefined || record.Sales__c != undefined) {
                                var stock = record;
                                stcokRecords.push(stock);
                            }
                        });
                        if (stcokRecords.length > 0) {
                            MOBILEDATABASE_ADD('Visit_Summary__c', stcokRecords, 'External_Id__c');
                        }
                        var transaction = [{
                            Account: $stateParams.AccountId,
                            stage: $stateParams.order,
                            stageValue: 'stocks',
                            Visit: $stateParams.visitId,
                            status: 'saved',
                            entryDate: today
                        }];
                        MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
                        if ($rootScope.audit != undefined && $rootScope.audit.visitType != undefined) {
                            $ionicLoading.hide();
                            if ($rootScope.audit.visitType == 'Chatai') {

                                $location.path('app/chataiSales/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date());
                            } else if ($rootScope.audit.visitType == 'Retail Audit') {

                                $location.path('app/retailSales/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date());
                            }
                        } else {
                            var page = {};

                            for (var i = 0; i < $rootScope.pages.length; i++) {
                                if ($rootScope.pages[i].pageDescription.Id == $rootScope.processFlowLineItems[parseInt($stateParams.order) + 1].Page__c) {
                                    page = $rootScope.pages[i].pageDescription;
                                    break;
                                }
                            }

                            $ionicLoading.hide();
                            var pageOrder = parseInt($stateParams.order) + 1;
                            if ($rootScope.processFlowLineItems[pageOrder] != undefined) {
                                if (page.Template_Name__c == 'Template1') {
                                    $location.path('app/stockDetails/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + pageOrder + '/' + new Date());
                                } else if (page.Template_Name__c == 'Template2') {
                                    $location.path('app/container2/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + pageOrder + '/' + new Date());
                                } else {
                                    $location.path('app/container3/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + pageOrder + '/' + new Date());
                                }
                            } else {
                                $ionicLoading.hide();
                                $location.path('app/checkout/' + $stateParams.AccountId + '/' + $stateParams.visitId);
                            }
                        }
                    },100);
                }
            } catch (err) {
                $ionicLoading.hide();
                WEBSERVICE_ERROR('StockCtrlError', err.name + '::' + err.message);
            }
        })
        .controller('StockDetailsCtrl', function($scope, $stateParams, $rootScope, $filter, $window, $location, $ionicLoading, $log, $timeout, FETCH_DATA_LOCAL, MOBILEDATABASE_ADD, FETCH_DATA, WEBSERVICE_ERROR) {
            try {
                $ionicLoading.show({
                    template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
                });
                $rootScope.backText = 'Back';
                $rootScope.params = $stateParams;
                $rootScope.name = "StockDetailsCtrl";
                var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
                var today = $filter('date')(new Date(), 'dd/MM/yyyy');
                var transaction = [{
                    Account: $stateParams.AccountId,
                    stage: $stateParams.order,
                    stageValue: 'stockDetails',
                    Visit: $stateParams.visitId,
                    status: 'pending',
                    entryDate: today
                }];
                MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');

                var monthList = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                var query = '';

                var d = new Date();
                $scope.currentMonth = $scope.MonthsList[d.getMonth()];
                var lastMonth = d.getMonth() - 1;
                var currentYear = d.getFullYear();

                $scope.stocDetailsPickValues = [];

                var d = new Date();
                var monthVal = d.getMonth();
                var yearVal = d.getFullYear();
                $scope.stocDetailsPickValues.push(monthList[monthVal] + ' - ' + yearVal.toString().substr(2, 3));
                for (var i = 0; i < 5; i++) {
                    monthVal = monthVal - 1;
                    if (monthVal < 0) {
                        monthVal = 11;
                        yearVal = currentYear - 1;
                    }
                    $scope.stocDetailsPickValues.push(monthList[monthVal] + ' - ' + yearVal.toString().substr(2, 3));
                }
                $scope.stocDetailsPickValues.push('Older');


                var oldvalues = [];
                angular.forEach($rootScope.StockAndSalesList, function(record, key) {
                    oldvalues[record.Product__c] = record;
                });
                if (oldvalues != undefined && oldvalues.length == 0) {
                    var visitRecord = FETCH_DATA.querySoup('Visit__c', 'External_Id__c', $stateParams.visitId);
                    var query = "{Visit_Summary__c:Visit__c} = '" + $stateParams.visitId + "'";
                    if (visitRecord.currentPageOrderedEntries != undefined && visitRecord.currentPageOrderedEntries[0] != undefined && visitRecord.currentPageOrderedEntries[0].Id != undefined) {
                        query = query + " OR {Visit_Summary__c:Visit__c} = '" + visitRecord.currentPageOrderedEntries[0].Id + "'";
                    }
                    var visitSummaryrecords = FETCH_DATA_LOCAL('Visit_Summary__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Visit_Summary__c:_soup} FROM {Visit_Summary__c} WHERE " + query, 500));
                    if (visitSummaryrecords.currentPageOrderedEntries != undefined && visitSummaryrecords.currentPageOrderedEntries.length > 0) {
                        angular.forEach(visitSummaryrecords.currentPageOrderedEntries, function(record, key) {
                            if (record[0].Product__c != undefined) {
                                oldvalues[record[0].Product__c] = record[0];
                            }
                        });
                    }
                }
                if($rootScope.StockAndSalesList.length<1)
                    $rootScope.StockAndSalesList = [];
                $scope.auditTypeValues = [];
                $scope.showAuditType = false;
                if ($rootScope.processFlow.Retail__c == true) {
                    $scope.showAuditType = true;
                    $scope.auditTypeValues.push('Retail Audit');
                }
                if ($rootScope.processFlow.Chatai__c == true) {
                    $scope.showAuditType = true;
                    $scope.auditTypeValues.push('Chatai');
                }

                var currentAccountLocal = FETCH_DATA.querySoup('Account', 'Id', $stateParams.AccountId);
                if (currentAccountLocal != undefined && currentAccountLocal.currentPageOrderedEntries != undefined && currentAccountLocal.currentPageOrderedEntries.length > 0) {
                    $scope.currentOutlet = currentAccountLocal.currentPageOrderedEntries[0];
                    var SKURecordTypes = FETCH_DATA.querySoup('DB_RecordTypes', 'SobjectType', 'Brands_and_Products__c');
                    var SKURecordTypeIds = [];
                    angular.forEach(SKURecordTypes.currentPageOrderedEntries, function(record, key) {
                        SKURecordTypeIds[record.Name] = record.Id;
                    });
                    var visitSummaryRtListLocal = FETCH_DATA.querySoup('DB_RecordTypes', 'SobjectType', 'Visit_Summary__c');
                    var visitSummaryRtList = [];
                    angular.forEach(visitSummaryRtListLocal.currentPageOrderedEntries, function(record, key) {
                        visitSummaryRtList[record.Name] = record.Id;
                    });
                    var outletProducts = FETCH_DATA_LOCAL('Product_Assignment__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Product_Assignment__c:_soup} FROM {Product_Assignment__c} WHERE  {Product_Assignment__c:Outlet_Id__c}='" + $stateParams.AccountId + "' AND {Product_Assignment__c:Added__c}='true' order by {Product_Assignment__c:Order__c} ", 500));
                    if (outletProducts == undefined || outletProducts.currentPageOrderedEntries == undefined || outletProducts.currentPageOrderedEntries.length == 0 || outletProducts.currentPageOrderedEntries[0][0] == undefined) {
                        var stateRecord = FETCH_DATA_LOCAL('Brands_and_Products__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Brands_and_Products__c:_soup} FROM {Brands_and_Products__c} WHERE {Brands_and_Products__c:RecordTypeId}='" + SKURecordTypeIds['State'] + "' ", 1));
                        if (stateRecord != undefined && stateRecord.currentPageOrderedEntries != undefined && stateRecord.currentPageOrderedEntries.length > 0) {
                            var stateActiveProducts = FETCH_DATA_LOCAL('Brands_and_Products__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Brands_and_Products__c:_soup} FROM {Brands_and_Products__c} WHERE   {Brands_and_Products__c:Related_to_State__c}='" + stateRecord.currentPageOrderedEntries[0][0].Id + "' AND {Brands_and_Products__c:RecordTypeId}='" + SKURecordTypeIds['Product'] + "' and {Brands_and_Products__c:Competitor_Product__c}='false' and {Brands_and_Products__c:Used_in_Chatai__c}='false'", 500));
                            if (stateActiveProducts != undefined && stateActiveProducts.currentPageOrderedEntries != undefined && stateActiveProducts.currentPageOrderedEntries.length > 0) {
                                var productOrderList = [];
                                angular.forEach(stateActiveProducts.currentPageOrderedEntries, function(record, key) {
                                    productOrderList.push({
                                        Id: ($stateParams.AccountId + record[0]['Id']),
                                        Outlet_Id__c: $stateParams.AccountId,
                                        Product_Id__c: record[0]['Id'],
                                        Product_Name__c: record[0]['SKU_Name__c'],
                                        Order__c: key + '',
                                        Added__c: 'true'
                                    });
                                    $rootScope.StockAndSalesList[key + ''] = {
                                        ProductName: record[0].SKU_Name__c,
                                        Product__c: record[0].Id,
                                        Visit__c: $stateParams.visitId,
                                        External_Id__c: $window.Math.random() * 10000000,
                                        RecordTypeId: visitSummaryRtList['Stock and Sales'],
                                        IsDirty: true
                                    };
                                });
                                MOBILEDATABASE_ADD('Product_Assignment__c', productOrderList, 'Id');
                            }
                        }

                    } else {
                        angular.forEach(outletProducts.currentPageOrderedEntries, function(record, key) {
                            var stock = {
                                ProductName: record[0].Product_Name__c,
                                Product__c: record[0].Product_Id__c,
                                Visit__c: $stateParams.visitId,
                                RecordTypeId: visitSummaryRtList['Stock and Sales'],
                                IsDirty: true
                            };
                            var productOldvalues = oldvalues[record[0]['Product_Id__c']];
                            if (productOldvalues != undefined) {
                                stock.Total_Stock__c = productOldvalues.Total_Stock__c;
                                stock.Sales__c = productOldvalues.Sales__c;
                                stock.Stock_Ageing__c = productOldvalues.Stock_Ageing__c;
                                stock.Stock_Available__c = productOldvalues.Stock_Available__c;
                                stock.Chilled_Stock__c = productOldvalues.Chilled_Stock__c;
                                stock.External_Id__c = productOldvalues.External_Id__c;
                            } else {
                                stock.External_Id__c = $window.Math.random() * 10000000;
                            }
                            $rootScope.StockAndSalesList[record[0]['Order__c']] = stock;
                        });
                    }
                }
                $ionicLoading.hide();
                $scope.selectRow = function(kk) {
                    $log.debug(kk + JSON.stringify($rootScope.StockAndSalesList));
                    angular.forEach($rootScope.StockAndSalesList, function(record, key) {
                        if (key == kk) {
                            record.clicked = true;
                        } else {
                            record.clicked = false;
                        }
                    });

                }

                $scope.selectRow_display = function(kk, event, index) {
                    for (var i = 0; i < $rootScope.StockAndSalesList.length; i++) {
                        document.getElementById('mySelect_' + (i)).style.opacity = '0';
                        document.getElementById('mySelect1_' + (i)).style.opacity = '0';
                    }
                    document.getElementById('mySelect_' + (index)).style.opacity = '1';
                    document.getElementById('mySelect1_' + (index)).style.opacity = '1';
                }

                $scope.SaveStocks = function() {
                    $timeout(function(){
                        var stcokRecords = [];
                        angular.forEach($rootScope.StockAndSalesList, function(record, key) {
                            if (record.Stock_Ageing__c != undefined || record.Chilled_Stock__c != undefined) {
                                var stock = record;
                                stcokRecords.push(stock);
                            }
                        });
                        if (stcokRecords.length > 0) {
                            MOBILEDATABASE_ADD('Visit_Summary__c', stcokRecords, 'External_Id__c');
                        }
                        var transaction = [{
                            Account: $stateParams.AccountId,
                            stage: $stateParams.order,
                            stageValue: 'stockDetails',
                            Visit: $stateParams.visitId,
                            status: 'saved',
                            entryDate: today
                        }];
                        MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
                        var pageOrder = parseInt($stateParams.order) + 1;
                        if ($rootScope.processFlowLineItems[pageOrder] != undefined) {
                            var page = {};
                            for (var i = 0; i < $rootScope.pages.length; i++) {
                                if ($rootScope.pages[i].pageDescription.Id == $rootScope.processFlowLineItems[pageOrder].Page__c) {
                                    page = $rootScope.pages[i].pageDescription;
                                    break;
                                }
                            }

                            $ionicLoading.hide();

                            if (page.Template_Name__c == 'Template1') {
                                // $location.path('app/stockDetails/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/'+pageOrder+'/' + new Date());
                            } else if (page.Template_Name__c == 'Template2') {
                                $location.path('app/container2/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + pageOrder + '/' + new Date());
                            } else {
                                $location.path('app/container3/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + pageOrder + '/' + new Date());
                            }
                        } else {
                            $ionicLoading.hide();
                            $location.path('app/checkout/' + $stateParams.AccountId + '/' + $stateParams.visitId);
                        }


                    
                    },100);
                }
            } catch (err) {
                $ionicLoading.hide();
                WEBSERVICE_ERROR('StockDetailsCtrlError', err.name + '::' + err.message);
            }
        })
        .controller('ChataiCtrl', function($scope, $stateParams, $rootScope, $filter, $window, $location, $ionicLoading, $log, FETCH_DATA_LOCAL, MOBILEDATABASE_ADD, FETCH_DATA, WEBSERVICE_ERROR) {
            try {
                $ionicLoading.show({
                    template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
                });
                $rootScope.backText = 'Back';
                $rootScope.params = $stateParams;
                $rootScope.name = "ChataiCtrl";
                if ($rootScope.ChataiMonth.Name == undefined) {
                    $rootScope.ChataiMonth.Name = new Date();
                }
                var today = $filter('date')(new Date(), 'dd/MM/yyyy');
                var transaction = [{
                    Account: $stateParams.AccountId,
                    stage: $stateParams.order,
                    stageValue: 'chatai',
                    Visit: $stateParams.visitId,
                    status: 'pending',
                    entryDate: today
                }];
                MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
                var oldvalues = [];
                angular.forEach($rootScope.chataiSales, function(record, key) {
                    oldvalues[record.Product__c] = record;
                });
                $rootScope.chataiSales = [];
                var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
                var currentAccountLocal = FETCH_DATA.querySoup('Account', 'Id', $stateParams.AccountId);
                if (currentAccountLocal != undefined && currentAccountLocal.currentPageOrderedEntries != undefined && currentAccountLocal.currentPageOrderedEntries.length > 0) {
                    $scope.currentOutlet = currentAccountLocal.currentPageOrderedEntries[0];
                    var SKURecordTypes = FETCH_DATA.querySoup('DB_RecordTypes', 'SobjectType', 'Brands_and_Products__c');
                    var SKURecordTypeIds = [];
                    angular.forEach(SKURecordTypes.currentPageOrderedEntries, function(record, key) {
                        SKURecordTypeIds[record.Name] = record.Id;
                    });
                    var visitSummaryRtListLocal = FETCH_DATA.querySoup('DB_RecordTypes', 'SobjectType', 'Visit_Summary__c');
                    var visitSummaryRtList = [];
                    angular.forEach(visitSummaryRtListLocal.currentPageOrderedEntries, function(record, key) {
                        visitSummaryRtList[record.Name] = record.Id;
                    });
                    var stateRecord = FETCH_DATA_LOCAL('Brands_and_Products__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Brands_and_Products__c:_soup} FROM {Brands_and_Products__c} WHERE {Brands_and_Products__c:RecordTypeId}='" + SKURecordTypeIds['State'] + "'", 1));
                    if (stateRecord != undefined && stateRecord.currentPageOrderedEntries != undefined && stateRecord.currentPageOrderedEntries.length > 0) {
                        var stateActiveProducts = FETCH_DATA_LOCAL('Brands_and_Products__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Brands_and_Products__c:_soup} FROM {Brands_and_Products__c} WHERE   {Brands_and_Products__c:Related_to_State__c}='" + stateRecord.currentPageOrderedEntries[0][0].Id + "' AND {Brands_and_Products__c:RecordTypeId}='" + SKURecordTypeIds['Product'] + "' and {Brands_and_Products__c:Used_in_Chatai__c}='true'", 500));
                        if (stateActiveProducts != undefined && stateActiveProducts.currentPageOrderedEntries != undefined && stateActiveProducts.currentPageOrderedEntries.length > 0) {
                            angular.forEach(stateActiveProducts.currentPageOrderedEntries, function(record, key) {
                                var sale = {
                                    ProductName: record[0].SKU_Name__c,
                                    Product__c: record[0].Id,
                                    Visit__c: $stateParams.visitId,
                                    External_Id__c: $window.Math.random() * 10000000,
                                    RecordTypeId: visitSummaryRtList['Stock and Sales'],
                                    IsDirty: true,
                                    Audit_Type__c: 'Chatai'
                                };
                                var productOldvalues = oldvalues[record[0].Id];
                                if (productOldvalues != undefined) {
                                    sale.Sales__c = productOldvalues.Sales__c;
                                    sale.External_Id__c = productOldvalues.External_Id__c;
                                } else {
                                    sale.External_Id__c = $window.Math.random() * 10000000;
                                }
                                $rootScope.chataiSales.push(sale);
                            });
                        }
                    }

                }
                $ionicLoading.hide();
                $scope.selectRow = function(kk) {
                    //  $log.debug(kk + JSON.stringify($rootScope.retailSalesList));
                    angular.forEach($rootScope.chataiSales, function(record, key) {
                        if (key == kk) {
                            record.clicked = true;
                        } else {
                            record.clicked = false;
                        }
                    });

                }
                $scope.SaveChataiSales = function() {
                    var saleList = [];
                    $log.debug('Month==' + $rootScope.ChataiMonth.Name.getMonth() + 'Year==' + $rootScope.ChataiMonth.Name.getFullYear())
                    angular.forEach($rootScope.chataiSales, function(record, key) {
                        if (record.Sales__c != undefined) {
                            record.Month__c = $rootScope.MonthsList[$rootScope.ChataiMonth.Name.getMonth()];
                            record.Year__c = $rootScope.ChataiMonth.Name.getFullYear().toString();
                            saleList.push(record);
                        }
                    });
                    $log.debug(JSON.stringify(saleList));
                    if (saleList.length > 0) {
                        MOBILEDATABASE_ADD('Visit_Summary__c', saleList, 'External_Id__c');
                    }
                    var transaction = [{
                        Account: $stateParams.AccountId,
                        stage: $stateParams.order,
                        stageValue: 'chatai',
                        Visit: $stateParams.visitId,
                        status: 'saved',
                        entryDate: today
                    }];
                    MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
                    $location.path('app/stockDetails/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date());
                }
            } catch (err) {
                $ionicLoading.hide();
                WEBSERVICE_ERROR('ChataiCtrlError', err.name + '::' + err.message);
            }
        })
        .controller('RetailCtrl', function($scope, $stateParams, $rootScope, $window, $filter, $location, $ionicLoading, $log, FETCH_DATA_LOCAL, MOBILEDATABASE_ADD, FETCH_DATA, WEBSERVICE_ERROR) {
            try {
                $ionicLoading.show({
                    template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
                });
                $rootScope.backText = 'Back';
                $rootScope.params = $stateParams;
                $rootScope.name = "RetailCtrl";
                if ($rootScope.Month.Name == undefined) {
                    $rootScope.Month.Name = new Date();
                }
                var oldvalues = [];
                angular.forEach($rootScope.retailSalesList, function(record, key) {
                    oldvalues[record.Product__c] = record;
                });
                $rootScope.retailSalesList = [];
                var today = $filter('date')(new Date(), 'dd/MM/yyyy');
                var transaction = [{
                    Account: $stateParams.AccountId,
                    stage: $stateParams.order,
                    stageValue: 'Retail',
                    Visit: $stateParams.visitId,
                    status: 'pending',
                    entryDate: today
                }];
                MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
                var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
                var currentAccountLocal = FETCH_DATA.querySoup('Account', 'Id', $stateParams.AccountId);
                if (currentAccountLocal != undefined && currentAccountLocal.currentPageOrderedEntries != undefined && currentAccountLocal.currentPageOrderedEntries.length > 0) {
                    $scope.currentOutlet = currentAccountLocal.currentPageOrderedEntries[0];
                    var SKURecordTypes = FETCH_DATA.querySoup('DB_RecordTypes', 'SobjectType', 'Brands_and_Products__c');
                    var SKURecordTypeIds = [];
                    angular.forEach(SKURecordTypes.currentPageOrderedEntries, function(record, key) {
                        SKURecordTypeIds[record.Name] = record.Id;
                    });
                    var visitSummaryRtListLocal = FETCH_DATA.querySoup('DB_RecordTypes', 'SobjectType', 'Visit_Summary__c');
                    var visitSummaryRtList = [];
                    angular.forEach(visitSummaryRtListLocal.currentPageOrderedEntries, function(record, key) {
                        visitSummaryRtList[record.Name] = record.Id;
                    });
                    var outletProducts = FETCH_DATA_LOCAL('Product_Assignment__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Product_Assignment__c:_soup} FROM {Product_Assignment__c} WHERE  {Product_Assignment__c:Outlet_Id__c}='" + $stateParams.AccountId + "' AND {Product_Assignment__c:Added__c}='true' order by {Product_Assignment__c:Order__c} ", 500));
                    if (outletProducts != undefined && outletProducts.currentPageOrderedEntries != undefined && outletProducts.currentPageOrderedEntries.length != 0 && outletProducts.currentPageOrderedEntries[0][0] != undefined) {
                        angular.forEach(outletProducts.currentPageOrderedEntries, function(record, key) {
                            var sale = {
                                ProductName: record[0].Product_Name__c,
                                Product__c: record[0].Product_Id__c,
                                Visit__c: $stateParams.visitId,
                                External_Id__c: $window.Math.random() * 10000000,
                                RecordTypeId: visitSummaryRtList['Stock and Sales'],
                                IsDirty: true,
                                Audit_Type__c: 'Retail Audit'
                            };
                            var productOldvalues = oldvalues[record[0]['Product_Id__c']];
                            if (productOldvalues != undefined) {
                                sale.Sales__c = productOldvalues.Sales__c;
                            }
                            $rootScope.retailSalesList[record[0]['Order__c']] = sale;
                        });
                    }
                    var stateRecord = FETCH_DATA_LOCAL('Brands_and_Products__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Brands_and_Products__c:_soup} FROM {Brands_and_Products__c} WHERE {Brands_and_Products__c:RecordTypeId}='" + SKURecordTypeIds['State'] + "'", 1));
                    if (stateRecord != undefined && stateRecord.currentPageOrderedEntries != undefined && stateRecord.currentPageOrderedEntries.length > 0) {
                        var stateActiveProducts = FETCH_DATA_LOCAL('Brands_and_Products__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Brands_and_Products__c:_soup} FROM {Brands_and_Products__c} WHERE   {Brands_and_Products__c:Related_to_State__c}='" + stateRecord.currentPageOrderedEntries[0][0].Id + "' AND {Brands_and_Products__c:RecordTypeId}='" + SKURecordTypeIds['Product'] + "' and {Brands_and_Products__c:Competitor_Product__c}='true' and {Brands_and_Products__c:Used_in_Chatai__c}='false'", 500));
                        if (stateActiveProducts != undefined && stateActiveProducts.currentPageOrderedEntries != undefined && stateActiveProducts.currentPageOrderedEntries.length > 0) {
                            var productSize = $rootScope.retailSalesList.length;
                            angular.forEach(stateActiveProducts.currentPageOrderedEntries, function(record, key) {
                                var sale = {
                                    ProductName: record[0].SKU_Name__c,
                                    Product__c: record[0].Id,
                                    Visit__c: $stateParams.visitId,
                                    External_Id__c: $window.Math.random() * 10000000,
                                    RecordTypeId: visitSummaryRtList['Stock and Sales'],
                                    IsDirty: true,
                                    Audit_Type__c: 'Retail Audit'
                                };
                                var productOldvalues = oldvalues[record[0].Id];
                                if (productOldvalues != undefined) {
                                    sale.Sales__c = productOldvalues.Sales__c;
                                    sale.External_Id__c = productOldvalues.External_Id__c;
                                } else {
                                    sale.External_Id__c = $window.Math.random() * 10000000;
                                }
                                $rootScope.retailSalesList[productSize++] = sale;
                            });
                        }


                    }
                }
                $ionicLoading.hide();
                $scope.selectRow = function(kk) {
                    //  $log.debug(kk + JSON.stringify($rootScope.retailSalesList));
                    angular.forEach($rootScope.retailSalesList, function(record, key) {
                        if (key == kk) {
                            record.clicked = true;
                        } else {
                            record.clicked = false;
                        }
                    });

                }
                $scope.SaveRetailSales = function() {
                    var saleList = [];
                    $log.debug('Month==' + $rootScope.Month.Name.getMonth() + 'Year==' + $rootScope.Month.Name.getFullYear())
                    angular.forEach($rootScope.retailSalesList, function(record, key) {
                        if (record.Sales__c != undefined) {
                            record.Month__c = $rootScope.MonthsList[$rootScope.Month.Name.getMonth()];
                            record.Year__c = $rootScope.Month.Name.getFullYear().toString();
                            saleList.push(record);
                        }
                    });
                    if (saleList.length > 0) {
                        MOBILEDATABASE_ADD('Visit_Summary__c', saleList, 'External_Id__c');
                    }
                    var transaction = [{
                        Account: $stateParams.AccountId,
                        stage: $stateParams.order,
                        stageValue: 'Retail',
                        Visit: $stateParams.visitId,
                        status: 'saved',
                        entryDate: today
                    }];
                    MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
                    $location.path('app/stockDetails/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date());
                }
            } catch (err) {
                $ionicLoading.hide();
                WEBSERVICE_ERROR('RetailCtrlError', err.name + '::' + err.message);
            }
        })
        .controller('productOrderCtrl', function($scope, $stateParams, $ionicSlideBoxDelegate, $cordovaCamera, $rootScope, $window, $state, $location, $q, $ionicModal, $timeout, $interval, $ionicLoading, $log, $filter, SalesforceSession, FETCH_DATA_LOCAL, SOUP_EXISTS, FETCH_DAILY_PLAN_ACCOUNTS, MOBILEDATABASE_ADD, FETCH_DATA, WEBSERVICE_ERROR) {
            try {
                var accountId = $stateParams.AccountId;
                var visitId = $stateParams.visitId;
                var stage = $stateParams.stage;
                $rootScope.backText = 'Back';
                $rootScope.params = $stateParams;
                $rootScope.name = "productOrderCtrl";
                var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
                if ($rootScope.Outlets.length == 0) {
                    $rootScope.Outlets = FETCH_DAILY_PLAN_ACCOUNTS($rootScope.day);
                }
                var currentAccountLocal = FETCH_DATA.querySoup('Account', 'Id', $stateParams.AccountId);
                if (currentAccountLocal != undefined && currentAccountLocal.currentPageOrderedEntries != undefined && currentAccountLocal.currentPageOrderedEntries.length > 0) {
                    $scope.currentOutlet = currentAccountLocal.currentPageOrderedEntries[0];
                    $scope.currentCustomer = currentAccountLocal.currentPageOrderedEntries[0];;
                }
                var rtList1 = FETCH_DATA_LOCAL('DB_RecordTypes', sfSmartstore.buildSmartQuerySpec("SELECT  {DB_RecordTypes:_soup} FROM {DB_RecordTypes} WHERE  {DB_RecordTypes:SobjectType}='Brands_and_Products__c'", 50));
                var ProductsRtList1 = [];
                angular.forEach(rtList1.currentPageOrderedEntries, function(record, key) {

                    ProductsRtList1[record[0].Name] = record[0].Id;


                });
                var stateRecord1 = FETCH_DATA_LOCAL('Brands_and_Products__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Brands_and_Products__c:_soup} FROM {Brands_and_Products__c} WHERE {Brands_and_Products__c:RecordTypeId}='" + ProductsRtList1['State'] + "'", 1));
                // $log.debug(JSON.stringify(stateRecord1));
                var stateActiveProducts1 = FETCH_DATA_LOCAL('Brands_and_Products__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Brands_and_Products__c:_soup} FROM {Brands_and_Products__c} WHERE  {Brands_and_Products__c:Related_to_State__c}='" + stateRecord1.currentPageOrderedEntries[0][0].Id + "' AND {Brands_and_Products__c:RecordTypeId}='" + ProductsRtList1['Product'] + "' and {Brands_and_Products__c:Competitor_Product__c}='false' and {Brands_and_Products__c:Used_in_Chatai__c}='false'", 500));
                var outletProducts = FETCH_DATA_LOCAL('Product_Assignment__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Product_Assignment__c:_soup} FROM {Product_Assignment__c} WHERE  {Product_Assignment__c:Outlet_Id__c}='" + accountId + "' order by {Product_Assignment__c:Order__c} ", 500));
                $scope.Pa = stateActiveProducts1.currentPageOrderedEntries;
                var outletProductsRecords = outletProducts.currentPageOrderedEntries;
                var z = [];
                var oz = [];
                var checkList = [];
                angular.forEach(outletProductsRecords, function(paaO, keyO) {
                    if (paaO[0]['Added__c'] == 'true')
                        checkList.push(paaO[0]['Product_Id__c']);
                });
                //$log.debug('checkList' + JSON.stringify(checkList));
                var zkey = 0;
                var ozkey = 0;
                angular.forEach($scope.Pa, function(paa, key) {
                    if (checkList.indexOf(paa[0]['Id']) == -1) {
                        z[zkey] = paa[0];
                        z[zkey].class = '';
                        angular.forEach(outletProductsRecords, function(paaO, keyO) {
                            if (paa[0]['Id'] == paaO[0]['Product_Id__c']) {
                                z[zkey].proOrder = paaO[0]['Order__c'];
                            }
                        });
                        zkey++;
                    } else {

                        angular.forEach(outletProductsRecords, function(paaO, keyO) {
                            if (paa[0]['Id'] == paaO[0]['Product_Id__c']) {
                                oz[paaO[0]['Order__c']] = paa[0];
                                oz[paaO[0]['Order__c']].class = '';
                                //alert(paaO[0]['Order__c']);
                                oz[paaO[0]['Order__c']].proOrder = paaO[0]['Order__c'];
                            }
                        });
                        //ozkey++;
                    }


                });
                $scope.Pa = z;
                /*$scope.Pa= [
                  { title: 'Reggae', id: 1,class:'' },
                  { title: 'Chill', id: 2 ,class:''},
                  { title: 'Dubstep', id: 3 ,class:''},
                  { title: 'Indie', id: 4,class:'' },
                  { title: 'Rap', id: 5 ,class:''},
                  { title: 'Cowbell', id: 6,class:'' }
                ];*/
                $scope.Pb = oz;
                $scope.class = '';
                $scope.changeClass = function(ind) {
                    if ($scope.Pa[ind].class === '') {
                        $scope.Pa[ind].class = 'selected';
                    } else {
                        $scope.Pa[ind].class = '';
                    }
                }
                $scope.changeClass2 = function(ind) {
                    if ($scope.Pb[ind].class === '') {
                        $scope.Pb[ind].class = 'selected';
                    } else {
                        $scope.Pb[ind].class = '';
                    }
                }
                var values_1 = $scope.Pa;
                $scope.moveToNextBlock = function() {
                    angular.forEach($scope.Pa, function(paa, key) {
                        if (paa.class == 'selected') {
                            $scope.Pb.push(paa);
                        }
                    });
                    var x = [];
                    angular.forEach($scope.Pa, function(paa, key) {
                        if (paa.class == 'selected') {
                            paa.class = '';
                        } else {
                            x.push(paa);
                        }
                    });
                    $scope.Pa = x;
                }
                $scope.moveToPrevBlock = function() {
                    angular.forEach($scope.Pb, function(paa, key) {
                        if (paa.class == 'selected') {
                            $scope.Pa.push(paa);
                        }
                    });
                    var x = [];
                    angular.forEach($scope.Pb, function(paa, key) {
                        if (paa.class == 'selected') {
                            paa.class = '';
                        } else {
                            x.push(paa);
                        }
                    });
                    $scope.Pb = x;
                }
                $scope.moveToUpBlock = function() {
                    var updateArray = [];
                    var storeKey_1 = false;
                    var storeKey_2 = false;
                    angular.forEach($scope.Pb, function(value, key) {
                        if ((key == 0 && value.class == 'selected')) {
                            storeKey_1 = true;
                        } else if (storeKey_1 && (key == 1 && value.class == 'selected')) {
                            storeKey_2 = true;
                        }
                    });
                    angular.forEach($scope.Pb, function(value, key) {
                        if (value.class == 'selected' && key != 0) {
                            var temp = [];
                            if (!storeKey_2) {
                                value.class = 'changed1';
                                $scope.Pb[key - 1].class = 'changed';
                                updateArray[key - 1] = value;
                                temp[0] = $scope.Pb[key - 1];
                                updateArray[key] = $scope.Pb[key - 1];
                                $scope.Pb[key] = temp[0];
                            }
                        }
                    });
                    angular.forEach($scope.Pb, function(value, key) {
                        if (value.class != 'changed' && value.class != 'changed1') {
                            updateArray[key] = $scope.Pb[key]
                        } else {

                        }
                    });
                    $scope.Pb = updateArray;
                    angular.forEach($scope.Pb, function(value, key) {
                        if (value.class == 'changed1')
                            value.class = 'selected';
                        else if (value.class == 'changed')
                            value.class = '';
                    });
                }

                $scope.moveToDownBlock = function() {
                    var updateArray = [];
                    var storeKey_1 = false;
                    var storeKey_2 = false;
                    var latestArray = $scope.Pb.reverse();
                    angular.forEach(latestArray, function(value, key) {
                        if ((key == 0 && value.class == 'selected')) {
                            storeKey_1 = true;
                        } else if (storeKey_1 && (key == 1 && value.class == 'selected')) {
                            storeKey_2 = true;
                        }
                    });
                    angular.forEach(latestArray, function(value, key) {
                        if (value.class == 'selected' && key != 0) {
                            var temp = [];
                            if (!storeKey_2) {
                                value.class = 'changed1';
                                $scope.Pb[key - 1].class = 'changed';
                                updateArray[key - 1] = value;
                                temp[0] = $scope.Pb[key - 1];
                                updateArray[key] = $scope.Pb[key - 1];
                                $scope.Pb[key] = temp[0];
                            }
                        }
                    });
                    angular.forEach($scope.Pb, function(value, key) {
                        if (value.class != 'changed' && value.class != 'changed1') {
                            updateArray[key] = $scope.Pb[key]
                        } else {

                        }
                    });
                    $scope.Pb = updateArray.reverse();
                    angular.forEach($scope.Pb, function(value, key) {
                        if (value.class == 'changed1')
                            value.class = 'selected';
                        else if (value.class == 'changed')
                            value.class = '';
                    });
                }
                $scope.saveProductOrder = function() {
                    $timeout(function(){
                        //$scope.Pb
                        var proOrder = [];
                        angular.forEach($scope.Pb, function(value, key) {
                            var ProOrd = {
                                Id: (accountId + value['Id']),
                                Outlet_Id__c: accountId,
                                Product_Id__c: value['Id'],
                                Order__c: key + '',
                                Added__c: 'true',
                                Product_Name__c: value['Sku_Code__c']
                            };
                            proOrder.push(ProOrd);
                            

                        });
                        angular.forEach($scope.Pa, function(value, key) {
                            var ProOrd = {
                                Id: (accountId + value['Id']),
                                Outlet_Id__c: accountId,
                                Product_Id__c: value['Id'],
                                Order__c: key + '',
                                Added__c: 'false',
                                Product_Name__c: value['Sku_Code__c']
                            };
                            proOrder.push(ProOrd);
                            angular.forEach($rootScope.StockAndSalesList, function(svalue, skey) {
                                if(value['Sku_Code__c']==svalue['ProductName'])
                                    $rootScope.StockAndSalesList.splice(skey,1);   
                            });
                        });

                        //$log.debug('checkout Product_Assignment__c' + JSON.stringify($scope.proOrder));
                        MOBILEDATABASE_ADD('Product_Assignment__c', proOrder, 'Id');
                        $ionicLoading.hide();
                        $location.path('app/stocks/' + accountId + '/' + visitId + '/' + $stateParams.order + '/' + new Date());
                    },100);
                }
            } catch (err) {
                $ionicLoading.hide();
                WEBSERVICE_ERROR('productOrderCtrlError', err.name + '::' + err.message);
            }
        })

        .controller('MapCtrl', function($scope, $stateParams, $rootScope, $q, $ionicModal, $timeout, $interval, $ionicLoading, $log, $filter, SalesforceSession, FETCH_DATA_LOCAL, SOUP_EXISTS, FETCH_DAILY_PLAN_ACCOUNTS, WEBSERVICE_ERROR,GEO_LOCATION) {
            try {
                $ionicLoading.show({
                    template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/>'
                });
                $rootScope.params = $stateParams;
                $rootScope.name = "MapCtrl";
                $scope.mapTitle = '';
                if (navigator && navigator.connection && navigator.connection.type != 'none') {
                    $scope.online = true;
                } else {
                    $scope.online = false;
                }
                if ($scope.online == true) {
                    var TodayDate = new Date();
                    $rootScope.backText = 'Back';
                    $rootScope.showBack = true;
                    $rootScope.showLogo = false;
                    $scope.outletList = $rootScope.Outlets;
                    if ($rootScope.day == 'tomorrow') {
                        TodayDate.setDate(TodayDate.getDate() + 1);
                    } else if ($rootScope.day == 'dayAfter') {
                        TodayDate.setDate(TodayDate.getDate() + 2);
                    }
                    $scope.mapTitle = 'Route Plan ' + $filter('date')(TodayDate, "EEE, dd MMM yy");

                    if ($scope.outletList == undefined || $scope.outletList.length == 0) {
                        $scope.outletList = FETCH_DAILY_PLAN_ACCOUNTS($rootScope.day);
                    }
                    $scope.markers = [];
                    //$log.debug('param'+JSON.stringify($stateParams));
                    $scope.map = {
                        show: true,
                        center: {
                            latitude: 20.593684,
                            longitude: 78.962880
                        },
                        options: {
                            streetViewControl: false,
                            panControl: false
                        },
                        zoom: 4,
                        dragging: false,
                        bounds: {},
                        markers: []
                    };
                    $scope.stroke = {
                        color: "#ff0000",
                        weight: 3
                    };
                    $scope.visible = true;
                    GEO_LOCATION.getCurrentPosition().then(function(position){
                        
                        $scope.map.center.latitude = position.latitude;
                        $scope.map.center.longitude = position.longitude;
                        $scope.map.zoom = 10;
                        var marker = {
                            latitude: $scope.map.center.latitude,
                            longitude: $scope.map.center.longitude,
                            id: 12323,
                            icon: "./img/gpsloc.png",
                            showWindow: false,
                            message: ' This is your current location ',
                            options: {
                                animation: 0,
                                labelContent: '',
                                labelAnchor: "22 0",
                                labelClass: "marker-labels"
                            }
                        };
                        $scope.map.markers.push(marker);
                    },function(error){
                        $log.debug("error=====" + JSON.stringify(error));
                    });
                    $scope.path = [];
                    //  $log.debug('accountid=== before'+$stateParams.accountId);
                    if ($stateParams.accountId == undefined || $stateParams.accountId == '') {
                        var i = 0;
                        angular.forEach($scope.outletList, function(record, key) {
                            if (record.Location__Latitude__s != undefined && record.Location__Longitude__s != undefined) {
                                var marker = {
                                    latitude: record.Location__Latitude__s,
                                    longitude: record.Location__Longitude__s,
                                    id: record.Id,
                                    icon: 'http://maps.google.com/mapfiles/kml/paddle/' + record.RoutePlan.Order__c + '.png',
                                    showWindow: false,
                                    message: ' ' + record.Name + '  ',
                                    options: {
                                        animation: 0,
                                        labelContent: '',
                                        labelAnchor: "22 0",
                                        labelClass: "marker-labels"
                                    }
                                };

                                $scope.map.markers.push(marker);
                            }


                        });

                    } else {
                        var i = 0;
                        //   $log.debug('accountid===after'+$stateParams.accountId);
                        angular.forEach($scope.outletList, function(record, key) {
                            if (record.Id === $stateParams.accountId) {
                                $scope.mapTitle = record.Name;
                                if (record.Location__Latitude__s != undefined && record.Location__Longitude__s != undefined) {
                                    // $log.debug('i=='+i);
                                    var marker = {
                                        latitude: record.Location__Latitude__s,
                                        longitude: record.Location__Longitude__s,
                                        id: record.Id,
                                        icon: 'http://maps.google.com/mapfiles/kml/paddle/' + record.RoutePlan.Order__c + '.png',
                                        showWindow: false,
                                        message: ' ' + record.Name + '  ',
                                        options: {
                                            animation: 0,
                                            labelContent: '',
                                            labelAnchor: "22 0",
                                            labelClass: "marker-labels"
                                        }
                                    };
                                    $scope.map.markers.push(marker);
                                }
                            }
                            // i++;
                        });

                    }
                    $timeout(function() {
                        if ($scope.map.markers.length <= 1) {
                            Splash.ShowToast('Outlets have not yet been tagged.', 'long', 'bottom', function(a) {
                                console.log(a)
                            });
                        }
                    }, 5000);
                }

                $ionicLoading.hide();
            } catch (err) {
                $ionicLoading.hide();
                WEBSERVICE_ERROR('MapCtrlError', err.name + '::' + err.message);
            }

        });