angular.module('UB_PRIDE_APP.controllers', []).run(function($ionicPlatform, $state, $log, $window, $ionicLoading, $rootScope, $ionicHistory) {
    $ionicPlatform.ready(function() {
        document.addEventListener("resume", function() {
            $ionicLoading.hide();
            $log.debug("The application is resuming from the background");
        }, false);
    });
    $ionicPlatform.registerBackButtonAction(function() {
        if ($state.current.name == "app.today") {
            navigator.app.exitApp();
        }else{
            $rootScope.myGoBack();
        }         
    }, 100);
}).config(function($ionicConfigProvider) {
    $ionicConfigProvider.views.maxCache(0);
    // note that you can also chain configs
    $ionicConfigProvider.backButton.text('Back').icon('ion-chevron-left');
}).filter('reverse', function() {
  return function(items) {
    if(items!=undefined)
    return items.slice().reverse();
    else
    return items;
  };
}).filter('cut', function() {
    return function(value, wordwise, max, tail) {
        if (!value) return '';
        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;
        value = value.substr(0, max);
        if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace != -1) {
                value = value.substr(0, lastspace);
            }
        }
        return value + (tail || ' ...');
    };
}).directive('myTouchstart', [function() {
    return function(scope, element, attr) {
        element.on('touchstart', function(event) {
            scope.$apply(function() {
                scope.$eval(attr.myTouchstart);
            });
        });
    };
}])
.directive('numbersOnly', function(){
   return {
     require: 'ngModel',
     link: function(scope, element, attrs, modelCtrl) {
       modelCtrl.$parsers.push(function (inputValue) {
           // this next if is necessary for when using ng-required on your input. 
           // In such cases, when a letter is typed first, this parser will be called
           // again, and the 2nd time, the value will be undefined
           if (inputValue == undefined) return '';
           inputValue=inputValue+'';
           var transformedInput = inputValue.replace(/[^0-9]/g, ''); 
           if (transformedInput!=inputValue) {
              modelCtrl.$setViewValue(transformedInput);
              modelCtrl.$render();
           }         

           return transformedInput;         
       });
     }
   };
})
.controller('AppCtrl', function(WRITE_TO_FILE,$scope, $ionicHistory, $rootScope, $ionicPopup, $window, $q, $ionicModal, $timeout, $interval, $location, $ionicLoading, $log, $filter, $state, SalesforceSession, fetchRecordTypes, SYNC_SFDC, IMG_SOUP, FETCH_MOBILE_SETTINGS, FETCH_OBJECT_RECORDS, FETCH_DATA_LOCAL, FETCH_DAILY_PLAN_ACCOUNTS, FETCH_DATA, SOUP_EXISTS, WEBSERVICE_ERROR, MOBILEDATABASE_ADD, SOUP_REGISTER,WEBSERVICE) {
    try {
        //OUTLET_IMAGES();
        //PURGE_DATA("Db_webservice_error_log","SELECT  {Db_webservice_error_log:_soup} FROM {Db_webservice_error_log} WHERE {Db_webservice_error_log:IsDirty} ='false'");
        var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
        var cordovaLog = cordova.require("salesforce/plugin/oauth");
        $rootScope.MonthsList = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        $scope.checkConnection = function() {
            if (navigator && navigator.connection && navigator.connection.type != 'none') {
                $scope.online = true;
            } else {
                $scope.online = false;
            }
        };
        $rootScope.progressBarCount = 0;
        $rootScope.appVersion = navigator.appVersion;
        $rootScope.appVersion = $rootScope.appVersion.substr($rootScope.appVersion.indexOf("PRIDE/") + 6, 5);
        $log.debug("Version" + $rootScope.appVersion);
        if ($rootScope.myUserName == undefined || $rootScope.myUserName == '') {
            var userDetails = FETCH_DATA_LOCAL('User', sfSmartstore.buildSmartQuerySpec("SELECT  {User:_soup} FROM {User} ", 100000));
            if (userDetails != undefined && userDetails.currentPageOrderedEntries != undefined){
                angular.forEach(userDetails.currentPageOrderedEntries, function(record, key) {
                    $rootScope.myUserName = record[0].Name;
                    $rootScope.Username = record[0].Username;
                    var tempUid = record[0].User_ID__c;
                    $rootScope.UserId=tempUid.toLowerCase();
                });
            }
        }
        $rootScope.noScreen = function(screen) {
            if (screen == 'Help1') {
                if (typeof navigator !== "undefined" && navigator.app) {
                    // Mobile device.
                    navigator.app.loadUrl('https://sfa-pride--ubuat.cs17.my.salesforce.com/?startURL=/apex/MTPPage&un=deepakjain@ubmail.com.ubuat&pw=ubuat@2017', {
                        openExternal: true
                    });
                } else {
                    // Possible web browser
                    window.open("https://sfa-pride--ubuat.cs17.my.salesforce.com/?startURL=/apex/MTPPage&un=deepakjain@ubmail.com.ubuat&pw=ubuat@2017", "_blank");
                }
            } else {
                Splash.ShowToast('Help is not currently available for this screen', 'long', 'center', function(a) {
                    console.log(a)
                });
            }
        }
        $rootScope.showLoadingScreen = function() {
            $ionicLoading.show({
                template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/>'
            });
        }
        $rootScope.goToHome = function() {
            $ionicHistory.nextViewOptions({
                disableBack: true
            });
             window.location.href = '#app/today/' + new Date();
            //$location.path('app/today/' + new Date());
        }

        //function to show error screen
        $rootScope.showWebServices = function()
        {
            console.log('show web clicked');
            window.location.href ='#app/showWebServices';
        }
        $rootScope.showErrors = function()
        {
            console.log('show errors clicked');
            window.location.href ='#app/showErrors';
        }
        $rootScope.showUserLogs = function()
        {
            console.log('show userLogs clicked');
            window.location.href ='#app/showUserLogs';
        }
        $rootScope.writeLog = function()
        {
            console.log('write log opr');
              var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
              var dbObjects = FETCH_D/ATA_LOCAL('Db_All_Logs', sfSmartstore.buildSmartQuerySpec("SELECT  {Db_All_Logs:_soup} FROM {Db_All_Logs}", 1000));
              var errorLog = FETCH_DATA_LOCAL('Db_webservice_error_log', sfSmartstore.buildSmartQuerySpec("SELECT  {Db_webservice_error_log:_soup} FROM {Db_webservice_error_log}", 1000));
              var userLogs = FETCH_DATA_LOCAL('Db_User_Logs', sfSmartstore.buildSmartQuerySpec("SELECT  {Db_User_Logs:_soup} FROM {Db_User_Logs}", 1000));
              WRITE_TO_FILE.write({data:dbObjects.currentPageOrderedEntries.reverse()},{data:errorLog.currentPageOrderedEntries.reverse()},{data:userLogs.currentPageOrderedEntries.reverse()});

        }
        //

        $rootScope.breadcrumClicked = function(accountId, visitId, oldPageOrder, pageOrder, from, companyName) {
            var flag = true;
            if (from == 'fromPage') {
                var visitedpages = FETCH_DATA.querySoup('Db_breadCrum', 'VisitedOrder', pageOrder);
                if (visitedpages.currentPageOrderedEntries == undefined || visitedpages.currentPageOrderedEntries.length == 0) {
                    flag = false;
                }
            }
            if (flag == true) {
                $rootScope.showLoadingScreen();
                $timeout(function() {
                    //$log.debug(accountId + visitId + oldPageOrder + pageOrder);
                    if ($rootScope.processFlowLineItems[pageOrder] != undefined) {
                        var currentPageId = $rootScope.processFlowLineItems[pageOrder].Page__c;
                        var nextPageId = $rootScope.processFlowLineItems[pageOrder].Page__c;
                        var currentPage, nextPage;
                        angular.forEach($rootScope.pages, function(record, key) {
                            if (record.pageDescription.Id == nextPageId) {
                                nextPage = record;
                            }
                            if (record.pageDescription.Id == currentPageId) {
                                currentPage = record;
                            }
                        });
                        var stageValue = 'CheckoutCtrl';
                        var today = $filter('date')(new Date(), 'dd/MM/yyyy');
                        if (currentPage.pageDescription.Template_Name__c == 'Template1') {
                            if (pageOrder == 2) stageValue = 'StockDetailsCtrl';
                            else stageValue = 'StockCtrl';
                        } else if (currentPage.pageDescription.Template_Name__c == 'Template2') {
                            stageValue = 'PRIDECallCtrl';
                        } else if (currentPage.pageDescription.Template_Name__c == 'Template3') {
                            stageValue = 'PRIDECallCtrl2';
                        } else if (currentPage.pageDescription.Template_Name__c == 'Template4') {
                            stageValue = 'Container4Ctrl';
                        } else if (currentPage.pageDescription.Template_Name__c == 'Template5') {
                            stageValue = 'PRIDECall5Ctrl';
                        } else if (currentPage.pageDescription.Template_Name__c == 'Template6') {
                            stageValue = 'PRIDECall6Ctrl';
                        } else if (currentPage.pageDescription.Template_Name__c == 'Template7') {
                            stageValue = 'PRIDECall7Ctrl';
                        } else if (currentPage.pageDescription.Template_Name__c == 'Template8') {
                            stageValue = 'PRIDECall8Ctrl';
                        } else {
                            stageValue = 'CheckoutCtrl';
                        }
                        $ionicLoading.hide();
                        if (nextPage.pageDescription.Template_Name__c == 'Template1') {
                            if (pageOrder == 2) $location.path('app/stockDetails/' + accountId + '/' + visitId + '/' + pageOrder + '/' + new Date());
                            else $location.path('app/stocks/' + accountId + '/' + visitId + '/' + pageOrder + '/' + new Date());
                        } else if (nextPage.pageDescription.Template_Name__c == 'Template2') {
                            $location.path('app/container2/' + accountId + '/' + visitId + '/' + pageOrder + '/' + new Date());
                        } else if (nextPage.pageDescription.Template_Name__c == 'Template3') {
                            $location.path('app/container3/' + accountId + '/' + visitId + '/' + pageOrder + '/' + new Date());
                        } else if (nextPage.pageDescription.Template_Name__c == 'Template4') {
                            $location.path('app/container4/' + accountId + '/' + visitId + '/' + pageOrder + '/' + new Date());
                        } else if (nextPage.pageDescription.Template_Name__c == 'Template5') {
                            $location.path('app/container5/' + accountId + '/' + visitId + '/' + pageOrder + '/' + new Date());
                        } else if (nextPage.pageDescription.Template_Name__c == 'Template6') {
                            $location.path('app/container6/' + accountId + '/' + visitId + '/' + pageOrder + '/' + new Date());
                        } else if (nextPage.pageDescription.Template_Name__c == 'Template7') {
                            $location.path('app/container7/' + accountId + '/' + visitId + '/' + pageOrder + '/' + new Date());
                        } else if (nextPage.pageDescription.Template_Name__c == 'Template8') {
                            $location.path('app/container8/' + accountId + '/' + visitId + '/' + pageOrder + '/' + new Date());
                        }else if(nextPage.pageDescription.Template_Name__c.indexOf('Depot')!=-1){
                             if(nextPage.pageDescription.Template_Name__c == 'DepotTemplate2'){
                                if(companyName == undefined || companyName == ''){
                                    companyName = 'UB'
                                }
                                $location.path('app/'+nextPage.pageDescription.Template_Name__c+'/' + accountId + '/' +visitId + '/' + pageOrder + '/'+ companyName +'/' + new Date());
                            }else{
                             $location.path('app/'+nextPage.pageDescription.Template_Name__c+'/' + accountId + '/' +visitId + '/' + pageOrder + '/' + new Date());   
                            } 
                        }else {
                           $location.path('app/'+nextPage.pageDescription.Template_Name__c+'/'  + accountId + '/' +visitId + '/' + pageOrder + '/' + new Date()); 
                        }
                    } else {
                        $ionicLoading.hide();
                        $location.path('app/checkout/' + accountId + '/' + visitId);
                    }
                }, 500);
            } else {
                Splash.ShowToast("You cannot move ahead in the call without pressing the 'Next' button.", 'long', 'bottom', function(a) {
                    console.log(a)
                });
            }
        }
        $rootScope.backText = 'Back';
        $rootScope.showBack = true;
        $rootScope.params = {};
        $rootScope.name = 'AppCtrl';
        $rootScope.controllersList = ["Container4Ctrl", "PRIDECall5Ctrl", "PRIDECall6Ctrl", "PRIDECall7Ctrl", "PRIDECall8Ctrl", "PRIDECallCtrl", "PRIDECallCtrl2", "CheckoutCtrl", "StockCtrl", "StockDetailsCtrl", "ChataiCtrl", "RetailCtrl","DepotTemplate1Ctrl","DepotTemplate2Ctrl","DepotTemplate3Ctrl","WholeSalerTemplate1Ctrl","WholeSalerTemplate2Ctrl","WholeSalerTemplate3Ctrl","RetailCtrl", "ChataiCtrl"];
        $rootScope.NonPerfectCallControllers = ["AddNew8Ctrl", "AddNew6Ctrl", "SalesTrackingCtrl", "Container4ViewCtrl", "Container4NewCtrl", "AddNewCtrl", "ReasonCtrl", "RemoveReasonCtrl", "MapCtrl", "productOrderCtrl","DepotTemplate1ViewCtrl","DepotTemplate1AddCtrl","WholeSalerTemplate1ViewCtrl","WholeSalerTemplate1AddCtrl","PRIDECallNewCtrl","AddNewWholeSalerTemplate3Ctrl"];
        $rootScope.myGoBack = function() {
             $rootScope.showLoadingScreen();
             $timeout(function() {
            if($rootScope.name == 'MapCtrl'){
                $rootScope.goToHome();
            } else if ($rootScope.NonPerfectCallControllers.indexOf($rootScope.name) != -1) {
                $window.history.back();
            } else if ($rootScope.controllersList.indexOf($rootScope.name) != -1) {
                if($rootScope.name == 'RetailCtrl'){
                    window.location.href = '#app/stocks/' + $rootScope.params.AccountId+ '/' + $rootScope.params.visitId + '/' + parseInt($rootScope.params.order) + '/' + new Date();
                } else if($rootScope.name == 'ChataiCtrl'){
                    window.location.href = '#app/stocks/' + $rootScope.params.AccountId+ '/' + $rootScope.params.visitId + '/' + parseInt($rootScope.params.order) + '/' + new Date();
                } else if ($rootScope.params.order != undefined || $rootScope.name == 'CheckoutCtrl') {
                    if ($rootScope.params.order != undefined && $rootScope.params.order == 0) {
                         if($rootScope.name.indexOf('Depot')!=-1){
                           window.location.href = '#app/depotOverview/' + $rootScope.params.AccountId;
                          // $location.path('app/depotOverview/' + $rootScope.params.AccountId); 
                        }else if($rootScope.name.indexOf('WholeSaler')!=-1){
                            window.location.href = '#app/wholesalerlastVistSummary/' + $rootScope.params.AccountId;
                          // $location.path('app/wholesalerlastVistSummary/' + $rootScope.params.AccountId);
                        }else{
                            window.location.href = '#app/lastVistSummary/' + $rootScope.params.AccountId;
                            //$location.path('app/lastVistSummary/' + $rootScope.params.AccountId);
                        }
                        Splash.ShowToast('You have exited the module without completing the call. Click resume button to resume the call.', 'long', 'bottom', function(a) {
                            console.log(a)
                        });
                    } else {
                        var pageOrder;
                        if ($rootScope.name == 'CheckoutCtrl') {
                            pageOrder = $rootScope.processFlowLineItems.length - 1;
                        } else {
                            pageOrder = parseInt($rootScope.params.order) - 1;
                        }
                        $rootScope.breadcrumClicked($rootScope.params.AccountId, $rootScope.params.visitId, pageOrder-1, pageOrder, 'fromCtrl');
                        
                    }
                }
            } else {
                if ($rootScope.name == 'OverviewCtrl' || $rootScope.name == 'depotOverviewCtrl' || $rootScope.name == 'wholesaleOverviewCtrl') {
                    $rootScope.goToHome();
                } else if ($rootScope.name == 'BusinessObjectivesCtrl' || $rootScope.name == 'MarketShareCtrl') {
                    window.location.href = '#app/overview/' + $rootScope.params.accountId;
                   // $location.path('app/overview/' + $rootScope.params.accountId);
                } else if ($rootScope.name == 'LastVisitSummaryCtrl') {
                      window.location.href = '#app/Objectives/' + $rootScope.params.accountId;
                   // $location.path('app/Objectives/' + $rootScope.params.accountId);
                    //$location.path('app/overview/' + $rootScope.params.accountId);
                }else if($rootScope.name == 'WholeSalerLastVisitSummaryCtrl'){
                    window.location.href = '#app/wholesaleOverview/' + $rootScope.params.accountId;
                   // $location.path('app/wholesaleOverview/' + $rootScope.params.accountId);
                }else {
                    $rootScope.goToHome();
                }
            }
             },500);
        }
        $rootScope.salestrackingList = [];
        //it will store the today's accounts with route plan
        $rootScope.Outlets = [];
        $scope.totalNotifications = 0;
        $rootScope.showLogo = true;
        $rootScope.completedAccountIds = [];
        $rootScope.unsyncedAccounts = [];
        $rootScope.uploadfields = [];
        $rootScope.currentvisitSummaryImages = [];
        $rootScope.galleryImages =[];
        $rootScope.slidesList = [];
        $rootScope.slideFields = [];
        $rootScope.StockAndSalesList = [];
        $rootScope.retailSalesList = {};
        $rootScope.chataiSales = [];
        $rootScope.audit = {};
        $rootScope.PrideImages = [];
        $rootScope.visitImages = [];
        $rootScope.Month = {};
        $rootScope.ChataiMonth = {};
        $rootScope.visitrecord = {};
        $scope.myplan = [];
        $rootScope.resentlyAdded = false;
        $rootScope.ubCoolerCount = 0;
        $rootScope.ubCoolerAdded = false;
        $rootScope.resentlyAdded2 = false;
        if (SOUP_EXISTS.soupExists('Db_transaction')) {
            var completedTasks = FETCH_DATA.querySoup('Db_transaction', 'status', 'saved');
            console.log('FETCH_DATA.querySoup(Db_transaction,status,saved)');
            console.log(completedTasks);
            if (completedTasks.currentPageOrderedEntries != undefined && completedTasks.currentPageOrderedEntries.length != 0) {
                angular.forEach(completedTasks.currentPageOrderedEntries, function(record, key) {
                    $rootScope.completedAccountIds.push(record.Account);
                });
            }
            if (SOUP_EXISTS.soupExists('Visit__c')) {
                var myVisits = FETCH_DATA.querySoup('Visit__c', 'IsDirty', true);
                console.log('FETCH_DATA.querySoup(Visit__c,IsDirty, true)');
                console.log(myVisits);
                if (myVisits.currentPageOrderedEntries != undefined && myVisits.currentPageOrderedEntries.length != 0) {
                    angular.forEach(myVisits.currentPageOrderedEntries, function(record, key) {
                        $rootScope.unsyncedAccounts[record.Account__c] = 'done';
                    });
                }
            }
        }
        console.log(' &&&& $rootScope.completedAccountIds');
        console.log($rootScope.completedAccountIds);
        console.log(' &&&& $rootScope.unsyncedAccounts');
        console.log($rootScope.unsyncedAccounts);
        //function to calculate the distance between two geolocations
        $scope.distance = function(lat1, lon1, lat2, lon2, unit) {
            var radlat1 = Math.PI * lat1 / 180;
            var radlat2 = Math.PI * lat2 / 180;
            var radlon1 = Math.PI * lon1 / 180;
            var radlon2 = Math.PI * lon2 / 180;
            var theta = lon1 - lon2;
            var radtheta = Math.PI * theta / 180;
            var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
            dist = Math.acos(dist);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;
            if (unit == "K") {
                dist = dist * 1.609344;
            } else if (unit == "N") {
                dist = dist * 0.8684;
            }
            return dist
        };
        
        $rootScope.processFlow = {};
        var confirmReumePopup = null;
        $rootScope.cancelResumeSuccess = function() {
            confirmReumePopup.close();
        }
        $rootScope.restartCall=function(accId,visitId){
            var today = $filter('date')(new Date(), 'dd/MM/yyyy');
            var transaction = [{
                Account: accId,
                stage: 0,
                stageValue: 'PRIDECall4Ctrl',
                Visit: visitId,
                status: 'pending',
                entryDate: today
            }];
            MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
            MOBILEDATABASE_ADD('Db_breadCrum', [{
                VisitedOrder: 0,
                Name: 'PRIDECall4Ctrl'
            }], 'VisitedOrder');
            $rootScope.pendingTask = true;
            $rootScope.confirmResumeSuccess();
        }
        $rootScope.confirmResumeSuccess = function() {
            if(confirmReumePopup!=null)
            confirmReumePopup.close();
            $rootScope.showLoadingScreen();
            $timeout(function(){
            var pendingTasks = FETCH_DATA.querySoup('Db_transaction', 'status', 'pending');
            $rootScope.processFlowLineItems = [];
            $rootScope.pages = [];
            $rootScope.processFlow = {};
            var currentCustomer = FETCH_DATA.querySoup('Account', 'Id', pendingTasks.currentPageOrderedEntries[0].Account);
            $scope.currentOutlet = currentCustomer.currentPageOrderedEntries[0];
            var flow = FETCH_DATA_LOCAL('Db_process_flow', sfSmartstore.buildSmartQuerySpec("SELECT  {Db_process_flow:_soup} FROM {Db_process_flow} WHERE  {Db_process_flow:Outlet_Type__c}='" + currentCustomer.currentPageOrderedEntries[0].RecordType.Name + "'", 1));
            $rootScope.processFlow = flow.currentPageOrderedEntries[0][0];
            var flowLineItems = FETCH_DATA.querySoup('Db_process_flow_line_item', "Process_Flow__c", flow.currentPageOrderedEntries[0][0].Id);
            var queryspec = '';
            var queryspecForSection = '';
            angular.forEach(flowLineItems.currentPageOrderedEntries, function(record, key) {
                if(record['Removed__c']==false){
                    $rootScope.processFlowLineItems[record.Order__c - 1] = record;
                    if (queryspec == '' && queryspecForSection == '') {
                        queryspec = "{DB_page:Id}='" + record.Page__c + "'";
                        queryspecForSection = "{DB_section:Page__c}='" + record.Page__c + "'";
                    } else {
                        queryspec = queryspec + " OR {DB_page:Id}='" + record.Page__c + "'";
                        queryspecForSection = queryspecForSection + " OR {DB_section:Page__c}='" + record.Page__c + "'";
                    }
                }
            });
            if (queryspec != '' && queryspecForSection != '') {
                var pages = FETCH_DATA_LOCAL('DB_page', sfSmartstore.buildSmartQuerySpec("SELECT  {DB_page:_soup} FROM {DB_page} WHERE " + queryspec, 20));
                var sections = FETCH_DATA_LOCAL('DB_section', sfSmartstore.buildSmartQuerySpec("SELECT  {DB_section:_soup} FROM {DB_section} WHERE " + queryspecForSection + " order by {DB_section:Order__c}", 20));
                queryspec = '';
                angular.forEach(sections.currentPageOrderedEntries, function(record, key) {
                    if (queryspec == '') {
                        queryspec = "{DB_fields:Section__c}='" + record[0].Id + "'";
                    } else {
                        queryspec = queryspec + " OR {DB_fields:Section__c}='" + record[0].Id + "'";
                    }
                });
                var fields = [];
                if (queryspec != '') {
                    fields = FETCH_DATA_LOCAL('DB_fields', sfSmartstore.buildSmartQuerySpec("SELECT  {DB_fields:_soup} FROM {DB_fields} WHERE " + queryspec + " and {DB_fields:Removed__c}='false' order by {DB_fields:Order__c}", 100000));
                }
                //   $log.debug('fields' + JSON.stringify(fields.currentPageOrderedEntries));
                angular.forEach(pages.currentPageOrderedEntries, function(page, pageKey) {
                    var pagedec = {};
                    pagedec.pageDescription = page[0];
                    pagedec.sections = [];
                    angular.forEach(sections.currentPageOrderedEntries, function(section, sectionKey) {
                        if (page[0].Id == section[0].Page__c) {
                            var sectiondec = {};
                            sectiondec.sectionDescription = section[0];
                            sectiondec.fields = [];
                            angular.forEach(fields.currentPageOrderedEntries, function(field, fieldKey) {
                                if (field[0].Section__c == section[0].Id) {
                                    var fieldDescription = field[0];
                                    fieldDescription.Order__c = parseInt(fieldDescription.Order__c);
                                    if (field[0].Picklist_Values__c != undefined) {
                                        fieldDescription.pickListValues = field[0].Picklist_Values__c.split(';');
                                    }
                                    sectiondec.fields.push(fieldDescription);
                                }
                            });
                            pagedec.sections.push(sectiondec);
                        }
                    });
                    $rootScope.pages.push(pagedec);
                });
            }
            if (SOUP_EXISTS.soupExists('Db_breadCrum') == false) {
                SOUP_REGISTER('Db_breadCrum', [{
                    "path": "Name",
                    "type": "string"
                }, {
                    "path": "VisitedOrder",
                    "type": "string"
                }]);
                var visitedPages = parseInt(pendingTasks.currentPageOrderedEntries[0].stage);
                var visitedPagesList = [];
                for (i = 0; i <= visitedPages; i++) {
                    visitedPagesList.push({
                        VisitedOrder: i.toString(),
                        Name: 'pridecall'
                    });
                }
                MOBILEDATABASE_ADD('Db_breadCrum', visitedPagesList, 'VisitedOrder');
            }
            if (pendingTasks.currentPageOrderedEntries[0].stageValue == 'chatai') {
                $location.path('app/chataiSales/' + pendingTasks.currentPageOrderedEntries[0].Account + '/' + pendingTasks.currentPageOrderedEntries[0].Visit + '/' + pendingTasks.currentPageOrderedEntries[0].stage + '/' + new Date());
            } else if (pendingTasks.currentPageOrderedEntries[0].stageValue == 'Retail') {
                $location.path('app/retailSales/' + pendingTasks.currentPageOrderedEntries[0].Account + '/' + pendingTasks.currentPageOrderedEntries[0].Visit + '/' + pendingTasks.currentPageOrderedEntries[0].stage + '/' + new Date());
            } else if (pendingTasks.currentPageOrderedEntries[0].stage == 'checkout') {
                $location.path('app/checkout/' + pendingTasks.currentPageOrderedEntries[0].Account + '/' + pendingTasks.currentPageOrderedEntries[0].Visit);
            } else {
                var nextPage = {};
                //$log.debug('processFlow == '+JSON.stringify($rootScope.processFlowLineItems));
                angular.forEach($rootScope.pages, function(record, key) {
                    $log.debug(record.pageDescription.Id + "====" + pendingTasks.currentPageOrderedEntries[0].stage + "====" + $rootScope.processFlowLineItems[pendingTasks.currentPageOrderedEntries[0].stage].Page__c);
                    if (record.pageDescription.Id == $rootScope.processFlowLineItems[pendingTasks.currentPageOrderedEntries[0].stage].Page__c) {
                        $log.debug(record.pageDescription.Id + "====" + pendingTasks.currentPageOrderedEntries[0].stage + "====" + $rootScope.processFlowLineItems[pendingTasks.currentPageOrderedEntries[0].stage].Page__c);
                        nextPage = record;
                    }
                });
                $log.debug('next page === ' + JSON.stringify(nextPage));
                if (nextPage.pageDescription.Template_Name__c == 'Template1') {
                    if (pendingTasks.currentPageOrderedEntries[0].stageValue == 'stocks') {
                        $location.path('app/stocks/' + pendingTasks.currentPageOrderedEntries[0].Account + '/' + pendingTasks.currentPageOrderedEntries[0].Visit + '/' + pendingTasks.currentPageOrderedEntries[0].stage + '/' + new Date());
                    } else {
                        $location.path('app/stockDetails/' + pendingTasks.currentPageOrderedEntries[0].Account + '/' + pendingTasks.currentPageOrderedEntries[0].Visit + '/' + pendingTasks.currentPageOrderedEntries[0].stage + '/' + new Date());
                    }
                } else if (nextPage.pageDescription.Template_Name__c == 'Template2') {
                    $location.path('app/container2/' + pendingTasks.currentPageOrderedEntries[0].Account + '/' + pendingTasks.currentPageOrderedEntries[0].Visit + '/' + pendingTasks.currentPageOrderedEntries[0].stage + '/' + new Date());
                } else if (nextPage.pageDescription.Template_Name__c == 'Template3') {
                    $location.path('app/container3/' + pendingTasks.currentPageOrderedEntries[0].Account + '/' + pendingTasks.currentPageOrderedEntries[0].Visit + '/' + pendingTasks.currentPageOrderedEntries[0].stage + '/' + new Date());
                } else if (nextPage.pageDescription.Template_Name__c == 'Template4') {
                    $location.path('app/container4/' + pendingTasks.currentPageOrderedEntries[0].Account + '/' + pendingTasks.currentPageOrderedEntries[0].Visit + '/' + pendingTasks.currentPageOrderedEntries[0].stage + '/' + new Date());
                } else if (nextPage.pageDescription.Template_Name__c == 'Template5') {
                    $location.path('app/container5/' + pendingTasks.currentPageOrderedEntries[0].Account + '/' + pendingTasks.currentPageOrderedEntries[0].Visit + '/' + pendingTasks.currentPageOrderedEntries[0].stage + '/' + new Date());
                } else if (nextPage.pageDescription.Template_Name__c == 'Template6') {
                    $location.path('app/container6/' + pendingTasks.currentPageOrderedEntries[0].Account + '/' + pendingTasks.currentPageOrderedEntries[0].Visit + '/' + pendingTasks.currentPageOrderedEntries[0].stage + '/' + new Date());
                } else if (nextPage.pageDescription.Template_Name__c == 'Template7') {
                    $location.path('app/container7/' + pendingTasks.currentPageOrderedEntries[0].Account + '/' + pendingTasks.currentPageOrderedEntries[0].Visit + '/' + pendingTasks.currentPageOrderedEntries[0].stage + '/' + new Date());
                } else if (nextPage.pageDescription.Template_Name__c == 'Template8') {
                    $location.path('app/container8/' + pendingTasks.currentPageOrderedEntries[0].Account + '/' + pendingTasks.currentPageOrderedEntries[0].Visit + '/' + pendingTasks.currentPageOrderedEntries[0].stage + '/' + new Date());
                }else if(nextPage.pageDescription.Template_Name__c.indexOf('Depot')!=-1){
                     if(nextPage.pageDescription.Template_Name__c == 'DepotTemplate2'){
                            $location.path('app/'+nextPage.pageDescription.Template_Name__c+'/' + pendingTasks.currentPageOrderedEntries[0].Account + '/' + pendingTasks.currentPageOrderedEntries[0].Visit + '/' + pendingTasks.currentPageOrderedEntries[0].stage + '/UB/' + new Date());
                            }else{
                             $location.path('app/'+nextPage.pageDescription.Template_Name__c+'/' + pendingTasks.currentPageOrderedEntries[0].Account + '/' + pendingTasks.currentPageOrderedEntries[0].Visit + '/' + pendingTasks.currentPageOrderedEntries[0].stage + '/' + new Date());
                            } 
                    
                }else {
                   $location.path('app/'+nextPage.pageDescription.Template_Name__c+'/' + pendingTasks.currentPageOrderedEntries[0].Account + '/' + pendingTasks.currentPageOrderedEntries[0].Visit + '/' + pendingTasks.currentPageOrderedEntries[0].stage + '/' + new Date()); 
                }
            }
           
         },500);
        }
        $rootScope.resumeCall = function() {
           
            if (SOUP_EXISTS.soupExists('Db_transaction')) {
                var pendingTasks = FETCH_DATA.querySoup('Db_transaction', 'status', 'pending');
                if (pendingTasks.currentPageOrderedEntries != undefined && pendingTasks.currentPageOrderedEntries.length != 0) {
                    $rootScope.pendingTask = true;
                    var currentCustomer = FETCH_DATA.querySoup('Account', 'Id', pendingTasks.currentPageOrderedEntries[0].Account);
                    $scope.currentOutlet = currentCustomer.currentPageOrderedEntries[0];
                    confirmReumePopup = $ionicPopup.confirm({
                        cssClass: 'resumeConfirm',
                        title: '',
                        scope: $rootScope,
                        template: 'You are about to resume the call at ' + $scope.currentOutlet.Name + '.<br/><br/><br/><div style="text-align:right; color:#38B6CB; margin-right: 18px; font-size: larger; font-weight: bold;"><span my-touchstart="cancelResumeSuccess()" style="padding-right: 35px;">NO</span> &nbsp;<span my-touchstart="confirmResumeSuccess()">YES</span> </div>',
                        buttons: []
                    });
                    confirmReumePopup.then(function(res) {});
                } else {
                    $rootScope.pendingTask = false;
                    Splash.ShowToast('You have no pending calls.', 'long', 'bottom', function(a) {
                        console.log(a)
                    });
                }
            }
          
        }
        $rootScope.pendingTask = false;
        $rootScope.day = 'today';
        $rootScope.Uploads = [];
        $ionicModal.fromTemplateUrl('./templates/notifications.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
        });
        $rootScope.processFlow = [];
        $rootScope.processFlowLineItems = [];
        $rootScope.pages = [];
        $rootScope.sections = [];
        $rootScope.fields = [];
        $scope.Notifications = [];
        $scope.getUploads = function() {
            $ionicLoading.show({
                template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/>'
            });
            $scope.myplan = $rootScope.Outlets;
            if ($scope.myplan.length != 0) {
                var today = $filter('date')(new Date(), 'yyyy-MM-dd');
                $scope.AccountsWithIds = {};
                var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
                var queryVariable = '';
                angular.forEach($scope.myplan, function(record, key) {
                    $scope.AccountsWithIds[record.Id] = record;
                    if (queryVariable == '') {
                        queryVariable = queryVariable + "{Upload__c:Account__c} = '" + record.Id + "'";
                    } else {
                        queryVariable = queryVariable + " OR {Upload__c:Account__c} = '" + record.Id + "'";
                    }
                });
                $scope.promos = [];
                $scope.pops = [];
                $scope.Notifications = [];
                $scope.complaints = [];
                $rootScope.Uploads = [];
                var rtList = FETCH_DATA_LOCAL('DB_RecordTypes', sfSmartstore.buildSmartQuerySpec("SELECT  {DB_RecordTypes:_soup} FROM {DB_RecordTypes} WHERE {DB_RecordTypes:SobjectType}='Upload__c'", 15));
                var recordTypesList = [];
                angular.forEach(rtList.currentPageOrderedEntries, function(record, key) {
                    recordTypesList[record[0].Name] = record[0].Id;
                });
                var DATA = FETCH_DATA_LOCAL('Upload__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Upload__c:_soup} FROM {Upload__c} WHERE ({Upload__c:RecordTypeId}='" + recordTypesList['Cooler'] + "' OR {Upload__c:RecordTypeId}='" + recordTypesList['POP'] + "' OR {Upload__c:RecordTypeId}='" + recordTypesList['Promotions'] + "') AND (" + queryVariable + ")", 100000));
                
                angular.forEach(DATA.currentPageOrderedEntries, function(record, key) {
                    if (record[0] != undefined && record[0] != '' && record[0].Status__c != 'Inactive') $rootScope.Uploads.push(record[0]);
                    if (record[0].Start_Date__c != undefined && record[0] != '' && (record[0].RecordType != undefined && (record[0].RecordType.Name === 'POP' || (record[0].RecordType.Name === 'Promotions' && record[0].Status__c != 'Completed')))) {
                        var astartDate = record[0].Start_Date__c;
                        var aEndDate = record[0].End_Date__c;
                        if (astartDate.indexOf('/') != -1) {
                            aStartDate = new Date(astartDate.split('/')[1] + '/' + astartDate.split('/')[0] + '/' + astartDate.split('/')[2]);
                        } else {
                            aStartDate = new Date(astartDate);
                        } 
                        if (aEndDate.indexOf('/') != -1) {
                            aEndDate = new Date(aEndDate.split('/')[1] + '/' + aEndDate.split('/')[0] + '/' + aEndDate.split('/')[2]);
                        } else {
                            aEndDate = new Date(aEndDate);
                        } 

                        var startDate = $filter('date')(new Date(aStartDate), 'yyyy-MM-dd');
                        var endDate = $filter('date')(new Date(aEndDate), 'yyyy-MM-dd');
                        if (today >= startDate && today <= endDate) {
                            var notification = record[0];
                            notification.RoutePlanOrder = $scope.AccountsWithIds[record[0].Account__c].RoutePlan.Order__c;
                            $scope.Notifications.push(notification);
                        }
                    }
                });
            }
            $ionicLoading.hide();
        }
        $rootScope.currentOutlet = {};
        $rootScope.selectedAccounts = [];
        $scope.superSync = function() {
            $ionicLoading.show({
                template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/>'
            });
            $scope.checkConnection();
            if ($scope.online == true) {
                $scope.SyncSFDC(true);
            } else {
                $ionicLoading.hide();
                Splash.ShowToast('No internet. please try again later.', 'long', 'bottom', function(a) {
                    console.log(a)
                });
            }
        }
        $scope.SyncSFDC = function(ifLast) {
            $q.all(SYNC_SFDC()).then(function(response) {
                if (response != undefined) $q.all({
                    SETTINGS_STATUS: FETCH_MOBILE_SETTINGS()
                }).then(function(response1) {
                    if (response1 != undefined) $q.all({
                        OBJECT_STATUS: FETCH_OBJECT_RECORDS()
                    }).then(function(response2) {
                        //$log.debug('result' + JSON.stringify(response2));
                        if (response2.OBJECT_STATUS == 'Success' || response2.OBJECT_STATUS == 'Failure') {
                            if (ifLast) {
                                $timeout(function() {
                                    if ($rootScope.name == "TodayCtrl" && $rootScope.day == "today") {
                                        $rootScope.unsyncedAccounts = [];
                                        $rootScope.goToHome();
                                    }
                                    $ionicLoading.hide();
                                }, 2000);
                            }
                        }
                    }, function(error2) {
                        if (ifLast) {
                            $timeout(function() {
                                if ($rootScope.name == "TodayCtrl" && $rootScope.day == "today") {
                                    $rootScope.unsyncedAccounts = [];
                                    $rootScope.goToHome();
                                }
                                $ionicLoading.hide();
                            }, 2000);
                        }
                    });
                }, function(error1) {
                    $q.all({
                        OBJECT_STATUS: FETCH_OBJECT_RECORDS()
                    }).then(function(response2) {
                        if (response2.OBJECT_STATUS == 'Success' || response2.OBJECT_STATUS == 'Failure') {
                            if (ifLast) {
                                $timeout(function() {
                                    if ($rootScope.name == "TodayCtrl" && $rootScope.day == "today") {
                                        $rootScope.unsyncedAccounts = [];
                                        $rootScope.goToHome();
                                    }
                                    $ionicLoading.hide();
                                }, 2000);
                            }
                        }
                    }, function(error2) {
                        if (ifLast) {
                            $timeout(function() {
                                if ($rootScope.name == "TodayCtrl" && $rootScope.day == "today") {
                                    $rootScope.unsyncedAccounts = [];
                                    $rootScope.goToHome();
                                }
                                $ionicLoading.hide();
                            }, 2000);
                        }
                    });
                })
            }, function(error) {
                $q.all({
                    SETTINGS_STATUS: FETCH_MOBILE_SETTINGS()
                }).then(function(response1) {
                    if (response1 != undefined) $q.all({
                        OBJECT_STATUS: FETCH_OBJECT_RECORDS()
                    }).then(function(response2) {
                        if (response2.OBJECT_STATUS == 'Success' || response2.OBJECT_STATUS == 'Failure') {
                            if (ifLast) {
                                $timeout(function() {
                                    if ($rootScope.name == "TodayCtrl" && $rootScope.day == "today") {
                                        $rootScope.unsyncedAccounts = [];
                                        $rootScope.goToHome();
                                    }
                                    $ionicLoading.hide();
                                }, 2000);
                            }
                        }
                    }, function(error2) {
                        if (ifLast) {
                            $timeout(function() {
                                if ($rootScope.name == "TodayCtrl" && $rootScope.day == "today") {
                                    $rootScope.unsyncedAccounts = [];
                                    $rootScope.goToHome();
                                }
                                $ionicLoading.hide();
                            }, 2000);
                        }
                    });
                }, function(error1) {
                    $q.all({
                        OBJECT_STATUS: FETCH_OBJECT_RECORDS()
                    }).then(function(response2) {
                        if (response2.OBJECT_STATUS == 'Success' || response2.OBJECT_STATUS == 'Failure') {
                            if (ifLast) {
                                $timeout(function() {
                                    if ($rootScope.name == "TodayCtrl" && $rootScope.day == "today") {
                                        $rootScope.unsyncedAccounts = [];
                                        $rootScope.goToHome();
                                    }
                                    $ionicLoading.hide();
                                }, 2000);
                            }
                        }
                    }, function(error2) {
                        if (ifLast) {
                            $timeout(function() {
                                if ($rootScope.name == "TodayCtrl" && $rootScope.day == "today") {
                                    $rootScope.unsyncedAccounts = [];
                                    $rootScope.goToHome();
                                }
                                $ionicLoading.hide();
                            }, 2000);
                        }
                    });
                });
            });
        }
        if (SOUP_EXISTS.soupExists('Db_transaction') == true) {
            var pendingTasks = FETCH_DATA.querySoup('Db_transaction', 'status', 'pending');
            if (pendingTasks.currentPageOrderedEntries != undefined && pendingTasks.currentPageOrderedEntries.length != 0) {
                $rootScope.pendingTask = true;
            } else {
                $rootScope.pendingTask = false;
            }
        }
        var transaction = function() {
            //  $log.debug('soupkk' + SOUP_EXISTS.soupExists('Db_transaction'));
            if (SOUP_EXISTS.soupExists('Db_transaction') == true) {
                var pendingTasks = FETCH_DATA.querySoup('Db_transaction', 'status', 'pending');
                if (pendingTasks.currentPageOrderedEntries != undefined && pendingTasks.currentPageOrderedEntries.length != 0) {
                    $rootScope.pendingTask = true;
                } else {
                    $rootScope.pendingTask = false;
                }
            }
        };
        /*var syncfunc = $interval(function() {
            $scope.checkConnection();
            //$log.debug('soupkkAccount' + SOUP_EXISTS.soupExists('Account'));
            if ($scope.online == true && SOUP_EXISTS.soupExists('Account') == true) {
                //Data sync between mobile and salesforce

                SYNC_SFDC();
            }
        }, 500000);*/
        $rootScope.logOut = function() {
            if (navigator && navigator.connection && navigator.connection.type != 'none') {
                $ionicLoading.show({
                    template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/>'
                });
                if (angular.isDefined(transaction)) {
                    $interval.cancel(transaction);
                }
                $q.all(SYNC_SFDC()).then(function(response) {
                    //$log.debug('sync response' + JSON.stringify(response));
                    //sfSmartstore.removeAllSoups();
                    sfSmartstore.removeSoup('Db_transaction', function(response) {}, function(error) {});
                    sfSmartstore.removeSoup('Account', function(response) {}, function(error) {});
                    $ionicLoading.show({
                        template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/>'
                    });
                    $timeout(function() {
                        $ionicLoading.hide();
                        cordovaLog.logout();
                        navigator.app.exitApp(); 
                    }, 2000);
                });
            } else {
                Splash.ShowToast('Internet connectivity is required to logout.', 'long', 'center', function(a) {
                    console.log(a)
                });
            }
        }
        $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        
        WEBSERVICE_ERROR('AppCtrlError', err.stack + '::' + err.message);
    }
}).controller('AddNewCtrl', function($scope, $stateParams, $rootScope, $q, $location, $timeout, $ionicLoading, $log, $filter, FETCH_LOCAL_DATA,FETCH_DATA_LOCAL, WEBSERVICE_ERROR) {
    try {
        $scope.AccountList = [];
        var queryVariable = '';
        $rootScope.backText = 'Back';
        $rootScope.showBack = true;
        $scope.selectAll = false;
        $rootScope.params = $stateParams;
        $scope.searchAccount = '';
        $rootScope.name = "AddNewCtrl";
        $scope.isSelected=false;
        $scope.selectedAccountsDisplay = [];
        $ionicLoading.show({
            template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/>'
        });
        $timeout(function() {
            var outletIds = [];
            var queryVariable=''
            $scope.routeNames=[];
            angular.forEach($rootScope.Outlets, function(record, key) {
                outletIds.push(record.Id);
                
            });
            
            var outlets = FETCH_LOCAL_DATA('Account', 'Id', null);
//             for (var i = 0; i < outlets.length; i++) {
//                 if (outletIds == undefined || outletIds.length == 0 || outletIds.indexOf(outlets[i].Id) == -1) {
                    
//                     if (queryVariable == '') {
//                         queryVariable = queryVariable + "{Route_Assignment__c:Account__c} = '" + outlets[i].Id + "'";
//                     } else {
//                         queryVariable = queryVariable + " OR {Route_Assignment__c:Account__c} = '" + outlets[i].Id + "'";
//                     }
//                 }
//             }

            var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
            var routeAssignment = FETCH_DATA_LOCAL('Route_Assignment__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Route_Assignment__c:_soup} FROM {Route_Assignment__c} WHERE  {Route_Assignment__c:Removed__c}='false' ", 100000));
            angular.forEach(routeAssignment.currentPageOrderedEntries, function(record, key) {
                for (var i = 0; i < outlets.length; i++) {
                    var tempExtId=record[0]['External_Id__c'];
                    tempExtId=tempExtId.toLowerCase();
                    var tempUserId=$rootScope.UserId;
                    tempUserId=tempUserId.toLowerCase();
                    if(tempExtId.indexOf(tempUserId)>-1 && (outletIds == undefined || outletIds.length == 0 || outletIds.indexOf(outlets[i].Id) == -1) &&record[0].Account__c==outlets[i].Id && ($scope.routeNames[record[0].Account__c]==undefined || $scope.routeNames[record[0].Account__c]=='Sunday')){
                        var tempRouteName=record[0].Route_Name__c;
                        tempRouteName=tempRouteName.replace("_"+$rootScope.myUserName, "");
                        tempRouteName=tempRouteName.substring(tempRouteName.indexOf('_')+1);
                        $scope.routeNames[record[0].Account__c]=tempRouteName;
                    }
                }
                    
            });
            for (var i = 0; i < outlets.length; i++) {
                if ((outletIds == undefined || outletIds.length == 0 || outletIds.indexOf(outlets[i].Id) == -1) && $scope.routeNames[outlets[i].Id]!=undefined && $scope.routeNames[outlets[i].Id]!='') {
                    
                    $scope.AccountList.push({
                        Id: outlets[i].Id,
                        Name: outlets[i].Name,
                        Outlet_ID__c: outlets[i].Outlet_ID__c,
                        Route__c: $scope.routeNames[outlets[i].Id],
                        Area_Outlet_Location__c: outlets[i].Area_Outlet_Location__c
                    });
                }
            }
            $ionicLoading.hide();
        }, 500);
        $scope.ifCalled = false;
        /*$scope.justSelectAllOutlets = function() {
            //$scope.selectAll = !$scope.selectAll;
            if (!$scope.ifCalled) {
                $scope.ifCalled = true;
                $scope.selectedAccountsDisplay = [];
                angular.forEach($scope.AccountList, function(record, key) {
                    if ($scope.selectAll == false) {
                        record.selected = false;
                    } else {
                        record.selected = true;
                        $scope.selectedAccountsDisplay.push(record);
                    }
                });
                $timeout(function() {
                    $scope.ifCalled = false
                }, 2000);
            }
        }
        $scope.selectAllOutlets = function() {
            if (!$scope.ifCalled) {
                $scope.ifCalled = true;
                $scope.selectAll = !$scope.selectAll;
                $scope.selectedAccountsDisplay = [];
                angular.forEach($scope.AccountList, function(record, key) {
                    if ($scope.selectAll == false) {
                        record.selected = false;
                    } else {
                        record.selected = true;
                        $scope.selectedAccountsDisplay.push(record);
                    }
                });
                $timeout(function() {
                    $scope.ifCalled = false
                }, 2000);
            }
        }
        $scope.selectRow = function(kk) {
            var checkAllSelcted = true;
            $scope.selectedAccountsDisplay = [];
            angular.forEach($scope.AccountList, function(record, key) {
                if (record.Id == kk) {
                    record.clicked = true;
                    record.selected = !record.selected;
                } else {
                    record.clicked = false;
                }
                if (record.selected == true) {
                    $scope.selectedAccountsDisplay.push(record);
                } else {
                    $scope.selectAll = false;
                    checkAllSelcted = false;
                }
            });
            if (checkAllSelcted) {
                $scope.selectAll = true;
            }
        }
        $scope.justSelect = function(kk) {
            var checkAllSelcted = true;
            $scope.selectedAccountsDisplay = [];
            angular.forEach($scope.AccountList, function(record, key) {
                if (record.Id == kk) {
                    record.clicked = true;
                } else {
                    record.clicked = false;
                }
                if (record.selected == true) {
                    $scope.selectedAccountsDisplay.push(record);
                } else {
                    $scope.selectAll = false;
                    checkAllSelcted = false;
                }
            });
            if (checkAllSelcted) {
                $scope.selectAll = true;
            }
        }*/
        $scope.selectedAccountIds={};
        $scope.selectRow = function(kk) {
            if($scope.selectedAccountIds[kk]==undefined)
                $scope.selectedAccountIds[kk]=true;
            else
                $scope.selectedAccountIds[kk]=!$scope.selectedAccountIds[kk];
            $scope.justSelect(kk);
        }
        $scope.justSelect = function(kk) {
            if($scope.selectedAccountIds[kk])
                $scope.isSelected=true;
        }
        $scope.selectedAccountIds=[];
        $scope.AddRoutes = function() {
            $scope.isSelected = false;
            $rootScope.selectedAccounts = [];
            angular.forEach($scope.AccountList, function(record, key) {
                if ($scope.selectedAccountIds[record.Id]==true) {
                    $rootScope.selectedAccounts.push(record);
                    $scope.isSelected = true;
                }
            });
            if ($scope.isSelected == true) $location.path('app/reason/' + new Date());
            else Splash.ShowToast('You must select at least one outlet to be added.', 'long', 'bottom', function(a) {
                console.log(a)
            });
        }
        $scope.CancelAddRoutes = function() {
            $rootScope.selectedAccounts = [];
            //$location.path('app/today/' + new Date());
            $rootScope.goToHome();
        }
        //$ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('AddNewCtrlError', err.stack + '::' + err.message);
    }
}).controller('ReasonCtrl', function($scope, $stateParams, $ionicPopup, $ionicSideMenuDelegate, $rootScope, $q, $window, $ionicModal, $location, $state, $timeout, $interval, $ionicLoading, $log, $filter, $cordovaCamera, SalesforceSession, FETCH_DATA, FETCH_DAILY_PLAN_ACCOUNTS, MOBILEDATABASE_ADD, FETCH_DATA_LOCAL, WEBSERVICE_ERROR) {
    try {
        $scope.addAccounts = {};
        $rootScope.backText = 'Back';
        $rootScope.showBack = true;
        $rootScope.params = $stateParams;
        $rootScope.name = "ReasonCtrl";
        var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
        $scope.showAccounts = true;
        $scope.Accounts = [];
        $scope.Accounts = $rootScope.selectedAccounts;
        $scope.routeNames=[];
        var queryVariable = '';
        var tempAccIds=[];
        angular.forEach($scope.Accounts, function(record, key) {
            tempAccIds.push(record.Id);
            if (queryVariable == '') {
                queryVariable = queryVariable + "{Route_Assignment__c:Account__c} = '" + record.Id + "'";
            } else {
                queryVariable = queryVariable + " OR {Route_Assignment__c:Account__c} = '" + record.Id + "'";
            }
        });
        //var routeAssignment = FETCH_DATA_LOCAL('Route_Assignment__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Route_Assignment__c:_soup} FROM {Route_Assignment__c} WHERE " + queryVariable+" and {Route_Assignment__c:Removed__c}='false' ", 100000));
        var routeAssignment = FETCH_DATA_LOCAL('Route_Assignment__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Route_Assignment__c:_soup} FROM {Route_Assignment__c} WHERE {Route_Assignment__c:Removed__c}='false' ", 100000));
        angular.forEach(routeAssignment.currentPageOrderedEntries, function(record, key) {
                var tempExtId=record[0]['External_Id__c'];
                tempExtId=tempExtId.toLowerCase();
                if(tempAccIds[record[0].Account__c]!=undefined){
                    if(tempExtId.indexOf($rootScope.UserId)>-1 && ($scope.routeNames[record[0].Account__c]==undefined || $scope.routeNames[record[0].Account__c]=='Sunday')){
                        var tempRouteName=record[0].Route_Name__c;
                        tempRouteName=tempRouteName.replace("_"+$rootScope.myUserName, "");
                        tempRouteName=tempRouteName.substring(tempRouteName.indexOf('_')+1);
                        $scope.routeNames[record[0].Account__c]=tempRouteName;
                    }
                }
                
        });
        $scope.confirmOutletAdd = function() {
            if ($scope.addAccounts.Reason != undefined && $scope.addAccounts.Reason != '') {
                $ionicLoading.show({
                    template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/>'
                });
                var routeplans = [];
                var routeAssignments = [];
                var today = $filter('date')(new Date(), 'yyyy-MM-dd');
                var queryVariable = '';
                angular.forEach($scope.Accounts, function(record, key) {
                    if (queryVariable == '') {
                        queryVariable = queryVariable + "{Route_Assignment__c:Account__c} = '" + record.Id + "'";
                    } else {
                        queryVariable = queryVariable + " OR {Route_Assignment__c:Account__c} = '" + record.Id + "'";
                    }
                });
                if (queryVariable != '') {
                    var routeOrder = 0;
                    if ($rootScope.Outlets.length > 0) routeOrder = $rootScope.Outlets[($rootScope.Outlets.length - 1)].RoutePlan.Order__c;
                    var d = new Date();
                    var currentMonth = $rootScope.MonthsList[d.getMonth()];
                    var currentYear = d.getFullYear();
                    var rtList = FETCH_DATA_LOCAL('DB_RecordTypes', sfSmartstore.buildSmartQuerySpec("SELECT  {DB_RecordTypes:_soup} FROM {DB_RecordTypes} WHERE {DB_RecordTypes:SobjectType}='Route_Plan__c' AND {DB_RecordTypes:Name}='Route Plan Line Item'", 1));
                    var routePlanRTId = FETCH_DATA_LOCAL('DB_RecordTypes', sfSmartstore.buildSmartQuerySpec("SELECT  {DB_RecordTypes:_soup} FROM {DB_RecordTypes} WHERE {DB_RecordTypes:SobjectType}='Route_Plan__c' AND {DB_RecordTypes:Name}='Route Plan'", 1));
                    var routeAssignment = FETCH_DATA_LOCAL('Route_Assignment__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Route_Assignment__c:_soup} FROM {Route_Assignment__c} WHERE (" + queryVariable+") and {Route_Assignment__c:Removed__c}='false' ", 100000));
                    var routeplanRecord = FETCH_DATA_LOCAL('Route_Plan__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Route_Plan__c:_soup} FROM {Route_Plan__c} WHERE  {Route_Plan__c:Month__c}='" + currentMonth + "' AND {Route_Plan__c:Year__c}='" + currentYear + "' AND {Route_Plan__c:RecordTypeId}='" + routePlanRTId.currentPageOrderedEntries[0][0].Id + "'", 1));
                    var routeAssignmentList = [];
                    angular.forEach(routeAssignment.currentPageOrderedEntries, function(record, key) {
                        var tempExtId=record[0]['External_Id__c'];
                        tempExtId=tempExtId.toLowerCase();
                        if(tempExtId.indexOf($rootScope.UserId)>-1)
                        angular.forEach($rootScope.selectedAccounts, function(rec, ke) {
                            if (record[0].Account__c == rec.Id && routeAssignmentList.indexOf(rec.Id) == -1) {
                                routeAssignmentList.push(rec.Id);
                                var routePlan = {
                                    Route_Outlet__c: record[0].Id,
                                    Id: record[0].Account__c + record[0].Id,
                                    External_Id__c: (new Date().getTime())+'-'+record[0].Account__c+'-'+$window.Math.random() * 100000000,
                                    Planned_Date__c: today,
                                    Visit_Type__c: 'Market visit',
                                    Status__c: 'New',
                                    Reason__c: $scope.addAccounts.Reason,
                                    Order__c: ++routeOrder,
                                    IsDirty: true
                                };
                                routePlan.Id = routePlan.External_Id__c;
                                if (routeplanRecord != undefined && routeplanRecord.currentPageOrderedEntries != undefined && routeplanRecord.currentPageOrderedEntries.length > 0 && routeplanRecord.currentPageOrderedEntries[0][0].Id != undefined) {
                                    routePlan['Route_Plan__c'] = routeplanRecord.currentPageOrderedEntries[0][0].Id;
                                }
                                if (rtList != undefined && rtList.currentPageOrderedEntries != undefined && rtList.currentPageOrderedEntries[0][0].Id != undefined) {
                                    routePlan['RecordTypeId'] = rtList.currentPageOrderedEntries[0][0].Id;
                                }
                                $log.debug('routePlan' + JSON.stringify(routePlan));
                                routeplans.push(routePlan);
                            }
                        });
                    });
                    if (routeplans.length > 0) {
                        MOBILEDATABASE_ADD('Route_Plan__c', routeplans, 'External_Id__c');
                    }
                }
                $timeout(function() {
                    //$ionicLoading.hide();
                    $rootScope.goToHome();
                }, 3000);
            } else {
                Splash.ShowToast('Please enter a reason for adding outlets to today\'s visit.', 'long', 'bottom', function(a) {
                    console.log(a)
                });
            }
        }
        $scope.CancelAddRoutes = function() {
            $rootScope.selectedAccounts = [];
            $location.path('app/AddOutlets/' + $stateParams.refresh);
        }
        $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('ReasonCtrlError', err.stack + '::' + err.message);
    }
}).controller('RemoveReasonCtrl', function($scope, $stateParams, $rootScope, $window, $location, $timeout, $ionicLoading, $log, $filter, SalesforceSession,FETCH_DATA_LOCAL, MOBILEDATABASE_ADD, WEBSERVICE_ERROR) {
    try {
        $scope.remove = {};
        $scope.selectedAccountToRemove = {};
        
        angular.forEach($rootScope.Outlets, function(record, key) {
            if (record.Id == $stateParams.accountId) {
                $scope.selectedAccountToRemove = record;
            }
        });
        var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
        $scope.routeNames=[];
        var routeAssignment = FETCH_DATA_LOCAL('Route_Assignment__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Route_Assignment__c:_soup} FROM {Route_Assignment__c} WHERE {Route_Assignment__c:Account__c} ='"+$scope.selectedAccountToRemove.Id+"' and {Route_Assignment__c:Removed__c}='false'" , 100000));
        angular.forEach(routeAssignment.currentPageOrderedEntries, function(record, key) {
                var tempExtId=record[0]['External_Id__c'];
                tempExtId=tempExtId.toLowerCase();
                if(tempExtId.indexOf($rootScope.UserId)>-1 && ($scope.routeNames[record[0].Account__c]==undefined || $scope.routeNames[record[0].Account__c]=='Sunday')){
                    var tempRouteName=record[0].Route_Name__c;
                    tempRouteName=tempRouteName.replace("_"+$rootScope.myUserName, "");
                    tempRouteName=tempRouteName.substring(tempRouteName.indexOf('_')+1);
                    $scope.routeNames[record[0].Account__c]=tempRouteName;
                }
                
        });
        $rootScope.params = $stateParams;
        $rootScope.name = "RemoveReasonCtrl";
        $scope.confirmOutletRemove = function() {
            if ($scope.remove.Reason != undefined && $scope.remove.Reason != '') {
                $ionicLoading.show({
                    template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/>'
                });
                var deleteRouteplan = [];

                if ($scope.selectedAccountToRemove.RoutePlan != undefined) {
                    var routeplanRecord = $scope.selectedAccountToRemove.RoutePlan;
                    routeplanRecord.Status__c = 'Removed';
                    routeplanRecord.Reason__c = $scope.remove.Reason;
                    routeplanRecord.IsDirty = true;
                    if (routeplanRecord.External_Id__c == undefined) routeplanRecord.External_Id__c = (new Date().getTime())+'_'+$window.Math.random() * 100000000;
                    deleteRouteplan.push(routeplanRecord);
                    MOBILEDATABASE_ADD('Route_Plan__c', deleteRouteplan, 'External_Id__c');
                }
                $timeout(function() {
                    $ionicLoading.hide();
                    $rootScope.goToHome();
                }, 3000);
            } else {
                Splash.ShowToast('Please Specify reason for removing outlet.', 'long', 'bottom', function(a) {
                    console.log(a);
                });
            }
        }
        $scope.CancelAddRoutes = function() {
            $rootScope.selectedAccounts = [];
            $rootScope.goToHome();
        }
        $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('RemoveReasonCtrlError', err.stack + '::' + err.message);
    }
}).controller('TodayCtrl', function($scope, $stateParams, $ionicSlideBoxDelegate, $ionicPopup, $ionicSideMenuDelegate, $rootScope, $q, $window, $ionicModal, $location, $state, $timeout, $interval, $ionicLoading, $log, $filter, $cordovaCamera, SalesforceSession, FETCH_DATA, FETCH_DAILY_PLAN_ACCOUNTS, MOBILEDATABASE_ADD, FETCH_DATA_LOCAL, SOUP_EXISTS, FETCH_REMOTE_DATA, WEBSERVICE_ERROR,SMARTSTORE,WEBSERVICE,GEO_LOCATION,GEO_LOCATION_ACCOUNT) {
    try {

        $ionicLoading.show({
            template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
        });
        $scope.showMessage = '';
        $rootScope.params = $stateParams;
        $rootScope.name = "TodayCtrl";
        var todayDateVal = new Date();
        $scope.TodayDate = $filter('date')(todayDateVal, 'EEE, dd MMM yy');
        todayDateVal.setDate(todayDateVal.getDate() + 1);
        $scope.TomorrowDate = $filter('date')(todayDateVal, 'EEE, dd MMM yy');
        todayDateVal.setDate(todayDateVal.getDate() + 1);
        $scope.DayAfterDate = $filter('date')(todayDateVal, 'EEE, dd MMM yy');
        $scope.AccountImages = [];
        $scope.SlidesList = [];
        $scope.toastSycn = false;
        $scope.incompleteSync=false;
        $scope.incompleteSyncText='';
        $scope.showNoOutletsMessage = false;
        var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
        var flow = FETCH_DATA_LOCAL('Db_process_flow', sfSmartstore.buildSmartQuerySpec("SELECT  {Db_process_flow:_soup} FROM {Db_process_flow} " , 100000));
        $scope.fields=[];
        $scope.msToTime = function(duration) {
            var milliseconds = parseInt((duration%1000)/100)
                , seconds = parseInt((duration/1000)%60)
                , minutes = parseInt((duration/(1000*60))%60)
                , hours = parseInt((duration/(1000*60*60))%24);
    
            /*hours = (hours < 10) ? "0" + hours : hours;
            minutes = (minutes < 10) ? "0" + minutes : minutes;
            seconds = (seconds < 10) ? "0" + seconds : seconds;*/
            if(hours < 1)
                return minutes + " minutes ";
            else
                return hours + " hours " + minutes + " minutes ";
            
        }
        $scope.updateLastSynced = function(){
            var webserviceList = FETCH_DATA_LOCAL('Db_webservice', sfSmartstore.buildSmartQuerySpec("SELECT  {Db_webservice:_soup} FROM {Db_webservice} WHERE {Db_webservice:Webservice}='FetchObjectsSoupsAndRecordsServiceNewAPI' ORDER BY {Db_webservice:lastSynced} desc",1));
            var recordWebservice = null;
            angular.forEach(webserviceList.currentPageOrderedEntries, function(record, key) {
                recordWebservice = record[0];
            });
            var webserviceListImg = FETCH_DATA_LOCAL('Db_webservice', sfSmartstore.buildSmartQuerySpec("SELECT  {Db_webservice:_soup} FROM {Db_webservice} WHERE {Db_webservice:Webservice}='FetchVisitSummaryAttachments' ORDER BY {Db_webservice:lastSynced} desc",1));
            var recordWebserviceImg = null;
            angular.forEach(webserviceListImg.currentPageOrderedEntries, function(record, key) {
                recordWebserviceImg = record[0];
            });
            var today_now = $filter('date')(new Date(), 'MM/dd/yyyy hh:mm a');
            if(recordWebservice==null || recordWebserviceImg==null || recordWebservice.name!=undefined || recordWebserviceImg.name!=undefined){
                $scope.incompleteSync=true;
                $scope.incompleteSyncText='Incomplete data.<br>Please Sync';
            }else{
                var tempSyncDate=recordWebservice.lastSynced;
                var tempSyncDateArray=tempSyncDate.split("/");
                tempSyncDate=tempSyncDateArray[1]+"/"+tempSyncDateArray[0]+"/"+tempSyncDateArray[2];
                var diff=new Date(today_now).getTime()-new Date(tempSyncDate).getTime();
                $scope.incompleteSync=true;
                //$scope.incompleteSyncText='<b>Last Synced</b><br/><b>'+$scope.msToTime(diff)+' ago</b>';
                $scope.incompleteSyncText='<b>Last Synced</b><br/><b>'+recordWebservice.lastSynced+'</b>';
            }

            //exception error handler
            if ($rootScope.errorException['Error'] != null && $scope.toastSycn == false) {
                $scope.toastSycn = true;
                $scope.incompleteSync=true;
                $scope.incompleteSyncText='Incomplete Sync. <br>Please sync again.';
            }

            //Account Error handler
            if ($rootScope.errorAccount['Error'] != null && $scope.toastSycn == false) {
                $scope.toastSycn = true;
                $scope.incompleteSync=true;
                $scope.incompleteSyncText='Incomplete Sync. <br>Please sync again.';
            }

            //Upload error handler
            if ($rootScope.errorUpload['Error'] != null && $scope.toastSycn == false) {
                $scope.toastSycn = true;
                $scope.incompleteSync=true;
                $scope.incompleteSyncText='Incomplete Sync. <br>Please sync again.';
            }

            //Route Plan Error handler
            if ($rootScope.errorRoutPlan['Error'] != null && $scope.toastSycn == false) {
                $scope.toastSycn = true;
                $scope.incompleteSync=true;
                $scope.incompleteSyncText='Incomplete Sync. <br>Please sync again.';
            }

            //Visit error handler
            if ($rootScope.errorVisit['Error'] != null && $scope.toastSycn == false) {
                $scope.toastSycn = true;
                $scope.incompleteSync=true;
                $scope.incompleteSyncText='Incomplete Sync. <br>Please sync again.';
            }

            //Visit Summary Query error handler
            if ($rootScope.errorQuerySummary['Error'] != null && $scope.toastSycn == false) {
                $scope.toastSycn = true;
                $scope.incompleteSync=true;
                $scope.incompleteSyncText='Data pending. <br>Please sync again.';
                $rootScope.errorQuerySummary = [];
            }

            //Visit Summary error handler
            if ($rootScope.errorVisitSummary['Error'] != null && $scope.toastSycn == false) {
                $scope.toastSycn = true;
                $scope.incompleteSync=true;
                $scope.incompleteSyncText='Incomplete Sync. <br>Please sync again.';
            }

            //Order Error Handler
            if ($rootScope.errorOrder['Error'] != null && $scope.toastSycn == false) {
                $scope.toastSycn = true;
                $scope.incompleteSync=true;
                $scope.incompleteSyncText='Incomplete Sync. <br>Please sync again.';
            }

            //Attachement Error handler
            if ($rootScope.errorAttachment['Error'] != null && $scope.toastSycn == false) {
                $scope.toastSycn = true;
                $scope.incompleteSync=true;
                $scope.incompleteSyncText='Incomplete Sync. <br>Please sync again.';
            }
            $scope.toastSycn = false;
        }
        $scope.goToMap = function(outletId) {
            $log.debug('outletId == ' + outletId);
            if (navigator && navigator.connection && navigator.connection.type != 'none') {
                if (outletId == 'All') $location.path('app/map/');
                else $location.path('app/map/' + outletId);
            } else {
                Splash.ShowToast('Internet connectivity is required for this feature.', 'long', 'center', function(a) {
                    console.log(a)
                });
            }
        }
        
        $scope.getPlan = function(day) {
            $ionicLoading.show({
                    template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
                });
            $timeout(function() {
                
                $rootScope.day = day;
                // $log.debug('debug;;;;;;'+day);          
                $rootScope.Outlets = [];
                $scope.fields=[];
                var flow = FETCH_DATA_LOCAL('Db_process_flow', sfSmartstore.buildSmartQuerySpec("SELECT  {Db_process_flow:_soup} FROM {Db_process_flow} where {Db_process_flow:Removed__c}='false' ", 100000));
                angular.forEach(flow.currentPageOrderedEntries, function(record, key) {
                    var temp = FETCH_DATA_LOCAL('DB_fields', sfSmartstore.buildSmartQuerySpec("SELECT  {DB_fields:_soup} FROM {DB_fields} where {DB_fields:Process_Flow__c} = '"+record[0]['Id']+"' and {DB_fields:Removed__c}='false' order by {DB_fields:Order__c} " , 100000));
                    $scope.fields[record[0]['Outlet_Type__c']]=temp.currentPageOrderedEntries;
                });
                if (day == 'today') {
                    var todayRoutePlan = FETCH_DAILY_PLAN_ACCOUNTS(day);
                    var slideNotoShow = 0;
                    if (todayRoutePlan.length > 0) {
                        var today = $filter('date')(new Date(), 'dd/MM/yyyy');
                        var transactionsList = [];
                        if (SOUP_EXISTS.soupExists('Db_transaction')) {
                            var completedTasks = FETCH_DATA.querySoup('Db_transaction', 'entryDate', today);
                            if (completedTasks.currentPageOrderedEntries != undefined && completedTasks.currentPageOrderedEntries.length != 0) {
                                angular.forEach(completedTasks.currentPageOrderedEntries, function(record, key) {
                                    transactionsList[record.Account] = record;
                                });
                            }
                        }

                        console.log('transactionsList RAW');
                        console.log(transactionsList);

                        var todayRoutePlan = FETCH_DAILY_PLAN_ACCOUNTS(day);
                        var todayRoutePlanwithStatus = [];
                        angular.forEach(todayRoutePlan, function(record, key) {
                            var accountRecord = record;
                            accountRecord.isNext = false;
                            if (transactionsList[accountRecord.Id] == undefined) {
                                accountRecord.isCompleted = 'Not started';
                            } else {
                                accountRecord.isCompleted = transactionsList[accountRecord.Id].status;
                            }
                            todayRoutePlanwithStatus.push(accountRecord);
                        });
                        var isAllCompleted = true;
                        for (var i = 0; i < todayRoutePlanwithStatus.length; i++) {
                            if (todayRoutePlanwithStatus[i].isCompleted == 'Not started') {
                                todayRoutePlanwithStatus[i].isNext = true;
                                slideNotoShow = i;
                                isAllCompleted = false;
                                break;
                            }
                        }
                        console.log('todayRoutePlanwithStatus RAW');
                        console.log(todayRoutePlanwithStatus);

                        for (var i = 0; i < todayRoutePlanwithStatus.length; i++) {
                            $rootScope.Outlets.push(todayRoutePlanwithStatus[i]);
                        }
                        console.log('$rootScope.Outlets RAW');
                        console.log($rootScope.Outlets);
                        
                        var completedTasks = FETCH_DATA.querySoup('Db_transaction', 'entryDate', null);
                        $rootScope.unsyncedAccounts = [];
                        var myVisits = FETCH_DATA.querySoup('Visit__c', 'IsDirty', true);
                        if (myVisits.currentPageOrderedEntries != undefined && myVisits.currentPageOrderedEntries.length != 0) {
                            angular.forEach(myVisits.currentPageOrderedEntries, function(record, key) {
                                $rootScope.unsyncedAccounts[record.Account__c] = 'done';
                            });
                        }
                        console.log(' ##### $rootScope.unsyncedAccounts');
                        console.log($rootScope.unsyncedAccounts);
                    }
                } else {
                    $rootScope.Outlets = FETCH_DAILY_PLAN_ACCOUNTS(day);
                }
                $scope.SlidesList = [];
                if ($rootScope.Outlets.length > 0) {
                    var slidesLength = parseInt($rootScope.Outlets.length / 4);
                    if ($rootScope.Outlets.length % 4 != 0) {
                        slidesLength = slidesLength + 1;
                    }
                    for (var i = 0; i < slidesLength; i++) {
                        $scope.SlidesList.push(i);
                    }
                    var queryVariable = '';
                    angular.forEach($rootScope.Outlets, function(record, key) {
                        $scope.AccountImages[record.Id] = './img/capture.png';
                        if (queryVariable == '') {
                            queryVariable = queryVariable + "{Db_images:ParentId} = '" + record.Id + "'";
                        } else {
                            queryVariable = queryVariable + " OR {Db_images:ParentId} = '" + record.Id + "'";
                        }
                    });
                    var imagesLocal = FETCH_DATA_LOCAL('Db_images', sfSmartstore.buildSmartQuerySpec("SELECT  {Db_images:_soup} FROM {Db_images} WHERE ( " + queryVariable + ") and not({Db_images:Name} like '%gallery%')", 100000));
                    angular.forEach(imagesLocal.currentPageOrderedEntries, function(record, key) {
                        if(record[0]['Name']!=undefined && record[0]['Name'].indexOf('gallery-')==-1)
                            $scope.AccountImages[record[0].ParentId] = "data:image/jpeg;base64," + record[0].Body;
                    });
                    try{
                        if (day == 'today') $scope.getUploads();
                    }catch(err){
                        $ionicLoading.hide();
                    }
                }
                $timeout(function() {
                    $ionicSlideBoxDelegate.update();
                    $ionicSlideBoxDelegate.slide(0, 200);
                    if (slideNotoShow != 0 && day == "today") {
                        $ionicSlideBoxDelegate.slide(parseInt(slideNotoShow / 4), 500)
                    }
                    $scope.updateLastSynced();
                    $ionicLoading.hide();
                }, 1000);
                var pendingTasks = FETCH_DATA.querySoup('Db_transaction', 'status', 'pending');
                if (day == 'today' && isAllCompleted == true && pendingTasks.currentPageOrderedEntries.length == 0) {
                    Splash.ShowToast('You have completed all calls for the day.', 'long', 'bottom', function(a) {
                        console.log(a)
                    });
                }
                //console.log($rootScope.errorArray);
                //console.log($rootScope.errorArray['Error'] != null);
                //console.log($rootScope.errorArray[0].length != 0);

                //if($rootScope.errorArray['Error'] != null && $rootScope.errorArray['Error'] != undefined){
                if ($scope.toastSycn == true) {
                    //console.log($rootScope.errorArray);
                    Splash.ShowToast('Data partially synced, please sync again.', 'long', 'top', function(a) {
                        console.log(a)
                    });
                }

                if ($rootScope.Outlets.length < 1) {
                    $scope.showNoOutletsMessage = true;
                    Splash.ShowToast('You have no outlet visits scheduled.', 'long', 'bottom', function(a) {
                        console.log(a)
                    });
                } else {
                    $scope.showNoOutletsMessage = false;
                }
            }, 1000);
        }
        if (typeof Splash !== "undefined" && Splash != undefined && Splash.isActive() == 1) {
            if($rootScope.progressBarCount<85)
            $rootScope.progressBarCount = $rootScope.progressBarCount + 10;
            Splash.setProgressText($rootScope.progressBarCount, 'Initializing...');
            $timeout(function() {
                if (navigator && navigator.connection && navigator.connection.type != 'none') {
                    $q.all({
                        Status: FETCH_REMOTE_DATA()
                    }).then(function(response) {
                        Splash.setProgressText(90, 'Launching Application...');
                        $log.debug('response===' + JSON.stringify(response));
                        if (response.Status != undefined) {
                            $scope.getPlan('today');
                            $timeout(function() {
                                if (SOUP_EXISTS.soupExists('Db_transaction') == true) {
                                    var pendingTasks = FETCH_DATA.querySoup('Db_transaction', 'status', 'pending');
                                    if (pendingTasks.currentPageOrderedEntries != undefined && pendingTasks.currentPageOrderedEntries.length != 0) {
                                        $rootScope.pendingTask = true;
                                    } else {
                                        $rootScope.pendingTask = false;
                                    }
                                }
                                if (typeof Splash !== "undefined" && Splash != undefined && Splash.isActive() == 1) Splash.Complete();
                                var userDetails = FETCH_DATA_LOCAL('User', sfSmartstore.buildSmartQuerySpec("SELECT  {User:_soup} FROM {User} ", 100000));
                                angular.forEach(userDetails.currentPageOrderedEntries, function(record, key) {
                                    $rootScope.myUserName = record[0].Name;
                                    $rootScope.Username = record[0].Username;
                                    var tempUid = record[0].User_ID__c;
                                    $rootScope.UserId = tempUid.toLowerCase();
                                });
                            }, 2000);
                        }
                    }, function(error) {
                        $timeout(function() {
                            if (typeof Splash !== "undefined" && Splash != undefined && Splash.isActive() == 1) Splash.Complete();
                            if (SOUP_EXISTS.soupExists('Db_transaction') == true) {
                                var pendingTasks = FETCH_DATA.querySoup('Db_transaction', 'status', 'pending');
                                if (pendingTasks.currentPageOrderedEntries != undefined && pendingTasks.currentPageOrderedEntries.length != 0) {
                                    $rootScope.pendingTask = true;
                                } else {
                                    $rootScope.pendingTask = false;
                                }
                            }
                        }, 2000);
                    });
                } else {
                    if (SOUP_EXISTS.soupExists('Account') == true && SOUP_EXISTS.soupExists('Route_Plan__c') == true && SOUP_EXISTS.soupExists('Route_Assignment__c') == true && SOUP_EXISTS.soupExists('Db_process_flow') == true) {
                        $scope.getPlan('today');
                        $scope.showMessage = false;
                    } else {
                        Splash.ShowToast('Internet connection is required to download user details.', 'long', 'bottom', function(a) {
                            console.log(a)
                        });
                        $scope.showMessage = true;
                        $scope.updateLastSynced();
                        $ionicLoading.hide();
                    }
                    Splash.setProgressText(90, 'Launching Application...');
                    $timeout(function() {
                        var pendingTasks = FETCH_DATA.querySoup('Db_transaction', 'status', 'pending');
                        if (pendingTasks.currentPageOrderedEntries != undefined && pendingTasks.currentPageOrderedEntries.length != 0) {
                            $rootScope.pendingTask = true;
                        } else {
                            $rootScope.pendingTask = false;
                        }
                        if (typeof Splash !== "undefined" && Splash != undefined && Splash.isActive() == 1) Splash.Complete();
                    }, 500);
                }
            }, 2000);
        } else {
            if (SOUP_EXISTS.soupExists('Account') == true && SOUP_EXISTS.soupExists('Route_Plan__c') == true && SOUP_EXISTS.soupExists('Route_Assignment__c') == true && SOUP_EXISTS.soupExists('Db_process_flow') == true) {
                $scope.getPlan('today');
                $scope.showMessage = false;
            } else {
                $scope.showMessage = true;
            }
        }
        $scope.takePicture = function(accId) {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.CAMERA,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 300,
                targetHeight: 300,
                saveToPhotoAlbum: false
            };
            $cordovaCamera.getPicture(options).then(function(imageData) {
                $scope.attachment = [{
                    ParentId: accId,
                    Name: new Date(),
                    Body: imageData,
                    IsDirty: true,
                    External_Id__c: accId,
                }];
                
                GEO_LOCATION_ACCOUNT.getCurrentPosition().then(function(position) {
                    $log.info('LOCATION' + JSON.stringify(position));
                    var latitude = position.latitude;
                    var longitude = position.longitude;
                    var currentAccount = {};
                    var currentAccRecord = FETCH_DATA.querySoup('Account', 'Id', accId);
                    if(latitude != null && latitude != 0.0 && Math.round(latitude) != 0 && latitude != undefined){
                    
                        currentAccount=currentAccRecord.currentPageOrderedEntries[0];
                        currentAccount.Location__Latitude__s =latitude;
                        currentAccount.Location__Longitude__s = longitude;
                        currentAccount.IsDirty = true;
                        var accountToUpate = [];
                        accountToUpate.push(currentAccount);
                        //update account lat long info
                        MOBILEDATABASE_ADD('Account', accountToUpate, 'Id');
                        
                        $scope.attachment[0].External_Id__c = accId+'_'+latitude+'_'+longitude;
                        //$log.info($scope.attachment);
                        //Start whether to update image with location or dont save the image at all
                        MOBILEDATABASE_ADD('Db_images', $scope.attachment, 'ParentId');

                        $scope.AccountImages[accId] = "data:image/jpeg;base64," + imageData;
                        //End whether to update image with location or dont save the image at all
                        //User log
                        var resultVis = 'Geo tag for this outlet '+accId+' is '+ latitude +','+longitude;
                        $log.info(resultVis);
                        WEBSERVICE_ERROR('GeoTagAccountActivity_POST', resultVis+'');

                    }else{
                        Splash.ShowToast('Unable to capture geo-location. Please enable Location and try again', 'long', 'bottom', function(a) {
                         });
                    }

                }, function(error) {
                    $log.error('ERROR' + JSON.stringify(error));
                });
            }, function(err) {
                $log.error('ERROR' + JSON.stringify(err));
            });
        }
        $scope.confirmDeleteOld = function(outletId) {
            if ($scope.remove.Reason != '') {
                $scope.selectedAccountToRemove = {};
                angular.forEach($rootScope.Outlets, function(record, key) {
                    if (record.Id == outletId) {
                        $scope.selectedAccountToRemove = record;
                    }
                });
                var deleteRouteplan = [];
                if ($scope.selectedAccountToRemove.RoutePlan != undefined) {
                    var routeplanRecord = $scope.selectedAccountToRemove.RoutePlan;
                    routeplanRecord.Status__c = 'Removed';
                    routeplanRecord.Reason__c = $scope.remove.Reason;
                    routeplanRecord.IsDirty = true;
                    if (routeplanRecord.External_Id__c == undefined) routeplanRecord.External_Id__c = (new Date().getTime())+'_'+$window.Math.random() * 100000000;
                    deleteRouteplan.push(routeplanRecord);
                    MOBILEDATABASE_ADD('Route_Plan__c', deleteRouteplan, 'External_Id__c');
                }
                $timeout(function() {
                    $rootScope.goToHome();
                }, 1000);
                $scope.confirmDeletePopup.close();
            } else {
                Splash.ShowToast('Please Specify reason for removing outlet', 'long', 'center', function(a) {
                    console.log(a)
                });
            }
        }
        $scope.confirmDelete = function(outletId) {
            $location.path('app/removeReason/' + outletId);
            $scope.confirmDeletePopup.close();
        }
        $scope.cancelConfirmDelete = function(outletId) {
            $scope.confirmDeletePopup.close();
        }
        $scope.confirmAddBack = function(outletId) {
            if ($scope.remove.Reason != '') {
                $scope.selectedAccountToRemove = {};
                angular.forEach($rootScope.Outlets, function(record, key) {
                    if (record.Id == outletId) {
                        $scope.selectedAccountToRemove = record;
                    }
                });
                var deleteRouteplan = [];
                if ($scope.selectedAccountToRemove.RoutePlan != undefined) {
                    var routeplanRecord = $scope.selectedAccountToRemove.RoutePlan;
                    routeplanRecord.Status__c = 'Added';
                    routeplanRecord.Reason__c = $scope.remove.Reason;
                    routeplanRecord.IsDirty = true;
                    if (routeplanRecord.External_Id__c == undefined) routeplanRecord.External_Id__c = (new Date().getTime())+'_'+$window.Math.random() * 100000000;
                    deleteRouteplan.push(routeplanRecord);
                    MOBILEDATABASE_ADD('Route_Plan__c', deleteRouteplan, 'External_Id__c');
                }
                $timeout(function() {
                    $rootScope.goToHome();
                }, 1000);
                $scope.confirmAddBackPopup.close();
            } else {
                Splash.ShowToast('Please Specify reason for adding outlet', 'long', 'center', function(a) {
                    console.log(a)
                });
            }
        }
        $scope.cancelConfirmAddBack = function(outletId) {
            $scope.confirmAddBackPopup.close();
        }
        $scope.onSwipeup = function(outletId) {
            if ($rootScope.day == 'today') {
                var canDelete = false;
                $scope.removeOutletName = '';
                angular.forEach($rootScope.Outlets, function(record, key) {
                    if (record.Id == outletId && record.isCompleted == 'Not started') {
                        canDelete = true;
                        $scope.removeOutletName = record.Name;
                    }
                });
                if (canDelete == true) {
                    $scope.confirmDeletePopup = $ionicPopup.confirm({
                        template: 'Are you sure you don\'t want to visit ' + $scope.removeOutletName + ' from today\'s route plan?  <br/><br/><br/> <div style="text-align:right; color:#38B6CB; margin-right: 10px; font-weight: bold;"><span my-touchstart="cancelConfirmDelete(\'' + outletId + '\')">NO</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span  my-touchstart="confirmDelete(\'' + outletId + '\')">YES</span> </div>',
                        cssClass: 'resumeConfirm',
                        title: '',
                        scope: $scope,
                        buttons: []
                    });
                } else {
                    var pendingTasks = FETCH_DATA.querySoup('Db_transaction', 'status', 'pending');
                    if (pendingTasks.currentPageOrderedEntries != undefined && pendingTasks.currentPageOrderedEntries.length > 0) {
                        if (pendingTasks.currentPageOrderedEntries[0].Account == outletId) {
                            Splash.ShowToast('You have already started the call for this outlet. It cannot be removed from today\'s plan', 'long', 'bottom', function(a) {
                                console.log(a)
                            });
                        } else {
                            Splash.ShowToast('You have already visited this outlet. It cannot be removed from today\'s plan', 'long', 'bottom', function(a) {
                                console.log(a)
                            });
                        }
                    } else {
                        Splash.ShowToast('You have already visited this outlet. It cannot be removed from today\'s plan', 'long', 'bottom', function(a) {
                            console.log(a)
                        });
                    }
                }
            } else {
                Splash.ShowToast('Outlets can only be de-selected from today\'s plan.', 'long', 'center', function(a) {
                    console.log(a)
                });
            }
        }
        $scope.onSwipeupOld = function(outletId) {
            if ($rootScope.day == 'today') {
                $scope.remove = [];
                $scope.remove.Reason = '';
                $scope.removeOutletName = '';
                $scope.OutletStatus = '';
                var canDelete = false;
                console.log('$rootScope.Outlets');
                console.log($rootScope.Outlets);
                angular.forEach($rootScope.Outlets, function(record, key) {
                    if (record.Id == outletId && record.isCompleted == 'Not started') {
                        canDelete = true;
                        $scope.removeOutletName = record.Name;
                        $scope.OutletStatus = record.RoutePlan.Status__c;
                    }
                });
                if (canDelete == true) {
                    if ($scope.OutletStatus != 'Removed') {
                        $scope.confirmDeletePopup = $ionicPopup.confirm({
                            template: 'Are you sure you don\'t want to visit ' + $scope.removeOutletName + ' from today\'s route plan?  <br/><br/><br/><span><input type="text" placeholder="Enter a reason for not visiting the outlet" ng-model="remove.Reason" ng-change="modelDataCheck()"><div class="piller"></div><div class="piller piller_1"></div></span><br/> <div style="text-align:right; color:#38B6CB; margin-right: 10px; font-weight: bold;"><span my-touchstart="cancelConfirmDelete(\'' + outletId + '\')">NO</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id="outletRemove_yes" class="coolerRemove_yes" ng-disabled="!remove.Reason" my-touchstart="confirmDelete(\'' + outletId + '\')">YES</span> </div>',
                            cssClass: 'myConfirm',
                            title: '',
                            scope: $scope,
                            buttons: []
                        });
                        $scope.confirmDeletePopup.then(function(res) {
                            if (res) {
                                console.log('Tapped!', res);
                            }
                        });
                    } else {
                        $scope.confirmAddBackPopup = $ionicPopup.confirm({
                            template: 'Are you sure you want to add ' + $scope.removeOutletName + ' to today\'s route plan?  <br/><br/><br/><span><input type="text" placeholder="Enter a reason for visiting the outlet" ng-model="remove.Reason" ng-change="modelDataCheck()"><div class="piller"></div><div class="piller piller_1"></div></span><br/> <div style="text-align:right; color:#38B6CB; margin-right: 10px;font-weight: bold;"><span my-touchstart="cancelConfirmAddBack(\'' + outletId + '\')">NO</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id="outletRemove_yes" class="coolerRemove_yes" ng-disabled="!remove.Reason" my-touchstart="confirmAddBack(\'' + outletId + '\')">YES</span> </div>',
                            cssClass: 'myConfirm',
                            title: '',
                            scope: $scope,
                            buttons: []
                        });
                        $scope.confirmAddBackPopup.then(function(res) {
                            if (res) {
                                console.log('Tapped!', res);
                            }
                        });
                    }
                } else {
                    var pendingTasks = FETCH_DATA.querySoup('Db_transaction', 'status', 'pending');
                    if (pendingTasks.currentPageOrderedEntries != undefined && pendingTasks.currentPageOrderedEntries.length > 0) {
                        if (pendingTasks.currentPageOrderedEntries[0].Account == outletId) {
                            Splash.ShowToast('You have already started the call for this outlet. It cannot be removed from today\'s plan', 'long', 'bottom', function(a) {
                                console.log(a)
                            });
                        } else {
                            Splash.ShowToast('You have already visited this outlet. It cannot be removed from today\'s plan', 'long', 'bottom', function(a) {
                                console.log(a)
                            });
                        }
                    } else {
                        Splash.ShowToast('You have already visited this outlet. It cannot be removed from today\'s plan', 'long', 'bottom', function(a) {
                            console.log(a)
                        });
                    }
                }
            } else {
                Splash.ShowToast('Outlets can only be de-selected from today\'s plan.', 'long', 'center', function(a) {
                    console.log(a)
                });
            }
        }
        $scope.modelDataCheck = function() {
            var _myEle = document.getElementById('outletRemove_yes');
            if ($scope.remove.Reason == '') {
                _myEle.style.color = '#aaaaaa';
            } else {
                _myEle.style.color = '#38B6CB';
            }
        }
        $scope.addNewOutlets = function() {
            if ($rootScope.day == 'today'){
                $ionicLoading.show({
                    template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
                });
                $timeout(function() {
                    $location.path('app/AddOutlets/' + new Date());
                },500);
            }
                
            else Splash.ShowToast('Outlets can only be added to today\'s plan.', 'long', 'center', function(a) {
                console.log(a)
            });
        }
        $scope.prevSlide = function() {
            $ionicSlideBoxDelegate.previous();
        }
        $scope.nextSlide = function() {
            $ionicSlideBoxDelegate.next();
        }
        $scope.goToOverview = function(_outletId, _OutletName) {
            if (_OutletName == 'Outlet') {
                $location.path('app/overview/' + _outletId);
            } else if (_OutletName == 'Depot') {
                $location.path('app/depotOverview/' + _outletId);
            } else {
                $location.path('app/wholesaleOverview/' + _outletId);
            }
        }
        $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('TodayCtrlError', err.stack + '::' + err.message);
    }
}).controller('OverviewCtrl', function(MOBILEDATABASE_ADD,$scope, $stateParams, $window, $location, $rootScope, $q, $ionicModal, $timeout, $interval, $ionicLoading, $log, $filter, FETCH_LOCAL_DATA, FETCH_DATA_LOCAL, WEBSERVICE_ERROR,SMARTSTORE) {
    try {
        $ionicLoading.show({
            template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
        });
        
        $rootScope.params = $stateParams;
        $rootScope.name = "OverviewCtrl";
        $scope.breadList = [{
            name: 'Outlet Summary '
        }, {
            name: 'Business Objectives'
        }, {
            name: 'Last Visit Summary'
        }];
        var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
        $rootScope.backText = 'Back';
        $rootScope.showBack = true;
        $scope.currentMonthSales = 0;
        $scope.avgSales = 0;
        $scope.promotions = [];
        $scope.lastThreeMonth = [];
        $scope.TotalMonthWiseSales = [];
        function getMonthsNoSales(currentMonth,currentYear) {
            var date = new Date();
            var date2 = new Date();
            //subttract 3 months
            date.setMonth(currentMonth-1);
            if(currentMonth==0)
            date.setFullYear(currentYear-1);
            else
            date.setFullYear(currentYear);
            var monthArray = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun",
                    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
        
            var menuMonths = new Array();
            var monthString='';
            var count = 3;
            var buffer = 10;
        
            while (count > 0) {
                var month = monthArray[date.getMonth()];
        
                if(currentMonth==1 && count==2)
                    date.setFullYear(currentYear-1);
                if(currentMonth==2 && count==1)
                    date.setFullYear(currentYear-1);
                menuMonths.push(month+"-"+date.getFullYear());
                monthString=monthString+month+"-"+date.getFullYear()+";"
                date.setMonth(date.getMonth() - 1);
        
                count -= 1;
            }
            return monthString;
        }
        $scope.CategoryWiseMs = {
            Overall: 0,
            Mild: 0,
            Strong: 0
        };
        var customer = FETCH_LOCAL_DATA('Account', 'Id', $stateParams.accountId);
        if (customer.length != 0 && customer[0] != undefined) {
            $scope.currentCustomer = customer[0];
        }
        var flow = FETCH_DATA_LOCAL('Db_process_flow', sfSmartstore.buildSmartQuerySpec("SELECT  {Db_process_flow:_soup} FROM {Db_process_flow} WHERE {Db_process_flow:Outlet_Type__c}='" + $scope.currentCustomer.RecordType.Name + "'", 1));
        var temp = FETCH_DATA_LOCAL('DB_fields', sfSmartstore.buildSmartQuerySpec("SELECT  {DB_fields:_soup} FROM {DB_fields} where {DB_fields:Process_Flow__c} = '"+flow.currentPageOrderedEntries[0][0].Id+"' and {DB_fields:Removed__c}='false' order by {DB_fields:Order__c} " , 100000));
        $scope.fields=temp.currentPageOrderedEntries;
        $rootScope.processFlow = flow.currentPageOrderedEntries[0][0];
        $scope.AccountImage = './img/capture.png';
        var images = FETCH_DATA_LOCAL('Db_images', sfSmartstore.buildSmartQuerySpec("SELECT  {Db_images:_soup} FROM {Db_images} WHERE {Db_images:ParentId} = '"+$stateParams.accountId+"' and not({Db_images:Name} like '%gallery%')", 100000));
        
        if (images.currentPageOrderedEntries.length > 0) {
            angular.forEach(images.currentPageOrderedEntries, function(record, key) {
                if(record[0]['Name']!=undefined && record[0]['Name'].indexOf('gallery-')==-1)
                    $scope.AccountImage = "data:image/jpeg;base64," + record[0].Body;
            });
            
        }
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var d = new Date();
        var monthVal = d.getMonth();
        var yearVal = d.getFullYear();
        var currentYear = d.getFullYear();
        var monthAndYearList = [];
        var monthAndYearListNew = [];
        var MonthAndCategoryWiseSales = [],
            industryMonthAndCategoryWiseSales = [];
        if ($scope.currentCustomer.Recent_Sales__c == undefined || $scope.currentCustomer.Recent_Sales__c == null || $scope.currentCustomer.Recent_Sales__c == ''){
            var currDate=new Date();
            $scope.currentCustomer.Recent_Sales__c = getMonthsNoSales(currDate.getMonth(),currDate.getFullYear());
        }else{
            var fakeMonthYearList = $scope.currentCustomer.Recent_Sales__c.split(";");
            if(fakeMonthYearList.length<4){
                var fakeFirstMonthYear=fakeMonthYearList[0];
                var fakeFirstMonthYearArray = fakeFirstMonthYear.split('-');
                var fakeMonthList="JanFebMarAprMayJunJulAugSepOctNovDec";
                $scope.currentCustomer.Recent_Sales__c = getMonthsNoSales((fakeMonthList.indexOf(fakeFirstMonthYearArray[0]) / 3+1),fakeFirstMonthYearArray[1]);
            }
        }
        
        if ($scope.currentCustomer.Recent_Sales__c != undefined && $scope.currentCustomer.Recent_Sales__c != null) {
            monthAndYearList = $scope.currentCustomer.Recent_Sales__c.split(";");
            monthAndYearList = monthAndYearList.slice(0, 3);
            var tempMonthAndYearListNew = $scope.currentCustomer.Recent_Sales__c;
            tempMonthAndYearListNew = tempMonthAndYearListNew.replace(/-/g, "");
            monthAndYearListNew = tempMonthAndYearListNew.split(";");
            for (var i = 0; i < monthAndYearList.length; i++) {
                var tempMonthYear = monthAndYearList[i].split('-');
                monthVal = monthVal - 1;
                if (monthVal < 0) {
                    monthVal = 11;
                    yearVal = currentYear - 1;
                }
                $scope.TotalMonthWiseSales[tempMonthYear[0]] = 0;
                $scope.lastThreeMonth.push({
                    Month: tempMonthYear[0],
                    Year: tempMonthYear[1]
                });
                MonthAndCategoryWiseSales[tempMonthYear[0]] = {
                    Overall: 0,
                    Mild: 0,
                    Strong: 0
                };
                industryMonthAndCategoryWiseSales[tempMonthYear[0]] = {
                    Overall: 0,
                    Mild: 0,
                    Strong: 0
                };
                //monthAndYearList.push(months[monthVal] + yearVal);
            }
        }
        var rtList = FETCH_LOCAL_DATA('DB_RecordTypes', 'SobjectType', 'Upload__c');
        var recordtypesWithIds = [];
        angular.forEach(rtList, function(record, key) {
            recordtypesWithIds[record.Name] = record.Id;
        });
        var uploads = FETCH_LOCAL_DATA('Upload__c', 'Account__c', $stateParams.accountId);
        var todayDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        for (var i = 0; i < uploads.length; i++) {
            var tempRemoved=false;
            if(uploads[i].Removed__c==true){
                tempRemoved=true;
            }
            if (uploads[i].RecordTypeId == recordtypesWithIds['Promotions'] && uploads[i].Status__c != 'Completed') {
                var astartDate = uploads[i].Start_Date__c;
                var aEndDate = uploads[i].End_Date__c;
                if (astartDate.indexOf('/') != -1) {
                    aStartDate = new Date(astartDate.split('/')[1] + '/' + astartDate.split('/')[0] + '/' + astartDate.split('/')[2]);
                } else {
                    aStartDate = new Date(astartDate);
                } 
                if (aEndDate.indexOf('/') != -1) {
                    aEndDate = new Date(aEndDate.split('/')[1] + '/' + aEndDate.split('/')[0] + '/' + aEndDate.split('/')[2]);
                } else {
                    aEndDate = new Date(aEndDate);
                } 

                var startDate = $filter('date')(new Date(aStartDate), 'yyyy-MM-dd');
                var endDate = $filter('date')(new Date(aEndDate), 'yyyy-MM-dd');
                if (todayDate >= startDate && todayDate <= endDate) {
                    $scope.promotions.push(uploads[i]);
                }
            } else if (uploads[i].RecordTypeId == recordtypesWithIds['Sales'] && uploads[i].Sales__c != undefined && tempRemoved==false && uploads[i].Sales_Month__c != undefined && (monthAndYearListNew.indexOf(uploads[i].Sales_Month__c) != -1) || (uploads[i].Month__c == months[d.getMonth()] && uploads[i].Year__c == d.getFullYear())) {
                if (uploads[i].Company__c == 'UB') {
                    if (uploads[i].Month__c == months[d.getMonth()] && uploads[i].Year__c == d.getFullYear()) {
                        $scope.currentMonthSales = $scope.currentMonthSales + uploads[i].Sales__c;
                    } else {
                        MonthAndCategoryWiseSales[uploads[i].Month__c].Overall = MonthAndCategoryWiseSales[uploads[i].Month__c].Overall + parseInt(uploads[i].Sales__c);
                        if (uploads[i].Category__c == 'Mild' || uploads[i].Category__c == 'MILD' || uploads[i].Category__c === 'Mild Beer' || uploads[i].Category__c == 'MILD BEER') {
                            MonthAndCategoryWiseSales[uploads[i].Month__c].Mild = MonthAndCategoryWiseSales[uploads[i].Month__c].Mild + parseInt(uploads[i].Sales__c);
                        }
                        if (uploads[i].Category__c == 'Strong' || uploads[i].Category__c == 'STRONG' || uploads[i].Category__c == 'Strong Beer' || uploads[i].Category__c == 'STRONG BEER') {
                            MonthAndCategoryWiseSales[uploads[i].Month__c].Strong = MonthAndCategoryWiseSales[uploads[i].Month__c].Strong + parseInt(uploads[i].Sales__c);
                        }
                        $scope.TotalMonthWiseSales[uploads[i].Month__c] = $scope.TotalMonthWiseSales[uploads[i].Month__c] + parseInt(uploads[i].Sales__c);
                    }
                }
                if ((uploads[i].Category__c == 'Mild' || uploads[i].Category__c == 'MILD' || uploads[i].Category__c == 'Mild Beer' || uploads[i].Category__c == 'MILD BEER') && !(uploads[i].Month__c == months[d.getMonth()] && uploads[i].Year__c == d.getFullYear())) {
                    industryMonthAndCategoryWiseSales[uploads[i].Month__c].Mild = industryMonthAndCategoryWiseSales[uploads[i].Month__c].Mild + parseInt(uploads[i].Sales__c);
                }
                if (uploads[i].Category__c == 'Strong' || uploads[i].Category__c == 'STRONG' || uploads[i].Category__c == 'Strong Beer' || uploads[i].Category__c == 'STRONG BEER' && !(uploads[i].Month__c == months[d.getMonth()] && uploads[i].Year__c == d.getFullYear())) {
                    industryMonthAndCategoryWiseSales[uploads[i].Month__c].Strong = industryMonthAndCategoryWiseSales[uploads[i].Month__c].Strong + parseInt(uploads[i].Sales__c);
                }
                if (!(uploads[i].Month__c == months[d.getMonth()] && uploads[i].Year__c == d.getFullYear())) industryMonthAndCategoryWiseSales[uploads[i].Month__c].Overall = industryMonthAndCategoryWiseSales[uploads[i].Month__c].Overall + parseInt(uploads[i].Sales__c);
            }
        }
        if (industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Overall != 0) {
            $scope.avgSales = industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Overall;
        }
        if (industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Overall != 0) {
            $scope.avgSales = $scope.avgSales + industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Overall;
        }
        if (industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[2].Month].Overall != 0) {
            $scope.avgSales = $scope.avgSales + industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[2].Month].Overall;
        }
        $scope.avgSales = Math.round($scope.avgSales / 3);
        // $log.debug(JSON.stringify(MonthAndCategoryWiseSales['Mar']));
        // $log.debug(JSON.stringify(industryMonthAndCategoryWiseSales['Mar']));  
        if (MonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Overall != 0) {
            $scope.CategoryWiseMs.Overall = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Overall / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Overall) * 100));
            $scope.CategoryWiseMs.Mild = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Mild / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Mild) * 100));
            $scope.CategoryWiseMs.Strong = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Strong / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Strong) * 100));
        } else if (MonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Overall != 0) {
            $scope.CategoryWiseMs.Overall = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Overall / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Overall) * 100));
            $scope.CategoryWiseMs.Mild = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Mild / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Mild) * 100));
            $scope.CategoryWiseMs.Strong = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Strong / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Strong) * 100));
        } else {
            $scope.CategoryWiseMs.Overall = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[2].Month].Overall / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[2].Month].Overall) * 100));
            $scope.CategoryWiseMs.Mild = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[2].Month].Mild / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[2].Month].Mild) * 100));
            $scope.CategoryWiseMs.Strong = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[2].Month].Strong / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[2].Month].Strong) * 100));
        }
        if (isNaN($scope.CategoryWiseMs.Overall)) {
            $scope.CategoryWiseMs.Overall = 0;
        }
        if (isNaN($scope.CategoryWiseMs.Mild)) {
            $scope.CategoryWiseMs.Mild = 0;
        }
        if (isNaN($scope.CategoryWiseMs.Strong)) {
            $scope.CategoryWiseMs.Strong = 0;
        }
        $scope.newIndustryMonthAndCategoryWiseSales=industryMonthAndCategoryWiseSales;
        $ionicLoading.hide();
        $scope.gotoObjectives = function() {
            
             //User log
            var today_now = $filter('date')(new Date(), 'dd/MM/yyyy hh:mm a');
            var userOprObj=[{
                ObjectData:'',
                time_stamp:today_now,
                comment:'User Pressed next on outlet summary'
            }];
            MOBILEDATABASE_ADD('Db_User_Logs', userOprObj, '_soupEntryId');

            $scope.backText = 'Back';
            //$location.path('app/lastVistSummary/' + $stateParams.accountId);
            $location.path('app/Objectives/' + $stateParams.accountId);
        }
        
        $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('OverviewCtrlError', err.stack + '::' + err.message);
    }
}).controller('depotOverviewCtrl', function($q, $log, $ionicLoading, $scope, $ionicPopup, $rootScope, $window, $stateParams, $location, $filter, $cordovaCamera, $ionicModal, $ionicSlideBoxDelegate, $timeout, SMARTSTORE, SOUP,GEO_LOCATION,MOBILEDATABASE_ADD, WEBSERVICE_ERROR,FETCH_DATA_LOCAL) {
    try {
        $ionicLoading.show({
            template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
        });
        $rootScope.params = $stateParams;
        $rootScope.name = "depotOverviewCtrl";
        $rootScope.backText = 'Back';
        $rootScope.showBack = true;
        $scope.breadList = [{
            name: 'Depot Summary '
        }, {
            name: 'Start Call'
        }];
        var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
        
        $scope.currentCustomer = SMARTSTORE.buildExactQuerySpec('Account', 'Id', $stateParams.accountId);
        $scope.AccountImage = './img/capture.png';
        var flow = FETCH_DATA_LOCAL('Db_process_flow', sfSmartstore.buildSmartQuerySpec("SELECT  {Db_process_flow:_soup} FROM {Db_process_flow} WHERE {Db_process_flow:Outlet_Type__c}='" + $scope.currentCustomer[0].RecordType.Name + "'", 1));
        var temp = FETCH_DATA_LOCAL('DB_fields', sfSmartstore.buildSmartQuerySpec("SELECT  {DB_fields:_soup} FROM {DB_fields} where {DB_fields:Process_Flow__c} = '"+flow.currentPageOrderedEntries[0][0].Id+"' and {DB_fields:Removed__c}='false' order by {DB_fields:Order__c} " , 100000));
        $scope.fields=temp.currentPageOrderedEntries;
        
        var images = SMARTSTORE.buildExactQuerySpec('Db_images', 'ParentId', $stateParams.accountId);
        if (images.length > 0) {
            angular.forEach(images, function(record, key) {
                if(record['Name']!=undefined && record['Name'].indexOf('gallery-')==-1)
                    $scope.AccountImage = "data:image/jpeg;base64," + record.Body;
            });
            
        }
        var d = new Date();
        var today = d.getTime().toString();
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        $scope.currentMonthSales = 0;
        $scope.avgSales = 0;
        $scope.promotions = [];
        $scope.lastThreeMonth = [];
        $scope.TotalMonthWiseSales = [];
        $scope.CategoryWiseMs = {
            Overall: 0,
            Mild: 0,
            Strong: 0
        };
        var lastVisit  = SMARTSTORE.buildSmartQuerySpec("SELECT {Visit__c:_soup} FROM {Visit__c} WHERE {Visit__c:Account__c}='" + $stateParams.accountId + "' ORDER BY {Visit__c:Check_In_DateTime__c} DESC", 1);
        if(lastVisit.length>0){
            $scope.lastVisitRecord = lastVisit[0][0];
            
        }
        var d = new Date();
        var monthVal = d.getMonth();
        var yearVal = d.getFullYear();
        var currentYear = d.getFullYear();
        var monthAndYearList = [];
        var monthAndYearListNew = [];
        var MonthAndCategoryWiseSales = [],
            industryMonthAndCategoryWiseSales = [];
        if ($scope.currentCustomer[0].Recent_Sales__c != undefined && $scope.currentCustomer[0].Recent_Sales__c != null) {
            monthAndYearList = $scope.currentCustomer[0].Recent_Sales__c.split(";");
            monthAndYearList = monthAndYearList.slice(0, 3);
            var tempMonthAndYearListNew = $scope.currentCustomer[0].Recent_Sales__c;
            tempMonthAndYearListNew = tempMonthAndYearListNew.replace(/-/g, "");
            monthAndYearListNew = tempMonthAndYearListNew.split(";");
            for (var i = 0; i < monthAndYearList.length; i++) {
                var tempMonthYear = monthAndYearList[i].split('-');
                monthVal = monthVal - 1;
                if (monthVal < 0) {
                    monthVal = 11;
                    yearVal = currentYear - 1;
                }
                $scope.TotalMonthWiseSales[tempMonthYear[0]] = 0;
                $scope.lastThreeMonth.push({
                    Month: tempMonthYear[0],
                    Year: tempMonthYear[1]
                });
                MonthAndCategoryWiseSales[tempMonthYear[0]] = {
                    Overall: 0,
                    Mild: 0,
                    Strong: 0
                };
                industryMonthAndCategoryWiseSales[tempMonthYear[0]] = {
                    Overall: 0,
                    Mild: 0,
                    Strong: 0
                };
                //monthAndYearList.push(months[monthVal] + yearVal);
            }
        }
        var rtList = SMARTSTORE.buildExactQuerySpec('DB_RecordTypes', 'SobjectType', 'Upload__c');
        var recordtypesWithIds = [];
        angular.forEach(rtList, function(record, key) {
            recordtypesWithIds[record.Name] = record.Id;
        });
        var uploads = SMARTSTORE.buildExactQuerySpec('Upload__c', 'Account__c', $stateParams.accountId);
        for (var i = 0; i < uploads.length; i++) {
            var tempRemoved=false;
            if(uploads[i].Removed__c==true){
                tempRemoved=true;
            }
            if (uploads[i].RecordTypeId == recordtypesWithIds['Sales'] && uploads[i].Sales__c != undefined && tempRemoved==false && uploads[i].Sales_Month__c != undefined && (monthAndYearListNew.indexOf(uploads[i].Sales_Month__c) != -1) || (uploads[i].Month__c == months[d.getMonth()] && uploads[i].Year__c == d.getFullYear())) {
                if (uploads[i].Company__c == 'UB') {
                    if (uploads[i].Month__c == months[d.getMonth()] && uploads[i].Year__c == d.getFullYear()) {
                        $scope.currentMonthSales = $scope.currentMonthSales + uploads[i].Sales__c;
                    } else {
                        MonthAndCategoryWiseSales[uploads[i].Month__c].Overall = MonthAndCategoryWiseSales[uploads[i].Month__c].Overall + parseInt(uploads[i].Sales__c);
                        if (uploads[i].Category__c == 'Mild' || uploads[i].Category__c == 'MILD' || uploads[i].Category__c === 'Mild Beer' || uploads[i].Category__c == 'MILD BEER') {
                            MonthAndCategoryWiseSales[uploads[i].Month__c].Mild = MonthAndCategoryWiseSales[uploads[i].Month__c].Mild + parseInt(uploads[i].Sales__c);
                        }
                        if (uploads[i].Category__c == 'Strong' || uploads[i].Category__c == 'STRONG' || uploads[i].Category__c == 'Strong Beer' || uploads[i].Category__c == 'STRONG BEER') {
                            MonthAndCategoryWiseSales[uploads[i].Month__c].Strong = MonthAndCategoryWiseSales[uploads[i].Month__c].Strong + parseInt(uploads[i].Sales__c);
                        }
                        $scope.TotalMonthWiseSales[uploads[i].Month__c] = $scope.TotalMonthWiseSales[uploads[i].Month__c] + parseInt(uploads[i].Sales__c);
                    }
                }
                if ((uploads[i].Category__c == 'Mild' || uploads[i].Category__c == 'MILD' || uploads[i].Category__c == 'Mild Beer' || uploads[i].Category__c == 'MILD BEER') && !(uploads[i].Month__c == months[d.getMonth()] && uploads[i].Year__c == d.getFullYear())) {
                    industryMonthAndCategoryWiseSales[uploads[i].Month__c].Mild = industryMonthAndCategoryWiseSales[uploads[i].Month__c].Mild + parseInt(uploads[i].Sales__c);
                }
                if (uploads[i].Category__c == 'Strong' || uploads[i].Category__c == 'STRONG' || uploads[i].Category__c == 'Strong Beer' || uploads[i].Category__c == 'STRONG BEER' && !(uploads[i].Month__c == months[d.getMonth()] && uploads[i].Year__c == d.getFullYear())) {
                    industryMonthAndCategoryWiseSales[uploads[i].Month__c].Strong = industryMonthAndCategoryWiseSales[uploads[i].Month__c].Strong + parseInt(uploads[i].Sales__c);
                }
                if (!(uploads[i].Month__c == months[d.getMonth()] && uploads[i].Year__c == d.getFullYear())) industryMonthAndCategoryWiseSales[uploads[i].Month__c].Overall = industryMonthAndCategoryWiseSales[uploads[i].Month__c].Overall + parseInt(uploads[i].Sales__c);
            }
        }
        if (industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Overall != 0) {
            $scope.avgSales = industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Overall;
        }
        if (industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Overall != 0) {
            $scope.avgSales = $scope.avgSales + industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Overall;
        }
        if (industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[2].Month].Overall != 0) {
            $scope.avgSales = $scope.avgSales + industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[2].Month].Overall;
        }
        $scope.avgSales = Math.round($scope.avgSales / 3);
        // $log.debug(JSON.stringify(MonthAndCategoryWiseSales['Mar']));
        // $log.debug(JSON.stringify(industryMonthAndCategoryWiseSales['Mar']));  
        if (MonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Overall != 0) {
            $scope.CategoryWiseMs.Overall = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Overall / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Overall) * 100));
            $scope.CategoryWiseMs.Mild = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Mild / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Mild) * 100));
            $scope.CategoryWiseMs.Strong = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Strong / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Strong) * 100));
        } else if (MonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Overall != 0) {
            $scope.CategoryWiseMs.Overall = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Overall / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Overall) * 100));
            $scope.CategoryWiseMs.Mild = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Mild / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Mild) * 100));
            $scope.CategoryWiseMs.Strong = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Strong / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Strong) * 100));
        } else {
            $scope.CategoryWiseMs.Overall = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[2].Month].Overall / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[2].Month].Overall) * 100));
            $scope.CategoryWiseMs.Mild = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[2].Month].Mild / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[2].Month].Mild) * 100));
            $scope.CategoryWiseMs.Strong = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[2].Month].Strong / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[2].Month].Strong) * 100));
        }
        if (isNaN($scope.CategoryWiseMs.Overall)) {
            $scope.CategoryWiseMs.Overall = 0;
        }
        if (isNaN($scope.CategoryWiseMs.Mild)) {
            $scope.CategoryWiseMs.Mild = 0;
        }
        if (isNaN($scope.CategoryWiseMs.Strong)) {
            $scope.CategoryWiseMs.Strong = 0;
        }
        $scope.newIndustryMonthAndCategoryWiseSales=industryMonthAndCategoryWiseSales;
        $ionicLoading.hide();
        $scope.depotCheckIn = function() {
            $timeout(function(){
            var pendingTasks = SMARTSTORE.buildExactQuerySpec('Db_transaction', 'status', 'pending');
            if ($rootScope.day == 'today') {
                if (pendingTasks.length == 0) {
                    if ($rootScope.day == 'today') {
                        $ionicLoading.show({
                            template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
                        });
                        $rootScope.StockAndSalesList = [];
                        $rootScope.uploadfields = [];
                        $rootScope.processFlowLineItems = [];
                        $rootScope.currentvisitSummaryImages = [];
                        $rootScope.StockAndSalesList = [];
                        $rootScope.retailSalesList = {};
                        $rootScope.uploadfields = [];
                        $rootScope.currentvisitSummaryImages = [];
                        $rootScope.galleryImages =[];
                        $rootScope.slidesList = [];
                        $rootScope.slideFields = [];
                        $rootScope.PrideImages = [];
                        $rootScope.visitImages = [];
                        $rootScope.CompeSales = [];
                        $rootScope.UblSales = [];
                        $rootScope.chataiSales = [];
                        $rootScope.audit = {};
                        $rootScope.tempvisitType = 'false';
                        $rootScope.pendingTask = true;
                        $rootScope.visitrecord = {};
                        $rootScope.pendingTask = true;
                        if (SOUP.isExists('Db_breadCrum') == true) {
                            SOUP.remove('Db_breadCrum');
                        }
                        SOUP.register('Db_breadCrum', [{
                            "path": "Name",
                            "type": "string"
                        }, {
                            "path": "VisitedOrder",
                            "type": "string"
                        }]);
                        $scope.currentLocation = {};
                        GEO_LOCATION.getCurrentPosition().then(function(position) {
                            $scope.currentLocation.latitude = position.latitude;
                            $scope.currentLocation.longitude = position.longitude;
                            $scope.Visit = [{
                                Account__c: $scope.currentCustomer[0].Id,
                                Check_In_DateTime__c: today,
                                Check_In_Location__Latitude__s: $scope.currentLocation.latitude,
                                Check_In_Location__Longitude__s: $scope.currentLocation.longitude,
                                External_Id__c: (new Date().getTime())+'-'+$stateParams.visitId+'-'+$window.Math.random() * 100000000,
                                Route_Plan__c: $scope.currentCustomer[0].RoutePlanId,
                                IsDirty: true
                            }];
                            if ($scope.currentCustomer[0].Location__Latitude__s == undefined || $scope.currentCustomer[0].Location__Latitude__s == null) {
                                $scope.currentCustomer[0].Location__Latitude__s = $scope.currentLocation.latitude;
                                $scope.currentCustomer[0].Location__Longitude__s = $scope.currentLocation.longitude;
                                $scope.currentCustomer[0].IsDirty = true;
                                
                                MOBILEDATABASE_ADD('Account', $scope.currentCustomer, 'Id');
                            } else if ($scope.distance($scope.currentCustomer[0].Location__Latitude__s, $scope.currentCustomer[0].Location__Longitude__s, $scope.currentLocation.latitude, $scope.currentLocation.longitude, 'K') > 1) {
                                $scope.Visit[0].Deviation_Reason__c = 'location is different from existing location of the outlet';
                            }
                            MOBILEDATABASE_ADD('Visit__c', $scope.Visit, 'External_Id__c');
                            var flow = SMARTSTORE.buildSmartQuerySpec("SELECT  {Db_process_flow:_soup} FROM {Db_process_flow} WHERE {Db_process_flow:Outlet_Type__c}='" + $scope.currentCustomer[0].RecordType.Name + "'", 1);
                            var flowLineItems = SMARTSTORE.buildExactQuerySpec('Db_process_flow_line_item', "Process_Flow__c", flow[0][0].Id);
                            var queryspec = '';
                            var queryspecForSection = '';
                            angular.forEach(flowLineItems, function(record, key) {
                                if(record['Removed__c']==false){
                                    $rootScope.processFlowLineItems[record.Order__c - 1] = record;
                                    if (queryspec == '' && queryspecForSection == '') {
                                        queryspec = "{DB_page:Id}='" + record.Page__c + "'";
                                        queryspecForSection = "{DB_section:Page__c}='" + record.Page__c + "'";
                                    } else {
                                        queryspec = queryspec + " OR {DB_page:Id}='" + record.Page__c + "'";
                                        queryspecForSection = queryspecForSection + " OR {DB_section:Page__c}='" + record.Page__c + "'";
                                    }
                                }
                            });
                            if (queryspec != '' && queryspecForSection != '') {
                                var pages = SMARTSTORE.buildSmartQuerySpec("SELECT  {DB_page:_soup} FROM {DB_page} WHERE " + queryspec, 20);
                                var sections = SMARTSTORE.buildSmartQuerySpec("SELECT  {DB_section:_soup} FROM {DB_section} WHERE " + queryspecForSection + " order by {DB_section:Order__c}", 20);
                                queryspec = '';
                                angular.forEach(sections, function(record, key) {
                                    if (queryspec == '') {
                                        queryspec = "{DB_fields:Section__c}='" + record[0].Id + "'";
                                    } else {
                                        queryspec = queryspec + " OR {DB_fields:Section__c}='" + record[0].Id + "'";
                                    }
                                });
                                var fields = [];
                                if (queryspec != '') {
                                    fields = SMARTSTORE.buildSmartQuerySpec("SELECT  {DB_fields:_soup} FROM {DB_fields} WHERE " + queryspec + " and {DB_fields:Removed__c}='false' order by {DB_fields:Order__c}", 200);
                                }
                                var pagesWithIds = [];
                                angular.forEach(pages, function(page, pageKey) {
                                    var pagedec = {};
                                    pagedec.pageDescription = page[0];
                                    pagesWithIds[page[0].Id] = page[0];
                                    pagedec.sections = [];
                                    angular.forEach(sections, function(section, sectionKey) {
                                        if (page[0].Id == section[0].Page__c) {
                                            var sectiondec = {};
                                            sectiondec.sectionDescription = section[0];
                                            sectiondec.fields = [];
                                            angular.forEach(fields, function(field, fieldKey) {
                                                if (field[0].Section__c == section[0].Id) {
                                                    var fieldDescription = field[0];
                                                    if (field[0].Picklist_Values__c != undefined) {
                                                        fieldDescription.pickListValues = field[0].Picklist_Values__c.split(';');
                                                    }
                                                    sectiondec.fields.push(fieldDescription);
                                                }
                                            });
                                            pagedec.sections.push(sectiondec);
                                        }
                                    });
                                    $rootScope.pages.push(pagedec);
                                });
                            }

                            var page = pagesWithIds[$rootScope.processFlowLineItems[0].Page__c];
                            $location.path('app/'+page.Template_Name__c+'/' + $scope.currentCustomer[0].Id + '/' + $scope.Visit[0].External_Id__c + '/0/' + new Date());
                        }, function(error) {
                            $log.error('ERROR' + JSON.stringify(error));
                        });
                    }
                } else {
                    $ionicLoading.hide();
                    Splash.ShowToast('A previous call has not been completed. Tap Resume icon to complete the open call.', 'long', 'bottom', function(a) {
                        console.log(a)
                    });
                }
            } else {
                $ionicLoading.hide();
                Splash.ShowToast('You can only start a call for outlets in today\'s route plan.', 'long', 'bottom', function(a) {
                    console.log(a)
                });
            }
        },500);
        }
        $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('depotOverviewError', err.stack + '::' + err.message);
    }
}).controller('wholesaleOverviewCtrl', function($q, $log, $ionicLoading, $scope, $ionicPopup, $rootScope, $window, $stateParams, $location, $filter, $cordovaCamera, $ionicModal, $ionicSlideBoxDelegate, $timeout, SMARTSTORE, SOUP,GEO_LOCATION,MOBILEDATABASE_ADD, WEBSERVICE_ERROR,FETCH_DATA_LOCAL) {
    try {
        $ionicLoading.show({
            template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
        });
        $rootScope.params = $stateParams;
        $rootScope.name = "wholesaleOverviewCtrl";
        $rootScope.backText = 'Back';
        $rootScope.showBack = true;
        $scope.breadList = [{
            name: 'Summary'
        }, {
            name: 'Last Visit Summary'
        }];
        var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
        
        $scope.currentCustomer = SMARTSTORE.buildExactQuerySpec('Account', 'Id', $stateParams.accountId);
        $scope.AccountImage = './img/capture.png';
        var flow = FETCH_DATA_LOCAL('Db_process_flow', sfSmartstore.buildSmartQuerySpec("SELECT  {Db_process_flow:_soup} FROM {Db_process_flow} WHERE {Db_process_flow:Outlet_Type__c}='" + $scope.currentCustomer[0].RecordType.Name + "'", 1));
        var temp = FETCH_DATA_LOCAL('DB_fields', sfSmartstore.buildSmartQuerySpec("SELECT  {DB_fields:_soup} FROM {DB_fields} where {DB_fields:Process_Flow__c} = '"+flow.currentPageOrderedEntries[0][0].Id+"' and {DB_fields:Removed__c}='false' order by {DB_fields:Order__c} " , 100000));
        $scope.fields=temp.currentPageOrderedEntries;
        
        var images = SMARTSTORE.buildExactQuerySpec('Db_images', 'ParentId', $stateParams.accountId);
        if (images.length > 0) {
            angular.forEach(images, function(record, key) {
                if(record['Name']!=undefined && record['Name'].indexOf('gallery-')==-1)
                    $scope.AccountImage = "data:image/jpeg;base64," + record.Body;
            });
            
        }
        $scope.target = 0;
        $scope.mtd=0;
        $scope.activeOrders = 0;
        var d = new Date();
        $scope.permits = [];
        var localPermits =  SMARTSTORE.buildSmartQuerySpec("SELECT {Order__c:_soup} FROM {Order__c} WHERE {Order__c:Account__c}='" + $stateParams.accountId + "' AND {Order__c:Order_stage__c}!='Inactive'", 100);
        angular.forEach(localPermits, function(record, key) {
            if(record[0].Permit_expiry_Date__c!=undefined && record[0].Permit_issue_Date__c!=undefined && new Date(record[0].Permit_expiry_Date__c)>=d){
                $scope.permits.push(record[0]);
                if(record[0].Volume__c!=undefined)$scope.activeOrders = $scope.activeOrders+record[0].Volume__c;
            }
        });

        var today = d.getTime().toString();
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        $scope.currentMonthSales = 0;
        $scope.avgSales = 0;
        $scope.promotions = [];
        $scope.lastThreeMonth = [];
        $scope.TotalMonthWiseSales = [];
        
        $scope.CategoryWiseMs = {
            Overall: 0,
            Mild: 0,
            Strong: 0
        };
        var monthVal = d.getMonth();
        var yearVal = d.getFullYear();
        var currentYear = d.getFullYear();
        var monthAndYearList = [];
        var monthAndYearListNew = [];
        var MonthAndCategoryWiseSales = [],
            industryMonthAndCategoryWiseSales = [];
        if ($scope.currentCustomer[0].Recent_Sales__c != undefined && $scope.currentCustomer[0].Recent_Sales__c != null) {
            monthAndYearList = $scope.currentCustomer[0].Recent_Sales__c.split(";");
            monthAndYearList = monthAndYearList.slice(0, 3);
            var tempMonthAndYearListNew = $scope.currentCustomer[0].Recent_Sales__c;
            tempMonthAndYearListNew = tempMonthAndYearListNew.replace(/-/g, "");
            monthAndYearListNew = tempMonthAndYearListNew.split(";");
            for (var i = 0; i < monthAndYearList.length; i++) {
                var tempMonthYear = monthAndYearList[i].split('-');
                monthVal = monthVal - 1;
                if (monthVal < 0) {
                    monthVal = 11;
                    yearVal = currentYear - 1;
                }
                $scope.TotalMonthWiseSales[tempMonthYear[0]] = 0;
                $scope.lastThreeMonth.push({
                    Month: tempMonthYear[0],
                    Year: tempMonthYear[1]
                });
                MonthAndCategoryWiseSales[tempMonthYear[0]] = {
                    Overall: 0,
                    Mild: 0,
                    Strong: 0
                };
                industryMonthAndCategoryWiseSales[tempMonthYear[0]] = {
                    Overall: 0,
                    Mild: 0,
                    Strong: 0
                };
                //monthAndYearList.push(months[monthVal] + yearVal);
            }
        }
        var rtList = SMARTSTORE.buildExactQuerySpec('DB_RecordTypes', 'SobjectType', 'Upload__c');
        var recordtypesWithIds = [];
        angular.forEach(rtList, function(record, key) {
            recordtypesWithIds[record.Name] = record.Id;
        });
        var uploads = SMARTSTORE.buildExactQuerySpec('Upload__c', 'Account__c', $stateParams.accountId);
        for (var i = 0; i < uploads.length; i++) {
            var tempRemoved=false;
            if(uploads[i].Removed__c==true){
                tempRemoved=true;
            }
            if (uploads[i].RecordTypeId == recordtypesWithIds['Sales'] && uploads[i].Sales__c != undefined && tempRemoved==false && uploads[i].Sales_Month__c != undefined && (monthAndYearListNew.indexOf(uploads[i].Sales_Month__c) != -1) || (uploads[i].Month__c == months[d.getMonth()] && uploads[i].Year__c == d.getFullYear())) {
                if (uploads[i].Company__c == 'UB') {
                    if (uploads[i].Month__c == months[d.getMonth()] && uploads[i].Year__c == d.getFullYear()) {
                        $scope.currentMonthSales = $scope.currentMonthSales + uploads[i].Sales__c;
                    } else {
                        MonthAndCategoryWiseSales[uploads[i].Month__c].Overall = MonthAndCategoryWiseSales[uploads[i].Month__c].Overall + parseInt(uploads[i].Sales__c);
                        if (uploads[i].Category__c == 'Mild' || uploads[i].Category__c == 'MILD' || uploads[i].Category__c === 'Mild Beer' || uploads[i].Category__c == 'MILD BEER') {
                            MonthAndCategoryWiseSales[uploads[i].Month__c].Mild = MonthAndCategoryWiseSales[uploads[i].Month__c].Mild + parseInt(uploads[i].Sales__c);
                        }
                        if (uploads[i].Category__c == 'Strong' || uploads[i].Category__c == 'STRONG' || uploads[i].Category__c == 'Strong Beer' || uploads[i].Category__c == 'STRONG BEER') {
                            MonthAndCategoryWiseSales[uploads[i].Month__c].Strong = MonthAndCategoryWiseSales[uploads[i].Month__c].Strong + parseInt(uploads[i].Sales__c);
                        }
                        $scope.TotalMonthWiseSales[uploads[i].Month__c] = $scope.TotalMonthWiseSales[uploads[i].Month__c] + parseInt(uploads[i].Sales__c);
                    }
                }
                if ((uploads[i].Category__c == 'Mild' || uploads[i].Category__c == 'MILD' || uploads[i].Category__c == 'Mild Beer' || uploads[i].Category__c == 'MILD BEER') && !(uploads[i].Month__c == months[d.getMonth()] && uploads[i].Year__c == d.getFullYear())) {
                    industryMonthAndCategoryWiseSales[uploads[i].Month__c].Mild = industryMonthAndCategoryWiseSales[uploads[i].Month__c].Mild + parseInt(uploads[i].Sales__c);
                }
                if (uploads[i].Category__c == 'Strong' || uploads[i].Category__c == 'STRONG' || uploads[i].Category__c == 'Strong Beer' || uploads[i].Category__c == 'STRONG BEER' && !(uploads[i].Month__c == months[d.getMonth()] && uploads[i].Year__c == d.getFullYear())) {
                    industryMonthAndCategoryWiseSales[uploads[i].Month__c].Strong = industryMonthAndCategoryWiseSales[uploads[i].Month__c].Strong + parseInt(uploads[i].Sales__c);
                }
                if (!(uploads[i].Month__c == months[d.getMonth()] && uploads[i].Year__c == d.getFullYear())) industryMonthAndCategoryWiseSales[uploads[i].Month__c].Overall = industryMonthAndCategoryWiseSales[uploads[i].Month__c].Overall + parseInt(uploads[i].Sales__c);
            }else if(uploads[i].RecordTypeId == recordtypesWithIds['Target'] && uploads[i].Month__c == months[d.getMonth()] && uploads[i].Year__c == d.getFullYear() && uploads[i].Target_volume__c!=undefined){
                $scope.target = uploads[i].Target_volume__c;
            }else if(uploads[i].RecordTypeId == recordtypesWithIds['MTD'] && uploads[i].Month__c == months[d.getMonth()] && uploads[i].Year__c == d.getFullYear() && uploads[i].MTD_Lifting__c!=undefined){
                $scope.mtd = uploads[i].MTD_Lifting__c;
            }
        }
        if (industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Overall != 0) {
            $scope.avgSales = industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Overall;
        }
        if (industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Overall != 0) {
            $scope.avgSales = $scope.avgSales + industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Overall;
        }
        if (industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[2].Month].Overall != 0) {
            $scope.avgSales = $scope.avgSales + industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[2].Month].Overall;
        }
        $scope.avgSales = Math.round($scope.avgSales / 3);
        // $log.debug(JSON.stringify(MonthAndCategoryWiseSales['Mar']));
        // $log.debug(JSON.stringify(industryMonthAndCategoryWiseSales['Mar']));  
        if (MonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Overall != 0) {
            $scope.CategoryWiseMs.Overall = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Overall / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Overall) * 100));
            $scope.CategoryWiseMs.Mild = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Mild / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Mild) * 100));
            $scope.CategoryWiseMs.Strong = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Strong / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month].Strong) * 100));
        } else if (MonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Overall != 0) {
            $scope.CategoryWiseMs.Overall = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Overall / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Overall) * 100));
            $scope.CategoryWiseMs.Mild = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Mild / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Mild) * 100));
            $scope.CategoryWiseMs.Strong = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Strong / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month].Strong) * 100));
        } else {
            $scope.CategoryWiseMs.Overall = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[2].Month].Overall / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[2].Month].Overall) * 100));
            $scope.CategoryWiseMs.Mild = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[2].Month].Mild / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[2].Month].Mild) * 100));
            $scope.CategoryWiseMs.Strong = Math.round(parseFloat((MonthAndCategoryWiseSales[$scope.lastThreeMonth[2].Month].Strong / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[2].Month].Strong) * 100));
        }
        if (isNaN($scope.CategoryWiseMs.Overall)) {
            $scope.CategoryWiseMs.Overall = 0;
        }
        if (isNaN($scope.CategoryWiseMs.Mild)) {
            $scope.CategoryWiseMs.Mild = 0;
        }
        if (isNaN($scope.CategoryWiseMs.Strong)) {
            $scope.CategoryWiseMs.Strong = 0;
        }
        $scope.newIndustryMonthAndCategoryWiseSales=industryMonthAndCategoryWiseSales;
        $ionicLoading.hide();
        $scope.gotoWholeSaleSammary = function() {
            $timeout(function(){
            $location.path('app/wholesalerlastVistSummary/' + $stateParams.accountId);
        },500);
    }
    $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('wholesaleOverviewCtrl ERROR', err.stack + '::' + err.message);
    }
}).controller('WholeSalerLastVisitSummaryCtrl', function($q, $log, $ionicLoading, $scope, $ionicPopup, $rootScope, $window, $stateParams, $location, $filter, $cordovaCamera, $ionicModal, $ionicSlideBoxDelegate, $timeout, SMARTSTORE, SOUP,GEO_LOCATION,MOBILEDATABASE_ADD, WEBSERVICE_ERROR) {
    try {
        $ionicLoading.show({
            template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
        });
        $rootScope.params = $stateParams;
        $rootScope.name = "WholeSalerLastVisitSummaryCtrl";
        $rootScope.backText = 'Back';
        $rootScope.showBack = true;
        $scope.breadList = [{
            name: 'Summary'
        }, {
            name: 'Last Visit Summary'
        }];
        $scope.currentCustomer = SMARTSTORE.buildExactQuerySpec('Account', 'Id', $stateParams.accountId);
        var noOfVisits = 2;
        var d = new Date();
        var today = d.getTime().toString();
        var pendingTasks = SMARTSTORE.buildExactQuerySpec('Db_transaction', 'status', 'pending');
        if (pendingTasks!= undefined && pendingTasks!= 0 && pendingTasks[0].Account == $stateParams.accountId) {
            noOfVisits = 3;
        }
        var lastVisit  = SMARTSTORE.buildSmartQuerySpec("SELECT {Visit__c:_soup} FROM {Visit__c} WHERE {Visit__c:Account__c}='" + $stateParams.accountId + "' ORDER BY {Visit__c:Check_In_DateTime__c} DESC", noOfVisits);
        if(lastVisit.length>0){
            $scope.lastVisitRecord = lastVisit[0][0];
            $scope.lastVisitRecord.Check_In_DateTime__c = $filter('date')($scope.lastVisitRecord.Check_In_DateTime__c, 'dd MMM yyyy');
            var query = "";
            for (var i = 0; i < lastVisit.length; i++) {
                query = query + " OR {Visit_Summary__c:Visit__c} = '" + lastVisit[i][0].External_Id__c + "'";
                if (lastVisit[i][0].Id != undefined) query = query + " OR {Visit_Summary__c:Visit__c} = '" + lastVisit[i][0].Id + "'";
            }
            var rtList =  SMARTSTORE.buildSmartQuerySpec("SELECT  {DB_RecordTypes:_soup} FROM {DB_RecordTypes} WHERE {DB_RecordTypes:SobjectType}='Visit_Summary__c' AND {DB_RecordTypes:Name}='Feedback and Complaints'", 1);
            var visitSummaryrecordsLocal = SMARTSTORE.buildSmartQuerySpec("SELECT {Visit_Summary__c:_soup} FROM {Visit_Summary__c} WHERE ({Visit_Summary__c:Account_Id__c} = '" + $stateParams.accountId + "' or {Visit_Summary__c:AccountId__c} = '" + $stateParams.accountId + "') AND {Visit_Summary__c:RecordTypeId} = '" + rtList[0][0].Id + "' AND  ({Visit_Summary__c:Status__c} = 'Open'" + query + " )", 100);
            $scope.visitSummaries = [];
            for (var i = 0; i < visitSummaryrecordsLocal.length; i++) {
                var record = visitSummaryrecordsLocal[i][0];
                if (record.Date__c != undefined) {
                    var createdDate;
                    if (record.Date__c.split('-')[0].length == 4) createdDate = new Date(record.Date__c);
                    else {
                        createdDate = new Date(record.Date__c.split('/')[1] + '/' + record.Date__c.split('/')[0] + '/' + record.Date__c.split('/')[2]);
                    }
                    record.Date__c = createdDate.getTime().toString();
                }
                $scope.visitSummaries.push(record);
            }
        }
        $ionicLoading.hide();
        $scope.wholeSalerCheckIn = function() {
            $timeout(function(){
            var pendingTasks = SMARTSTORE.buildExactQuerySpec('Db_transaction', 'status', 'pending');
            if ($rootScope.day == 'today') {
                if (pendingTasks.length == 0) {
                    if ($rootScope.day == 'today') {
                        $ionicLoading.show({
                            template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
                        });
                        $rootScope.StockAndSalesList = [];
                        $rootScope.uploadfields = [];
                        $rootScope.processFlowLineItems = [];
                        $rootScope.currentvisitSummaryImages = [];
                        $rootScope.StockAndSalesList = [];
                        $rootScope.retailSalesList = {};
                        $rootScope.uploadfields = [];
                        $rootScope.currentvisitSummaryImages = [];
                        $rootScope.galleryImages =[];
                        $rootScope.slidesList = [];
                        $rootScope.slideFields = [];
                        $rootScope.PrideImages = [];
                        $rootScope.visitImages = [];
                        $rootScope.CompeSales = [];
                        $rootScope.UblSales = [];
                        $rootScope.chataiSales = [];
                        $rootScope.audit = {};
                        $rootScope.tempvisitType = 'false';
                        $rootScope.pendingTask = true;
                        $rootScope.visitrecord = {};
                        if (SOUP.isExists('Db_breadCrum') == true) {
                            SOUP.remove('Db_breadCrum');
                        }
                        SOUP.register('Db_breadCrum', [{
                            "path": "Name",
                            "type": "string"
                        }, {
                            "path": "VisitedOrder",
                            "type": "string"
                        }]);
                        $scope.currentLocation = {};
                        GEO_LOCATION.getCurrentPosition().then(function(position) {
                            $scope.currentLocation.latitude = position.latitude;
                            $scope.currentLocation.longitude = position.longitude;
                            $scope.Visit = [{
                                Account__c: $scope.currentCustomer[0].Id,
                                Check_In_DateTime__c: today,
                                Check_In_Location__Latitude__s: $scope.currentLocation.latitude,
                                Check_In_Location__Longitude__s: $scope.currentLocation.longitude,
                                External_Id__c: (new Date().getTime())+'-'+$stateParams.visitId+'-'+$window.Math.random() * 100000000,
                                Route_Plan__c: $scope.currentCustomer[0].RoutePlanId,
                                IsDirty: true
                            }];
                            if ($scope.currentCustomer[0].Location__Latitude__s == undefined || $scope.currentCustomer[0].Location__Latitude__s == null) {
                                $scope.currentCustomer[0].Location__Latitude__s = $scope.currentLocation.latitude;
                                $scope.currentCustomer[0].Location__Longitude__s = $scope.currentLocation.longitude;
                                $scope.currentCustomer[0].IsDirty = true;
                                
                                MOBILEDATABASE_ADD('Account', $scope.currentCustomer, 'Id');
                            } else if ($scope.distance($scope.currentCustomer[0].Location__Latitude__s, $scope.currentCustomer[0].Location__Longitude__s, $scope.currentLocation.latitude, $scope.currentLocation.longitude, 'K') > 1) {
                                $scope.Visit[0].Deviation_Reason__c = 'location is different from existing location of the outlet';
                            }
                            MOBILEDATABASE_ADD('Visit__c', $scope.Visit, 'External_Id__c');
                            var flow = SMARTSTORE.buildSmartQuerySpec("SELECT  {Db_process_flow:_soup} FROM {Db_process_flow} WHERE {Db_process_flow:Outlet_Type__c}='" + $scope.currentCustomer[0].RecordType.Name + "'", 1);
                            var flowLineItems = SMARTSTORE.buildExactQuerySpec('Db_process_flow_line_item', "Process_Flow__c", flow[0][0].Id);
                            var queryspec = '';
                            var queryspecForSection = '';
                            angular.forEach(flowLineItems, function(record, key) {
                                if(record['Removed__c']==false){
                                    $rootScope.processFlowLineItems[record.Order__c - 1] = record;
                                    if (queryspec == '' && queryspecForSection == '') {
                                        queryspec = "{DB_page:Id}='" + record.Page__c + "'";
                                        queryspecForSection = "{DB_section:Page__c}='" + record.Page__c + "'";
                                    } else {
                                        queryspec = queryspec + " OR {DB_page:Id}='" + record.Page__c + "'";
                                        queryspecForSection = queryspecForSection + " OR {DB_section:Page__c}='" + record.Page__c + "'";
                                    }
                                }
                            });
                            if (queryspec != '' && queryspecForSection != '') {
                                var pages = SMARTSTORE.buildSmartQuerySpec("SELECT  {DB_page:_soup} FROM {DB_page} WHERE " + queryspec, 20);
                                var sections = SMARTSTORE.buildSmartQuerySpec("SELECT  {DB_section:_soup} FROM {DB_section} WHERE " + queryspecForSection + " order by {DB_section:Order__c}", 20);
                                queryspec = '';
                                angular.forEach(sections, function(record, key) {
                                    if (queryspec == '') {
                                        queryspec = "{DB_fields:Section__c}='" + record[0].Id + "'";
                                    } else {
                                        queryspec = queryspec + " OR {DB_fields:Section__c}='" + record[0].Id + "'";
                                    }
                                });
                                var fields = [];
                                if (queryspec != '') {
                                    fields = SMARTSTORE.buildSmartQuerySpec("SELECT  {DB_fields:_soup} FROM {DB_fields} WHERE " + queryspec + " and {DB_fields:Removed__c}='false' order by {DB_fields:Order__c}", 200);
                                }
                                var pagesWithIds = [];
                                angular.forEach(pages, function(page, pageKey) {
                                    var pagedec = {};
                                    pagedec.pageDescription = page[0];
                                    pagesWithIds[page[0].Id] = page[0];
                                    pagedec.sections = [];
                                    angular.forEach(sections, function(section, sectionKey) {
                                        if (page[0].Id == section[0].Page__c) {
                                            var sectiondec = {};
                                            sectiondec.sectionDescription = section[0];
                                            sectiondec.fields = [];
                                            angular.forEach(fields, function(field, fieldKey) {
                                                if (field[0].Section__c == section[0].Id) {
                                                    var fieldDescription = field[0];
                                                    if (field[0].Picklist_Values__c != undefined) {
                                                        fieldDescription.pickListValues = field[0].Picklist_Values__c.split(';');
                                                    }
                                                    sectiondec.fields.push(fieldDescription);
                                                }
                                            });
                                            pagedec.sections.push(sectiondec);
                                        }
                                    });
                                    $rootScope.pages.push(pagedec);
                                });
                            }

                            var page = pagesWithIds[$rootScope.processFlowLineItems[0].Page__c];
                            $location.path('app/'+page.Template_Name__c+'/' + $scope.currentCustomer[0].Id + '/' + $scope.Visit[0].External_Id__c + '/0/' + new Date());
                        }, function(error) {
                            $log.error('ERROR' + JSON.stringify(error));
                        });
                    }
                } else {
                    $ionicLoading.hide();
                    Splash.ShowToast('A previous call has not been completed. Tap Resume icon to complete the open call.', 'long', 'bottom', function(a) {
                        console.log(a)
                    });
                }
            } else {
                $ionicLoading.hide();
                Splash.ShowToast('You can only start a call for outlets in today\'s route plan.', 'long', 'bottom', function(a) {
                    console.log(a)
                });
            }
        },500);
        }
        $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('WholeSalerLastVisitSummaryCtrl error', err.stack + '::' + err.message);
    }
}).controller('MarketShareCtrl', function($scope, $stateParams, $window, $location, $rootScope, $ionicLoading, $log, $filter, $ionicSlideBoxDelegate, FETCH_LOCAL_DATA, FETCH_DATA_LOCAL, WEBSERVICE_ERROR) {
    try {
        $ionicLoading.show({
            template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
        });
        $rootScope.params = $stateParams;
        $rootScope.name = "MarketShareCtrl";
        $scope.MSbreadList = [{
            name: 'Mild'
        }, {
            name: 'Strong'
        }];
        
        function getMonthsNoSales(currentMonth,currentYear) {
            var date = new Date();
            var date2 = new Date();
            //subttract 3 months
            date.setMonth(currentMonth-1);
            if(currentMonth==0)
            date.setFullYear(currentYear-1);
            else
            date.setFullYear(currentYear);
            var monthArray = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun",
                    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
        
            var menuMonths = new Array();
            var monthString='';
            var count = 3;
            var buffer = 10;
        
            while (count > 0) {
                var month = monthArray[date.getMonth()];
        
                if(currentMonth==1 && count==2)
                    date.setFullYear(currentYear-1);
                if(currentMonth==2 && count==1)
                    date.setFullYear(currentYear-1);
                menuMonths.push(month+"-"+date.getFullYear());
                monthString=monthString+month+"-"+date.getFullYear()+";"
                date.setMonth(date.getMonth() - 1);
        
                count -= 1;
            }
            return monthString;
        }
        $scope.Math = window.Math;
        $rootScope.backText = 'Back';
        $rootScope.showBack = true;
        $scope.lastThreeMonth = [];
        var customer = FETCH_LOCAL_DATA('Account', 'Id', $stateParams.accountId);
        if (customer.length != 0 && customer[0] != undefined) {
            $scope.currentCustomer = customer[0];
        }
        var MonthAndCategoryWiseSales = [],
            industryMonthAndCategoryWiseSales = [];
        if ($scope.currentCustomer.Recent_Sales__c == undefined || $scope.currentCustomer.Recent_Sales__c == null || $scope.currentCustomer.Recent_Sales__c == ''){
            var currDate=new Date();
            $scope.currentCustomer.Recent_Sales__c = getMonthsNoSales(currDate.getMonth(),currDate.getFullYear());
        }else{
            var fakeMonthYearList = $scope.currentCustomer.Recent_Sales__c.split(";");
            if(fakeMonthYearList.length<4){
                var fakeFirstMonthYear=fakeMonthYearList[0];
                var fakeFirstMonthYearArray = fakeFirstMonthYear.split('-');
                var fakeMonthList="JanFebMarAprMayJunJulAugSepOctNovDec";
                $scope.currentCustomer.Recent_Sales__c = getMonthsNoSales((fakeMonthList.indexOf(fakeFirstMonthYearArray[0]) / 3+1),fakeFirstMonthYearArray[1]);
            }
        }
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var d = new Date();
        var monthVal = d.getMonth();
        var yearVal = d.getFullYear();
        var currentYear = d.getFullYear();
        var monthAndYearList = [];
        if($scope.currentCustomer.Recent_Sales__c==undefined)
            $scope.currentCustomer.Recent_Sales__c='Mar-2016;Feb-2016;Jan-2016;';
        monthAndYearList = $scope.currentCustomer.Recent_Sales__c.split(";");
        monthAndYearList = monthAndYearList.slice(0, 3);
        monthAndYearList.push(months[monthVal] + '-' + yearVal);
        var monthAndYearListNew = [];
        var tempMonthAndYearListNew = $scope.currentCustomer.Recent_Sales__c;
        tempMonthAndYearListNew = tempMonthAndYearListNew.replace(/-/g, "");
        monthAndYearListNew = tempMonthAndYearListNew.split(";");
        monthAndYearListNew = monthAndYearListNew.slice(0, 3);
        monthAndYearListNew.push(months[monthVal] + yearVal);
        industryMonthAndCategoryWiseSales[months[monthVal]] = {
            Overall: 0,
            Mild: 0,
            Strong: 0
        };
        for (var i = 0; i < monthAndYearList.length; i++) {
            var tempMonthYear = monthAndYearList[i].split('-');
            monthVal = monthVal - 1;
            if (monthVal < 0) {
                monthVal = 11;
                yearVal = currentYear - 1;
            }
            $scope.lastThreeMonth.push({
                Month: tempMonthYear[0],
                Year: tempMonthYear[1]
            });
            industryMonthAndCategoryWiseSales[tempMonthYear[0]] = {
                Overall: 0,
                Mild: 0,
                Strong: 0
            };
            //monthAndYearList.push(months[monthVal] + yearVal);
        }
        var rtList = FETCH_LOCAL_DATA('DB_RecordTypes', 'SobjectType', 'Upload__c');
        var recordtypesWithIds = [];
        angular.forEach(rtList, function(record, key) {
            recordtypesWithIds[record.Name] = record.Id;
        });
        $scope.historicalSalesCategoryWise = [];
        $scope.historicalSalesCategoryWise['Mild'] = [];
        $scope.historicalSalesCategoryWise['Strong'] = [];
        var SalesMonthsAvailable = [];
        var CategoryWiseSales = [];
        var uploads = FETCH_LOCAL_DATA('Upload__c', 'Account__c', $stateParams.accountId);
        var salesRecords = [];
        var products = [];
        $scope.categoryList = ["Mild", "Strong"];
        for (var i = 0; i < uploads.length; i++) {
            var tempRemoved=false;
            if(uploads[i].Removed__c==true){
                tempRemoved=true;
            }
            if (uploads[i].RecordTypeId == recordtypesWithIds['Sales'] && uploads[i].Sales__c != undefined && tempRemoved==false && uploads[i].Sales_Month__c != undefined && monthAndYearListNew.indexOf(uploads[i].Sales_Month__c) != -1) {
                if (uploads[i].Category__c == 'Mild' || uploads[i].Category__c == 'MILD' || uploads[i].Category__c == 'Mild Beer' || uploads[i].Category__c == 'MILD BEER') {
                    industryMonthAndCategoryWiseSales[uploads[i].Month__c].Mild = industryMonthAndCategoryWiseSales[uploads[i].Month__c].Mild + parseInt(uploads[i].Sales__c);
                }
                if (uploads[i].Category__c == 'Strong' || uploads[i].Category__c == 'STRONG' || uploads[i].Category__c == 'Strong Beer' || uploads[i].Category__c == 'STRONG BEER') {
                    industryMonthAndCategoryWiseSales[uploads[i].Month__c].Strong = industryMonthAndCategoryWiseSales[uploads[i].Month__c].Strong + parseInt(uploads[i].Sales__c);
                }
                industryMonthAndCategoryWiseSales[uploads[i].Month__c].Overall = industryMonthAndCategoryWiseSales[uploads[i].Month__c].Overall + parseInt(uploads[i].Sales__c);
                if (products.indexOf(uploads[i].Brands_and_Products__c) == -1) {
                    products.push(uploads[i].Brands_and_Products__c);
                }
                var record = {};
                if (salesRecords[uploads[i].Brands_and_Products__c] != undefined) {
                    record = salesRecords[uploads[i].Brands_and_Products__c];
                } else {
                    salesRecords[uploads[i].Brands_and_Products__c] = [];
                    record['productName'] = uploads[i].SKU__c;
                    record['category'] = uploads[i].Category__c;
                    record[$scope.lastThreeMonth[0].Month] = {
                        Sales: '-',
                        CurrentMs: 0,
                        LastMs: 0,
                        avgSales: '-',
                        Color: 'black'
                    };
                    record[$scope.lastThreeMonth[1].Month] = {
                        Sales: '-',
                        CurrentMs: 0,
                        LastMs: 0,
                        avgSales: '-',
                        Color: 'black'
                    };
                    record[$scope.lastThreeMonth[2].Month] = {
                        Sales: '-',
                        CurrentMs: 0,
                        LastMs: 0,
                        avgSales: '-',
                        Color: 'black'
                    };
                    record[$scope.lastThreeMonth[3].Month] = {
                        Sales: '-',
                        CurrentMs: 0,
                        LastMs: 0,
                        avgSales: '-',
                        Color: 'black'
                    };
                }
                if (SalesMonthsAvailable.indexOf(uploads[i].Month__c) != -1) {
                    SalesMonthsAvailable.push(uploads[i].Month__c);
                }
                if (uploads[i].Month__c == $scope.lastThreeMonth[0].Month) {
                    record['LatestSales'] = uploads[i].Sales__c;
                    record['VolumeOrder'] = uploads[i].Order__c;
                }
                if (record['LatestSales'] == undefined || record['LatestSales'] == null || record['LatestSales'] == '') {
                    record['LatestSales'] = 0;
                }
                if (record['VolumeOrder'] == undefined || record['VolumeOrder'] == null || record['VolumeOrder'] == '') {
                    record['VolumeOrder'] = 0;
                }
                record[uploads[i].Month__c] = {
                    Sales: uploads[i].Sales__c,
                    CurrentMs: Math.round(parseFloat(uploads[i].Current_MS__c)),
                    LastMs: Math.round(parseFloat(uploads[i].Last_MS__c)),
                    avgSales: Math.round(parseFloat(uploads[i].Average_Sales__c)),
                    Color: uploads[i].Color__c
                };
                salesRecords[uploads[i].Brands_and_Products__c] = record;
            }
        }
        angular.forEach(products, function(sku, key) {
            //$log.debug('SalesRecord'+sku);
            var salesRec = salesRecords[sku];
            /*if (SalesMonthsAvailable.indexOf($scope.lastThreeMonth[0].Month) != -1 && salesRec[$scope.lastThreeMonth[0].Month].Sales == '-') {
                salesRec[$scope.lastThreeMonth[0].Month].Sales = 0;
            } else {
                if (salesRec[$scope.lastThreeMonth[0].Month].Sales != '-' && salesRec[$scope.lastThreeMonth[0].Month].Sales > 0) {
                    salesRec[$scope.lastThreeMonth[0].Month].CurrentMs = Math.round((salesRec[$scope.lastThreeMonth[0].Month].Sales / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[0].Month][salesRec.category]) * 100);
                }
            }
            if (SalesMonthsAvailable.indexOf($scope.lastThreeMonth[1].Month) != -1 && salesRec[$scope.lastThreeMonth[1].Month].Sales == '-') {
                salesRec[$scope.lastThreeMonth[1].Month].Sales = 0;
            } else {
                if (salesRec[$scope.lastThreeMonth[1].Month].Sales != '-' && salesRec[$scope.lastThreeMonth[1].Month].Sales > 0) {
                    salesRec[$scope.lastThreeMonth[1].Month].CurrentMs = Math.round((salesRec[$scope.lastThreeMonth[1].Month].Sales / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[1].Month][salesRec.category]) * 100);
                }
            }
            if (SalesMonthsAvailable.indexOf($scope.lastThreeMonth[2].Month) != -1 && salesRec[$scope.lastThreeMonth[2].Month].Sales == '-') {
                salesRec[$scope.lastThreeMonth[2].Month].Sales = 0;
            } else {
                if (salesRec[$scope.lastThreeMonth[2].Month].Sales != '-' && salesRec[$scope.lastThreeMonth[2].Month].Sales > 0) {
                    salesRec[$scope.lastThreeMonth[2].Month].CurrentMs = Math.round((salesRec[$scope.lastThreeMonth[2].Month].Sales / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[2].Month][salesRec.category]) * 100);
                }
            }
            if (SalesMonthsAvailable.indexOf($scope.lastThreeMonth[3].Month) != -1 && salesRec[$scope.lastThreeMonth[3].Month].Sales == '-') {
                salesRec[$scope.lastThreeMonth[3].Month].Sales = 0;
            } else {
                if (salesRec[$scope.lastThreeMonth[3].Month].Sales != '-' && salesRec[$scope.lastThreeMonth[3].Month].Sales > 0) {
                    salesRec[$scope.lastThreeMonth[3].Month].CurrentMs = Math.round((salesRec[$scope.lastThreeMonth[3].Month].Sales / industryMonthAndCategoryWiseSales[$scope.lastThreeMonth[3].Month][salesRec.category]) * 100);
                }
            }*/
            var avgsales = 0,
                sales1 = salesRec[$scope.lastThreeMonth[0].Month].Sales,
                sales2 = salesRec[$scope.lastThreeMonth[1].Month].Sales,
                sales3 = salesRec[$scope.lastThreeMonth[2].Month].Sales;
            if (sales1 != '-') {
                avgsales = avgsales + parseInt(sales1);
            }
            if (sales2 != '-') {
                avgsales = avgsales + parseInt(sales2);
            }
            if (sales3 != '-') {
                avgsales = avgsales + parseInt(sales3);
            }
            /*
             *Description : now average is not calculated but is taken from most recent sales average field.
             *Date : 28-05-2015
             *Author : Kunal Bindal
             */
            if (salesRec[$scope.lastThreeMonth[0].Month].avgSales != '-') {
                salesRec['AvgSales'] = salesRec[$scope.lastThreeMonth[0].Month].avgSales;
            } else if (salesRec[$scope.lastThreeMonth[1].Month].avgSales != '-') {
                salesRec['AvgSales'] = salesRec[$scope.lastThreeMonth[1].Month].avgSales;
            } else if (salesRec[$scope.lastThreeMonth[2].Month].avgSales != '-') {
                salesRec['AvgSales'] = salesRec[$scope.lastThreeMonth[2].Month].avgSales;
            }
            $log.debug('Avg Sales ===' + avgsales);
            $log.debug($scope.lastThreeMonth[0].Month + '===' + sales1);
            $log.debug($scope.lastThreeMonth[1].Month + '===' + sales2);
            $log.debug($scope.lastThreeMonth[2].Month + '===' + sales3);
            avgsales = Math.round(parseFloat(avgsales / 3));
            //salesRec['AvgSales'] = avgsales;
            if ($scope.historicalSalesCategoryWise[salesRecords[sku].category] == undefined) {
                $scope.historicalSalesCategoryWise[salesRecords[sku].category] = [];
            }
            $scope.historicalSalesCategoryWise[salesRecords[sku].category].push(salesRecords[sku]);
        });
        
        //Salience to be calculated correctly
        $scope.monthTotal = [];
        $scope.monthTotal['Mild'] = [];
        $scope.monthTotal['Strong'] = [];
        $scope.monthTotal['Mild'][$scope.lastThreeMonth[0].Month] = 0;
        $scope.monthTotal['Strong'][$scope.lastThreeMonth[0].Month] = 0;
        $scope.monthTotal['Mild'][$scope.lastThreeMonth[1].Month] = 0;
        $scope.monthTotal['Strong'][$scope.lastThreeMonth[1].Month] = 0;
        for (var i = 0; i < $scope.historicalSalesCategoryWise['Mild'].length; i++) {
            var salesRecord = $scope.historicalSalesCategoryWise['Mild'][i];
            if (salesRecord[$scope.lastThreeMonth[0].Month].Sales != '-') $scope.monthTotal['Mild'][$scope.lastThreeMonth[0].Month] = $scope.monthTotal['Mild'][$scope.lastThreeMonth[0].Month] + salesRecord[$scope.lastThreeMonth[0].Month].Sales;
            if (salesRecord[$scope.lastThreeMonth[1].Month].Sales != '-') $scope.monthTotal['Mild'][$scope.lastThreeMonth[1].Month] = $scope.monthTotal['Mild'][$scope.lastThreeMonth[1].Month] + salesRecord[$scope.lastThreeMonth[1].Month].Sales;
        }
        for (var i = 0; i < $scope.historicalSalesCategoryWise['Strong'].length; i++) {
            var salesRecord = $scope.historicalSalesCategoryWise['Strong'][i];
            if (salesRecord[$scope.lastThreeMonth[0].Month].Sales != '-') $scope.monthTotal['Strong'][$scope.lastThreeMonth[0].Month] = $scope.monthTotal['Strong'][$scope.lastThreeMonth[0].Month] + salesRecord[$scope.lastThreeMonth[0].Month].Sales;
            if (salesRecord[$scope.lastThreeMonth[1].Month].Sales != '-') $scope.monthTotal['Strong'][$scope.lastThreeMonth[1].Month] = $scope.monthTotal['Strong'][$scope.lastThreeMonth[1].Month] + salesRecord[$scope.lastThreeMonth[1].Month].Sales;
        }
        //Salience to be calculated correctly
        if ($scope.historicalSalesCategoryWise['Mild'].length < 1) {
            Splash.ShowToast('No sales found for Mild.', 'long', 'bottom', function(a) {
                console.log(a)
            });
        }
        $scope.prevSlide = function() {
            $ionicSlideBoxDelegate.previous();
            if ($scope.historicalSalesCategoryWise['Mild'].length < 1) {
                Splash.ShowToast('No sales found for Mild.', 'long', 'bottom', function(a) {
                    console.log(a)
                });
            }
        }
        $scope.nextSlide = function() {
            $ionicSlideBoxDelegate.next();
            if ($scope.historicalSalesCategoryWise['Strong'].length < 1) {
                Splash.ShowToast('No sales found for Strong.', 'long', 'bottom', function(a) {
                    console.log(a)
                });
            }
        }
        $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('MarketShareCtrlError', err.stack + '::' + err.message);
    }
}).controller('BusinessObjectivesCtrl', function(MOBILEDATABASE_ADD,$scope, $stateParams, $location, $rootScope, $ionicLoading, $log, $filter, SalesforceSession, FETCH_DATA_LOCAL, FETCH_DATA, WEBSERVICE_ERROR) {
    try {
        $ionicLoading.show({
            template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
        });
        $rootScope.params = $stateParams;
        $rootScope.name = "BusinessObjectivesCtrl";
        $rootScope.backText = 'Back';
        $scope.breadList = [{
            name: 'Outlet Summary '
        }, {
            name: 'Business Objectives'
        }, {
            name: 'Last Visit Summary'
        }];
        $scope.currentAccount = {};
        var currentAccRecord = FETCH_DATA.querySoup('Account', 'Id', $stateParams.accountId);
        $scope.currentAccount = currentAccRecord.currentPageOrderedEntries[0];
        $scope.gotoLastvistSummary = function() {
             //User log
            var today_now = $filter('date')(new Date(), 'dd/MM/yyyy hh:mm a');
            var userOprObj=[{
                ObjectData:'',
                time_stamp:today_now,
                comment:'Clicked on next from Business Objectives to go Last visit Summary Screen.'
            }];
            MOBILEDATABASE_ADD('Db_User_Logs', userOprObj, '_soupEntryId');
            $location.path('app/lastVistSummary/' + $scope.currentAccount.Id);
        }
        var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
        var rtList = FETCH_DATA_LOCAL('DB_RecordTypes', sfSmartstore.buildSmartQuerySpec("SELECT  {DB_RecordTypes:_soup} FROM {DB_RecordTypes} WHERE {DB_RecordTypes:SobjectType}='Upload__c' AND {DB_RecordTypes:Name}='Business Objectives'", 1));
        if(rtList!=undefined && rtList.currentPageOrderedEntries!=undefined && rtList.currentPageOrderedEntries[0]!=undefined && rtList.currentPageOrderedEntries[0][0]!=undefined ){
            var businessObjectivesRecords = FETCH_DATA_LOCAL('Upload__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Upload__c:_soup} FROM {Upload__c} WHERE {Upload__c:Account__c}='" + $stateParams.accountId + "' AND {Upload__c:RecordTypeId}='" + rtList.currentPageOrderedEntries[0][0].Id + "'", 100000));
            var date = new Date();
            $scope.businessObjectives = [];
            angular.forEach(businessObjectivesRecords.currentPageOrderedEntries, function(record, key) {
                var startDate = new Date(record[0].Start_Date__c);
                var endDate = new Date(record[0].End_Date__c);
                if (startDate <= date && endDate >= date) {
                    $scope.businessObjectives.push(record[0]);
                }
            });
        }
        $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('BusinessObjectivesCtrlError', err.stack + '::' + err.message);
    }
}).controller('LastVisitSummaryCtrl', function($scope, $stateParams, $ionicSlideBoxDelegate, $cordovaCamera, $rootScope, $window, $state, $location, $q, $ionicModal, $timeout, $interval, $ionicLoading, $log, $filter, SalesforceSession, FETCH_DATA_LOCAL, SOUP_EXISTS, FETCH_DAILY_PLAN_ACCOUNTS, MOBILEDATABASE_ADD, FETCH_DATA, WEBSERVICE_ERROR, GEO_LOCATION, SOUP_REGISTER) {
    try {
        $ionicLoading.show({
            template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
        });
        $rootScope.params = $stateParams;
        $rootScope.name = "LastVisitSummaryCtrl";
        var isRestartCall='';
        $scope.breadList = [{
            name: 'Outlet Summary '
        }, {
            name: 'Business Objectives'
        }, {
            name: 'Last Visit Summary'
        }];
        $rootScope.backText = 'Back';
        $scope.currentAccount = {};
        var currentAccRecord = FETCH_DATA.querySoup('Account', 'Id', $stateParams.accountId);
        $scope.currentAccount = currentAccRecord.currentPageOrderedEntries[0];
        var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
        var d = new Date();
        var today = d.getTime().toString();
        var todayRoutePlan = FETCH_DAILY_PLAN_ACCOUNTS('today');
        angular.forEach(todayRoutePlan, function(acc, accKey) {
            if (acc.Id == $scope.currentAccount.Id) $scope.currentAccount.RoutePlanId = acc.RoutePlanId;
        });
        $scope.openComplaints = [];
        var rtList = FETCH_DATA_LOCAL('DB_RecordTypes', sfSmartstore.buildSmartQuerySpec("SELECT  {DB_RecordTypes:_soup} FROM {DB_RecordTypes} WHERE {DB_RecordTypes:SobjectType}='Visit_Summary__c' AND {DB_RecordTypes:Name}='Feedback and Complaints'", 1));
        $scope.visitSummaries = [];
        var noOfVisits = 2;
        var pendingTasks = FETCH_DATA.querySoup('Db_transaction', 'status', 'pending');
        if (pendingTasks.currentPageOrderedEntries != undefined && pendingTasks.currentPageOrderedEntries.length != 0 && pendingTasks.currentPageOrderedEntries[0].Account == $stateParams.accountId) {
            noOfVisits = 3;
        }
        var lastVisit = FETCH_DATA_LOCAL('Visit__c', sfSmartstore.buildSmartQuerySpec("SELECT {Visit__c:_soup} FROM {Visit__c} WHERE {Visit__c:Account__c}='" + $stateParams.accountId + "' and {Visit__c:Check_Out_DateTime__c}!='' ORDER BY {Visit__c:Check_In_DateTime__c} DESC", noOfVisits));
        $scope.lastVisitRecord = {};
        $scope.OldSKU = [];
        $scope.OldestSKU = [];
        $scope.noSKU = [];
        var monthList = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var query = '';
        var d = new Date();
        $scope.currentMonth = $scope.MonthsList[d.getMonth()];
        var lastMonth = d.getMonth() - 1;
        var currentYear = d.getFullYear();
        $scope.stocDetailsPickValues = [];
        var d = new Date();
        var monthVal = d.getMonth();
        var yearVal = d.getFullYear();
        var monthDropdownValues = [];
        for (var i = 0; i < 5; i++) {
            monthVal = monthVal - 1;
            if (monthVal < 0) {
                monthVal = 11;
                yearVal = currentYear - 1;
            }
            if (i >= 2) {
                monthDropdownValues.push(monthList[monthVal] + ' - ' + yearVal.toString().substr(2, 3));
            }
        }
        //var skuMasterList = FETCH_DATA.querySoup('Brands_and_Products__c', 'Id', null);
        var skuMasterList = FETCH_DATA_LOCAL('Brands_and_Products__c', sfSmartstore.buildSmartQuerySpec("SELECT {Brands_and_Products__c:_soup} FROM {Brands_and_Products__c} WHERE {Brands_and_Products__c:Removed__c}='false' and {Brands_and_Products__c:Competitor_Product__c}='false' and {Brands_and_Products__c:Used_in_Chatai__c}='false'", 1000));
        var skuMasterWithIds = [];
        angular.forEach(skuMasterList.currentPageOrderedEntries, function(recordTemp, key) {
            var record=recordTemp[0];
            if (record.Sku_Code__c != undefined && record.Competitor_Product__c==false && record.Used_in_Chatai__c==false) {
                skuMasterWithIds[record.Id] = record['Sku_Code__c'];
            }
        });
        if (lastVisit!=undefined && lastVisit.currentPageOrderedEntries != undefined && lastVisit.currentPageOrderedEntries.length > 0 && lastVisit.currentPageOrderedEntries[0][0] != undefined) {
            $scope.lastVisitRecord = lastVisit.currentPageOrderedEntries[0][0];
            angular.forEach($rootScope.Outlets, function(record, key) {
                if($stateParams.accountId==record.Id && record.isCompleted=='saved'){
                    //To show last call other then today's call
                    //$scope.lastVisitRecord = lastVisit.currentPageOrderedEntries[1][0];
                    isRestartCall=lastVisit.currentPageOrderedEntries[0][0]['External_Id__c']
                }
            });
            
            var skuList1 = FETCH_DATA.querySoup('Visit_Summary__c', 'Visit__c', $scope.lastVisitRecord.External_Id__c);
            angular.forEach(skuList1.currentPageOrderedEntries, function(record, key) {
                if (record.Stock_Ageing__c == 'Older' && skuMasterWithIds[record.Product__c]!=undefined) {
                    record.SKUName = skuMasterWithIds[record.Product__c];
                    $scope.OldestSKU.push(record);
                } else if (monthDropdownValues.indexOf(record.Stock_Ageing__c) != -1 && skuMasterWithIds[record.Product__c]!=undefined) {
                    record.SKUName = skuMasterWithIds[record.Product__c];
                    $scope.OldSKU.push(record);
                }
                //Records with no availablity
                if (record.Stock_Available__c == false && skuMasterWithIds[record.Product__c]!=undefined && record.Audit_Type__c != 'Audit') {
                    record.SKUName = skuMasterWithIds[record.Product__c];
                    $scope.noSKU.push(record);
                }
            });
            if ($scope.lastVisitRecord.Id != undefined) {
                var skuList2 = FETCH_DATA.querySoup('Visit_Summary__c', 'Visit__c', $scope.lastVisitRecord.Id);
                angular.forEach(skuList2.currentPageOrderedEntries, function(record, key) {
                    if (record.Stock_Ageing__c == 'Older' && skuMasterWithIds[record.Product__c]!=undefined) {
                        record.SKUName = skuMasterWithIds[record.Product__c];
                        $scope.OldestSKU.push(record);
                    } else if (monthDropdownValues.indexOf(record.Stock_Ageing__c) != -1  && skuMasterWithIds[record.Product__c]!=undefined) {
                        record.SKUName = skuMasterWithIds[record.Product__c];
                        $scope.OldSKU.push(record);
                    }
                    //Records with no availablity
                    if (record.Stock_Available__c == false && skuMasterWithIds[record.Product__c]!=undefined && record.Audit_Type__c != 'Audit') {
                        record.SKUName = skuMasterWithIds[record.Product__c];
                        $scope.noSKU.push(record);
                    }
                });
            }
            $scope.lastVisitRecord.Check_In_DateTime__c = $filter('date')($scope.lastVisitRecord.Check_In_DateTime__c, 'dd MMM yyyy');
            var query = "";
            for (var i = 0; i < lastVisit.currentPageOrderedEntries.length; i++) {
                if (lastVisit.currentPageOrderedEntries[i][0].External_Id__c != undefined) query = query + " OR {Visit_Summary__c:Visit__c} = '" + lastVisit.currentPageOrderedEntries[i][0].External_Id__c + "'";
                if (lastVisit.currentPageOrderedEntries[i][0].Id != undefined) query = query + " OR {Visit_Summary__c:Visit__c} = '" + lastVisit.currentPageOrderedEntries[i][0].Id + "'";
            }
            var visitSummaryrecordsLocal = FETCH_DATA_LOCAL('Visit_Summary__c', sfSmartstore.buildSmartQuerySpec("SELECT {Visit_Summary__c:_soup} FROM {Visit_Summary__c} WHERE ({Visit_Summary__c:Account_Id__c} = '" + $stateParams.accountId + "' or {Visit_Summary__c:AccountId__c} = '" + $stateParams.accountId + "') AND {Visit_Summary__c:RecordTypeId} = '" + rtList.currentPageOrderedEntries[0][0].Id + "' AND  ({Visit_Summary__c:Status__c} = 'Open'" + query + " )", 1000));
            for (var i = 0; i < visitSummaryrecordsLocal.currentPageOrderedEntries.length; i++) {
                var record = visitSummaryrecordsLocal.currentPageOrderedEntries[i][0];
                if (record.Date__c != undefined) {
                    var createdDate;
                    if (record.Date__c.split('-')[0].length == 4) createdDate = new Date(record.Date__c);
                    else {
                        createdDate = new Date(record.Date__c.split('/')[1] + '/' + record.Date__c.split('/')[0] + '/' + record.Date__c.split('/')[2]);
                    }
                    record.Date__c = createdDate.getTime().toString();
                }
                $scope.visitSummaries.push(record);
            }

        }
        else{
            var visitSummaryrecordsLocal = FETCH_DATA_LOCAL('Visit_Summary__c', sfSmartstore.buildSmartQuerySpec("SELECT {Visit_Summary__c:_soup} FROM {Visit_Summary__c} WHERE ({Visit_Summary__c:Account_Id__c} = '" + $stateParams.accountId + "' or {Visit_Summary__c:AccountId__c} = '" + $stateParams.accountId + "') AND {Visit_Summary__c:RecordTypeId} = '" + rtList.currentPageOrderedEntries[0][0].Id + "' AND  ({Visit_Summary__c:Status__c} = 'Open')", 1000));
            if(visitSummaryrecordsLocal.currentPageOrderedEntries!=undefined)
            for (var i = 0; i < visitSummaryrecordsLocal.currentPageOrderedEntries.length; i++) {
                var record = visitSummaryrecordsLocal.currentPageOrderedEntries[i][0];
                if (record.Date__c != undefined) {
                    var createdDate;
                    if (record.Date__c.split('-')[0].length == 4) createdDate = new Date(record.Date__c);
                    else {
                        createdDate = new Date(record.Date__c.split('/')[1] + '/' + record.Date__c.split('/')[0] + '/' + record.Date__c.split('/')[2]);
                    }
                    record.Date__c = createdDate.getTime().toString();
                }
                $scope.visitSummaries.push(record);
            }
        }
        $rootScope.processFlowLineItems = [];
        $rootScope.pages = [];
        var flow = FETCH_DATA_LOCAL('Db_process_flow', sfSmartstore.buildSmartQuerySpec("SELECT  {Db_process_flow:_soup} FROM {Db_process_flow} WHERE {Db_process_flow:Outlet_Type__c}='" + $scope.currentAccount.RecordType.Name + "'", 1));
        $rootScope.processFlow = flow.currentPageOrderedEntries[0][0];
        var flowLineItems = FETCH_DATA.querySoup('Db_process_flow_line_item', "Process_Flow__c", flow.currentPageOrderedEntries[0][0].Id);
        var queryspec = '';
        var queryspecForSection = '';
        angular.forEach(flowLineItems.currentPageOrderedEntries, function(record, key) {
            if(record['Removed__c']==false){
                $rootScope.processFlowLineItems[record.Order__c - 1] = record;
                if (queryspec == '' && queryspecForSection == '') {
                    queryspec = "{DB_page:Id}='" + record.Page__c + "'";
                    queryspecForSection = "{DB_section:Page__c}='" + record.Page__c + "'";
                } else {
                    queryspec = queryspec + " OR {DB_page:Id}='" + record.Page__c + "'";
                    queryspecForSection = queryspecForSection + " OR {DB_section:Page__c}='" + record.Page__c + "'";
                }
            }
        });
        if (queryspec != '' && queryspecForSection != '') {
            var pages = FETCH_DATA_LOCAL('DB_page', sfSmartstore.buildSmartQuerySpec("SELECT  {DB_page:_soup} FROM {DB_page} WHERE " + queryspec, 20));
            var sections = FETCH_DATA_LOCAL('DB_section', sfSmartstore.buildSmartQuerySpec("SELECT  {DB_section:_soup} FROM {DB_section} WHERE " + queryspecForSection + " order by {DB_section:Order__c}", 20));
            queryspec = '';
            angular.forEach(sections.currentPageOrderedEntries, function(record, key) {
                if (queryspec == '') {
                    queryspec = "{DB_fields:Section__c}='" + record[0].Id + "'";
                } else {
                    queryspec = queryspec + " OR {DB_fields:Section__c}='" + record[0].Id + "'";
                }
            });
            var fields = [];
            if (queryspec != '') {
                fields = FETCH_DATA_LOCAL('DB_fields', sfSmartstore.buildSmartQuerySpec("SELECT  {DB_fields:_soup} FROM {DB_fields} WHERE " + queryspec + " and {DB_fields:Removed__c}='false' order by {DB_fields:Order__c}", 100000));
            }
            //$log.debug('fields' + JSON.stringify(fields.currentPageOrderedEntries));
            var pagesWithIds = [];
            angular.forEach(pages.currentPageOrderedEntries, function(page, pageKey) {
                var pagedec = {};
                pagedec.pageDescription = page[0];
                pagesWithIds[page[0].Id] = page[0];
                pagedec.sections = [];
                angular.forEach(sections.currentPageOrderedEntries, function(section, sectionKey) {
                    if (page[0].Id == section[0].Page__c) {
                        var sectiondec = {};
                        sectiondec.sectionDescription = section[0];
                        sectiondec.fields = [];
                        angular.forEach(fields.currentPageOrderedEntries, function(field, fieldKey) {
                            if (field[0].Section__c == section[0].Id) {
                                var fieldDescription = field[0];
                                if (field[0].Picklist_Values__c != undefined) {
                                    fieldDescription.pickListValues = field[0].Picklist_Values__c.split(';');
                                }
                                sectiondec.fields.push(fieldDescription);
                            }
                        });
                        pagedec.sections.push(sectiondec);
                    }
                });
                $rootScope.pages.push(pagedec);
            });
        }
        $ionicLoading.hide();
        $scope.checkIn = function() {
            var pendingTasks = FETCH_DATA.querySoup('Db_transaction', 'status', 'pending');
            if ($rootScope.day == 'today') {
                if (pendingTasks.currentPageOrderedEntries.length == 0) {
                    if ($rootScope.day == 'today') {
                        if(isRestartCall!=''){
                            //call restart
                            $rootScope.restartCall($stateParams.accountId,isRestartCall)
                        }
                        else{
                        $ionicLoading.show({
                            template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
                        });
                        $rootScope.StockAndSalesList = [];
                        $rootScope.retailSalesList = {};
                        $rootScope.uploadfields = [];
                        $rootScope.currentvisitSummaryImages = [];
                        $rootScope.galleryImages =[];
                        $rootScope.slidesList = [];
                        $rootScope.slideFields = [];
                        $rootScope.PrideImages = [];
                        $rootScope.visitImages = [];
                        $rootScope.CompeSales = [];
                        $rootScope.UblSales = [];
                        $rootScope.chataiSales = [];
                        $rootScope.audit = {};
                        $rootScope.ChataiMonth.Name=undefined;
                        $rootScope.Month.Name=undefined;
                        $rootScope.tempvisitType = 'false';
                        $rootScope.pendingTask = true;
                        $rootScope.visitrecord = {};
                        $rootScope.Month = {};
                        if (SOUP_EXISTS.soupExists('Db_breadCrum') == true) {
                            sfSmartstore.removeSoup('Db_breadCrum', function(response) {}, function(error) {});
                        }
                        SOUP_REGISTER('Db_breadCrum', [{
                            "path": "Name",
                            "type": "string"
                        }, {
                            "path": "VisitedOrder",
                            "type": "string"
                        }]);
                        $scope.currentLocation = {};
                        GEO_LOCATION.getCurrentPosition().then(function(position) {
                            $scope.currentLocation.latitude = position.latitude;
                            $scope.currentLocation.longitude = position.longitude;
                            $scope.Visit = [{
                                Account__c: $scope.currentAccount.Id,
                                Check_In_DateTime__c: today,
                                Check_In_Location__Latitude__s: $scope.currentLocation.latitude,
                                Check_In_Location__Longitude__s: $scope.currentLocation.longitude,
                                External_Id__c: (new Date().getTime())+'-'+$scope.currentAccount.Id+'-'+$window.Math.random() * 100000000,
                                Route_Plan__c: $scope.currentAccount.RoutePlanId,
                                IsDirty: true
                            }];
                            if ($scope.currentAccount.Location__Latitude__s == undefined || $scope.currentAccount.Location__Latitude__s == null) {
                                $scope.currentAccount.Location__Latitude__s = $scope.currentLocation.latitude;
                                $scope.currentAccount.Location__Longitude__s = $scope.currentLocation.longitude;
                                $scope.currentAccount.IsDirty = true;
                                var accountToUpate = [];
                                accountToUpate.push($scope.currentAccount);
                                //update account lat long info
                                //MOBILEDATABASE_ADD('Account', accountToUpate, 'Id');
                                $log.debug('accountToUpate' + JSON.stringify(accountToUpate));
                            } else {
                                if ($scope.distance($scope.currentOutlet.Location__Latitude__s, $scope.currentOutlet.Location__Longitude__s, $scope.currentLocation.latitude, $scope.currentLocation.longitude, 'K') > 1) {
                                    $scope.Visit[0].Deviation_Reason__c = 'location is different from existing location of the outlet';
                                }
                            }
                            MOBILEDATABASE_ADD('Visit__c', $scope.Visit, 'External_Id__c');
                            $ionicLoading.hide();
                            var page = pagesWithIds[$rootScope.processFlowLineItems[0].Page__c];
                            if (page.Template_Name__c == 'Template1') {
                                $location.path('app/stocks/' + $scope.currentAccount.Id + '/' + $scope.Visit[0].External_Id__c + '/0/' + new Date());
                            } else if (page.Template_Name__c == 'Template2') {
                                $location.path('app/container2/' + $scope.currentAccount.Id + '/' + $scope.Visit[0].External_Id__c + '/0/' + new Date());
                            } else if (page.Template_Name__c == 'Template3') {
                                $location.path('app/container3/' + $scope.currentAccount.Id + '/' + $scope.Visit[0].External_Id__c + '/0/' + new Date());
                            } else {
                                $location.path('app/container4/' + $scope.currentAccount.Id + '/' + $scope.Visit[0].External_Id__c + '/0/' + new Date());
                            }
                            $log.info('LOCATION' + JSON.stringify(position));


                            //User log
                            var today_now = $filter('date')(new Date(), 'dd/MM/yyyy hh:mm a');
                            var userOprObj=[{
                                ObjectData:$scope.Visit,
                                time_stamp:today_now,
                                comment:'Clicked on start call with Location'
                            }];
                            MOBILEDATABASE_ADD('Db_User_Logs', userOprObj, '_soupEntryId');

                        }, function(error) {
                            $log.error('ERROR' + JSON.stringify(error));
                        });
                    }
                    }
                } else {
                    Splash.ShowToast('A previous call has not been completed. Tap Resume icon to complete the open call.', 'long', 'bottom', function(a) {
                        console.log(a)
                    });
                }
            } else {
                Splash.ShowToast('You can only start a call for outlets in today\'s route plan.', 'long', 'bottom', function(a) {
                    console.log(a)
                });
            }
        };
        $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('LastVisitSummaryCtrlError', err.stack + '::' + err.message);
    }
}).controller('PRIDECallCtrl', function($scope, $stateParams, $ionicScrollDelegate, $ionicSlideBoxDelegate, $cordovaCamera, $rootScope, $window, $state, $location, $q, $ionicModal, $ionicPopup, $timeout, $interval, $ionicLoading, $log, $filter, SalesforceSession, FETCH_DATA_LOCAL, SOUP_EXISTS, FETCH_DAILY_PLAN_ACCOUNTS, MOBILEDATABASE_ADD, FETCH_DATA, WEBSERVICE_ERROR) {
    try {
        $rootScope.backText = 'Back';
        $rootScope.showBack = true;
        $rootScope.params = $stateParams;
        $rootScope.name = "PRIDECallCtrl";
        $scope.cancelConfirmTop=jQuery( window ).height()-227;
        $scope.nextTop=jQuery( window ).height()-152;
        $scope.isFieldVisible = function(x, y) {
            // $log.debug('x=='+x +'y==='+y);
            if (x != undefined && y != undefined) return x.indexOf(y) == -1;
            else return false;
        }
        $scope.updateSlide = function(hand) {
            $timeout(function() {
                $log.debug('testing');
                $ionicScrollDelegate.$getByHandle('small-' + hand).resize();
                $log.debug(hand);
            }, 1000);
        }
        $scope.AccountId = $stateParams.AccountId;
        $scope.visitId = $stateParams.visitId;
        $scope.uploadId = '';
        if ($stateParams.uploadId != undefined && $stateParams.uploadId != '') $scope.uploadId = $stateParams.uploadId;
        $scope.remove = [];
        var today = $filter('date')(new Date(), 'dd/MM/yyyy');
        $scope.breadActive = 'Cooler';
        $scope.uploadslist = [];
        var transaction = [{
            Account: $stateParams.AccountId,
            stage: $stateParams.order,
            stageValue: 'pridecall',
            Visit: $stateParams.visitId,
            status: 'pending',
            entryDate: today
        }];
        MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
        MOBILEDATABASE_ADD('Db_breadCrum', [{
            VisitedOrder: $stateParams.order,
            Name: 'pridecall'
        }], 'VisitedOrder');
        $scope.myplan = $rootScope.Outlets;
        // if ($rootScope.day == 'today') {
        //     $scope.myplan = $rootScope.TodaysPlan;
        // } else if ($rootScope.day == 'tomorrow') {
        //     $scope.myplan = $rootScope.TomorrowPlan;
        // } else {
        //     $scope.myplan = $rootScope.DayAfterPlan;
        // }
        var currentAccountLocal = FETCH_DATA.querySoup('Account', 'Id', $stateParams.AccountId);
        if (currentAccountLocal != undefined && currentAccountLocal.currentPageOrderedEntries != undefined && currentAccountLocal.currentPageOrderedEntries.length > 0) {
            $scope.currentOutlet = currentAccountLocal.currentPageOrderedEntries[0];
            $scope.currentCustomer = currentAccountLocal.currentPageOrderedEntries[0];;
        }
        var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
        $scope.remove = [];
        $scope.remove.Reason = '';
        $scope.order = $stateParams.order;
        $scope.confirmUploadDeleteOld = function(removeUploadId) {
            if ($scope.remove.Reason != '') {
                angular.forEach($scope.uploadslist, function(record, key) {
                    var uploadsToDelete = [];
                    if (record.External_Id__c == removeUploadId) {
                        delIndex = key;
                        record.Status__c = 'Inactive';
                        record.Reason__c = $scope.remove.Reason;
                        record.IsDirty = true;
                        uploadsToDelete.push(record);
                        MOBILEDATABASE_ADD('Upload__c', uploadsToDelete, 'External_Id__c');
                        $location.path('app/container2/' + $scope.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date() + 'save');
                    }
                });
                $scope.confirmUploadDeletePopup.close();
            } else {
                Splash.ShowToast('Please Specify reason for removing ' + $scope.breadActive + ' ', 'long', 'center', function(a) {
                    console.log(a)
                });
            }
        }
        $scope.confirmUploadDelete = function(removeUploadId) {
            $location.path('app/removeUploadReason/' + removeUploadId + '/' + $scope.AccountId + '/' + $scope.visitId + '/' + $scope.order);
            $scope.confirmUploadDeletePopup.close();
        }
        $scope.cancelConfirmUploadDelete = function(removeUploadId) {
            $scope.confirmUploadDeletePopup.close();
        }
        $scope.removeUploadOld = function(removeUploadId) {
            $scope.confirmUploadDeletePopup = $ionicPopup.confirm({
                template: 'Are you sure want to remove the ' + $scope.breadActive.toLowerCase() + ' from this outlet?  <br/><br/><br/><span><input placeholder="Enter the reason for removing the ' + $scope.breadActive.toLowerCase() + '" type="text" ng-change="modelDataCheck()" ng-model="remove.Reason"><div class="piller"></div><div class="piller piller_1"></div></span><br/> <div style="text-align:right; color:#38B6CB;margin-right: 10px;font-weight: bold;"><span my-touchstart="cancelConfirmUploadDelete(\'' + removeUploadId + '\')">NO</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id="coolerRemove_yes" class="coolerRemove_yes" ng-disabled="!remove.Reason" my-touchstart="confirmUploadDelete(\'' + removeUploadId + '\')">YES</span> </div>',
                cssClass: 'myConfirm',
                title: '',
                scope: $scope,
                buttons: []
            });
            $scope.confirmUploadDeletePopup.then(function(res) {
                if (res) {
                    $location.path('app/removeUploadReason/' + removeUploadId + '/' + $scope.AccountId + '/' + $scope.visitId + '/' + $scope.order);
                    console.log('You are sure');
                } else {
                    console.log('You are not sure');
                }
            });
        }
        $scope.removeUpload = function(removeUploadId) {
            $scope.remove.Reason = '';
            $scope.confirmUploadDeletePopup = $ionicPopup.confirm({
                template: 'Are you sure you want to remove the ' + $scope.breadActive.toLowerCase() + ' from this outlet?<br /><br /><br /><div style="text-align:right; color:#38B6CB;margin-right: 10px;font-weight: bold;"><span my-touchstart="cancelConfirmUploadDelete(\'' + removeUploadId + '\')">NO</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span my-touchstart="confirmUploadDelete(\'' + removeUploadId + '\')">YES</span></div>',
                cssClass: 'resumeConfirm',
                title: '',
                scope: $scope,
                buttons: []
            });
            $scope.confirmUploadDeletePopup.then(function(res) {
                if (res) {
                    $location.path('app/removeUploadReason/' + removeUploadId + '/' + $scope.AccountId + '/' + $scope.visitId + '/' + $scope.order);
                    console.log('You are sure');
                } else {
                    console.log('You are not sure');
                }
            });
        }
        $scope.modelDataCheck = function() {
            var _myEle = document.getElementById('coolerRemove_yes');
            if ($scope.remove.Reason == '') {
                _myEle.style.color = '#aaaaaa';
            } else {
                _myEle.style.color = '#38B6CB';
            }
        }
        $scope.showWarning = function(field, value) {
            if (field == 'Working_Condition__c' && value == 'No') {
                Splash.ShowToast('Ensure that you have added a complaint in the Feedback module for the cooler not working.', 'long', 'bottom', function(a) {
                    console.log(a)
                });
            }
        }
        $scope.changeValue = function(a, b, c, d) {
            // $log.debug($rootScope.uploadfields[a][b][c]);
            // $log.debug(a + b + c + d);
            //  $log.debug(JSON.stringify($rootScope.uploadfields));
            //  if($rootScope.uploadfields[a]==undefined)
            //      $rootScope.uploadfields[a]=[];
            //  $log.debug(JSON.stringify($rootScope.uploadfields[a]));
            //  if($rootScope.uploadfields[a][b]==undefined)
            //      $rootScope.uploadfields[a][b]=[];
            //  $log.debug(JSON.stringify($rootScope.uploadfields[a][b]));
            // $rootScope.uploadfields[a][b][c]=d;
            // $log.debug($rootScope.uploadfields[a][b][c]);
        }
        $scope.confirmRemoveUpload = function() {
            if ($scope.remove.Reason != undefined && $scope.remove.Reason != '') {
                $ionicLoading.show({
                    template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
                });
                var removeUploadId = $scope.uploadId;
                //var delIndex=-1;
                angular.forEach($scope.uploadslist, function(record, key) {
                    var uploadsToDelete = [];
                    if (record.External_Id__c == removeUploadId) {
                        delIndex = key;
                        record.Status__c = 'Inactive';
                        record.Reason__c = $scope.remove.Reason;
                        record.IsDirty = true;
                        uploadsToDelete.push(record);
                        MOBILEDATABASE_ADD('Upload__c', uploadsToDelete, 'External_Id__c');
                    }
                });
                $ionicLoading.hide();
                $location.path('app/container2/' + $scope.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date() + 'save');
            } else {
                Splash.ShowToast('Please Specify reason for removing ' + $scope.breadActive.toLowerCase() + '.', 'long', 'bottom', function(a) {
                    console.log(a);
                });
            }
        }
        $scope.prevSlide = function() {
            $ionicSlideBoxDelegate.previous();
        }
        $scope.nextSlide = function() {
            $ionicSlideBoxDelegate.next();
        }
        var pageId = $rootScope.processFlowLineItems[$stateParams.order].Page__c;
        $scope.breadActive = $rootScope.processFlowLineItems[$stateParams.order].Name;
        var currentPage = {};
        angular.forEach($rootScope.pages, function(record, key) {
            if (record.pageDescription.Id == pageId) {
                currentPage = record;
            }
        });
        if (currentPage.sections.length > 1) {
            $scope.addNewSectionFields = currentPage.sections[1].fields;
        } else {
            $scope.addNewSectionFields = currentPage.sections[0].fields;
        }
        $scope.currentPageTitle = currentPage.pageDescription.Title__c;
        $scope.addNewButton = currentPage.pageDescription.Add_New__c;
        $scope.addNewLabel = currentPage.pageDescription.Add_New_Label__c;
        $scope.showPlanogram = currentPage.pageDescription.Is_Planogram_Show__c;
        $scope.showCaptureSales = currentPage.pageDescription.Capture_Sales__c;
        //$scope.uploadfields = [];
        var coolerList = FETCH_DATA.querySoup('Upload__c', 'Account__c', $scope.AccountId);
        var planogram_cooler_type = '';
        $rootScope.ubCoolerCount = 0;
        angular.forEach(coolerList.currentPageOrderedEntries, function(record, key) {
            //$log.debug('cout' + JSON.stringify($scope.AccountId));
            if ($scope.AccountId == record.Account__c && record.RecordTypeId == currentPage.sections[0].sectionDescription.Input_Record_Type_Id__c) {
                //$log.debug('uploadssss111' + JSON.stringify($rootScope.Uploads));
                var upload = {};
                upload = record;
                if (planogram_cooler_type == '') planogram_cooler_type = " {Db_planogram:Type_of_cooler__c}='" + record.Cooler_Type__c + "' ";
                else planogram_cooler_type = planogram_cooler_type + " OR {Db_planogram:Type_of_cooler__c}='" + record.Cooler_Type__c + "' ";
                if (upload.External_Id__c == undefined) {
                    upload.External_Id__c = (new Date().getTime())+'_'+$window.Math.random() * 10000000;
                }
                if (upload.Company__c == undefined && upload.Company__c == '') {
                    upload.Company__c = 'UBL';
                }
                if (upload.Company__c == 'UBL' && upload.Status__c != 'Inactive') {
                    upload.setOrder = 1000000 - key;
                    $rootScope.ubCoolerCount++;
                } else {
                    upload.setOrder = 10000 - key;
                }
                upload.fields = currentPage.sections[0].fields;
                if (upload.Status__c == undefined || upload.Status__c != 'Inactive') $scope.uploadslist.push(upload);
            }
        });
        $scope.uploadslist = $filter('orderBy')($scope.uploadslist, 'setOrder', true)
        $scope.titlefields = [];
        var fieldsCounts = 0;
        angular.forEach(currentPage.sections[0].fields, function(field, key) {
            if (field.Type__c == 'title') {
                $scope.titlefields.push(field.Field_API__c);
            } else if (field.Type__c != 'textarea' && field.Type__c != 'output') {
                fieldsCounts++;
            }
        });
        /*Last Visit start*/
        $scope.lastCoolers = [];
        var lastVisit = FETCH_DATA_LOCAL('Visit__c', sfSmartstore.buildSmartQuerySpec("SELECT {Visit__c:_soup} FROM {Visit__c} WHERE {Visit__c:Account__c}='" + $stateParams.AccountId + "' ORDER BY {Visit__c:Check_In_DateTime__c} DESC", 2));
        $log.debug('last visits' + JSON.stringify(lastVisit));
        var rtList1 = FETCH_DATA_LOCAL('DB_RecordTypes', sfSmartstore.buildSmartQuerySpec("SELECT  {DB_RecordTypes:_soup} FROM {DB_RecordTypes} WHERE {DB_RecordTypes:SobjectType}='Visit_Summary__c' AND {DB_RecordTypes:Name}!='Feedback and Complaints'", 50));
        $log.debug('visit coolers' + JSON.stringify(rtList1));
        var query = '';
        if (lastVisit.currentPageOrderedEntries != undefined && lastVisit.currentPageOrderedEntries.length > 0 && lastVisit.currentPageOrderedEntries[0][0] != undefined) {
            angular.forEach(lastVisit.currentPageOrderedEntries, function(record, key) {
                if (record[0].External_Id__c != $stateParams.visitId) {
                    var visitId = '';
                    var extId = record[0].External_Id__c;
                    if (record[0].Id != undefined) {
                        visitId = record[0].Id;
                    }
                    if (query == '') {
                        if (visitId != '') {
                            query = " {Visit_Summary__c:Visit__c}='" + visitId + "' OR {Visit_Summary__c:Visit__c}='" + extId + "'";
                        } else {
                            query = " {Visit_Summary__c:Visit__c}='" + extId + "'";
                        }
                    } else {
                        if (visitId != '') {
                            query = query + " OR {Visit_Summary__c:Visit__c}='" + visitId + "' OR {Visit_Summary__c:Visit__c}='" + extId + "'";
                        } else {
                            query = query + " OR {Visit_Summary__c:Visit__c}='" + extId + "'";
                        }
                    }
                    $log.debug('query==' + query);
                }
            });
        }
        $log.debug("myquery SELECT {Visit_Summary__c:_soup} FROM {Visit_Summary__c} " + query + " ");
        if (query != '') {
            var lastCoolers = FETCH_DATA_LOCAL('Visit_Summary__c', sfSmartstore.buildSmartQuerySpec("SELECT {Visit_Summary__c:_soup} FROM {Visit_Summary__c} where {Visit_Summary__c:RecordTypeId}='" + currentPage.sections[0].sectionDescription.Record_Type_Id__c + "' AND (" + query + ")", 50));
            //$log.debug('lastCooler records' + JSON.stringify(lastCoolers));
            var img_query = '';
            angular.forEach(lastCoolers.currentPageOrderedEntries, function(record, key) {
                if (record[0].External_Id__c != undefined && record[0].External_Id__c != '') {
                    $log.debug('External_Id__c==' + record[0].External_Id__c);
                    $scope.lastCoolers.push(record[0]);
                    if (img_query == '') {
                        if (record[0].Id != undefined) img_query = " WHERE {Db_images:ParentId}='" + record[0].External_Id__c + "' OR {Db_images:ParentId}='" + record[0].Id + "'";
                        else img_query = " WHERE {Db_images:ParentId}='" + record[0].External_Id__c + "' ";
                    } else {
                        if (record[0].Id != undefined) img_query = img_query + " OR {Db_images:ParentId}='" + record[0].External_Id__c + "' OR {Db_images:ParentId}='" + record[0].Id + "'";
                        else img_query = img_query + " OR {Db_images:ParentId}='" + record[0].External_Id__c + "' ";
                    }
                }
            });
            $log.debug('lastCoolers' + JSON.stringify($scope.lastCoolers));
            $log.debug("query lastCooler SELECT {Db_images:_soup} FROM {Db_images} " + img_query);
            $scope.lastCoolers = [];
            if (img_query != '') {
                var lastCoolerImages = FETCH_DATA_LOCAL('Db_images', sfSmartstore.buildSmartQuerySpec("SELECT {Db_images:_soup} FROM {Db_images} " + img_query, 50));
                //$log.debug('lastCoolerImages' + JSON.stringify(lastCoolerImages));
                angular.forEach(lastCoolerImages.currentPageOrderedEntries, function(record, key) {
                    $scope.lastCoolers.push(record[0]);
                });
            }
            //$log.debug('lastCoolerImages' + JSON.stringify($scope.lastCoolers));
        }
        if ($rootScope.resentlyAdded == true) {
            $timeout(function() {
                //$rootScope.ubCoolerAdded;
                $log.debug($ionicSlideBoxDelegate.slidesCount());
                $log.debug($rootScope.resentlyAdded);
                $log.debug($scope.uploadslist.length);
                if ($rootScope.ubCoolerAdded == true) {
                    $log.debug($rootScope.ubCoolerCount);
                    $ionicSlideBoxDelegate.slide($rootScope.ubCoolerCount - 1, 500);
                } else $ionicSlideBoxDelegate.slide($ionicSlideBoxDelegate.slidesCount() - 1, 500);
                $rootScope.resentlyAdded = false;
            }, 1500)
        }
        if ($scope.uploadslist.length > 0 && ($rootScope.uploadfields[$scope.order] == undefined || $rootScope.uploadfields[$scope.order].length == 0)) {
            var visitRecord = FETCH_DATA.querySoup('Visit__c', 'External_Id__c', $stateParams.visitId);
            var query = "{Visit_Summary__c:Visit__c} = '" + $stateParams.visitId + "'";
            if (visitRecord.currentPageOrderedEntries != undefined && visitRecord.currentPageOrderedEntries[0] != undefined && visitRecord.currentPageOrderedEntries[0].Id != undefined) {
                query = query + " OR {Visit_Summary__c:Visit__c} = '" + visitRecord.currentPageOrderedEntries[0].Id + "'";
            }
            var visitSummaryrecords = FETCH_DATA_LOCAL('Visit_Summary__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Visit_Summary__c:_soup} FROM {Visit_Summary__c} WHERE " + query, 100000));
            if (visitSummaryrecords.currentPageOrderedEntries != undefined && visitSummaryrecords.currentPageOrderedEntries.length > 0) {
                var summaryRecords = [];
                angular.forEach(visitSummaryrecords.currentPageOrderedEntries, function(record, key) {
                    if (record[0].Upload__c != undefined) {
                        summaryRecords.push(record[0]);
                    }
                });
                $rootScope.uploadfields[$scope.order] = summaryRecords;
            }
        }
        if ($scope.currentOutlet.Region__c == undefined || $scope.currentOutlet.Region__c == null) $scope.currentOutlet.Region__c = '';
        if ($scope.currentOutlet.Category__c == undefined || $scope.currentOutlet.Category__c == null) $scope.currentOutlet.Category__c = '';
        if (planogram_cooler_type != '') planogram_cooler_type = ' and (' + planogram_cooler_type + ') ';
        var planogram = FETCH_DATA_LOCAL('Db_planogram', sfSmartstore.buildSmartQuerySpec("SELECT {Db_planogram:_soup} FROM {Db_planogram} WHERE {Db_planogram:Region__c}='" + $scope.currentOutlet.Region__c + "' and {Db_planogram:Category__c}='" + $scope.currentOutlet.Category__c + "' " + planogram_cooler_type, 100000));
        var planogramIds = '';
        $scope.planogramImages = [];
        $scope.planogram = [];
        angular.forEach(planogram.currentPageOrderedEntries, function(record, key) {
            $scope.planogram[record[0].Region__c + record[0].Type_of_cooler__c + record[0].Category__c] = record[0].Id;
            if (planogramIds == '') planogramIds = " {Db_images:ParentId}='" + record[0].Id + "'";
            else planogramIds = planogramIds + " OR {Db_images:ParentId}='" + record[0].Id + "'";
        });
        if(planogramIds == ''){
            planogram = FETCH_DATA_LOCAL('Db_planogram', sfSmartstore.buildSmartQuerySpec("SELECT {Db_planogram:_soup} FROM {Db_planogram}"  , 100000));
            angular.forEach(planogram.currentPageOrderedEntries, function(record, key) {
                if(record[0].Region__c==undefined && record[0].Type_of_cooler__c==undefined && record[0].Category__c==undefined){
                    $scope.planogram['default'] = record[0].Id;
                    if (planogramIds == '') planogramIds = " {Db_images:ParentId}='" + record[0].Id + "'";
                    else planogramIds = planogramIds + " OR {Db_images:ParentId}='" + record[0].Id + "'";
                }
            });
        }
        $log.debug(planogramIds);
        if (planogramIds != '') {
            var planogramImages = FETCH_DATA_LOCAL('Db_images', sfSmartstore.buildSmartQuerySpec("SELECT {Db_images:_soup} FROM {Db_images} WHERE " + planogramIds, 100000));
            angular.forEach(planogramImages.currentPageOrderedEntries, function(record, key) {
                $scope.planogramImages[record[0].ParentId] = record[0];
            });
        }
        $log.debug($scope.planogram);
        $log.debug($scope.planogramImages);
        /*Last Visit end*/
        //  $log.debug('uploadssss' + JSON.stringify($rootScope.Uploads));
        $scope.saveAndNext = function() {
            $timeout(function() {
                var visitSummaryRecords = [];
                var attachments = [];
                angular.forEach($rootScope.uploadfields[$scope.order], function(record, key) {
                    if($scope.uploadslist[key]!=undefined){
                        var visitSummary = {};
                        visitSummary = record;
                        visitSummary['RecordTypeId'] = currentPage.sections[0].sectionDescription.Record_Type_Id__c;
                        if (record.External_Id__c == undefined) {
                            visitSummary['External_Id__c'] = (new Date().getTime())+'-'+$stateParams.visitId +'-'+$window.Math.random() * 10000000;
                        }
                        if (record.Visit__c == undefined) {
                            visitSummary['Visit__c'] = $stateParams.visitId;
                        }
                        if ($scope.uploadslist[key].Id != undefined) {
                            visitSummary['Upload__c'] = $scope.uploadslist[key].Id;
                        } else {
                            visitSummary['Upload__c'] = $scope.uploadslist[key].External_Id__c;
                        }
                        visitSummary['IsDirty'] = true;
                        visitSummary.Account_Id__c = $stateParams.AccountId;
                        $log.debug('mycooler' + JSON.stringify(visitSummary));
                        visitSummaryRecords.push(visitSummary);
                        angular.forEach($rootScope.PrideImages[$scope.uploadslist[key].External_Id__c], function(imageData, key) {
                            $log.debug('image parent id' + visitSummary['External_Id__c']);
                            attachments.push({
                                ParentId: visitSummary['External_Id__c'],
                                Name: visitSummary['Upload__c'] + '.jpg',
                                Body: imageData,
                                IsDirty: true,
                                External_Id__c: visitSummary['Upload__c'] + '--' + visitSummary['External_Id__c'] + key
                            });
                        });
                    }
                });
                if (visitSummaryRecords.length > 0) {
                    MOBILEDATABASE_ADD(currentPage.pageDescription.Master_Object__c, visitSummaryRecords, 'External_Id__c');
                }
                if (attachments.length > 0) {
                    MOBILEDATABASE_ADD('Db_images', attachments, 'External_Id__c');
                }
                $log.debug('attachments' + JSON.stringify(attachments));
                $rootScope.resentlyAdded = true;
                var transaction = [{
                    Account: $stateParams.AccountId,
                    stage: $stateParams.order,
                    stageValue: 'pridecall',
                    Visit: $stateParams.visitId,
                    status: 'saved',
                    entryDate: today
                }];
                MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
                MOBILEDATABASE_ADD('Db_breadCrum', [{
                    VisitedOrder: $stateParams.order,
                    Name: 'pridecall'
                }], 'VisitedOrder');
                $rootScope.breadcrumClicked($stateParams.AccountId, $stateParams.visitId, $stateParams.order, parseInt($stateParams.order) + 1, 'fromCtrl');
            }, 100);
        }
        $scope.takePicture = function(uploadId) {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.CAMERA,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 990,
                targetHeight: 360,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
            };
            $cordovaCamera.getPicture(options).then(function(imageData) {
                var imageslist = [];
                if ($rootScope.PrideImages[uploadId] != undefined) {
                    imageslist = $rootScope.PrideImages[uploadId];
                }
                imageslist[0] = imageData;
                $rootScope.PrideImages[uploadId] = imageslist;
            }, function(err) {});
        }
        $scope.saveNew = function() {
            $timeout(function() {
                var uploadsToInsert = [];
                var record = {};
                var ifcheck = true;
                angular.forEach($scope.addNewSectionFields, function(field, key) {
                    if ((field.fieldvalue == undefined || field.fieldvalue == '') && ifcheck) {
                        Splash.ShowToast('Please enter ' + field.Name + '.', 'long', 'bottom', function(a) {
                            console.log(a)
                        });
                        ifcheck = false;
                        $ionicLoading.hide();
                    }
                    record[field.Field_API__c] = field.fieldvalue;
                });
                if ($scope.showAdd) {
                    record.Account__c = $scope.AccountId;
                    record.RecordTypeId = currentPage.sections[0].sectionDescription.Input_Record_Type_Id__c;
                    record.External_Id__c = $stateParams.visitId + (new Date().getTime())+'_'+$window.Math.random() * 10000000;
                    if (record.Company__c == 'UBL') {
                        $rootScope.ubCoolerCount++;
                        $rootScope.ubCoolerAdded = true;
                    } else {
                        $rootScope.ubCoolerAdded = false;
                    }
                    record.IsDirty = true;
                    $rootScope.Uploads.push(record);
                    uploadsToInsert.push(record);
                    MOBILEDATABASE_ADD('Upload__c', uploadsToInsert, 'External_Id__c');
                    $ionicLoading.hide();
                    angular.forEach($scope.addNewSectionFields, function(field, key) {
                        field.fieldvalue = '';
                    });
                    Splash.ShowToast('The cooler has been added.', 'long', 'bottom', function(a) {
                        console.log(a)
                    });
                    $rootScope.resentlyAdded = true;
                    $location.path('app/container2/' + $scope.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date() + 'save');
                }
            }, 100);
        }
        $scope.showAdd = false;
        $scope.checkValues = function() {
            $scope.showAdd = true;
            angular.forEach($scope.addNewSectionFields, function(field, key) {
                if (field.fieldvalue == undefined || field.fieldvalue == '') {
                    $scope.showAdd = false;
                }
            });
        }
        $scope.cancelAddNew = function() {
            angular.forEach($scope.addNewSectionFields, function(field, key) {
                field.fieldvalue = '';
            });
            $location.path('app/container2/' + $scope.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date() + 'save');
        }
        $scope.CancelUploadRemove = function() {
            $scope.remove.Reason = '';
            $location.path('app/container2/' + $scope.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date() + 'save');
        }
        $scope.capturesales = function(uploadId) {
            $location.path('app/capturesales/' + uploadId);
        }
        $scope.addNew = function() {
            $location.path('app/AddNew/' + $scope.AccountId + '/' + $scope.visitId + '/' + $scope.order);
        }
    $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('PRIDECallCtrlError', err.stack + '::' + err.message);
    }
}).controller('PRIDECallNewCtrl', function($scope, $stateParams, $ionicScrollDelegate, $ionicSlideBoxDelegate, $cordovaCamera, $rootScope, $window, $state, $location, $q, $ionicModal, $ionicPopup, $timeout, $interval, $ionicLoading, $log, $filter, SalesforceSession, FETCH_DATA_LOCAL, SOUP_EXISTS, FETCH_DAILY_PLAN_ACCOUNTS, MOBILEDATABASE_ADD, FETCH_DATA, WEBSERVICE_ERROR) {
    try {
        $rootScope.backText = 'Back';
        $rootScope.showBack = true;
        $rootScope.params = $stateParams;
        $rootScope.name = "PRIDECallNewCtrl";
        $scope.isFieldVisible = function(x, y) {
            // $log.debug('x=='+x +'y==='+y);
            if (x != undefined && y != undefined) return x.indexOf(y) == -1;
            else return false;
        }
        $scope.updateSlide = function(hand) {
            $timeout(function() {
                $log.debug('testing');
                $ionicScrollDelegate.$getByHandle('small-' + hand).resize();
                $log.debug(hand);
            }, 1000);
        }
        $scope.AccountId = $stateParams.AccountId;
        $scope.visitId = $stateParams.visitId;
        $scope.uploadId = '';
        if ($stateParams.uploadId != undefined && $stateParams.uploadId != '') $scope.uploadId = $stateParams.uploadId;
        $scope.remove = [];
        var today = $filter('date')(new Date(), 'dd/MM/yyyy');
        $scope.breadActive = 'Cooler';
        $scope.uploadslist = [];
        var transaction = [{
            Account: $stateParams.AccountId,
            stage: $stateParams.order,
            stageValue: 'pridecall',
            Visit: $stateParams.visitId,
            status: 'pending',
            entryDate: today
        }];
        MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
        MOBILEDATABASE_ADD('Db_breadCrum', [{
            VisitedOrder: $stateParams.order,
            Name: 'pridecall'
        }], 'VisitedOrder');
        $scope.myplan = $rootScope.Outlets;
        // if ($rootScope.day == 'today') {
        //     $scope.myplan = $rootScope.TodaysPlan;
        // } else if ($rootScope.day == 'tomorrow') {
        //     $scope.myplan = $rootScope.TomorrowPlan;
        // } else {
        //     $scope.myplan = $rootScope.DayAfterPlan;
        // }
        var currentAccountLocal = FETCH_DATA.querySoup('Account', 'Id', $stateParams.AccountId);
        if (currentAccountLocal != undefined && currentAccountLocal.currentPageOrderedEntries != undefined && currentAccountLocal.currentPageOrderedEntries.length > 0) {
            $scope.currentOutlet = currentAccountLocal.currentPageOrderedEntries[0];
            $scope.currentCustomer = currentAccountLocal.currentPageOrderedEntries[0];;
        }
        var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
        $scope.remove = [];
        $scope.remove.Reason = '';
        $scope.order = $stateParams.order;
        $scope.confirmUploadDeleteOld = function(removeUploadId) {
            if ($scope.remove.Reason != '') {
                angular.forEach($scope.uploadslist, function(record, key) {
                    var uploadsToDelete = [];
                    if (record.External_Id__c == removeUploadId) {
                        delIndex = key;
                        record.Status__c = 'Inactive';
                        record.Reason__c = $scope.remove.Reason;
                        record.IsDirty = true;
                        uploadsToDelete.push(record);
                        MOBILEDATABASE_ADD('Upload__c', uploadsToDelete, 'External_Id__c');
                        $location.path('app/container2/' + $scope.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date() + 'save');
                    }
                });
                $scope.confirmUploadDeletePopup.close();
            } else {
                Splash.ShowToast('Please Specify reason for removing ' + $scope.breadActive + ' ', 'long', 'center', function(a) {
                    console.log(a)
                });
            }
        }
        $scope.confirmUploadDelete = function(removeUploadId) {
            $location.path('app/removeUploadReason/' + removeUploadId + '/' + $scope.AccountId + '/' + $scope.visitId + '/' + $scope.order);
            $scope.confirmUploadDeletePopup.close();
        }
        $scope.cancelConfirmUploadDelete = function(removeUploadId) {
            $scope.confirmUploadDeletePopup.close();
        }
        $scope.removeUploadOld = function(removeUploadId) {
            $scope.confirmUploadDeletePopup = $ionicPopup.confirm({
                template: 'Are you sure want to remove the ' + $scope.breadActive.toLowerCase() + ' from this outlet?  <br/><br/><br/><span><input placeholder="Enter the reason for removing the ' + $scope.breadActive.toLowerCase() + '" type="text" ng-change="modelDataCheck()" ng-model="remove.Reason"><div class="piller"></div><div class="piller piller_1"></div></span><br/> <div style="text-align:right; color:#38B6CB;margin-right: 10px;font-weight: bold;"><span my-touchstart="cancelConfirmUploadDelete(\'' + removeUploadId + '\')">NO</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id="coolerRemove_yes" class="coolerRemove_yes" ng-disabled="!remove.Reason" my-touchstart="confirmUploadDelete(\'' + removeUploadId + '\')">YES</span> </div>',
                cssClass: 'myConfirm',
                title: '',
                scope: $scope,
                buttons: []
            });
            $scope.confirmUploadDeletePopup.then(function(res) {
                if (res) {
                    $location.path('app/removeUploadReason/' + removeUploadId + '/' + $scope.AccountId + '/' + $scope.visitId + '/' + $scope.order);
                    console.log('You are sure');
                } else {
                    console.log('You are not sure');
                }
            });
        }
        $scope.removeUpload = function(removeUploadId) {
            $scope.confirmUploadDeletePopup = $ionicPopup.confirm({
                template: 'Are you sure you want to remove the ' + $scope.breadActive.toLowerCase() + ' from this outlet?<br /><br /><br /><div style="text-align:right; color:#38B6CB;margin-right: 10px;font-weight: bold;"><span my-touchstart="cancelConfirmUploadDelete(\'' + removeUploadId + '\')">NO</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span my-touchstart="confirmUploadDelete(\'' + removeUploadId + '\')">YES</span></div>',
                cssClass: 'resumeConfirm',
                title: '',
                scope: $scope,
                buttons: []
            });
            $scope.confirmUploadDeletePopup.then(function(res) {
                if (res) {
                    $location.path('app/removeUploadReason/' + removeUploadId + '/' + $scope.AccountId + '/' + $scope.visitId + '/' + $scope.order);
                    console.log('You are sure');
                } else {
                    console.log('You are not sure');
                }
            });
        }
        $scope.modelDataCheck = function() {
            var _myEle = document.getElementById('coolerRemove_yes');
            if ($scope.remove.Reason == '') {
                _myEle.style.color = '#aaaaaa';
            } else {
                _myEle.style.color = '#38B6CB';
            }
        }
        $scope.showWarning = function(field, value) {
            if (field == 'Working_Condition__c' && value == 'No') {
                Splash.ShowToast('Ensure that you have added a complaint in the Feedback module for the cooler not working.', 'long', 'bottom', function(a) {
                    console.log(a)
                });
            }
        }
        $scope.changeValue = function(a, b, c, d) {
            // $log.debug($rootScope.uploadfields[a][b][c]);
            // $log.debug(a + b + c + d);
            //  $log.debug(JSON.stringify($rootScope.uploadfields));
            //  if($rootScope.uploadfields[a]==undefined)
            //      $rootScope.uploadfields[a]=[];
            //  $log.debug(JSON.stringify($rootScope.uploadfields[a]));
            //  if($rootScope.uploadfields[a][b]==undefined)
            //      $rootScope.uploadfields[a][b]=[];
            //  $log.debug(JSON.stringify($rootScope.uploadfields[a][b]));
            // $rootScope.uploadfields[a][b][c]=d;
            // $log.debug($rootScope.uploadfields[a][b][c]);
        }
        $scope.confirmRemoveUpload = function() {
            if ($scope.remove.Reason != undefined && $scope.remove.Reason != '') {
                $ionicLoading.show({
                    template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
                });
                var removeUploadId = $scope.uploadId;
                //var delIndex=-1;
                angular.forEach($scope.uploadslist, function(record, key) {
                    var uploadsToDelete = [];
                    if (record.External_Id__c == removeUploadId) {
                        delIndex = key;
                        record.Status__c = 'Inactive';
                        record.Reason__c = $scope.remove.Reason;
                        record.IsDirty = true;
                        uploadsToDelete.push(record);
                        MOBILEDATABASE_ADD('Upload__c', uploadsToDelete, 'External_Id__c');
                    }
                });
                $ionicLoading.hide();
                $location.path('app/container2/' + $scope.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date() + 'save');
            } else {
                Splash.ShowToast('Please Specify reason for removing ' + $scope.breadActive.toLowerCase() + '.', 'long', 'bottom', function(a) {
                    console.log(a);
                });
            }
        }
        $scope.prevSlide = function() {
            $ionicSlideBoxDelegate.previous();
        }
        $scope.nextSlide = function() {
            $ionicSlideBoxDelegate.next();
        }
        var pageId = $rootScope.processFlowLineItems[$stateParams.order].Page__c;
        $scope.breadActive = $rootScope.processFlowLineItems[$stateParams.order].Name;
        var currentPage = {};
        angular.forEach($rootScope.pages, function(record, key) {
            if (record.pageDescription.Id == pageId) {
                currentPage = record;
            }
        });
        if (currentPage.sections.length > 1) {
            $scope.addNewSectionFields = currentPage.sections[1].fields;
        } else {
            $scope.addNewSectionFields = currentPage.sections[0].fields;
        }
        $scope.currentPageTitle = currentPage.pageDescription.Title__c;
        $scope.addNewButton = currentPage.pageDescription.Add_New__c;
        $scope.addNewLabel = currentPage.pageDescription.Add_New_Label__c;
        $scope.showPlanogram = currentPage.pageDescription.Is_Planogram_Show__c;
        $scope.showCaptureSales = currentPage.pageDescription.Capture_Sales__c;
        //$scope.uploadfields = [];
        var coolerList = FETCH_DATA.querySoup('Upload__c', 'Account__c', $scope.AccountId);
        var planogram_cooler_type = '';
        $rootScope.ubCoolerCount = 0;
        angular.forEach(coolerList.currentPageOrderedEntries, function(record, key) {
            //$log.debug('cout' + JSON.stringify($scope.AccountId));
            if ($scope.AccountId == record.Account__c && record.RecordTypeId == currentPage.sections[0].sectionDescription.Input_Record_Type_Id__c) {
                //$log.debug('uploadssss111' + JSON.stringify($rootScope.Uploads));
                var upload = {};
                upload = record;
                if (planogram_cooler_type == '') planogram_cooler_type = " {Db_planogram:Type_of_cooler__c}='" + record.Cooler_Type__c + "' ";
                else planogram_cooler_type = planogram_cooler_type + " OR {Db_planogram:Type_of_cooler__c}='" + record.Cooler_Type__c + "' ";
                if (upload.External_Id__c == undefined) {
                    upload.External_Id__c = (new Date().getTime())+'_'+$window.Math.random() * 10000000;
                }
                if (upload.Company__c == undefined && upload.Company__c == '') {
                    upload.Company__c = 'UBL';
                }
                if (upload.Company__c == 'UBL' && upload.Status__c != 'Inactive') {
                    upload.setOrder = 1000000 - key;
                    $rootScope.ubCoolerCount++;
                } else {
                    upload.setOrder = 10000 - key;
                }
                upload.fields = currentPage.sections[0].fields;
                if (upload.Status__c == undefined || upload.Status__c != 'Inactive') $scope.uploadslist.push(upload);
            }
        });
        $scope.uploadslist = $filter('orderBy')($scope.uploadslist, 'setOrder', true)
        $scope.titlefields = [];
        var fieldsCounts = 0;
        angular.forEach(currentPage.sections[0].fields, function(field, key) {
            if (field.Type__c == 'title') {
                $scope.titlefields.push(field.Field_API__c);
            } else if (field.Type__c != 'textarea' && field.Type__c != 'output') {
                fieldsCounts++;
            }
        });
        /*Last Visit start*/
        $scope.lastCoolers = [];
        var lastVisit = FETCH_DATA_LOCAL('Visit__c', sfSmartstore.buildSmartQuerySpec("SELECT {Visit__c:_soup} FROM {Visit__c} WHERE {Visit__c:Account__c}='" + $stateParams.AccountId + "' ORDER BY {Visit__c:Check_In_DateTime__c} DESC", 2));
        $log.debug('last visits' + JSON.stringify(lastVisit));
        var rtList1 = FETCH_DATA_LOCAL('DB_RecordTypes', sfSmartstore.buildSmartQuerySpec("SELECT  {DB_RecordTypes:_soup} FROM {DB_RecordTypes} WHERE {DB_RecordTypes:SobjectType}='Visit_Summary__c' AND {DB_RecordTypes:Name}!='Feedback and Complaints'", 50));
        $log.debug('visit coolers' + JSON.stringify(rtList1));
        var query = '';
        if (lastVisit.currentPageOrderedEntries != undefined && lastVisit.currentPageOrderedEntries.length > 0 && lastVisit.currentPageOrderedEntries[0][0] != undefined) {
            angular.forEach(lastVisit.currentPageOrderedEntries, function(record, key) {
                if (record[0].External_Id__c != $stateParams.visitId) {
                    var visitId = '';
                    var extId = record[0].External_Id__c;
                    if (record[0].Id != undefined) {
                        visitId = record[0].Id;
                    }
                    if (query == '') {
                        if (visitId != '') {
                            query = " {Visit_Summary__c:Visit__c}='" + visitId + "' OR {Visit_Summary__c:Visit__c}='" + extId + "'";
                        } else {
                            query = " {Visit_Summary__c:Visit__c}='" + extId + "'";
                        }
                    } else {
                        if (visitId != '') {
                            query = query + " OR {Visit_Summary__c:Visit__c}='" + visitId + "' OR {Visit_Summary__c:Visit__c}='" + extId + "'";
                        } else {
                            query = query + " OR {Visit_Summary__c:Visit__c}='" + extId + "'";
                        }
                    }
                    $log.debug('query==' + query);
                }
            });
        }
        $log.debug("myquery SELECT {Visit_Summary__c:_soup} FROM {Visit_Summary__c} " + query + " ");
        if (query != '') {
            var lastCoolers = FETCH_DATA_LOCAL('Visit_Summary__c', sfSmartstore.buildSmartQuerySpec("SELECT {Visit_Summary__c:_soup} FROM {Visit_Summary__c} where {Visit_Summary__c:RecordTypeId}='" + currentPage.sections[0].sectionDescription.Record_Type_Id__c + "' AND (" + query + ")", 50));
            //$log.debug('lastCooler records' + JSON.stringify(lastCoolers));
            var img_query = '';
            angular.forEach(lastCoolers.currentPageOrderedEntries, function(record, key) {
                if (record[0].External_Id__c != undefined && record[0].External_Id__c != '') {
                    $log.debug('External_Id__c==' + record[0].External_Id__c);
                    $scope.lastCoolers.push(record[0]);
                    if (img_query == '') {
                        if (record[0].Id != undefined) img_query = " WHERE {Db_images:ParentId}='" + record[0].External_Id__c + "' OR {Db_images:ParentId}='" + record[0].Id + "'";
                        else img_query = " WHERE {Db_images:ParentId}='" + record[0].External_Id__c + "' ";
                    } else {
                        if (record[0].Id != undefined) img_query = img_query + " OR {Db_images:ParentId}='" + record[0].External_Id__c + "' OR {Db_images:ParentId}='" + record[0].Id + "'";
                        else img_query = img_query + " OR {Db_images:ParentId}='" + record[0].External_Id__c + "' ";
                    }
                }
            });
            $log.debug('lastCoolers' + JSON.stringify($scope.lastCoolers));
            $log.debug("query lastCooler SELECT {Db_images:_soup} FROM {Db_images} " + img_query);
            $scope.lastCoolers = [];
            if (img_query != '') {
                var lastCoolerImages = FETCH_DATA_LOCAL('Db_images', sfSmartstore.buildSmartQuerySpec("SELECT {Db_images:_soup} FROM {Db_images} " + img_query, 50));
                //$log.debug('lastCoolerImages' + JSON.stringify(lastCoolerImages));
                angular.forEach(lastCoolerImages.currentPageOrderedEntries, function(record, key) {
                    $scope.lastCoolers.push(record[0]);
                });
            }
            //$log.debug('lastCoolerImages' + JSON.stringify($scope.lastCoolers));
        }
        if ($rootScope.resentlyAdded == true) {
            $timeout(function() {
                //$rootScope.ubCoolerAdded;
                $log.debug($ionicSlideBoxDelegate.slidesCount());
                $log.debug($rootScope.resentlyAdded);
                $log.debug($scope.uploadslist.length);
                if ($rootScope.ubCoolerAdded == true) {
                    $log.debug($rootScope.ubCoolerCount);
                    $ionicSlideBoxDelegate.slide($rootScope.ubCoolerCount - 1, 500);
                } else $ionicSlideBoxDelegate.slide($ionicSlideBoxDelegate.slidesCount() - 1, 500);
                $rootScope.resentlyAdded = false;
            }, 1500)
        }
        if ($scope.uploadslist.length > 0 && ($rootScope.uploadfields[$scope.order] == undefined || $rootScope.uploadfields[$scope.order].length == 0)) {
            var visitRecord = FETCH_DATA.querySoup('Visit__c', 'External_Id__c', $stateParams.visitId);
            var query = "{Visit_Summary__c:Visit__c} = '" + $stateParams.visitId + "'";
            if (visitRecord.currentPageOrderedEntries != undefined && visitRecord.currentPageOrderedEntries[0] != undefined && visitRecord.currentPageOrderedEntries[0].Id != undefined) {
                query = query + " OR {Visit_Summary__c:Visit__c} = '" + visitRecord.currentPageOrderedEntries[0].Id + "'";
            }
            var visitSummaryrecords = FETCH_DATA_LOCAL('Visit_Summary__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Visit_Summary__c:_soup} FROM {Visit_Summary__c} WHERE " + query, 100000));
            if (visitSummaryrecords.currentPageOrderedEntries != undefined && visitSummaryrecords.currentPageOrderedEntries.length > 0) {
                var summaryRecords = [];
                angular.forEach(visitSummaryrecords.currentPageOrderedEntries, function(record, key) {
                    if (record[0].Upload__c != undefined) {
                        summaryRecords.push(record[0]);
                    }
                });
                $rootScope.uploadfields[$scope.order] = summaryRecords;
            }
        }
        if ($scope.currentOutlet.Region__c == undefined || $scope.currentOutlet.Region__c == null) $scope.currentOutlet.Region__c = '';
        if ($scope.currentOutlet.Category__c == undefined || $scope.currentOutlet.Category__c == null) $scope.currentOutlet.Category__c = '';
        if (planogram_cooler_type != '') planogram_cooler_type = ' and (' + planogram_cooler_type + ') ';
        var planogram = FETCH_DATA_LOCAL('Db_planogram', sfSmartstore.buildSmartQuerySpec("SELECT {Db_planogram:_soup} FROM {Db_planogram} WHERE {Db_planogram:Region__c}='" + $scope.currentOutlet.Region__c + "' and {Db_planogram:Category__c}='" + $scope.currentOutlet.Category__c + "' " + planogram_cooler_type, 100000));
        var planogramIds = '';
        $scope.planogramImages = [];
        $scope.planogram = [];
        angular.forEach(planogram.currentPageOrderedEntries, function(record, key) {
            $scope.planogram[record[0].Region__c + record[0].Type_of_cooler__c + record[0].Category__c] = record[0].Id;
            if (planogramIds == '') planogramIds = " {Db_images:ParentId}='" + record[0].Id + "'";
            else planogramIds = planogramIds + " OR {Db_images:ParentId}='" + record[0].Id + "'";
        });
        $log.debug(planogramIds);
        if (planogramIds != '') {
            var planogramImages = FETCH_DATA_LOCAL('Db_images', sfSmartstore.buildSmartQuerySpec("SELECT {Db_images:_soup} FROM {Db_images} WHERE " + planogramIds, 100000));
            angular.forEach(planogramImages.currentPageOrderedEntries, function(record, key) {
                $scope.planogramImages[record[0].ParentId] = record[0];
            });
        }
        $log.debug($scope.planogram);
        $log.debug($scope.planogramImages);
        /*Last Visit end*/
        //  $log.debug('uploadssss' + JSON.stringify($rootScope.Uploads));
        $scope.saveAndNext = function() {
            $timeout(function() {
                var visitSummaryRecords = [];
                var attachments = [];
                angular.forEach($rootScope.uploadfields[$scope.order], function(record, key) {
                    if($scope.uploadslist[key]!=undefined){
                        var visitSummary = {};
                        visitSummary = record;
                        visitSummary['RecordTypeId'] = currentPage.sections[0].sectionDescription.Record_Type_Id__c;
                        if (record.External_Id__c == undefined) {
                            visitSummary['External_Id__c'] = (new Date().getTime())+'-'+$stateParams.visitId +'-'+$window.Math.random() * 10000000;
                        }
                        if (record.Visit__c == undefined) {
                            visitSummary['Visit__c'] = $stateParams.visitId;
                        }
                        if ($scope.uploadslist[key].Id != undefined) {
                            visitSummary['Upload__c'] = $scope.uploadslist[key].Id;
                        } else {
                            visitSummary['Upload__c'] = $scope.uploadslist[key].External_Id__c;
                        }
                        visitSummary['IsDirty'] = true;
                        visitSummary.Account_Id__c = $stateParams.AccountId;
                        $log.debug('mycooler' + JSON.stringify(visitSummary));
                        visitSummaryRecords.push(visitSummary);
                        angular.forEach($rootScope.PrideImages[$scope.uploadslist[key].External_Id__c], function(imageData, key) {
                            $log.debug('image parent id' + visitSummary['External_Id__c']);
                            attachments.push({
                                ParentId: visitSummary['External_Id__c'],
                                Name: visitSummary['Upload__c'] + '.jpg',
                                Body: imageData,
                                IsDirty: true,
                                External_Id__c: visitSummary['Upload__c'] + '--' + visitSummary['External_Id__c'] + key
                            });
                        });
                    }
                });
                if (visitSummaryRecords.length > 0) {
                    MOBILEDATABASE_ADD(currentPage.pageDescription.Master_Object__c, visitSummaryRecords, 'External_Id__c');
                }
                if (attachments.length > 0) {
                    MOBILEDATABASE_ADD('Db_images', attachments, 'External_Id__c');
                }
                $log.debug('attachments' + JSON.stringify(attachments));
                $rootScope.resentlyAdded = true;
                var transaction = [{
                    Account: $stateParams.AccountId,
                    stage: $stateParams.order,
                    stageValue: 'pridecall',
                    Visit: $stateParams.visitId,
                    status: 'saved',
                    entryDate: today
                }];
                MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
                MOBILEDATABASE_ADD('Db_breadCrum', [{
                    VisitedOrder: $stateParams.order,
                    Name: 'pridecall'
                }], 'VisitedOrder');
                $rootScope.breadcrumClicked($stateParams.AccountId, $stateParams.visitId, $stateParams.order, parseInt($stateParams.order) + 1, 'fromCtrl');
            }, 100);
        }
        $scope.takePicture = function(uploadId) {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.CAMERA,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 990,
                targetHeight: 360,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
            };
            $cordovaCamera.getPicture(options).then(function(imageData) {
                var imageslist = [];
                if ($rootScope.PrideImages[uploadId] != undefined) {
                    imageslist = $rootScope.PrideImages[uploadId];
                }
                imageslist[0] = imageData;
                $rootScope.PrideImages[uploadId] = imageslist;
            }, function(err) {});
        }
        $scope.saveNew = function() {
            $timeout(function() {
                var uploadsToInsert = [];
                var record = {};
                var ifcheck = true;
                angular.forEach($scope.addNewSectionFields, function(field, key) {
                    if ((field.fieldvalue == undefined || field.fieldvalue == '') && ifcheck) {
                        Splash.ShowToast('Please enter ' + field.Name + '.', 'long', 'bottom', function(a) {
                            console.log(a)
                        });
                        ifcheck = false;
                        $ionicLoading.hide();
                    }
                    record[field.Field_API__c] = field.fieldvalue;
                });
                if ($scope.showAdd) {
                    record.Account__c = $scope.AccountId;
                    record.RecordTypeId = currentPage.sections[0].sectionDescription.Input_Record_Type_Id__c;
                    record.External_Id__c = $stateParams.visitId + (new Date().getTime())+'_'+$window.Math.random() * 10000000;
                    if (record.Company__c == 'UBL') {
                        $rootScope.ubCoolerCount++;
                        $rootScope.ubCoolerAdded = true;
                    } else {
                        $rootScope.ubCoolerAdded = false;
                    }
                    record.IsDirty = true;
                    $rootScope.Uploads.push(record);
                    uploadsToInsert.push(record);
                    MOBILEDATABASE_ADD('Upload__c', uploadsToInsert, 'External_Id__c');
                    $ionicLoading.hide();
                    angular.forEach($scope.addNewSectionFields, function(field, key) {
                        field.fieldvalue = '';
                    });
                    Splash.ShowToast('The cooler has been added.', 'long', 'bottom', function(a) {
                        console.log(a)
                    });
                    $rootScope.resentlyAdded = true;
                    $location.path('app/container2/' + $scope.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date() + 'save');
                }
            }, 100);
        }
        $scope.showAdd = false;
        $scope.checkValues = function() {
            $scope.showAdd = true;
            angular.forEach($scope.addNewSectionFields, function(field, key) {
                if (field.fieldvalue == undefined || field.fieldvalue == '') {
                    $scope.showAdd = false;
                }
            });
        }
        $scope.cancelAddNew = function() {
            angular.forEach($scope.addNewSectionFields, function(field, key) {
                field.fieldvalue = '';
            });
            $location.path('app/container2/' + $scope.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date() + 'save');
        }
        $scope.CancelUploadRemove = function() {
            $scope.remove.Reason = '';
            $location.path('app/container2/' + $scope.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date() + 'save');
        }
        $scope.capturesales = function(uploadId) {
            $location.path('app/capturesales/' + uploadId);
        }
        $scope.addNew = function() {
            $location.path('app/AddNew/' + $scope.AccountId + '/' + $scope.visitId + '/' + $scope.order);
        }
    $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('PRIDECallNewCtrlError', err.stack + '::' + err.message);
    }
}).controller('PRIDECallCtrl2', function($scope, $ionicScrollDelegate, $stateParams, $ionicSlideBoxDelegate, $cordovaCamera, $rootScope, $window, $state, $location, $q, $ionicModal, $timeout, $interval, $ionicLoading, $log, $filter, SalesforceSession, FETCH_DATA_LOCAL, SOUP_EXISTS, FETCH_DAILY_PLAN_ACCOUNTS, MOBILEDATABASE_ADD, FETCH_DATA, WEBSERVICE_ERROR) {
    try {
        $ionicLoading.show({
            template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
        });
        $rootScope.params = $stateParams;
        $rootScope.name = "PRIDECallCtrl2";
        $rootScope.backText = 'Back';
        $scope.isFieldVisible = function(x, y) {
            if (x != undefined && y != undefined) return x.indexOf(y) == -1;
            else return false;
        }
        var today = $filter('date')(new Date(), 'dd/MM/yyyy');
        var transaction = [{
            Account: $stateParams.AccountId,
            stage: $stateParams.order,
            stageValue: 'pridecall2',
            Visit: $stateParams.visitId,
            status: 'pending',
            entryDate: today
        }];
        MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
        MOBILEDATABASE_ADD('Db_breadCrum', [{
            VisitedOrder: $stateParams.order,
            Name: 'pridecall2'
        }], 'VisitedOrder');
        $scope.updateSlide = function(hand) {
            $timeout(function() {
                $log.debug('testing');
                $ionicScrollDelegate.$getByHandle('small-' + hand).resize();
                $log.debug(hand);
            }, 1000);
        }
        $scope.myplan = $rootScope.Outlets;
        // if ($rootScope.day == 'today') {
        //     $scope.myplan = $rootScope.TodaysPlan;
        // } else if ($rootScope.day == 'tomorrow') {
        //     $scope.myplan = $rootScope.TomorrowPlan;
        // } else {
        //     $scope.myplan = $rootScope.DayAfterPlan;
        // }
        var currentAccountLocal = FETCH_DATA.querySoup('Account', 'Id', $stateParams.AccountId);
        if (currentAccountLocal != undefined && currentAccountLocal.currentPageOrderedEntries != undefined && currentAccountLocal.currentPageOrderedEntries.length > 0) {
            $scope.currentOutlet = currentAccountLocal.currentPageOrderedEntries[0];
            $scope.currentCustomer = currentAccountLocal.currentPageOrderedEntries[0];;
        }
        var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
        $scope.breadActive = 'Cooler';
        $scope.order = $stateParams.order;
        $scope.pagesList = [];
        $scope.pagesList = $rootScope.slidesList[$stateParams.order];
        var pageId = $rootScope.processFlowLineItems[$stateParams.order].Page__c;
        $scope.breadActive = $rootScope.processFlowLineItems[$stateParams.order].Name;
        var currentPage = {};
        angular.forEach($rootScope.pages, function(record, key) {
            if (record.pageDescription.Id == pageId) {
                currentPage = record;
            }
        });
        $scope.addNewButton = currentPage.pageDescription.Add_New__c;
        $scope.addNewLabel = currentPage.pageDescription.Add_New_Label__c;
        if ($rootScope.slidesList[$stateParams.order] == undefined && currentPage.pageDescription.Title__c.indexOf('Complaint') != -1 || currentPage.pageDescription.Title__c.indexOf('complaint') != -1) {
            var lastVisit = FETCH_DATA_LOCAL('Visit__c', sfSmartstore.buildSmartQuerySpec("SELECT {Visit__c:_soup} FROM {Visit__c} WHERE {Visit__c:Account__c}='" + $stateParams.AccountId + "' ORDER BY {Visit__c:Check_In_DateTime__c} DESC", 20));
            var rtList = FETCH_DATA_LOCAL('DB_RecordTypes', sfSmartstore.buildSmartQuerySpec("SELECT  {DB_RecordTypes:_soup} FROM {DB_RecordTypes} WHERE {DB_RecordTypes:SobjectType}='Visit_Summary__c' AND {DB_RecordTypes:Name}='Feedback and Complaints'", 50));
            $scope.openComplaints = [];
            var query = '';
            if (lastVisit.currentPageOrderedEntries != undefined && lastVisit.currentPageOrderedEntries.length > 0 && lastVisit.currentPageOrderedEntries[0][0] != undefined) {
                angular.forEach(lastVisit.currentPageOrderedEntries, function(record, key) {
                    var visitId = '';
                    var extId = record[0].External_Id__c;
                    if (record[0].Id != undefined) {
                        visitId = record[0].Id;
                    }
                    if (query == '') {
                        if (visitId != '') {
                            query = "{Visit_Summary__c:Visit__c}='" + visitId + "' OR {Visit_Summary__c:Visit__c}='" + extId + "'";
                        } else {
                            query = "{Visit_Summary__c:Visit__c}='" + extId + "'";
                        }
                    } else {
                        if (visitId != '') {
                            query = query + " OR {Visit_Summary__c:Visit__c}='" + visitId + "' OR {Visit_Summary__c:Visit__c}='" + extId + "'";
                        } else {
                            query = query + " OR {Visit_Summary__c:Visit__c}='" + extId + "'";
                        }
                    }
                });
                if (query == '') {
                    query = "{Visit_Summary__c:Visit__c}='" + $stateParams.visitId + "'";
                } else {
                    query = query + " OR {Visit_Summary__c:Visit__c}='" + $stateParams.visitId + "'";
                }
                // var openComplaints = FETCH_DATA_LOCAL('Visit_Summary__c', sfSmartstore.buildSmartQuerySpec("SELECT {Visit_Summary__c:_soup} FROM {Visit_Summary__c} WHERE {Visit_Summary__c:Status__c}='Logged' AND {Visit_Summary__c:RecordTypeId}='" + rtList.currentPageOrderedEntries[0][0].Id + "' AND (" + query + ")", 50));
                var openComplaints = FETCH_DATA_LOCAL('Visit_Summary__c', sfSmartstore.buildSmartQuerySpec("SELECT {Visit_Summary__c:_soup} FROM {Visit_Summary__c} WHERE ({Visit_Summary__c:Status__c}='Logged' or {Visit_Summary__c:Status__c}='logged') AND {Visit_Summary__c:RecordTypeId}='" + rtList.currentPageOrderedEntries[0][0].Id + "' and ({Visit_Summary__c:Account_Id__c}='" + $stateParams.AccountId + "' or (" + query + ")) ", 50));
                // $log.debug('openComplaints' + JSON.stringify(openComplaints));
                angular.forEach(openComplaints.currentPageOrderedEntries, function(record, key) {
                    //$scope.openComplaints.push(record[0]);
                    var page = {};
                    page.External_Id__c = record[0].External_Id__c;
                    page.fields = currentPage.sections[0].fields;
                    $log.debug('My complaints' + JSON.stringify(record[0]));
                    if (record[0].Status__c == 'logged') record[0].Status__c = 'Logged';
                    if (record[0].Source__c != undefined && record[0].Source__c.toLowerCase() == 'trade complaint') record[0].Source__c = 'Trade Complaint';
                    if (record[0].Source__c != undefined && record[0].Source__c.toLowerCase() == 'consumer complaint') record[0].Source__c = 'Consumer Complaint';
                    var uploadfield = {
                        Response_Type__c: record[0].Response_Type__c,
                        Source__c: record[0].Source__c,
                        Category__c: record[0].Category__c,
                        Status__c: record[0].Status__c,
                        Notify_Supervisor__c: record[0].Notify_Supervisor__c,
                        Brand__c: record[0].Brand__c,
                        Description__c: record[0].Description__c,
                        Visit__c: record[0].Visit__c,
                        RecordTypeId: record[0].RecordTypeId,
                        External_Id__c: record[0].External_Id__c,
                        Account_Id__c: $stateParams.AccountId
                    };
                    $rootScope.slideFields[record[0].External_Id__c] = uploadfield;
                    if ($rootScope.slidesList[$stateParams.order] == undefined) {
                        $rootScope.slidesList[$stateParams.order] = [];
                        $scope.pagesList = [];
                    }
                    $scope.pagesList.push(page);
                    $rootScope.slidesList[$stateParams.order].push(page);
                });
            }
        }
        $ionicLoading.hide();
        $scope.addNewPage = function() {
            $ionicLoading.show({
                template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
            });
            var firstPage = {};
            firstPage.External_Id__c = $stateParams.visitId + (new Date().getTime())+'_'+$window.Math.random() * 10000000;
            firstPage.fields = currentPage.sections[0].fields;
            if ($rootScope.slidesList[$stateParams.order] == undefined) {
                $rootScope.slidesList[$stateParams.order] = [];
                $scope.pagesList = [];
            }
            //  alert($stateParams.order+'before slidesList'+$rootScope.slidesList[$stateParams.order].length+'before '+$scope.pagesList.length);
            $rootScope.slidesList[$stateParams.order].push(firstPage);
            $scope.pagesList = $rootScope.slidesList[$stateParams.order];
            if ($scope.breadActive == 'Complaints') {
                $rootScope.slideFields[firstPage.External_Id__c] = {};
                $rootScope.slideFields[firstPage.External_Id__c]['Status__c'] = 'Logged';
            }
            // alert($stateParams.order+'after slidesList'+$rootScope.slidesList[$stateParams.order].length+'after '+$scope.pagesList.length);
            $ionicSlideBoxDelegate.update();
            $timeout(function() {
                $ionicSlideBoxDelegate.slide($ionicSlideBoxDelegate.slidesCount() - 1, 500);
                $ionicLoading.hide();
            }, 500);
        }
        if ($scope.pagesList != undefined && $scope.pagesList.length > 0 && $rootScope.slideFields != undefined && $rootScope.slideFields.length == 0) {
            var visitRecord = FETCH_DATA.querySoup('Visit__c', 'External_Id__c', $stateParams.visitId);
            var query = "{Visit_Summary__c:Visit__c} = '" + $stateParams.visitId + "'";
            if (visitRecord.currentPageOrderedEntries != undefined && visitRecord.currentPageOrderedEntries[0] != undefined && visitRecord.currentPageOrderedEntries[0].Id != undefined) {
                query = query + " OR {Visit_Summary__c:Visit__c} = '" + visitRecord.currentPageOrderedEntries[0].Id + "'";
            }
            var visitSummaryrecords = FETCH_DATA_LOCAL('Visit_Summary__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Visit_Summary__c:_soup} FROM {Visit_Summary__c} WHERE " + query, 100000));
            if (visitSummaryrecords.currentPageOrderedEntries != undefined && visitSummaryrecords.currentPageOrderedEntries.length > 0) {
                angular.forEach(visitSummaryrecords.currentPageOrderedEntries, function(record, key) {
                    if (record[0].External_Id__c != undefined) {
                        $rootScope.slideFields[record[0].External_Id__c] = record[0];
                    }
                });
            }
        }
        $scope.saveAndNext = function() {
            $timeout(function() {
                var todayDate = $filter('date')(new Date(), 'dd/MM/yyyy');
                if ($rootScope.slidesList[$stateParams.order] != undefined && $rootScope.slidesList[$stateParams.order].length > 0) {
                    var visitSummaryRecords = [];
                    angular.forEach($rootScope.slidesList[$stateParams.order], function(record, key) {
                        var visitSummary = {};
                        $log.debug('bbb' + $rootScope.slideFields[record.External_Id__c]);
                        visitSummary = $rootScope.slideFields[record.External_Id__c];
                        if (visitSummary != undefined) {
                            if (visitSummary.Source__c != undefined && visitSummary.Source__c.indexOf('Feedback') != -1) visitSummary.Response_Type__c = 'Feedback';
                            else visitSummary.Response_Type__c = 'Complaint';
                            if (visitSummary.Status__c != undefined && visitSummary.Status__c == 'Resolved') visitSummary.Resolution_Date__c = todayDate;
                            $log.debug('visit feedback and complaints' + JSON.stringify(visitSummary));
                            visitSummary['RecordTypeId'] = currentPage.sections[0].sectionDescription.Record_Type_Id__c;
                            if (visitSummary.External_Id__c == undefined) {
                                visitSummary['External_Id__c'] = (new Date().getTime())+'-'+$stateParams.visitId +'-'+$window.Math.random() * 10000000;
                                $rootScope.slideFields[record.External_Id__c].External_Id__c = visitSummary['External_Id__c'];
                            }
                            if (visitSummary.Visit__c == undefined) {
                                visitSummary['Visit__c'] = $stateParams.visitId;
                            }
                            visitSummary['IsDirty'] = true;
                            visitSummary.Account_Id__c = $stateParams.AccountId;
                            visitSummaryRecords.push(visitSummary);
                        }
                    });
                    $log.debug('visitSummaryRecords' + JSON.stringify(visitSummaryRecords));
                    if (visitSummaryRecords.length > 0) {
                        MOBILEDATABASE_ADD(currentPage.pageDescription.Master_Object__c, visitSummaryRecords, 'External_Id__c');
                    }
                } else {}
                var pageOrder = parseInt($stateParams.order) + 1;
                var transaction = [{
                    Account: $stateParams.AccountId,
                    stage: $stateParams.order,
                    stageValue: 'pridecall2',
                    Visit: $stateParams.visitId,
                    status: 'saved',
                    entryDate: today
                }];
                MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
                MOBILEDATABASE_ADD('Db_breadCrum', [{
                    VisitedOrder: $stateParams.order,
                    Name: 'pridecall2'
                }], 'VisitedOrder');
                $ionicLoading.hide();
                if ($rootScope.processFlowLineItems[pageOrder] != undefined) {
                    var nextPageId = $rootScope.processFlowLineItems[pageOrder].Page__c;
                    var nextPage;
                    angular.forEach($rootScope.pages, function(record, key) {
                        if (record.pageDescription.Id == nextPageId) {
                            nextPage = record;
                        }
                    });
                    if (nextPage.pageDescription.Template_Name__c == 'Template1') {
                        $location.path('app/stocks/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + pageOrder + '/' + new Date());
                    } else if (nextPage.pageDescription.Template_Name__c == 'Template2') {
                        $location.path('app/container2/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + pageOrder + '/' + new Date());
                    } else {
                        $location.path('app/container3/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + pageOrder + '/' + new Date());
                    }
                } else {
                    $location.path('app/checkout/' + $stateParams.AccountId + '/' + $stateParams.visitId);
                }
            }, 100);
        }
        $scope.prevSlide = function() {
            $ionicSlideBoxDelegate.previous();
        }
        $scope.nextSlide = function() {
            $ionicSlideBoxDelegate.next();
        }
        if ($rootScope.resentlyAdded2 == true) {
            $timeout(function() {
                $ionicSlideBoxDelegate.slide($ionicSlideBoxDelegate.slidesCount() - 1, 500);
                $rootScope.resentlyAdded2 = false;
            }, 1500)
        }
        $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('PRIDECallCtrl2Error', err.stack + '::' + err.message);
    }
}).controller('CheckoutCtrl', function($scope, $stateParams, $ionicPopup, $rootScope, $window, $state, $location, $q, $ionicModal, $timeout, $interval, $ionicLoading, $log, $filter, $cordovaCamera, $ionicSlideBoxDelegate, SalesforceSession, FETCH_DATA_LOCAL, SOUP_EXISTS, FETCH_DAILY_PLAN_ACCOUNTS, MOBILEDATABASE_ADD, FETCH_DATA, WEBSERVICE_ERROR, GEO_LOCATION) {
    try {
        // $scope.visitrecord = {};
        $ionicLoading.show({
            template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
        });
        if ($rootScope.visitrecord.Visit_Summary__c == undefined) {
            $rootScope.visitrecord = {};
        }
        $scope.endCallTop=jQuery( window ).height()-425;
        var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
        $rootScope.params = $stateParams;
        $rootScope.name = "CheckoutCtrl";
        $rootScope.backText = 'Back';
        $rootScope.showBack = true;
        $scope.currentOutlet = {};
        $scope.currentCustomer = {};
        $scope.imageCount=0;
        var today = $filter('date')(new Date(), 'dd/MM/yyyy');
        var transaction = [{
            Account: $stateParams.AccountId,
            stage: 'checkout',
            stageValue: 'checkout',
            Visit: $stateParams.visitId,
            status: 'pending',
            entryDate: today
        }];
        MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
        MOBILEDATABASE_ADD('Db_breadCrum', [{
            VisitedOrder: 'checkout',
            Name: 'checkout'
        }], 'VisitedOrder');
        var currentAccountLocal = FETCH_DATA.querySoup('Account', 'Id', $stateParams.AccountId);
        if (currentAccountLocal != undefined && currentAccountLocal.currentPageOrderedEntries != undefined && currentAccountLocal.currentPageOrderedEntries.length > 0) {
            $scope.currentOutlet = currentAccountLocal.currentPageOrderedEntries[0];
            $scope.currentCustomer = currentAccountLocal.currentPageOrderedEntries[0];;
        }
        var currentVisitImages = FETCH_DATA.querySoup('Db_images', 'ParentId', $stateParams.AccountId);
        if($rootScope.galleryImages.length<1){
            angular.forEach(currentVisitImages.currentPageOrderedEntries, function(record, key) {
                if (record['Name']!=undefined && record['Name'].indexOf('gallery')>-1) {
                    var tempOrder=record['Name'].substr(record['Name'].indexOf('gallery-')+8);
                    record['myOrder']=parseInt(tempOrder.substr(0, tempOrder.indexOf('@')));
                    console.log(record['Name']+'==>'+record['myOrder']);
                    $rootScope.galleryImages.push(record);
                    $scope.imageCount++;
                }
            });
        }
        $scope.lastVisitImages = [];
        $scope.lastVisitSummaries = [];
        var lastVisit = FETCH_DATA_LOCAL('Visit__c', sfSmartstore.buildSmartQuerySpec("SELECT {Visit__c:_soup} FROM {Visit__c} WHERE {Visit__c:Account__c}='" + $stateParams.AccountId + "' ORDER BY {Visit__c:Check_In_DateTime__c} DESC", 11));
        angular.forEach(lastVisit.currentPageOrderedEntries, function(record, key) {
            $scope.lastVisitSummaries.push(record[0]);
        });
        if(($rootScope.visitrecord.Visit_Summary__c == undefined || $rootScope.visitrecord.Visit_Summary__c == '' ) && $scope.lastVisitSummaries!=undefined && $scope.lastVisitSummaries.length>0){
            $rootScope.visitrecord.Visit_Summary__c = $scope.lastVisitSummaries[0].Visit_Summary__c;
        }
        if(($rootScope.visitrecord.Visit_Summary__c == undefined || $rootScope.visitrecord.Visit_Summary__c == '' ) && $scope.lastVisitSummaries!=undefined && $scope.lastVisitSummaries.length>1){
            $rootScope.visitrecord.Visit_Summary__c = $scope.lastVisitSummaries[1].Visit_Summary__c;
        }
        if (lastVisit.currentPageOrderedEntries != undefined && lastVisit.currentPageOrderedEntries.length > 0 && lastVisit.currentPageOrderedEntries[0][0] != undefined) {
            $scope.lastVisitRecord = lastVisit.currentPageOrderedEntries[0][0];
            if ($scope.lastVisitRecord.Check_Out_DateTime__c == undefined || $scope.lastVisitRecord.Check_Out_DateTime__c == '') {
                if (lastVisit.currentPageOrderedEntries[1] != undefined && lastVisit.currentPageOrderedEntries[1][0] != undefined) $scope.lastVisitRecord = lastVisit.currentPageOrderedEntries[1][0];
                else $scope.lastVisitRecord = {};
                if ($scope.lastVisitRecord.External_Id__c != undefined) {
                    if ($scope.lastVisitRecord.Id == undefined && $scope.lastVisitRecord.External_Id__c != undefined) $scope.lastVisitRecord.Id = $scope.lastVisitRecord.External_Id__c;
                    var lastVisitImages = FETCH_DATA.querySoup('Db_images', 'ParentId', $scope.lastVisitRecord.Id);
                    angular.forEach(lastVisitImages.currentPageOrderedEntries, function(record, key) {
                        $scope.lastVisitImages.push(record);
                    });
                }
            }
        }
        /*var lastVisitRecord = FETCH_DATA.querySoup('Visit__c', 'Account__c', $stateParams.AccountId);
        $scope.lastVisitImages=[];
        if (lastVisitRecord.currentPageOrderedEntries != undefined && lastVisitRecord.currentPageOrderedEntries[lastVisitRecord.currentPageOrderedEntries.length-2] != undefined ) {
                if(lastVisitRecord.currentPageOrderedEntries[lastVisitRecord.currentPageOrderedEntries.length-2].Id == undefined && lastVisitRecord.currentPageOrderedEntries[lastVisitRecord.currentPageOrderedEntries.length-2].External_Id__c != undefined)
                        lastVisitRecord.currentPageOrderedEntries[lastVisitRecord.currentPageOrderedEntries.length-2].Id =lastVisitRecord.currentPageOrderedEntries[lastVisitRecord.currentPageOrderedEntries.length-2].External_Id__c;
                if(lastVisitRecord.currentPageOrderedEntries[lastVisitRecord.currentPageOrderedEntries.length-2].Id != undefined){
                        var lastVisitImages = FETCH_DATA.querySoup('Db_images', 'ParentId', lastVisitRecord.currentPageOrderedEntries[lastVisitRecord.currentPageOrderedEntries.length-2].Id);
                        angular.forEach(lastVisitImages.currentPageOrderedEntries, function(record, key) {
                            $scope.lastVisitImages.push(record);
                        });
                }
                
            }*/
        var d = new Date();
        var today = d.getTime().toString();
        var todayRoutePlan = FETCH_DAILY_PLAN_ACCOUNTS('today');
        angular.forEach(todayRoutePlan, function(acc, accKey) {
            if (acc.Id == $scope.currentCustomer.Id) $scope.currentCustomer.RoutePlanId = acc.RoutePlanId;
        });
        $ionicLoading.hide();
        $scope.orderByFunction = function(obj){
            return parseInt(obj.myOrder);
        };
        $scope.noCheckoutPopup = function() {
            confirmcheckoutPopup.close();
        }
        $scope.yesCheckoutPopup = function() {
            $scope.checkout();
            confirmcheckoutPopup.close();
        }
        $scope.confirmCheckout = function() {
            confirmcheckoutPopup = $ionicPopup.confirm({
                template: 'You are about to end the Call. It can be modified by restarting the call.<br/><br/><br/><div style="text-align:right; color:#38B6CB;margin-right: 18px; text-align:right; color:#38B6CB;margin-right: 32px; font-size: 18px;font-weight: bold;"><span my-touchstart="noCheckoutPopup()" style="padding-right: 35px;">NO</span> &nbsp;<span my-touchstart="yesCheckoutPopup()">YES</span> </div>',
                cssClass: 'checkoutConfirm',
                title: '',
                scope: $scope,
                buttons: []
            });
            confirmcheckoutPopup.then(function(res) {});
            //User log
            var today_now = $filter('date')(new Date(), 'dd/MM/yyyy hh:mm a');
            var userOprObj=[{
                ObjectData:'',
                time_stamp:today_now,
                comment:'End Call Clicked'
            }];
            MOBILEDATABASE_ADD('Db_User_Logs', userOprObj, '_soupEntryId');
        }
        var currentVisitImagesSlider = $ionicSlideBoxDelegate.$getByHandle('visitCheckoutImagesSlider');
        var lastVisitImagesSlider = $ionicSlideBoxDelegate.$getByHandle('lastVisitCheckoutImagesSlider');
        $ionicModal.fromTemplateUrl('current-image-modal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.currentImagemodal = modal;
        });
        $scope.openCurrentImageModal = function() {
            currentVisitImagesSlider.slide(0);
            $scope.currentImagemodal.show();
        };
        $scope.closeCurrentImageModal = function() {
            $scope.currentImagemodal.hide();
        };
        $scope.goToSlideCurrentImageModel = function(index) {
                $scope.currentImagemodal.show();
                $timeout(function() {
                    currentVisitImagesSlider.slide(index);
                }, 500);
            }
            // Called each time the slide changes
        $scope.slideCurrentImageIndex = 0;
        $scope.slideChangedCurrentImageModel = function(index) {
            $scope.slideIndex1 = index;
            $scope.slideCurrentImageIndex = index;
        };
        $ionicModal.fromTemplateUrl('image-modal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
        });
        $scope.openModal = function() {
            lastVisitImagesSlider.slide(0);
            $scope.modal.show();
        };
        $scope.closeModal = function() {
            $scope.modal.hide();
        };
        $scope.goToSlide = function(index) {
                $scope.modal.show();
                $timeout(function() {
                    lastVisitImagesSlider.slide(index);
                }, 500);
            }
            // Called each time the slide changes
        $scope.slideLastVisitImageIndex = 0;
        $scope.slideChanged = function(index) {
            $scope.slideIndex = index;
            $scope.slideLastVisitImageIndex = index;
        };
        $scope.lastVisitPrevSlide = function() {
            lastVisitImagesSlider.previous();
        }
        $scope.lastVisitNextSlide = function() {
            lastVisitImagesSlider.next();
        }
        $scope.currentVisitPrevSlide = function() {
            currentVisitImagesSlider.previous();
        }
        $scope.currentVisitNextSlide = function() {
            currentVisitImagesSlider.next();
        }
        var imagelist = [];
        imagelist = $rootScope.galleryImages;
        // imagelist.reverse();
        $scope.takePicture = function() {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.CAMERA,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 700,
                targetHeight: 500,
                saveToPhotoAlbum: false
            };
            $cordovaCamera.getPicture(options).then(function(imageData) {
                imagelist.reverse();
                imagelist.push({
                    Body: imageData,
                    IsDirty: true,
                    Name:'',
                    myOrder:imagelist.length
                });
                $rootScope.galleryImages = [];
                var images = imagelist.reverse();
                for (var i = 0; i < 20; i++) {
                    if (images[i] != undefined) {
                        $rootScope.galleryImages.push(images[i]);
                    }
                }
            }, function(err) {});
        }
        $scope.checkout = function() {
            $ionicLoading.show({
                template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
            });

            //this below part code in get to know whether the user has not entered anything or
            //there is error in pushing visit summary to SFDC
            var isVisitSummaryPresent;
            isVisitSummaryPresent = FETCH_DATA_LOCAL('Visit_Summary__c', sfSmartstore.buildSmartQuerySpec("SELECT {Visit_Summary__c:_soup} FROM {Visit_Summary__c} WHERE {Visit_Summary__c:Visit__c} ='"+$stateParams.visitId+"'" , 10));
            if (isVisitSummaryPresent != undefined && isVisitSummaryPresent.currentPageOrderedEntries != undefined && isVisitSummaryPresent.currentPageOrderedEntries.length > 0) {
                console.log('Activity is Present');
            }else{
                var resultVis = 'Activity result '+$stateParams.visitId+' on Outlet '+$stateParams.AccountId+' on Date '+$filter('date')(new Date(),'dd/MM/yyyy');
                WEBSERVICE_ERROR('NotCreateActivityFromVisit_POST', resultVis+'');
            }


            $scope.currentLocation = {};
            GEO_LOCATION.getCurrentPosition().then(function(position) {
                $scope.currentLocation.latitude = position.latitude;
                $scope.currentLocation.longitude = position.longitude;
                var today = $filter('date')(new Date(), 'dd/MM/yyyy hh:mm a');
                var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
                var lastVisit = FETCH_DATA_LOCAL('Visit__c', sfSmartstore.buildSmartQuerySpec("SELECT {Visit__c:_soup} FROM {Visit__c} WHERE {Visit__c:External_Id__c}='" + $stateParams.visitId + "' OR {Visit__c:Id}='" + $stateParams.visitId + "'", 1));
                $scope.Visit = [{
                    Account__c: $stateParams.AccountId,
                    Id: lastVisit.currentPageOrderedEntries[0][0].Id,
                    Check_In_Location__Latitude__s: lastVisit.currentPageOrderedEntries[0][0].Check_In_Location__Latitude__s,
                    Check_In_Location__Longitude__s: lastVisit.currentPageOrderedEntries[0][0].Check_In_Location__Longitude__s,
                    Check_In_DateTime__c: lastVisit.currentPageOrderedEntries[0][0].Check_In_DateTime__c,
                    Route_Plan__c: $scope.currentCustomer.RoutePlanId,
                    Check_Out_DateTime__c: today,
                    Check_Out_Location__Latitude__s: $scope.currentLocation.latitude,
                    Check_Out_Location__Longitude__s: $scope.currentLocation.longitude,
                    External_Id__c: $stateParams.visitId,
                    Visit_Summary__c: $rootScope.visitrecord.Visit_Summary__c,
                    IsDirty: true
                }];
                //$log.debug('checkout visitrecord' + JSON.stringify($scope.Visit))
                MOBILEDATABASE_ADD('Visit__c', $scope.Visit, 'External_Id__c');
                var attachments = [];
                for (var i = 0; i < 20; i++) {
                    if ($rootScope.galleryImages[i] != undefined && $rootScope.galleryImages[i]['Name'] == '') {
                        attachments.push({
                            ParentId: $stateParams.AccountId,
                            Name: 'gallery-'+$rootScope.galleryImages[i].myOrder+'@'+$stateParams.visitId,
                            Body: $rootScope.galleryImages[i].Body,
                            IsDirty: true,
                            External_Id__c: $stateParams.visitId + '--' + (new Date().getTime())+'_'+$window.Math.random() * 100000000
                        });
                        $scope.imageCount++;
                    }
                }
                if (attachments.length > 0) MOBILEDATABASE_ADD('Db_images', attachments, 'External_Id__c');
                today = $filter('date')(lastVisit.currentPageOrderedEntries[0][0].Check_In_DateTime__c, 'dd/MM/yyyy');
                var transaction = [{
                    Account: $stateParams.AccountId,
                    stage: 'checkout',
                    Visit: $stateParams.visitId,
                    status: 'saved',
                    entryDate: today
                }];
                MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
                MOBILEDATABASE_ADD('Db_breadCrum', [{
                    VisitedOrder: 'checkout',
                    Name: 'checkout'
                }], 'VisitedOrder');
                $rootScope.resentlyAdded = false;
                $rootScope.resentlyAdded2 = false;
                var pendingTasks = FETCH_DATA.querySoup('Db_transaction', 'status', 'pending');
                if (pendingTasks.currentPageOrderedEntries != undefined && pendingTasks.currentPageOrderedEntries.length != 0) {
                    $rootScope.pendingTask = true;
                } else {
                    $rootScope.pendingTask = false;
                }
                $ionicLoading.hide();
                $rootScope.goToHome();
                //$location.path('app/today/');
                $log.info('LOCATION' + JSON.stringify(position));
            }, function(error) {
                $log.error('ERROR' + JSON.stringify(error));
            });
        }
        $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('CheckoutCtrlError', err.stack + '::' + err.message);
    }
}).controller('StockCtrl', function($scope, $stateParams, $ionicScrollDelegate, $rootScope, $window, $state, $location, $q, $ionicModal, $timeout, $interval, $ionicLoading, $log, $filter, SalesforceSession, FETCH_DATA_LOCAL, SOUP_EXISTS, FETCH_DAILY_PLAN_ACCOUNTS, MOBILEDATABASE_ADD, FETCH_DATA, WEBSERVICE_ERROR) {
    try {
        $ionicLoading.show({
            template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
        });
        $rootScope.backText = 'Back';
        var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
        $rootScope.params = $stateParams;
        $rootScope.name = "StockCtrl";
        var oldvalues = [];
        angular.forEach($rootScope.StockAndSalesList, function(record, key) {
            oldvalues[record.Product__c] = record;
        });
        if (oldvalues != undefined && oldvalues.length == 0) {
            var visitRecord = FETCH_DATA.querySoup('Visit__c', 'External_Id__c', $stateParams.visitId);
            var query = "{Visit_Summary__c:Visit__c} = '" + $stateParams.visitId + "'";
            if (visitRecord.currentPageOrderedEntries != undefined && visitRecord.currentPageOrderedEntries[0] != undefined && visitRecord.currentPageOrderedEntries[0].Id != undefined) {
                query = query + " OR {Visit_Summary__c:Visit__c} = '" + visitRecord.currentPageOrderedEntries[0].Id + "'";
            }
            var visitSummaryrecords = FETCH_DATA_LOCAL('Visit_Summary__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Visit_Summary__c:_soup} FROM {Visit_Summary__c} WHERE " + query , 100000));
            if (visitSummaryrecords.currentPageOrderedEntries != undefined && visitSummaryrecords.currentPageOrderedEntries.length > 0) {
                angular.forEach(visitSummaryrecords.currentPageOrderedEntries, function(record, key) {
                    if (record[0].Product__c != undefined && record[0].Audit_Type__c==undefined) {
                        oldvalues[record[0].Product__c] = record[0];
                    }
                });
            }
        }
        var containsObject=function(obj, list) {
            var i;
            for (i = 0; i < list.length; i++) {
                if (list[i] === obj) {
                    return true;
                }
            }
        
            return false;
        }
        var today = $filter('date')(new Date(), 'dd/MM/yyyy');
        var transaction = [{
            Account: $stateParams.AccountId,
            stage: $stateParams.order,
            stageValue: 'stocks',
            Visit: $stateParams.visitId,
            status: 'pending',
            entryDate: today
        }];
        MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
        MOBILEDATABASE_ADD('Db_breadCrum', [{
            VisitedOrder: $stateParams.order,
            Name: 'stocks'
        }], 'VisitedOrder');
        if ($rootScope.StockAndSalesList.length < 1) $rootScope.StockAndSalesList = [];
        $scope.auditTypeValues = [];
        $scope.showAuditType = false;
        if ($rootScope.processFlow.Retail__c == true) {
            $scope.showAuditType = true;
            $scope.auditTypeValues.push('Audit');
        }
        if ($rootScope.processFlow.Chatai__c == true) {
            $scope.showAuditType = true;
            $scope.auditTypeValues.push('Chatai');
        }
        var currentAccountLocal = FETCH_DATA.querySoup('Account', 'Id', $stateParams.AccountId);
        if (currentAccountLocal != undefined && currentAccountLocal.currentPageOrderedEntries != undefined && currentAccountLocal.currentPageOrderedEntries.length > 0) {
            $scope.currentOutlet = currentAccountLocal.currentPageOrderedEntries[0];
            var SKURecordTypes = FETCH_DATA.querySoup('DB_RecordTypes', 'SobjectType', 'Brands_and_Products__c');
            var SKURecordTypeIds = [];
            angular.forEach(SKURecordTypes.currentPageOrderedEntries, function(record, key) {
                SKURecordTypeIds[record.Name] = record.Id;
            });
            var visitSummaryRtListLocal = FETCH_DATA.querySoup('DB_RecordTypes', 'SobjectType', 'Visit_Summary__c');
            var visitSummaryRtList = [];
            angular.forEach(visitSummaryRtListLocal.currentPageOrderedEntries, function(record, key) {
                visitSummaryRtList[record.Name] = record.Id;
            });
            var isComp=[];
            var SKUMap=[];
            $scope.companyMap=[];
            $scope.companyMapList=[];
            $scope.companyMapList.push('UB');
            var stateRecordTemp = FETCH_DATA_LOCAL('Brands_and_Products__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Brands_and_Products__c:_soup} FROM {Brands_and_Products__c} WHERE {Brands_and_Products__c:RecordTypeId}='" + SKURecordTypeIds['State'] + "' ", 1));
            var stateActiveProductsTemp = FETCH_DATA_LOCAL('Brands_and_Products__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Brands_and_Products__c:_soup} FROM {Brands_and_Products__c} WHERE   {Brands_and_Products__c:Related_to_State__c}='" + stateRecordTemp.currentPageOrderedEntries[0][0].Id + "' AND {Brands_and_Products__c:RecordTypeId}='" + SKURecordTypeIds['Product'] + "' and {Brands_and_Products__c:Used_in_Chatai__c}='false' and {Brands_and_Products__c:Removed__c}='false' order by {Brands_and_Products__c:Order__c}", 100000));
            if (stateActiveProductsTemp != undefined && stateActiveProductsTemp.currentPageOrderedEntries != undefined && stateActiveProductsTemp.currentPageOrderedEntries.length > 0) {
                angular.forEach(stateActiveProductsTemp.currentPageOrderedEntries, function(record, key) {
                    isComp[record[0]['Id']]=record[0]['Stock_Capture__c'];
                    SKUMap[record[0]['Id']]=record[0]['Sku_Code__c'];
                    $scope.companyMap[record[0]['Id']]=record[0]['Company__c'];
                    if(!containsObject(record[0]['Company__c'],$scope.companyMapList) && record[0]['Stock_Capture__c']==true)
                        $scope.companyMapList[parseInt(record[0]['Order__c'])]=record[0]['Company__c'];
                });

            }
            var tempcompanyMapList=[];
            angular.forEach($scope.companyMapList, function(record, key) {
                tempcompanyMapList.push(record);
            });
            $scope.companyMapList=tempcompanyMapList;
            var outletProducts = FETCH_DATA_LOCAL('Product_Assignment__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Product_Assignment__c:_soup} FROM {Product_Assignment__c} WHERE  {Product_Assignment__c:Outlet_Id__c}='" + $stateParams.AccountId + "' order by {Product_Assignment__c:Order__c} ", 100000));
            if (outletProducts == undefined || outletProducts.currentPageOrderedEntries == undefined || outletProducts.currentPageOrderedEntries.length == 0 || outletProducts.currentPageOrderedEntries[0][0] == undefined) {
                var stateRecord = FETCH_DATA_LOCAL('Brands_and_Products__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Brands_and_Products__c:_soup} FROM {Brands_and_Products__c} WHERE {Brands_and_Products__c:RecordTypeId}='" + SKURecordTypeIds['State'] + "' ", 1));
                if (stateRecord != undefined && stateRecord.currentPageOrderedEntries != undefined && stateRecord.currentPageOrderedEntries.length > 0) {
                    var stateActiveProducts = FETCH_DATA_LOCAL('Brands_and_Products__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Brands_and_Products__c:_soup} FROM {Brands_and_Products__c} WHERE   {Brands_and_Products__c:Related_to_State__c}='" + stateRecord.currentPageOrderedEntries[0][0].Id + "' AND {Brands_and_Products__c:RecordTypeId}='" + SKURecordTypeIds['Product'] + "' and {Brands_and_Products__c:Used_in_Chatai__c}='false' and {Brands_and_Products__c:Removed__c}='false' order by {Brands_and_Products__c:Order__c}", 100000));
                    if (stateActiveProducts != undefined && stateActiveProducts.currentPageOrderedEntries != undefined && stateActiveProducts.currentPageOrderedEntries.length > 0) {
                        var productOrderList = [];
                        var keyUB=0;
                        angular.forEach(stateActiveProducts.currentPageOrderedEntries, function(record, key) {
                            productOrderList.push({
                                Id: ($stateParams.AccountId + record[0]['Id']),
                                Outlet_Id__c: $stateParams.AccountId,
                                Product_Id__c: record[0]['Id'],
                                Product_Name__c: record[0]['Sku_Code__c'],
                                Order__c: record[0]['Order__c'] + '',
                                Added__c: 'true'
                            });
                            if(record[0]['Stock_Capture__c']==true ){
                                $rootScope.StockAndSalesList[keyUB ] = {
                                    ProductName: record[0]['Sku_Code__c'],
                                    Product__c: record[0]['Id'],
                                    Order__c: parseInt(record[0]['Order__c']),
                                    Visit__c: $stateParams.visitId,
                                    External_Id__c: (new Date().getTime())+'-'+$stateParams.visitId+'-'+$window.Math.random() * 100000000,
                                    RecordTypeId: visitSummaryRtList['Stock and Sales'],
                                    IsDirty: true
                                };
                                keyUB++;
                            }
                        });
                        MOBILEDATABASE_ADD('Product_Assignment__c', productOrderList, 'Id');
                    }
                }
            } else {
                var keyUB=0;
                angular.forEach(outletProducts.currentPageOrderedEntries, function(record, key) {
                    if (record[0].Added__c == 'true') {
                        var stock = {
                            ProductName: SKUMap[record[0].Product_Id__c],
                            Product__c: record[0].Product_Id__c,
                            Visit__c: $stateParams.visitId,
                            Order__c: parseInt(record[0]['Order__c']) ,
                            RecordTypeId: visitSummaryRtList['Stock and Sales'],
                            IsDirty: true
                        };
                        var productOldvalues = oldvalues[record[0]['Product_Id__c']];
                        if (productOldvalues != undefined) {
                            stock.Total_Stock__c = productOldvalues.Total_Stock__c;
                            stock.Stock_Available__c = productOldvalues.Stock_Available__c;
                            stock.Chilled_Stock__c = productOldvalues.Chilled_Stock__c;
                            stock.Sales__c = productOldvalues.Sales__c;
                            stock.Stock_Ageing__c = productOldvalues.Stock_Ageing__c;
                            stock.External_Id__c = productOldvalues.External_Id__c;
                        } else {
                            stock.External_Id__c = (new Date().getTime())+'-'+$stateParams.visitId+'-'+$window.Math.random() * 100000000;
                        }
                        if(isComp[record[0].Product_Id__c]==true){
                            $rootScope.StockAndSalesList[keyUB] = stock;
                            keyUB++;
                        }
                    }
                });
            }
        }
        var tempStockAndSalesList = [];
        //$rootScope.StockAndSalesList=[];
        //angular.copy($rootScope.StockAndSalesList)
        angular.forEach($rootScope.StockAndSalesList, function(record, key) {
            tempStockAndSalesList[record.Order__c]=record;
        });
        $rootScope.StockAndSalesList=[];
        $rootScope.StockAndSalesList=tempStockAndSalesList;
        var tempStockAndSalesList2 = [];
        var tCount=0;
        angular.forEach($rootScope.StockAndSalesList, function(record, key) {
            tempStockAndSalesList2[tCount]=record;
            tCount++;
        });
        $rootScope.StockAndSalesList=tempStockAndSalesList2;
        if ($rootScope.StockAndSalesList.length == 0) {
            Splash.ShowToast('No SKU added to this outlet. Please click on product to add products.', 'long', 'bottom', function(a) {
                console.log(a)
            });
        }
        $ionicLoading.hide();
        $scope.selectRow = function(kk) {
                // $rootScope.StockAndSalesList
                $log.debug(kk + JSON.stringify($rootScope.StockAndSalesList));
                angular.forEach($rootScope.StockAndSalesList, function(record, key) {
                    if (key == kk) {
                        record.clicked = true;
                    } else {
                        record.clicked = false;
                    }
                });
            }
            // var scrollPosition = 0;
        $scope.focusCheckForStock = function(index) {
            // scrollPosition =scrollPosition+43;
            // $scope.selectRow(index);
            if (index % 8 == 0) {
                $ionicScrollDelegate.scrollTo(0, index * 43, true);
            }
            for (var i = 0; i < $rootScope.StockAndSalesList.length; i++) {
                document.getElementById('mySelect_' + (i)).style.opacity = '0';
                document.getElementById('mySelect1_' + (i)).style.opacity = '0';
            }
            document.getElementById('mySelect_' + (index)).style.opacity = '1';
            document.getElementById('mySelect1_' + (index)).style.opacity = '1';
        }
        $scope.orderByFunction = function(obj){
            return parseInt(obj.myOrder);
        };
        $scope.changeStock = function(st, ind) {
            if (st == false) {
                $rootScope.StockAndSalesList[ind].Total_Stock__c = 0;
                $scope.selectAll = false;
            } else if ($rootScope.StockAndSalesList[ind].Total_Stock__c == '0') {
                $rootScope.StockAndSalesList[ind].Total_Stock__c = '';
                $rootScope.StockAndSalesList[ind].Stock_Available__c = true;
            }
            if (st) {
                var noOfAvailables = 0;
                for (var i = 0; i < $rootScope.StockAndSalesList.length; i++) {
                    if ($rootScope.StockAndSalesList[i].Stock_Available__c != undefined || $rootScope.StockAndSalesList[i].Stock_Available__c == false) break;
                    noOfAvailables++
                };
                if (noOfAvailables == $rootScope.StockAndSalesList.length) {
                    $scope.selectAll = true;
                }
            }
            if($rootScope.StockAndSalesList[ind].Stock_Available__c ==false)
            {
                $rootScope.StockAndSalesList[ind].Stock_Ageing__c='';
                $rootScope.StockAndSalesList[ind].Chilled_Stock__c=false;
            }
        };
        $scope.checkedStock = function(st, ind) {
            //$log.debug(st + ind);
            if (st == true) document.getElementById("myinput_" + ind).focus();
        };
        $scope.selectRow_display = function(kk, event, index) {
            for (var i = 0; i < $rootScope.StockAndSalesList.length; i++) {
                document.getElementById('mySelect_' + (i)).style.opacity = '0';
                document.getElementById('mySelect1_' + (i)).style.opacity = '0';
            }
            document.getElementById('mySelect_' + (index)).style.opacity = '1';
            document.getElementById('mySelect1_' + (index)).style.opacity = '1';
        }
        $scope.restrictKeyup = function(_event, _index) {
            $log.debug(_event.keyCode);
            var max = 4;
            var mytext = document.getElementById("myinput_" + (parseInt(_index))).value;
            if (mytext.length >= max) {
                mytext = mytext.substr(0, max);
                document.getElementById("myinput_" + (parseInt(_index))).value = parseInt(mytext);
                $rootScope.StockAndSalesList[_index].Total_Stock__c = parseInt(mytext);
                $scope.changeTotalStock(parseInt(mytext), _index);
            }
        }
        $scope.restrictKeydown = function(_event, _index) {
            $log.debug(_event.keyCode);
            if (_event.which == 9) {
                document.getElementById("myinput_" + (parseInt(_index) + 1)).focus();
                _event.preventDefault();
            }
        }
        $scope.changeTotalStock = function(st, ind) {
            $log.debug(st + ind);
            if (st > 0 && st != '') $rootScope.StockAndSalesList[ind].Stock_Available__c = true;
            else {
                $rootScope.StockAndSalesList[ind].Stock_Available__c = false;
                $rootScope.StockAndSalesList[ind].Stock_Ageing__c='';
                $rootScope.StockAndSalesList[ind].Chilled_Stock__c=false;
            }
        };
        $scope.gotoProductOrder = function() {
            $location.path('app/productOrder/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $scope.ProductStage + '/' + $stateParams.order + '/UB/' + new Date());
        }
        $scope.selectAll = false;
        $scope.isRunning = false;
        $scope.selectAllAsAvailable = function() {
            $timeout(function() {
                if (!$scope.isRunning) {
                    $scope.isRunning = true;
                    $log.debug('$scope.selectAll' + $scope.selectAll + 'select');
                    $scope.selectAll = !$scope.selectAll;
                    angular.forEach($rootScope.StockAndSalesList, function(record, key) {
                        $log.debug('$scope.selectAll' + $scope.selectAll + 'record.Total_Stock__c' + record.Total_Stock__c);
                        if ($scope.selectAll == false && (record.Total_Stock__c == undefined || record.Total_Stock__c == 0)) {
                            record.Stock_Available__c = false;
                            record.Stock_Ageing__c='';
                            record.Chilled_Stock__c=false;
                        } else {
                            record.Stock_Available__c = true;
                        }
                        if ($rootScope.StockAndSalesList.length == key + 1) {
                            $timeout(function() {
                                $scope.isRunning = false;
                            }, 2000);
                        }
                    });
                }
                $ionicLoading.hide();
            }, 250);
        }
        $scope.justSelectAllAsAvailable = function() {
            if (!$scope.isRunning) {
                $scope.isRunning = true;
                $log.debug('$scope.selectAll' + $scope.selectAll + 'justSelect');
                angular.forEach($rootScope.StockAndSalesList, function(record, key) {
                    $log.debug('$scope.selectAll' + $scope.selectAll + 'record.Total_Stock__c' + record.Total_Stock__c);
                    if ($scope.selectAll == false && (record.Total_Stock__c == undefined || record.Total_Stock__c == 0)) {
                        record.Stock_Available__c = false;
                        record.Stock_Ageing__c='';
                        record.Chilled_Stock__c=false;
                    } else {
                        record.Stock_Available__c = true;
                    }
                    if ($rootScope.StockAndSalesList.length == key + 1) {
                        $timeout(function() {
                            $scope.isRunning = false;
                        }, 2000);
                    }
                });
            }
        }
        $scope.SaveStocks = function() {
            $timeout(function() {
                var stcokRecords = [];
                angular.forEach($rootScope.StockAndSalesList, function(record, key) {
                    if (record.Stock_Available__c != undefined || record.Total_Stock__c != undefined || record.Sales__c != undefined || record.Chilled_Stock__c != undefined || record.Stock_Ageing__c != undefined) {
                        var stock = record;
                        stock.Account_Id__c = $stateParams.AccountId;
                        stcokRecords.push(stock);
                    }
                });
                if (stcokRecords.length > 0) {
                    MOBILEDATABASE_ADD('Visit_Summary__c', stcokRecords, 'External_Id__c');
                }
                var transaction = [{
                    Account: $stateParams.AccountId,
                    stage: $stateParams.order,
                    stageValue: 'stocks',
                    Visit: $stateParams.visitId,
                    status: 'saved',
                    entryDate: today
                }];
                MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
                MOBILEDATABASE_ADD('Db_breadCrum', [{
                    VisitedOrder: $stateParams.order,
                    Name: 'stocks'
                }], 'VisitedOrder');
                if ($rootScope.audit != undefined && $rootScope.audit.visitType != undefined) {
                    $ionicLoading.hide();
                    if ($rootScope.audit.visitType == 'Chatai') {
                        $location.path('app/chataiSales/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date());
                    } else if ($rootScope.audit.visitType == 'Audit') {
                        $location.path('app/retailSales/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date());
                    }
                } else {
                    var page = {};
                    for (var i = 0; i < $rootScope.pages.length; i++) {
                        if ($rootScope.pages[i].pageDescription.Id == $rootScope.processFlowLineItems[parseInt($stateParams.order) + 1].Page__c) {
                            page = $rootScope.pages[i].pageDescription;
                            break;
                        }
                    }
                    $ionicLoading.hide();
                    var pageOrder = parseInt($stateParams.order) + 1;
                    if ($rootScope.processFlowLineItems[pageOrder] != undefined) {
                        if (page.Template_Name__c == 'Template1') {
                            $location.path('app/stockDetails/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + pageOrder + '/' + new Date());
                        } else if (page.Template_Name__c == 'Template2') {
                            $location.path('app/container2/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + pageOrder + '/' + new Date());
                        } else {
                            $location.path('app/container3/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + pageOrder + '/' + new Date());
                        }
                    } else {
                        $ionicLoading.hide();
                        $location.path('app/checkout/' + $stateParams.AccountId + '/' + $stateParams.visitId);
                    }

                }
                var today_now = $filter('date')(new Date(), 'dd/MM/yyyy hh:mm a');
                var userOprObj=[{
                    ObjectData:stcokRecords,
                    time_stamp:today_now,
                    comment:'From Stock Clicked next'
                }];
                MOBILEDATABASE_ADD('Db_User_Logs', userOprObj, '_soupEntryId');
            }, 100);
        }
        $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('StockCtrlError', err.stack + '::' + err.message);
    }
}).controller('StockDetailsCtrl', function($scope, $stateParams, $rootScope, $filter, $window, $location, $ionicLoading, $log, $timeout, FETCH_DATA_LOCAL, MOBILEDATABASE_ADD, FETCH_DATA, WEBSERVICE_ERROR) {
    try {
        $ionicLoading.show({
            template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
        });
        $rootScope.backText = 'Back';
        $rootScope.params = $stateParams;
        $rootScope.name = "StockDetailsCtrl";
        var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
        var today = $filter('date')(new Date(), 'dd/MM/yyyy');
        var transaction = [{
            Account: $stateParams.AccountId,
            stage: $stateParams.order,
            stageValue: 'stockDetails',
            Visit: $stateParams.visitId,
            status: 'pending',
            entryDate: today
        }];
        MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
        MOBILEDATABASE_ADD('Db_breadCrum', [{
            VisitedOrder: $stateParams.order,
            Name: 'stockDetails'
        }], 'VisitedOrder');
        var monthList = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var query = '';
        var d = new Date();
        $scope.currentMonth = $scope.MonthsList[d.getMonth()];
        var lastMonth = d.getMonth() - 1;
        var currentYear = d.getFullYear();
        $scope.stocDetailsPickValues = [];
        var d = new Date();
        var monthVal = d.getMonth();
        var yearVal = d.getFullYear();
        var containsObject=function(obj, list) {
            var i;
            for (i = 0; i < list.length; i++) {
                if (list[i] === obj) {
                    return true;
                }
            }
        
            return false;
        }
        $scope.stocDetailsPickValues.push(monthList[monthVal] + ' - ' + yearVal.toString().substr(2, 3));
        for (var i = 0; i < 5; i++) {
            monthVal = monthVal - 1;
            if (monthVal < 0) {
                monthVal = 11;
                yearVal = currentYear - 1;
            }
            $scope.stocDetailsPickValues.push(monthList[monthVal] + ' - ' + yearVal.toString().substr(2, 3));
        }
        $scope.stocDetailsPickValues.push('Older');
        var oldvalues = [];
        angular.forEach($rootScope.StockAndSalesList, function(record, key) {
            oldvalues[record.Product__c] = record;
        });
        if (oldvalues != undefined && oldvalues.length == 0) {
            var visitRecord = FETCH_DATA.querySoup('Visit__c', 'External_Id__c', $stateParams.visitId);
            var query = "{Visit_Summary__c:Visit__c} = '" + $stateParams.visitId + "'";
            if (visitRecord.currentPageOrderedEntries != undefined && visitRecord.currentPageOrderedEntries[0] != undefined && visitRecord.currentPageOrderedEntries[0].Id != undefined) {
                query = query + " OR {Visit_Summary__c:Visit__c} = '" + visitRecord.currentPageOrderedEntries[0].Id + "'";
            }
            var visitSummaryrecords = FETCH_DATA_LOCAL('Visit_Summary__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Visit_Summary__c:_soup} FROM {Visit_Summary__c} WHERE " + query , 100000));
            if (visitSummaryrecords.currentPageOrderedEntries != undefined && visitSummaryrecords.currentPageOrderedEntries.length > 0) {
                angular.forEach(visitSummaryrecords.currentPageOrderedEntries, function(record, key) {
                    if (record[0].Product__c != undefined && record[0].Audit_Type__c==undefined) {
                        oldvalues[record[0].Product__c] = record[0];
                    }
                });
            }
        }
        if ($rootScope.StockAndSalesList.length < 1) $rootScope.StockAndSalesList = [];
        $scope.auditTypeValues = [];
        $scope.showAuditType = false;
        if ($rootScope.processFlow.Retail__c == true) {
            $scope.showAuditType = true;
            $scope.auditTypeValues.push('Audit');
        }
        if ($rootScope.processFlow.Chatai__c == true) {
            $scope.showAuditType = true;
            $scope.auditTypeValues.push('Chatai');
        }
        var currentAccountLocal = FETCH_DATA.querySoup('Account', 'Id', $stateParams.AccountId);
        if (currentAccountLocal != undefined && currentAccountLocal.currentPageOrderedEntries != undefined && currentAccountLocal.currentPageOrderedEntries.length > 0) {
            $scope.currentOutlet = currentAccountLocal.currentPageOrderedEntries[0];
            var SKURecordTypes = FETCH_DATA.querySoup('DB_RecordTypes', 'SobjectType', 'Brands_and_Products__c');
            var SKURecordTypeIds = [];
            angular.forEach(SKURecordTypes.currentPageOrderedEntries, function(record, key) {
                SKURecordTypeIds[record.Name] = record.Id;
            });
            var visitSummaryRtListLocal = FETCH_DATA.querySoup('DB_RecordTypes', 'SobjectType', 'Visit_Summary__c');
            var visitSummaryRtList = [];
            angular.forEach(visitSummaryRtListLocal.currentPageOrderedEntries, function(record, key) {
                visitSummaryRtList[record.Name] = record.Id;
            });
            var isComp=[];
            $scope.companyMap=[];
            $scope.companyMapList=[];
            $scope.companyMapList.push('UB');
            var stateRecordTemp = FETCH_DATA_LOCAL('Brands_and_Products__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Brands_and_Products__c:_soup} FROM {Brands_and_Products__c} WHERE {Brands_and_Products__c:RecordTypeId}='" + SKURecordTypeIds['State'] + "' ", 1));
            var stateActiveProductsTemp = FETCH_DATA_LOCAL('Brands_and_Products__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Brands_and_Products__c:_soup} FROM {Brands_and_Products__c} WHERE   {Brands_and_Products__c:Related_to_State__c}='" + stateRecordTemp.currentPageOrderedEntries[0][0].Id + "' AND {Brands_and_Products__c:RecordTypeId}='" + SKURecordTypeIds['Product'] + "' and {Brands_and_Products__c:Used_in_Chatai__c}='false' and {Brands_and_Products__c:Removed__c}='false' order by {Brands_and_Products__c:Order__c}", 100000));
            if (stateActiveProductsTemp != undefined && stateActiveProductsTemp.currentPageOrderedEntries != undefined && stateActiveProductsTemp.currentPageOrderedEntries.length > 0) {
                angular.forEach(stateActiveProductsTemp.currentPageOrderedEntries, function(record, key) {
                    isComp[record[0]['Id']]=record[0]['Stock_Capture__c'];
                    $scope.companyMap[record[0]['Id']]=record[0]['Company__c'];
                    if(!containsObject(record[0]['Company__c'],$scope.companyMapList) && record[0]['Stock_Capture__c']==true)
                        $scope.companyMapList.push(record[0]['Company__c']);
                });

            }
            var outletProducts = FETCH_DATA_LOCAL('Product_Assignment__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Product_Assignment__c:_soup} FROM {Product_Assignment__c} WHERE  {Product_Assignment__c:Outlet_Id__c}='" + $stateParams.AccountId + "' order by {Product_Assignment__c:Order__c} ", 100000));
            if (outletProducts == undefined || outletProducts.currentPageOrderedEntries == undefined || outletProducts.currentPageOrderedEntries.length == 0 || outletProducts.currentPageOrderedEntries[0][0] == undefined) {
                var stateRecord = FETCH_DATA_LOCAL('Brands_and_Products__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Brands_and_Products__c:_soup} FROM {Brands_and_Products__c} WHERE {Brands_and_Products__c:RecordTypeId}='" + SKURecordTypeIds['State'] + "' ", 1));
                if (stateRecord != undefined && stateRecord.currentPageOrderedEntries != undefined && stateRecord.currentPageOrderedEntries.length > 0) {
                    var stateActiveProducts = FETCH_DATA_LOCAL('Brands_and_Products__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Brands_and_Products__c:_soup} FROM {Brands_and_Products__c} WHERE   {Brands_and_Products__c:Related_to_State__c}='" + stateRecord.currentPageOrderedEntries[0][0].Id + "' AND {Brands_and_Products__c:RecordTypeId}='" + SKURecordTypeIds['Product'] + "' and {Brands_and_Products__c:Used_in_Chatai__c}='false' and {Brands_and_Products__c:Removed__c}='false' order by {Brands_and_Products__c:Order__c}", 100000));
                    if (stateActiveProducts != undefined && stateActiveProducts.currentPageOrderedEntries != undefined && stateActiveProducts.currentPageOrderedEntries.length > 0) {
                        var productOrderList = [];
                        var keyUB=0;
                        angular.forEach(stateActiveProducts.currentPageOrderedEntries, function(record, key) {
                            productOrderList.push({
                                Id: ($stateParams.AccountId + record[0]['Id']),
                                Outlet_Id__c: $stateParams.AccountId,
                                Product_Id__c: record[0]['Id'],
                                Product_Name__c: record[0]['SKU_Name__c'],
                                Order__c: record[0]['Order__c'] + '',
                                Added__c: 'true'
                            });
                            if(record[0]['Stock_Capture__c']==true ){
                            $rootScope.StockAndSalesList[keyUB] = {
                                ProductName: record[0].SKU_Name__c,
                                Product__c: record[0].Id,
                                Order__c: parseInt(record[0]['Order__c']) ,
                                Visit__c: $stateParams.visitId,
                                External_Id__c: (new Date().getTime())+'-'+$stateParams.visitId+'-'+$window.Math.random() * 100000000,
                                RecordTypeId: visitSummaryRtList['Stock and Sales'],
                                IsDirty: true
                            };
                            keyUB++;
                            }
                        });
                        MOBILEDATABASE_ADD('Product_Assignment__c', productOrderList, 'Id');
                    }
                }
            } else {
                var keyUB=0;
                angular.forEach(outletProducts.currentPageOrderedEntries, function(record, key) {
                    if (record[0].Added__c == 'true') {
                        var stock = {
                            ProductName: record[0].Product_Name__c,
                            Product__c: record[0].Product_Id__c,
                            Order__c: parseInt(record[0]['Order__c']) ,
                            Visit__c: $stateParams.visitId,
                            RecordTypeId: visitSummaryRtList['Stock and Sales'],
                            IsDirty: true
                        };
                        var productOldvalues = oldvalues[record[0]['Product_Id__c']];
                        if (productOldvalues != undefined) {
                            stock.Total_Stock__c = productOldvalues.Total_Stock__c;
                            stock.Sales__c = productOldvalues.Sales__c;
                            stock.Stock_Ageing__c = productOldvalues.Stock_Ageing__c;
                            stock.Stock_Available__c = productOldvalues.Stock_Available__c;
                            stock.Chilled_Stock__c = productOldvalues.Chilled_Stock__c;
                            stock.External_Id__c = productOldvalues.External_Id__c;
                        } else {
                            stock.External_Id__c = (new Date().getTime())+'-'+$stateParams.visitId+'-'+$window.Math.random() * 100000000;
                        }
                        if(isComp[record[0].Product_Id__c]==true){
                            $rootScope.StockAndSalesList[keyUB] = stock;
                            keyUB++;
                        }
                    }
                });
            }
        }
        var tempStockAndSalesList = [];
        //$rootScope.StockAndSalesList=[];
        //angular.copy($rootScope.StockAndSalesList)
        angular.forEach($rootScope.StockAndSalesList, function(record, key) {
            tempStockAndSalesList[record.Order__c]=record;
        });
        $rootScope.StockAndSalesList=[];
        $rootScope.StockAndSalesList=tempStockAndSalesList;
        var tempStockAndSalesList2 = [];
        var tCount=0;
        angular.forEach($rootScope.StockAndSalesList, function(record, key) {
            tempStockAndSalesList2[tCount]=record;
            tCount++;
        });
        $rootScope.StockAndSalesList=tempStockAndSalesList2;
        if ($rootScope.StockAndSalesList.length == 0) {
            Splash.ShowToast('No SKU added to this outlet. Please click on product to add products.', 'long', 'bottom', function(a) {
                console.log(a)
            });
        }
        $ionicLoading.hide();
        $scope.selectRow = function(kk) {
            $log.debug(kk + JSON.stringify($rootScope.StockAndSalesList));
            angular.forEach($rootScope.StockAndSalesList, function(record, key) {
                if (key == kk) {
                    record.clicked = true;
                } else {
                    record.clicked = false;
                }
            });
        }
        $scope.selectRow_display = function(kk, event, index) {
            for (var i = 0; i < $rootScope.StockAndSalesList.length; i++) {
                document.getElementById('mySelect_' + (i)).style.opacity = '0';
                document.getElementById('mySelect1_' + (i)).style.opacity = '0';
            }
            document.getElementById('mySelect_' + (index)).style.opacity = '1';
            document.getElementById('mySelect1_' + (index)).style.opacity = '1';
        }
        $scope.selectAllChilled = false;
        $scope.selectAllAsChilled = function() {
            $timeout(function() {
                $scope.selectAllChilled = !$scope.selectAllChilled;
                angular.forEach($rootScope.StockAndSalesList, function(record, key) {
                    if(record.Stock_Available__c)
                        record.Chilled_Stock__c = $scope.selectAllChilled;
                });
                $ionicLoading.hide();
            }, 250);
        }
        $scope.changeChilledStock = function(st, ind) {
            if (st == false) {
                $scope.selectAllChilled = false;
            }
            if (st) {
                var noOfAvailables = 0;
                for (var i = 0; i < $rootScope.StockAndSalesList.length; i++) {
                    if ($rootScope.StockAndSalesList[i].Chilled_Stock__c != undefined || $rootScope.StockAndSalesList[i].Chilled_Stock__c == false) break;
                    noOfAvailables++;
                };
            }
            if (noOfAvailables == $rootScope.StockAndSalesList.length) {
                $scope.selectAllChilled = true;
            }
        }
        $scope.SaveStocks = function() {
            $timeout(function() {
                var stcokRecords = [];
                angular.forEach($rootScope.StockAndSalesList, function(record, key) {
                    if (record.Stock_Ageing__c != undefined || record.Chilled_Stock__c != undefined) {
                        var stock = record;
                        stock.Account_Id__c = $stateParams.AccountId;
                        stcokRecords.push(stock);
                    }
                });
                if (stcokRecords.length > 0) {
                    MOBILEDATABASE_ADD('Visit_Summary__c', stcokRecords, 'External_Id__c');
                }
                var transaction = [{
                    Account: $stateParams.AccountId,
                    stage: $stateParams.order,
                    stageValue: 'stockDetails',
                    Visit: $stateParams.visitId,
                    status: 'saved',
                    entryDate: today
                }];
                MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
                MOBILEDATABASE_ADD('Db_breadCrum', [{
                    VisitedOrder: $stateParams.order,
                    Name: 'stockDetails'
                }], 'VisitedOrder');
                var pageOrder = parseInt($stateParams.order) + 1;
                if ($rootScope.processFlowLineItems[pageOrder] != undefined) {
                    var page = {};
                    for (var i = 0; i < $rootScope.pages.length; i++) {
                        if ($rootScope.pages[i].pageDescription.Id == $rootScope.processFlowLineItems[pageOrder].Page__c) {
                            page = $rootScope.pages[i].pageDescription;
                            break;
                        }
                    }
                    $ionicLoading.hide();
                    if (page.Template_Name__c == 'Template1') {
                        // $location.path('app/stockDetails/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/'+pageOrder+'/' + new Date());
                    } else if (page.Template_Name__c == 'Template2') {
                        $location.path('app/container2/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + pageOrder + '/' + new Date());
                    } else {
                        $location.path('app/container3/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + pageOrder + '/' + new Date());
                    }
                } else {
                    $ionicLoading.hide();
                    $location.path('app/checkout/' + $stateParams.AccountId + '/' + $stateParams.visitId);
                }
                var today_now = $filter('date')(new Date(), 'dd/MM/yyyy hh:mm a');
                var userOprObj=[{
                    ObjectData:stcokRecords,
                    time_stamp:today_now,
                    comment:'From Stock Clicked next'
                }];
                MOBILEDATABASE_ADD('Db_User_Logs', userOprObj, '_soupEntryId');
            }, 100);
        }
        $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('StockDetailsCtrlError', err.stack + '::' + err.message);
    }
}).controller('ChataiCtrl', function($scope, $stateParams, $rootScope, $filter, $window, $location, $ionicLoading, $log, $ionicScrollDelegate, FETCH_DATA_LOCAL, MOBILEDATABASE_ADD, FETCH_DATA, WEBSERVICE_ERROR) {
    try {
        $ionicLoading.show({
            template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
        });
        $rootScope.backText = 'Back';
        $rootScope.params = $stateParams;
        $rootScope.name = "ChataiCtrl";
        
        var today = $filter('date')(new Date(), 'dd/MM/yyyy');
        var transaction = [{
            Account: $stateParams.AccountId,
            stage: $stateParams.order,
            stageValue: 'chatai',
            Visit: $stateParams.visitId,
            status: 'pending',
            entryDate: today
        }];
        MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
        MOBILEDATABASE_ADD('Db_breadCrum', [{
            VisitedOrder: $stateParams.order,
            Name: 'Chatai'
        }], 'VisitedOrder');
        var monthList = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var d = new Date();
        $scope.currentMonth = $scope.MonthsList[d.getMonth()];
        var lastMonth = d.getMonth() - 1;
        var currentYear = d.getFullYear();
        $scope.stocDetailsPickValues = [];
        var d = new Date();
        var monthVal = d.getMonth();
        var yearVal = d.getFullYear();
        $scope.chataiTotal=[];
        $scope.chataiTotalVolume=[];
        //$scope.stocDetailsPickValues.push(monthList[monthVal] + ' - ' + yearVal.toString().substr(2, 3));
        for (var i = 0; i < 6; i++) {
            monthVal = monthVal - 1;
            if (monthVal < 0) {
                monthVal = 11;
                yearVal = currentYear - 1;
            }
            $scope.stocDetailsPickValues.push(monthList[monthVal] + ' - ' + yearVal.toString().substr(2, 3));
            $scope.chataiTotal[monthList[monthVal] + ' - ' + yearVal.toString().substr(2, 3)]=[];
            $scope.chataiTotalVolume[monthList[monthVal] + ' - ' + yearVal.toString().substr(2, 3)]=[];
        }
        if ($rootScope.ChataiMonth.Name == undefined)
        $rootScope.ChataiMonth.Name=$scope.stocDetailsPickValues[0];
        var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
        var oldvalues = [];
        angular.forEach($rootScope.chataiSales, function(record, key) {
            if(oldvalues[record.Product__c]==undefined)
            oldvalues[record.Product__c]=[];
            angular.forEach($scope.stocDetailsPickValues, function(recordInner, keyInner) {
                oldvalues[record.Product__c][recordInner] = record;
            });
        });
        if (oldvalues != undefined && oldvalues.length == 0) {
            var visitRecord = FETCH_DATA.querySoup('Visit__c', 'External_Id__c', $stateParams.visitId);
            var query = "{Visit_Summary__c:Visit__c} = '" + $stateParams.visitId + "'";
            if (visitRecord.currentPageOrderedEntries != undefined && visitRecord.currentPageOrderedEntries[0] != undefined && visitRecord.currentPageOrderedEntries[0].Id != undefined) {
                query = query + " OR {Visit_Summary__c:Visit__c} = '" + visitRecord.currentPageOrderedEntries[0].Id + "'";
            }
            var visitSummaryrecords = FETCH_DATA_LOCAL('Visit_Summary__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Visit_Summary__c:_soup} FROM {Visit_Summary__c} WHERE (" + query + ") and {Visit_Summary__c:Audit_Type__c}='Chatai'" , 100000));
            if (visitSummaryrecords.currentPageOrderedEntries != undefined && visitSummaryrecords.currentPageOrderedEntries.length > 0) {
                angular.forEach(visitSummaryrecords.currentPageOrderedEntries, function(record, key) {
                    if(oldvalues[record[0].Product__c]==undefined)
                        oldvalues[record[0].Product__c]=[];
                    if (record[0].Product__c != undefined ) {
                        oldvalues[record[0].Product__c][record[0].Month__c+" - "+record[0].Year__c] = record[0];
                    }
                });
            }
        }
        
        $rootScope.chataiSales = [];
        $scope.productSegement=[];
        var currentAccountLocal = FETCH_DATA.querySoup('Account', 'Id', $stateParams.AccountId);
        if (currentAccountLocal != undefined && currentAccountLocal.currentPageOrderedEntries != undefined && currentAccountLocal.currentPageOrderedEntries.length > 0) {
            $scope.currentOutlet = currentAccountLocal.currentPageOrderedEntries[0];
            var SKURecordTypes = FETCH_DATA.querySoup('DB_RecordTypes', 'SobjectType', 'Brands_and_Products__c');
            var SKURecordTypeIds = [];
            angular.forEach(SKURecordTypes.currentPageOrderedEntries, function(record, key) {
                SKURecordTypeIds[record.Name] = record.Id;
            });
            var visitSummaryRtListLocal = FETCH_DATA.querySoup('DB_RecordTypes', 'SobjectType', 'Visit_Summary__c');
            var visitSummaryRtList = [];
            angular.forEach(visitSummaryRtListLocal.currentPageOrderedEntries, function(record, key) {
                visitSummaryRtList[record.Name] = record.Id;
            });
            var stateRecord = FETCH_DATA_LOCAL('Brands_and_Products__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Brands_and_Products__c:_soup} FROM {Brands_and_Products__c} WHERE {Brands_and_Products__c:RecordTypeId}='" + SKURecordTypeIds['State'] + "'", 1));
            if (stateRecord != undefined && stateRecord.currentPageOrderedEntries != undefined && stateRecord.currentPageOrderedEntries.length > 0) {
                var stateActiveProducts = FETCH_DATA_LOCAL('Brands_and_Products__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Brands_and_Products__c:_soup} FROM {Brands_and_Products__c} WHERE   {Brands_and_Products__c:Related_to_State__c}='" + stateRecord.currentPageOrderedEntries[0][0].Id + "' AND {Brands_and_Products__c:RecordTypeId}='" + SKURecordTypeIds['Product'] + "' and {Brands_and_Products__c:Used_in_Chatai__c}='true' and {Brands_and_Products__c:Removed__c}='false'", 100000));
                if (stateActiveProducts != undefined && stateActiveProducts.currentPageOrderedEntries != undefined && stateActiveProducts.currentPageOrderedEntries.length > 0) {
                    angular.forEach(stateActiveProducts.currentPageOrderedEntries, function(record, key) {
                        angular.forEach($scope.stocDetailsPickValues, function(recordInner, keyInner) {
                            $scope.productSegement[record[0].Id]=record[0].Segment__c;
                            /*if(oldvalues[record[0].Id]==undefined)
                                oldvalues[record[0].Id]=[];
                            oldvalues[record[0].Id][recordInner] = record;*/
                            var sale = {
                                ProductName: record[0].Sku_Code__c,
                                Product__c: record[0].Id,
                                Order__c:record[0].Order__c,
                                Visit__c: $stateParams.visitId,
                                External_Id__c: (new Date().getTime())+'-'+$stateParams.visitId+'-'+$window.Math.random() * 100000000,
                                RecordTypeId: visitSummaryRtList['Stock and Sales'],
                                IsDirty: true,
                                Audit_Type__c: 'Chatai'
                            };
                            if(oldvalues[record[0].Id]!=undefined){
                                var productOldvalues = oldvalues[record[0].Id][recordInner];
                                if (productOldvalues != undefined) {
                                    sale.Sales__c = productOldvalues.Sales__c;
                                    sale.Sales_Volume__c=productOldvalues.Sales_Volume__c;
                                    sale.External_Id__c = productOldvalues.External_Id__c;
                                } else {
                                    sale.External_Id__c = (new Date().getTime())+'-'+$stateParams.visitId+'-'+$window.Math.random() * 100000000;
                                }
                            }else {
                                sale.External_Id__c = (new Date().getTime())+'-'+$stateParams.visitId+'-'+$window.Math.random() * 100000000;
                            }
                            if($rootScope.chataiSales[recordInner]==undefined)
                            $rootScope.chataiSales[recordInner]=[];
                            $rootScope.chataiSales[recordInner].push(sale);
                        });
                    });
                }
            }
        }
        angular.forEach($scope.stocDetailsPickValues, function(recordInner, keyInner) {
            var tempChataiSales = [];
            angular.forEach($rootScope.chataiSales[recordInner], function(record, key) {
                tempChataiSales[record.Order__c]=record;
            });
            $rootScope.chataiSales[recordInner]=[];
            $rootScope.chataiSales[recordInner]=tempChataiSales;
            var tempChataiSales2 = [];
            var tCount=0;
            angular.forEach($rootScope.chataiSales[recordInner], function(record, key) {
                tempChataiSales2[tCount]=record;
                tCount++;
            });
            $rootScope.chataiSales[recordInner]=tempChataiSales2;
        });
        $ionicLoading.hide();
        $scope.refreshList = function(){
            //$scope.apply();
        }
        $scope.selectRow = function(kk) {
            //  $log.debug(kk + JSON.stringify($rootScope.retailSalesList));
            angular.forEach($rootScope.chataiSales, function(record, key) {
                if (key == kk) {
                    record.clicked = true;
                } else {
                    record.clicked = false;
                }
            });
        }

         $scope.restrictKeyup = function(_event, _index, inputId) {
            $log.debug(_event.keyCode);
            var max = 5;
            var mytext = document.getElementById(inputId + (parseInt(_index))).value;
            if (mytext.length >= max) {
                mytext = mytext.substr(0, max);
                document.getElementById(inputId + (parseInt(_index))).value = parseInt(mytext);
                //$rootScope.chataiSales[_index].Sales__c = parseInt(mytext);                
            }
        }

        $scope.restrictKeyup_volume = function(_event, _index, inputId) {
            $log.debug(_event.keyCode);
            var max = 5;
            var mytext = document.getElementById(inputId + (parseInt(_index))).value;
            if (mytext.length >= max) {
                mytext = mytext.substr(0, max);
                document.getElementById(inputId + (parseInt(_index))).value = parseInt(mytext);
                //$rootScope.chataiSales[_index].Sales_Volume__c = parseInt(mytext);                
            }
        }
        $scope.calculateTotalSales=function(){
            
            angular.forEach($scope.stocDetailsPickValues, function(recordInner, keyInner) {
                   $scope.chataiTotal[recordInner]['Mild']=0;
                   $scope.chataiTotalVolume[recordInner]['Mild']=0;
                   $scope.chataiTotal[recordInner]['Strong']=0;
                   $scope.chataiTotalVolume[recordInner]['Strong']=0;
                   $scope.chataiTotal[recordInner]['Spirit']=0;
                   $scope.chataiTotalVolume[recordInner]['Spirit']=0;
                   var max = 5;
                   angular.forEach($rootScope.chataiSales[recordInner], function(record, key) {
                        if (record.Sales__c != undefined && !isNaN(record.Sales__c)) {
                            if($scope.chataiTotal[recordInner][$scope.productSegement[record.Product__c]]==undefined)
                                $scope.chataiTotal[recordInner][$scope.productSegement[record.Product__c]]=0;
                            if(record.Sales__c!=undefined && record.Sales__c!=null && record.Sales__c!=''){
                                var mySalesTemp = record.Sales__c;
                                var mySales=mySalesTemp.substr(0, max);
                                $rootScope.chataiSales[recordInner][key]['Sales__c']=mySales;
                                $scope.chataiTotal[recordInner][$scope.productSegement[record.Product__c]]=parseInt(mySales)+parseInt($scope.chataiTotal[recordInner][$scope.productSegement[record.Product__c]]);
                            }
                        }
                        if (record.Sales_Volume__c != undefined && !isNaN(record.Sales_Volume__c)) {
                            if($scope.chataiTotalVolume[recordInner][$scope.productSegement[record.Product__c]]==undefined)
                                $scope.chataiTotalVolume[recordInner][$scope.productSegement[record.Product__c]]=0;
                            if(record.Sales_Volume__c!=undefined && record.Sales_Volume__c!=null && record.Sales_Volume__c!=''){
                                var mySalesVolumeTemp=record.Sales_Volume__c;
                                var mySalesVolume=mySalesVolumeTemp.substr(0, max);
                                $rootScope.chataiSales[recordInner][key]['Sales_Volume__c']=mySalesVolume;
                                $scope.chataiTotalVolume[recordInner][$scope.productSegement[record.Product__c]]=parseInt(mySalesVolume)+parseInt($scope.chataiTotalVolume[recordInner][$scope.productSegement[record.Product__c]]);
                            }
                        }
                    });
            });
        }
        $scope.restrictKeydown = function(_event, _index) {
            $log.debug(_event.keyCode);
            if (_event.which == 9) {
                document.getElementById("myinput_" + (parseInt(_index) + 1)).focus();
                _event.preventDefault();
            }
        }

        $scope.focusCheckForStock = function(index, event) {
            var _currentHeight = jQuery(event.target).parents('.row').height() - 4;
            $ionicScrollDelegate.scrollTo(0, index * _currentHeight, true);
        }

        $scope.selectRow_display = function(kk, event, index, monthIndex) {
            for (var i = 0; i < $rootScope.chataiSales[monthIndex].length; i++) {
                document.getElementById('mySelect_' + (i)).style.opacity = '0';
                document.getElementById('mySelect1_' + (i)).style.opacity = '0';
            }
            document.getElementById('mySelect_' + (index)).style.opacity = '1';
            document.getElementById('mySelect1_' + (index)).style.opacity = '1';
        }
       
        $scope.SaveChataiSales = function() {
            var saleList = [];
            //$log.debug('Month==' + $rootScope.ChataiMonth.Name.getMonth() + 'Year==' + $rootScope.ChataiMonth.Name.getFullYear())
            angular.forEach($scope.stocDetailsPickValues, function(recordInner, keyInner) {
                angular.forEach($rootScope.chataiSales[recordInner], function(record, key) {
                    if (record.Sales__c != undefined || record.Sales_Volume__c!=undefined) {
                        var monthYear=recordInner.split(" - ");
                        record.Month__c = monthYear[0];
                        record.Year__c = monthYear[1];
                        record.Account_Id__c = $stateParams.AccountId;
                        saleList.push(record);
                    }
                });
            });
            $log.debug(JSON.stringify(saleList));
            if (saleList.length > 0) {
                MOBILEDATABASE_ADD('Visit_Summary__c', saleList, 'External_Id__c');
            }
            var transaction = [{
                Account: $stateParams.AccountId,
                stage: $stateParams.order,
                stageValue: 'chatai',
                Visit: $stateParams.visitId,
                status: 'saved',
                entryDate: today
            }];
            MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
            MOBILEDATABASE_ADD('Db_breadCrum', [{
                VisitedOrder: $stateParams.order,
                Name: 'Chatai'
            }], 'VisitedOrder');
            $location.path('app/stockDetails/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + (parseInt($stateParams.order)+1) + '/' + new Date());
        }
        $scope.calculateTotalSales();
        $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('ChataiCtrlError', err.stack + '::' + err.message);
    }
}).controller('RetailCtrl', function($scope, $stateParams, $rootScope, $window, $filter, $location, $ionicLoading, $log, $ionicScrollDelegate, FETCH_DATA_LOCAL, MOBILEDATABASE_ADD, FETCH_DATA, WEBSERVICE_ERROR) {
    try {
        $ionicLoading.show({
            template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
        });
        $rootScope.backText = 'Back';
        $rootScope.params = $stateParams;
        $rootScope.name = "RetailCtrl";
        var containsObject=function(obj, list) {
            var i;
            for (i = 0; i < list.length; i++) {
                if (list[i] === obj) {
                    return true;
                }
            }
        
            return false;
        }
        
        var monthList = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var d = new Date();
        $scope.currentMonth = $scope.MonthsList[d.getMonth()];
        var lastMonth = d.getMonth() - 1;
        var currentYear = d.getFullYear();
        $scope.stocDetailsPickValues = [];
        $scope.auditTotal=[];
        var d = new Date();
        var monthVal = d.getMonth();
        var yearVal = d.getFullYear();
        //$scope.stocDetailsPickValues.push(monthList[monthVal] + ' - ' + yearVal.toString().substr(2, 3));
        for (var i = 0; i < 6; i++) {
            monthVal = monthVal - 1;
            if (monthVal < 0) {
                monthVal = 11;
                yearVal = currentYear - 1;
            }
            $scope.stocDetailsPickValues.push(monthList[monthVal] + ' - ' + yearVal.toString().substr(2, 3));
            $scope.auditTotal[monthList[monthVal] + ' - ' + yearVal.toString().substr(2, 3)]=[];
            
        }
        // $scope.stocDetailsPickValues.push('Older');
        console.log($scope.stocDetailsPickValues)
        if ($rootScope.Month.Name == undefined)
        $rootScope.Month.Name=$scope.stocDetailsPickValues[0];
        
        var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
        
        var oldvalues = [];
        angular.forEach($rootScope.retailSalesList, function(record, key) {
            if(oldvalues[record.Product__c]==undefined)
            oldvalues[record.Product__c]=[];
            angular.forEach($scope.stocDetailsPickValues, function(recordInner, keyInner) {
                oldvalues[record.Product__c][recordInner] = record;
            });
        });
        if (oldvalues != undefined && oldvalues.length == 0) {
            var visitRecord = FETCH_DATA.querySoup('Visit__c', 'External_Id__c', $stateParams.visitId);
            var query = "{Visit_Summary__c:Visit__c} = '" + $stateParams.visitId + "'";
            if (visitRecord.currentPageOrderedEntries != undefined && visitRecord.currentPageOrderedEntries[0] != undefined && visitRecord.currentPageOrderedEntries[0].Id != undefined) {
                query = query + " OR {Visit_Summary__c:Visit__c} = '" + visitRecord.currentPageOrderedEntries[0].Id + "'";
            }
            var visitSummaryrecords = FETCH_DATA_LOCAL('Visit_Summary__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Visit_Summary__c:_soup} FROM {Visit_Summary__c} WHERE (" + query + ") and {Visit_Summary__c:Audit_Type__c}='Audit'" , 100000));
            if (visitSummaryrecords.currentPageOrderedEntries != undefined && visitSummaryrecords.currentPageOrderedEntries.length > 0) {
                angular.forEach(visitSummaryrecords.currentPageOrderedEntries, function(record, key) {
                    if(oldvalues[record[0].Product__c]==undefined)
                        oldvalues[record[0].Product__c]=[];
                    if (record[0].Product__c != undefined ) {
                        oldvalues[record[0].Product__c][record[0].Month__c+" - "+record[0].Year__c] = record[0];
                    }
                });
            }
        }
        $rootScope.retailSalesList = {};
        var today = $filter('date')(new Date(), 'dd/MM/yyyy');
        var transaction = [{
            Account: $stateParams.AccountId,
            stage: $stateParams.order,
            stageValue: 'Retail',
            Visit: $stateParams.visitId,
            status: 'pending',
            entryDate: today
        }];
        MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
        MOBILEDATABASE_ADD('Db_breadCrum', [{
            VisitedOrder: $stateParams.order,
            Name: 'Retail'
        }], 'VisitedOrder');
        var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
        var currentAccountLocal = FETCH_DATA.querySoup('Account', 'Id', $stateParams.AccountId);
        if (currentAccountLocal != undefined && currentAccountLocal.currentPageOrderedEntries != undefined && currentAccountLocal.currentPageOrderedEntries.length > 0) {
            $scope.currentOutlet = currentAccountLocal.currentPageOrderedEntries[0];
            var SKURecordTypes = FETCH_DATA.querySoup('DB_RecordTypes', 'SobjectType', 'Brands_and_Products__c');
            var SKURecordTypeIds = [];
            angular.forEach(SKURecordTypes.currentPageOrderedEntries, function(record, key) {
                SKURecordTypeIds[record.Name] = record.Id;
            });
            var visitSummaryRtListLocal = FETCH_DATA.querySoup('DB_RecordTypes', 'SobjectType', 'Visit_Summary__c');
            var visitSummaryRtList = [];
            angular.forEach(visitSummaryRtListLocal.currentPageOrderedEntries, function(record, key) {
                visitSummaryRtList[record.Name] = record.Id;
            });
            var isComp=[];
            $scope.companyMap=[];
            $scope.companyMapList=[];
            $scope.productSegement=[];
            
            $scope.companyMapList.push('UB');
            var stateRecordTemp = FETCH_DATA_LOCAL('Brands_and_Products__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Brands_and_Products__c:_soup} FROM {Brands_and_Products__c} WHERE {Brands_and_Products__c:RecordTypeId}='" + SKURecordTypeIds['State'] + "' ", 1));
            var stateActiveProductsTemp = FETCH_DATA_LOCAL('Brands_and_Products__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Brands_and_Products__c:_soup} FROM {Brands_and_Products__c} WHERE   {Brands_and_Products__c:Related_to_State__c}='" + stateRecordTemp.currentPageOrderedEntries[0][0].Id + "' AND {Brands_and_Products__c:RecordTypeId}='" + SKURecordTypeIds['Product'] + "' and {Brands_and_Products__c:Used_in_Chatai__c}='false' and {Brands_and_Products__c:Removed__c}='false'", 100000));
            if (stateActiveProductsTemp != undefined && stateActiveProductsTemp.currentPageOrderedEntries != undefined && stateActiveProductsTemp.currentPageOrderedEntries.length > 0) {
                angular.forEach(stateActiveProductsTemp.currentPageOrderedEntries, function(record, key) {
                    isComp[record[0]['Id']]=record[0]['Competitor_Product__c'];
                    $scope.companyMap[record[0]['Id']]=record[0]['Company__c'];
                    $scope.productSegement[record[0]['Id']]=record[0]['Segment__c'];
                    if(!containsObject(record[0]['Company__c'],$scope.companyMapList))
                        $scope.companyMapList[parseInt(record[0]['Order__c'])]=record[0]['Company__c'];
                });

            }
            var tempcompanyMapList=[];
            angular.forEach($scope.companyMapList, function(record, key) {
                tempcompanyMapList.push(record);
            });
            $scope.companyMapList=tempcompanyMapList;
            var outletProducts = FETCH_DATA_LOCAL('Product_Assignment__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Product_Assignment__c:_soup} FROM {Product_Assignment__c} WHERE  {Product_Assignment__c:Outlet_Id__c}='" + $stateParams.AccountId + "' order by {Product_Assignment__c:Order__c} ", 100000));
            if (outletProducts != undefined && outletProducts.currentPageOrderedEntries != undefined && outletProducts.currentPageOrderedEntries.length != 0 && outletProducts.currentPageOrderedEntries[0][0] != undefined) {
                angular.forEach(outletProducts.currentPageOrderedEntries, function(record, key) {
                    angular.forEach($scope.stocDetailsPickValues, function(recordInner, keyInner) {
                        if (record[0].Added__c == 'true') {
                            if(isComp[record[0].Product_Id__c]==false){
                                var sale = {
                                    ProductName: record[0].Product_Name__c,
                                    Product__c: record[0].Product_Id__c,
                                    Visit__c: $stateParams.visitId,
                                    External_Id__c: (new Date().getTime())+'-'+$stateParams.visitId+'-'+$window.Math.random() * 100000000,
                                    RecordTypeId: visitSummaryRtList['Stock and Sales'],
                                    myOrder:parseInt(record[0]['Order__c'])+1,
                                    IsDirty: true,
                                    Audit_Type__c: 'Audit'
                                };
                            }else{
                                var sale = {
                                    ProductName: record[0].Product_Name__c,
                                    Product__c: record[0].Product_Id__c,
                                    Visit__c: $stateParams.visitId,
                                    External_Id__c: (new Date().getTime())+'-'+$stateParams.visitId+'-'+$window.Math.random() * 100000000,
                                    RecordTypeId: visitSummaryRtList['Stock and Sales'],
                                    myOrder:parseInt(record[0]['Order__c'])+1000,
                                    IsDirty: true,
                                    Audit_Type__c: 'Audit'
                                };
                            }
                            
                            if(oldvalues[record[0].Product_Id__c]!=undefined){
                                var productOldvalues = oldvalues[record[0].Product_Id__c][recordInner];
                                if (productOldvalues != undefined) {
                                    sale.Sales__c = productOldvalues.Sales__c;
                                    sale.External_Id__c = productOldvalues.External_Id__c;
                                } else {
                                    sale.External_Id__c = (new Date().getTime())+'-'+$stateParams.visitId+'-'+$window.Math.random() * 100000000;
                                }
                            }else {
                                sale.External_Id__c = (new Date().getTime())+'-'+$stateParams.visitId+'-'+$window.Math.random() * 100000000;
                            }
                            if($rootScope.retailSalesList[$scope.companyMap[record[0]['Product_Id__c']]]==undefined)
                                $rootScope.retailSalesList[$scope.companyMap[record[0]['Product_Id__c']]]={};
                            if($rootScope.retailSalesList[$scope.companyMap[record[0]['Product_Id__c']]][recordInner]==undefined)
                                $rootScope.retailSalesList[$scope.companyMap[record[0]['Product_Id__c']]][recordInner]=[];
                            $rootScope.retailSalesList[$scope.companyMap[record[0]['Product_Id__c']]][recordInner].push(sale);
                        }
                    });
                });
            }
            var stateRecord = FETCH_DATA_LOCAL('Brands_and_Products__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Brands_and_Products__c:_soup} FROM {Brands_and_Products__c} WHERE {Brands_and_Products__c:RecordTypeId}='" + SKURecordTypeIds['State'] + "'", 1));
            if (stateRecord != undefined && stateRecord.currentPageOrderedEntries != undefined && stateRecord.currentPageOrderedEntries.length > 0) {
                var stateActiveProducts = FETCH_DATA_LOCAL('Brands_and_Products__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Brands_and_Products__c:_soup} FROM {Brands_and_Products__c} WHERE   {Brands_and_Products__c:Related_to_State__c}='" + stateRecord.currentPageOrderedEntries[0][0].Id + "' AND {Brands_and_Products__c:RecordTypeId}='" + SKURecordTypeIds['Product'] + "' and {Brands_and_Products__c:Competitor_Product__c}='true' and {Brands_and_Products__c:Used_in_Chatai__c}='false' and {Brands_and_Products__c:Removed__c}='false'", 100000));
                if (stateActiveProducts != undefined && stateActiveProducts.currentPageOrderedEntries != undefined && stateActiveProducts.currentPageOrderedEntries.length > 0) {
                    var productSize = $rootScope.retailSalesList.length;
                    angular.forEach(stateActiveProducts.currentPageOrderedEntries, function(record, key) {
                        var sale = {
                            ProductName: record[0].Sku_Code__c,
                            Product__c: record[0].Id,
                            Visit__c: $stateParams.visitId,
                            External_Id__c: (new Date().getTime())+'-'+$stateParams.visitId+'-'+$window.Math.random() * 100000000,
                            RecordTypeId: visitSummaryRtList['Stock and Sales'],
                            IsDirty: true,
                            Audit_Type__c: 'Audit'
                        };
                        var productOldvalues = oldvalues[record[0].Id];
                        if (productOldvalues != undefined) {
                            sale.Sales__c = productOldvalues.Sales__c;
                            sale.External_Id__c = productOldvalues.External_Id__c;
                        } else {
                            sale.External_Id__c = (new Date().getTime())+'-'+$stateParams.visitId+'-'+$window.Math.random() * 100000000;
                        }
                        //$rootScope.retailSalesList[productSize++] = sale;
                    });
                }
            }
            
            angular.forEach($rootScope.retailSalesList, function(record1, key1) {
                angular.forEach($rootScope.retailSalesList[key1], function(record2, key2) {
                    var tempRetailSalesList={};
                    angular.forEach($rootScope.retailSalesList[key1][key2], function(record3, key3) {
                        tempRetailSalesList[record3.myOrder]=record3
                    });
                    $rootScope.retailSalesList[key1][key2]=tempRetailSalesList;
                    var tempRetailSalesList1=[];
                    angular.forEach($rootScope.retailSalesList[key1][key2], function(record3, key3) {
                        tempRetailSalesList1.push(record3);
                    });
                    $rootScope.retailSalesList[key1][key2]=tempRetailSalesList1;
                });
            });
            $scope.monthArray=[];
            angular.forEach($scope.stocDetailsPickValues, function(recordMonth, keyMonth) {
                var indexCount=0;
                angular.forEach($rootScope.retailSalesList, function(record, key) {
                    angular.forEach(record[recordMonth], function(record1, key1) {
                        indexCount++;
                        if($scope.monthArray[key]==undefined)
                            $scope.monthArray[key]=[];
                        $scope.monthArray[key][key1]=indexCount;
                        
                    });
                });
            });
            
        }
        $ionicLoading.hide();
        $scope.onClickSelect=function(){
            $ionicLoading.show({
                template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/>'
            });
        }
        $scope.onCompleteSelect=function(){
            $ionicLoading.hide();
            document.getElementById("auditMonth").blur();
        }
        $scope.selectRow = function(kk) {
            //  $log.debug(kk + JSON.stringify($rootScope.retailSalesList));
            angular.forEach($rootScope.retailSalesList, function(record, key) {
                if (key == kk) {
                    record.clicked = true;
                } else {
                    record.clicked = false;
                }
            });
        }
        
        $scope.orderByFunction = function(obj){
            return parseInt(obj.myOrder);
        };
         $scope.restrictKeyup = function(_event, _index, orderId, companyIndex, monthIndex) {
            var max = 5;
            var mytext = document.getElementById("myinput_" +companyIndex+'_'+monthIndex.replace(" - ","_")+ '_' + (parseInt(orderId))).value;
            if (mytext.length >= max) {
                mytext = mytext.substr(0, max);
                document.getElementById("myinput_" +companyIndex+'_'+monthIndex.replace(" - ","_")+ '_' + (parseInt(orderId))).value = parseInt(mytext);
                $rootScope.retailSalesList[companyIndex][monthIndex][_index].Sales__c = parseInt(mytext);               
            }
            $scope.calculateTotalSales();
        }
        $scope.calculateTotalSales=function(){
            angular.forEach($scope.stocDetailsPickValues, function(recordInner, keyInner) {
                   $scope.auditTotal[recordInner]=[];
            });
            angular.forEach($scope.companyMapList, function(recordOuter, keyOuter) {
               angular.forEach($scope.stocDetailsPickValues, function(recordInner, keyInner) {
               if($rootScope.retailSalesList[recordOuter]!=undefined && $rootScope.retailSalesList[recordOuter][recordInner]!=undefined)
                   angular.forEach($rootScope.retailSalesList[recordOuter][recordInner], function(record, key) {
                        if (record.Sales__c != undefined) {
                            if($scope.auditTotal[recordInner][$scope.productSegement[record.Product__c]]==undefined)
                                $scope.auditTotal[recordInner][$scope.productSegement[record.Product__c]]=0;
                            $scope.auditTotal[recordInner][$scope.productSegement[record.Product__c]]=parseInt(record.Sales__c)+parseInt($scope.auditTotal[recordInner][$scope.productSegement[record.Product__c]]);
                        }
                    });
                });
            });
        }
        $scope.focusCheckForStock = function(index, event) {
            var _currentHeight = jQuery(event.target).parents('.row').height() - 5;
            $ionicScrollDelegate.scrollTo(0, index * _currentHeight, true);
        }

        $scope.selectRow_display = function(kk, event, index, companyIndex, monthIndex) {
            //change code here
            angular.forEach($scope.companyMapList, function(recordInner, keyInner) {
                angular.forEach($rootScope.retailSalesList[recordInner][monthIndex], function(recordOuter, keyOuter) {
                    document.getElementById('mySelect_' +recordInner+'_'+monthIndex.replace(" - ","_")+ '_' + (keyOuter)).style.opacity = '0';
                    document.getElementById('mySelect1_' +recordInner+'_'+monthIndex.replace(" - ","_")+ '_' + (keyOuter)).style.opacity = '0';
                });
            });
            document.getElementById('mySelect_'+companyIndex+'_'+monthIndex.replace(" - ","_")+ '_' + (index)).style.opacity = '1';
            document.getElementById('mySelect1_' +companyIndex+'_'+monthIndex.replace(" - ","_")+ '_' + (index)).style.opacity = '1';
            
        }

        $scope.gotoProductOrder = function() {       
            $location.path('app/productOrder/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $scope.ProductStage + '/' + $stateParams.order + '/All/' + new Date());
        }
        $scope.replaceMonthYear = function(mY) {       
            return mY.replace(" - ","_");
        }
        $scope.SaveRetailSales = function() {
            var saleList = [];
           // $log.debug('Month==' + $rootScope.Month.Name.getMonth() + 'Year==' + $rootScope.Month.Name.getFullYear())
           
           angular.forEach($scope.companyMapList, function(recordOuter, keyOuter) {
               angular.forEach($scope.stocDetailsPickValues, function(recordInner, keyInner) {
               if($rootScope.retailSalesList[recordOuter]!=undefined && $rootScope.retailSalesList[recordOuter][recordInner]!=undefined)
                   angular.forEach($rootScope.retailSalesList[recordOuter][recordInner], function(record, key) {
                        if (record.Sales__c != undefined) {
                            var monthYear=recordInner.split(" - ");
                            record.Month__c = monthYear[0];
                            record.Year__c = monthYear[1];
                            record.Account_Id__c = $stateParams.AccountId;
                            record.myOrder=undefined;
                            saleList.push(record);
                        }
                    });
                });
            });
            if (saleList.length > 0) {
                MOBILEDATABASE_ADD('Visit_Summary__c', saleList, 'External_Id__c');
            }
            var transaction = [{
                Account: $stateParams.AccountId,
                stage: $stateParams.order,
                stageValue: 'Retail',
                Visit: $stateParams.visitId,
                status: 'saved',
                entryDate: today
            }];
            MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
            MOBILEDATABASE_ADD('Db_breadCrum', [{
                VisitedOrder: $stateParams.order,
                Name: 'Retail'
            }], 'VisitedOrder');
            $location.path('app/stockDetails/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + (parseInt($stateParams.order)+1) + '/' + new Date());
        }
        $scope.calculateTotalSales();
        $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('RetailCtrlError', err.stack + '::' + err.message);
    }
}).controller('productOrderCtrl', function($scope, $stateParams, $ionicSlideBoxDelegate, $cordovaCamera, $rootScope, $window, $state, $location, $q, $ionicModal, $timeout, $interval, $ionicLoading, $log, $filter, SalesforceSession, FETCH_DATA_LOCAL, SOUP_EXISTS, FETCH_DAILY_PLAN_ACCOUNTS, MOBILEDATABASE_ADD, FETCH_DATA, WEBSERVICE_ERROR) {
    try {
        var accountId = $stateParams.AccountId;
        var visitId = $stateParams.visitId;
        var stage = $stateParams.stage;
        var companyName = $stateParams.company;

        $rootScope.backText = 'Back';
        $rootScope.params = $stateParams;
        $rootScope.name = "productOrderCtrl";
        var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
        if ($rootScope.Outlets.length == 0) {
            $rootScope.Outlets = FETCH_DAILY_PLAN_ACCOUNTS($rootScope.day);
        }
        var currentAccountLocal = FETCH_DATA.querySoup('Account', 'Id', $stateParams.AccountId);
        if (currentAccountLocal != undefined && currentAccountLocal.currentPageOrderedEntries != undefined && currentAccountLocal.currentPageOrderedEntries.length > 0) {
            $scope.currentOutlet = currentAccountLocal.currentPageOrderedEntries[0];
            $scope.currentCustomer = currentAccountLocal.currentPageOrderedEntries[0];;
        }
        var rtList1 = FETCH_DATA_LOCAL('DB_RecordTypes', sfSmartstore.buildSmartQuerySpec("SELECT  {DB_RecordTypes:_soup} FROM {DB_RecordTypes} WHERE  {DB_RecordTypes:SobjectType}='Brands_and_Products__c'", 50));
        var ProductsRtList1 = [];
        angular.forEach(rtList1.currentPageOrderedEntries, function(record, key) {
            ProductsRtList1[record[0].Name] = record[0].Id;
        });
        var stateRecord1 = FETCH_DATA_LOCAL('Brands_and_Products__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Brands_and_Products__c:_soup} FROM {Brands_and_Products__c} WHERE {Brands_and_Products__c:RecordTypeId}='" + ProductsRtList1['State'] + "'", 1));
        // $log.debug(JSON.stringify(stateRecord1));
        // var stateActiveProducts1 = FETCH_DATA_LOCAL('Brands_and_Products__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Brands_and_Products__c:_soup} FROM {Brands_and_Products__c} WHERE  {Brands_and_Products__c:Related_to_State__c}='" + stateRecord1.currentPageOrderedEntries[0][0].Id + "' AND {Brands_and_Products__c:RecordTypeId}='" + ProductsRtList1['Product'] + "' and {Brands_and_Products__c:Company__c}='"+$stateParams.company+"' and {Brands_and_Products__c:Used_in_Chatai__c}='false'", 500));
        if(companyName=='All')
            var stateActiveProducts1 = FETCH_DATA_LOCAL('Brands_and_Products__c', sfSmartstore.buildSmartQuerySpec('SELECT  {Brands_and_Products__c:_soup} FROM {Brands_and_Products__c} WHERE  {Brands_and_Products__c:Related_to_State__c}="' + stateRecord1.currentPageOrderedEntries[0][0].Id + '" AND {Brands_and_Products__c:RecordTypeId}="' + ProductsRtList1['Product'] + '" and {Brands_and_Products__c:Used_in_Chatai__c}="false" and {Brands_and_Products__c:Removed__c}="false"', 100000));
        else
            var stateActiveProducts1 = FETCH_DATA_LOCAL('Brands_and_Products__c', sfSmartstore.buildSmartQuerySpec('SELECT  {Brands_and_Products__c:_soup} FROM {Brands_and_Products__c} WHERE  {Brands_and_Products__c:Related_to_State__c}="' + stateRecord1.currentPageOrderedEntries[0][0].Id + '" AND {Brands_and_Products__c:RecordTypeId}="' + ProductsRtList1['Product'] + '" and {Brands_and_Products__c:Company__c}="'+$stateParams.company+'" and {Brands_and_Products__c:Used_in_Chatai__c}="false" and {Brands_and_Products__c:Removed__c}="false"', 100000));
        var skuIds = [];
        angular.forEach(stateActiveProducts1.currentPageOrderedEntries, function(sku, key){
            skuIds[sku[0].Id] = sku[0];
        });
        var outletProducts = FETCH_DATA_LOCAL('Product_Assignment__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Product_Assignment__c:_soup} FROM {Product_Assignment__c} WHERE  {Product_Assignment__c:Outlet_Id__c}='" + accountId + "' order by {Product_Assignment__c:Order__c} ", 100000));
        $scope.Pa = stateActiveProducts1.currentPageOrderedEntries;
        var outletProductsRecords =  [];
        angular.forEach(outletProducts.currentPageOrderedEntries, function(product, key){
            if(skuIds[product[0].Product_Id__c]!=undefined){
                outletProductsRecords.push(product);
            }
        }); 

        var z = [];
        var oz = [];
        var checkList = [];
        angular.forEach(outletProductsRecords, function(paaO, keyO) {
            if (paaO[0]['Added__c'] == 'true') checkList.push(paaO[0]['Product_Id__c']);
        });
        //$log.debug('checkList' + JSON.stringify(checkList));
        var zkey = 0;
        var ozkey = 0;
        angular.forEach($scope.Pa, function(paa, key) {
            if (checkList.indexOf(paa[0]['Id']) == -1) {
                z[zkey] = paa[0];
                z[zkey].class = '';
                angular.forEach(outletProductsRecords, function(paaO, keyO) {
                    if (paa[0]['Id'] == paaO[0]['Product_Id__c']) {
                        z[zkey].proOrder = paaO[0]['Order__c'];
                    }
                });
                zkey++;
            } else {
                angular.forEach(outletProductsRecords, function(paaO, keyO) {
                    if (paa[0]['Id'] == paaO[0]['Product_Id__c']) {
                        var toz=paa[0];
                        toz.class='';
                        toz.proOrder = paaO[0]['Order__c'];
                        oz.push(toz);
                        // oz[paaO[0]['Order__c']] = paa[0];
                        // oz[paaO[0]['Order__c']].class = '';
                        // //alert(paaO[0]['Order__c']);
                        // oz[paaO[0]['Order__c']].proOrder = paaO[0]['Order__c'];
                    }
                });
                //ozkey++;
            }
        });
        $scope.Pa = z;
        /*$scope.Pa= [
          { title: 'Reggae', id: 1,class:'' },
          { title: 'Chill', id: 2 ,class:''},
          { title: 'Dubstep', id: 3 ,class:''},
          { title: 'Indie', id: 4,class:'' },
          { title: 'Rap', id: 5 ,class:''},
          { title: 'Cowbell', id: 6,class:'' }
        ];*/
        $scope.Pb = oz;
        $scope.class = '';
        $scope.changeClass = function(ind) {
            if ($scope.Pa[ind].class === '') {
                $scope.Pa[ind].class = 'selected';
            } else {
                $scope.Pa[ind].class = '';
            }
        }
        $scope.changeClass2 = function(ind) {
            if ($scope.Pb[ind].class === '') {
                $scope.Pb[ind].class = 'selected';
            } else {
                $scope.Pb[ind].class = '';
            }
        }
        var values_1 = $scope.Pa;
        $scope.moveToNextBlock = function() {
            angular.forEach($scope.Pa, function(paa, key) {
                if (paa.class == 'selected') {
                    $scope.Pb.push(paa);
                }
            });
            var x = [];
            angular.forEach($scope.Pa, function(paa, key) {
                if (paa.class == 'selected') {
                    paa.class = '';
                } else {
                    x.push(paa);
                }
            });
            $scope.Pa = x;
        }
        $scope.moveToPrevBlock = function() {
            angular.forEach($scope.Pb, function(paa, key) {
                if (paa.class == 'selected') {
                    $scope.Pa.push(paa);
                }
            });
            var x = [];
            angular.forEach($scope.Pb, function(paa, key) {
                if (paa.class == 'selected') {
                    paa.class = '';
                } else {
                    x.push(paa);
                }
            });
            $scope.Pb = x;
        }
        $scope.moveToUpBlock = function() {
            var updateArray = [];
            var storeKey_1 = false;
            var storeKey_2 = false;
            angular.forEach($scope.Pb, function(value, key) {
                if ((key == 0 && value.class == 'selected')) {
                    storeKey_1 = true;
                } else if (storeKey_1 && (key == 1 && value.class == 'selected')) {
                    storeKey_2 = true;
                }
            });
            angular.forEach($scope.Pb, function(value, key) {
                if (value.class == 'selected' && key != 0) {
                    var temp = [];
                    if (!storeKey_2) {
                        value.class = 'changed1';
                        $scope.Pb[key - 1].class = 'changed';
                        updateArray[key - 1] = value;
                        temp[0] = $scope.Pb[key - 1];
                        updateArray[key] = $scope.Pb[key - 1];
                        $scope.Pb[key] = temp[0];
                    }
                }
            });
            angular.forEach($scope.Pb, function(value, key) {
                if (value.class != 'changed' && value.class != 'changed1') {
                    updateArray[key] = $scope.Pb[key]
                } else {}
            });
            $scope.Pb = updateArray;
            angular.forEach($scope.Pb, function(value, key) {
                if (value.class == 'changed1') value.class = 'selected';
                else if (value.class == 'changed') value.class = '';
            });
        }
        $scope.moveToDownBlock = function() {
            var updateArray = [];
            var storeKey_1 = false;
            var storeKey_2 = false;
            var latestArray = $scope.Pb.reverse();
            angular.forEach(latestArray, function(value, key) {
                if ((key == 0 && value.class == 'selected')) {
                    storeKey_1 = true;
                } else if (storeKey_1 && (key == 1 && value.class == 'selected')) {
                    storeKey_2 = true;
                }
            });
            angular.forEach(latestArray, function(value, key) {
                if (value.class == 'selected' && key != 0) {
                    var temp = [];
                    if (!storeKey_2) {
                        value.class = 'changed1';
                        $scope.Pb[key - 1].class = 'changed';
                        updateArray[key - 1] = value;
                        temp[0] = $scope.Pb[key - 1];
                        updateArray[key] = $scope.Pb[key - 1];
                        $scope.Pb[key] = temp[0];
                    }
                }
            });
            angular.forEach($scope.Pb, function(value, key) {
                if (value.class != 'changed' && value.class != 'changed1') {
                    updateArray[key] = $scope.Pb[key]
                } else {}
            });
            $scope.Pb = updateArray.reverse();
            angular.forEach($scope.Pb, function(value, key) {
                if (value.class == 'changed1') value.class = 'selected';
                else if (value.class == 'changed') value.class = '';
            });
        }
        $scope.saveProductOrder = function() {
            $timeout(function() {
                //$scope.Pb
                var proOrder = [];
                angular.forEach($scope.Pb, function(value, key) {
                    var ProOrd = {
                        Id: (accountId + value['Id']),
                        Outlet_Id__c: accountId,
                        Product_Id__c: value['Id'],
                        Order__c: key + '',
                        Added__c: 'true',
                        Product_Name__c: value['Sku_Code__c']
                    };
                    proOrder.push(ProOrd);
                });
                angular.forEach($scope.Pa, function(value, key) {
                    var ProOrd = {
                        Id: (accountId + value['Id']),
                        Outlet_Id__c: accountId,
                        Product_Id__c: value['Id'],
                        Order__c: key + '',
                        Added__c: 'false',
                        Product_Name__c: value['Sku_Code__c']
                    };
                    proOrder.push(ProOrd);
                    angular.forEach($rootScope.StockAndSalesList, function(svalue, skey) {
                        if (value['Sku_Code__c'] == svalue['ProductName']) $rootScope.StockAndSalesList.splice(skey, 1);
                    });
                });
                //$log.debug('checkout Product_Assignment__c' + JSON.stringify($scope.proOrder));
                MOBILEDATABASE_ADD('Product_Assignment__c', proOrder, 'Id');
                //$ionicLoading.hide();
                if(companyName=='All')
                    $location.path('app/retailSales/' + accountId + '/' + visitId + '/' + $stateParams.order + '/' + new Date());
                else
                    $rootScope.breadcrumClicked(accountId, visitId, parseInt($stateParams.order)-1, $stateParams.order, 'fromCtrl', companyName);
               // $location.path('app/stocks/' + accountId + '/' + visitId + '/' + $stateParams.order + '/' + new Date());
            }, 100);
        }
        $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('productOrderCtrlError', err.stack + '::' + err.message);
    }
}).controller('MapCtrl', function($scope, $stateParams, $rootScope, $q, $ionicModal, $timeout, $interval, $ionicLoading, $log, $filter, SalesforceSession, FETCH_DATA_LOCAL, SOUP_EXISTS, FETCH_DAILY_PLAN_ACCOUNTS, WEBSERVICE_ERROR, GEO_LOCATION) {
    try {
        $ionicLoading.show({
            template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/>'
        });
        $rootScope.params = $stateParams;
        $rootScope.name = "MapCtrl";
        $scope.mapTitle = '';
        if (navigator && navigator.connection && navigator.connection.type != 'none') {
            $scope.online = true;
        } else {
            $scope.online = false;
        }
        if ($scope.online == true) {
            var TodayDate = new Date();
            $rootScope.backText = 'Back';
            $rootScope.showBack = true;
            $rootScope.showLogo = false;
            $scope.outletList = $rootScope.Outlets;
            if ($rootScope.day == 'tomorrow') {
                TodayDate.setDate(TodayDate.getDate() + 1);
            } else if ($rootScope.day == 'dayAfter') {
                TodayDate.setDate(TodayDate.getDate() + 2);
            }
            $scope.mapTitle = 'Route Plan ' + $filter('date')(TodayDate, "EEE, dd MMM yy");
            if ($scope.outletList == undefined || $scope.outletList.length == 0) {
                $scope.outletList = FETCH_DAILY_PLAN_ACCOUNTS($rootScope.day);
            }
            $scope.markers = [];
            //$log.debug('param'+JSON.stringify($stateParams));
            $scope.map = {
                show: true,
                center: {
                    latitude: 20.593684,
                    longitude: 78.962880
                },
                options: {
                    streetViewControl: false,
                    panControl: false
                },
                zoom: 14,
                dragging: false,
                bounds: {},
                markers: []
            };
            $scope.stroke = {
                color: "#ff0000",
                weight: 3
            };
            $scope.visible = true;
            GEO_LOCATION.getCurrentPosition().then(function(position) {
                $scope.map.center.latitude = position.latitude;
                $scope.map.center.longitude = position.longitude;
                $scope.map.zoom = 10;
                var marker = {
                    latitude: $scope.map.center.latitude,
                    longitude: $scope.map.center.longitude,
                    id: 12323,
                    icon: "./img/gpsloc.png",
                    showWindow: false,
                    message: ' This is your current location ',
                    options: {
                        animation: 0,
                        labelContent: '',
                        labelAnchor: "22 0",
                        labelClass: "marker-labels"
                    }
                };
                $scope.map.markers.push(marker);
            }, function(error) {
                $log.debug("error=====" + JSON.stringify(error));
            });
            $scope.path = [];
            //  $log.debug('accountid=== before'+$stateParams.accountId);
            if ($stateParams.accountId == undefined || $stateParams.accountId == '') {
                var i = 0;
                angular.forEach($scope.outletList, function(record, key) {
                    if (record.Location__Latitude__s != undefined && record.Location__Longitude__s != undefined) {
                        var marker = {
                            latitude: record.Location__Latitude__s,
                            longitude: record.Location__Longitude__s,
                            id: record.Id,
                            icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' + record.RoutePlan.Order__c + '|FE6256|000000',
                            showWindow: false,
                            message: ' ' + record.Name + '  ',
                            options: {
                                animation: 0,
                                labelContent: '',
                                labelAnchor: "22 0",
                                labelClass: "marker-labels"
                            }
                        };
                        $scope.map.markers.push(marker);
                    }
                });
            } else {
                var i = 0;
                //   $log.debug('accountid===after'+$stateParams.accountId);
                angular.forEach($scope.outletList, function(record, key) {
                    if (record.Id === $stateParams.accountId) {
                        $scope.mapTitle = record.Name;
                        if (record.Location__Latitude__s != undefined && record.Location__Longitude__s != undefined) {
                            // $log.debug('i=='+i);
                            var marker = {
                                latitude: record.Location__Latitude__s,
                                longitude: record.Location__Longitude__s,
                                id: record.Id,
                                icon: 'http://maps.google.com/mapfiles/kml/paddle/' + record.RoutePlan.Order__c + '.png',
                                showWindow: false,
                                message: ' ' + record.Name + '  ',
                                options: {
                                    animation: 0,
                                    labelContent: '',
                                    labelAnchor: "22 0",
                                    labelClass: "marker-labels"
                                }
                            };
                            $scope.map.markers.push(marker);
                        }
                    }
                    // i++;
                });
            }
            $timeout(function() {
                if ($scope.map.markers.length <= 1) {
                    Splash.ShowToast('Outlets have not yet been tagged.', 'long', 'bottom', function(a) {
                        console.log(a)
                    });
                }
            }, 5000);
        }
        $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('MapCtrlError', err.stack + '::' + err.message);
    }
}).controller('Container4Ctrl', function($q, $log, $ionicLoading, $scope, $rootScope, $filter, $stateParams, $location, $timeout, SMARTSTORE, MOBILEDATABASE_ADD) {
    try{
    $rootScope.backText = 'Back';
    $rootScope.showBack = true;
    $rootScope.params = $stateParams;
    $rootScope.name = "Container4Ctrl";
    $scope.AccountId = $stateParams.AccountId;
    $scope.visitId = $stateParams.visitId;
    $scope.order = $stateParams.order;
    var today = $filter('date')(new Date(), 'dd/MM/yyyy');
    var transaction = [{
        Account: $stateParams.AccountId,
        stage: $stateParams.order,
        stageValue: 'Container4Ctrl',
        Visit: $stateParams.visitId,
        status: 'pending',
        entryDate: today
    }];
    MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
    MOBILEDATABASE_ADD('Db_breadCrum', [{
        VisitedOrder: $stateParams.order,
        Name: 'PRIDECall4Ctrl'
    }], 'VisitedOrder');
    $scope.outlet = SMARTSTORE.buildExactQuerySpec('Account', 'Id', $stateParams.AccountId);
    var visits = SMARTSTORE.buildSmartQuerySpec("SELECT {Visit__c:_soup} FROM {Visit__c} WHERE {Visit__c:Account__c}='" + $stateParams.AccountId + "' ORDER BY {Visit__c:Check_In_DateTime__c} DESC", 3);
    var pageId = $rootScope.processFlowLineItems[$stateParams.order].Page__c;
    $scope.breadActive = $rootScope.processFlowLineItems[$stateParams.order].Name;
    $scope.currentPage = {};
    angular.forEach($rootScope.pages, function(record, key) {
        if (record.pageDescription.Id == pageId) {
            $scope.currentPage = record;
        }
    });
    $scope.listViewFields = [];
    if ($scope.currentPage.sections[0] != undefined && $scope.currentPage.sections[0].fields != undefined) {
        $scope.listViewFields = $scope.currentPage.sections[0].fields;
    }
    var query = "";
    for (var i = 0; i < visits.length; i++) {
        if (visits[i][0].External_Id__c != undefined) query = query + " OR {" + $scope.currentPage.sections[0].sectionDescription.Input_Object_API__c + ":Visit__c} = '" + visits[i][0].External_Id__c + "'";
        if (visits[i][0].Id != undefined) query = query + " OR {" + $scope.currentPage.sections[0].sectionDescription.Input_Object_API__c + ":Visit__c} = '" + visits[i][0].Id + "'";
    }
    $scope.visitSummaries = [];
    var visitSummaryrecordsLocal = SMARTSTORE.buildSmartQuerySpec("SELECT {" + $scope.currentPage.sections[0].sectionDescription.Input_Object_API__c + ":_soup} FROM {" + $scope.currentPage.sections[0].sectionDescription.Input_Object_API__c + "} WHERE ({" + $scope.currentPage.sections[0].sectionDescription.Input_Object_API__c + ":Account_Id__c} = '" + $stateParams.AccountId + "' or {" + $scope.currentPage.sections[0].sectionDescription.Input_Object_API__c + ":AccountId__c} = '" + $stateParams.AccountId + "') AND {" + $scope.currentPage.sections[0].sectionDescription.Input_Object_API__c + ":RecordTypeId} = '" + $scope.currentPage.sections[0].sectionDescription.Record_Type_Id__c + "' AND  ({" + $scope.currentPage.sections[0].sectionDescription.Input_Object_API__c + ":Status__c} = 'Open'" + query + " )", 100);
    for (var i = 0; i < visitSummaryrecordsLocal.length; i++) {
        var record = visitSummaryrecordsLocal[i][0];
        if (record.Date__c != undefined) {
            var createdDate;
            if (record.Date__c.split('-')[0].length == 4) createdDate = new Date(record.Date__c);
            else {
                createdDate = new Date(record.Date__c.split('/')[1] + '/' + record.Date__c.split('/')[0] + '/' + record.Date__c.split('/')[2]);
            }
            record.Date__c = createdDate.getTime().toString();
        }
        $scope.visitSummaries.push(record);
    }
    $ionicLoading.hide();
    $scope.viewVisitSummary = function(index, visitSummaryId) {
        $timeout(function() {
            $location.path('app/container4View/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + visitSummaryId + '/' + index);
        }, 500);
    }
    $scope.addNewVisitSummary = function() {
        $timeout(function() {
            $location.path('app/container4New/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order);
        }, 500);
    }
    $timeout(function() {
        $scope.scrollHeight = jQuery('.lastComplaints_feedback > .scroll').height();
    }, 20);
    $scope.goToNext = function() {
         //User log
        var today_now = $filter('date')(new Date(), 'dd/MM/yyyy hh:mm a');
        var userOprObj=[{
            ObjectData:$stateParams.order,
            time_stamp:today_now,
            comment:'Next clicked from Feedback'
        }];
        MOBILEDATABASE_ADD('Db_User_Logs', userOprObj, '_soupEntryId');

        MOBILEDATABASE_ADD('Db_breadCrum', [{
            VisitedOrder: $stateParams.order,
            Name: 'PRIDECall4Ctrl'
        }], 'VisitedOrder');
        if (!jQuery('.feedbackNext').hasClass('btn-disabled')) {
            $rootScope.breadcrumClicked($stateParams.AccountId, $stateParams.visitId, $stateParams.order, parseInt($stateParams.order) + 1, 'fromCtrl');
        } else {
            Splash.ShowToast('Please scroll for enabling next.', 'long', 'bottom', function(a) {
                console.log(a)
            });
        }
    }
    $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('Container4Ctrl', err.stack + '::' + err.message);
    }
}).controller('Container4NewCtrl', function($q, $log, $ionicLoading, $scope, $rootScope, $filter, $window, $stateParams, $location, $cordovaCamera, $ionicModal, $timeout, $ionicSlideBoxDelegate, SMARTSTORE, MOBILEDATABASE_ADD,WEBSERVICE_ERROR) {
    try{
    $rootScope.backText = 'Back';
    $rootScope.showBack = true;
    $rootScope.params = $stateParams;
    $rootScope.name = "Container4NewCtrl";
    $scope.outlet = SMARTSTORE.buildExactQuerySpec('Account', 'Id', $stateParams.AccountId);
    var visits = SMARTSTORE.buildSmartQuerySpec("SELECT {Visit__c:_soup} FROM {Visit__c} WHERE {Visit__c:Account__c}='" + $stateParams.AccountId + "' ORDER BY {Visit__c:Check_In_DateTime__c} DESC", 3);
    var pageId = $rootScope.processFlowLineItems[$stateParams.order].Page__c;
    $scope.breadActive = $rootScope.processFlowLineItems[$stateParams.order].Name;
    $scope.currentPage = {};
    angular.forEach($rootScope.pages, function(record, key) {
        if (record.pageDescription.Id == pageId) {
            $scope.currentPage = record;
        }
    });
    $scope.Fields = [];
    if ($scope.currentPage.sections[1] != undefined && $scope.currentPage.sections[1].fields != undefined) {
        $scope.Fields = $scope.currentPage.sections[1].fields;
    }
    $scope.visitSummary = {};
    $scope.isFieldVisible = function(x, y) {
        $log.debug(x + '' + y);
        if (x != undefined || y != undefined) {
            if (x == undefined || x.indexOf(y) == -1) {
                return true;
            }else{
                return false;
            }
        } else return false;
    }
    $scope.checkBelowFields = function(myOrder) {
        angular.forEach($scope.Fields, function(record, key) {
            if(record.Default_Value__c!=undefined && record.Default_Value__c!="" && !$scope.isFieldVisible(record.Dependent_Value__c,$scope.visitSummary[record.Dependent_Field__c]) && parseInt(record.Order__c)>parseInt(myOrder)){
                if(record.Default_Value__c=='-')
                    $scope.visitSummary[record.Field_API__c]='';
                else
                    $scope.visitSummary[record.Field_API__c]=record.Default_Value__c;
                
            }
        });
    }
    angular.forEach($scope.Fields, function(record, key) {
        if(record.Default_Value__c!=undefined && record.Default_Value__c!="" && !$scope.isFieldVisible(record.Dependent_Value__c,$scope.visitSummary[record.Dependent_Field__c]))
        $scope.visitSummary[record.Field_API__c]=record.Default_Value__c;
    });
    $scope.visitSummary.Date__c = new Date();

    $scope.visitSummaryImages = [];
    $ionicLoading.hide();
    var imagelist = [];
    $ionicModal.fromTemplateUrl('image-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal = modal;
    });
    $scope.openModal = function() {
        $ionicSlideBoxDelegate.slide(0);
        $scope.modal.show();
    };
    $scope.closeModal = function() {
        $scope.modal.hide();
    };
    // Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
        $scope.modal.remove();
    });
    // Execute action on hide modal
    $scope.$on('modal.hide', function() {
        // Execute action
    });
    // Execute action on remove modal
    $scope.$on('modal.removed', function() {
        // Execute action
    });
    $scope.$on('modal.shown', function() {
        console.log('Modal is shown!');
    });
    // Call this functions if you need to manually control the slides
    $scope.next = function() {
        $ionicSlideBoxDelegate.next();
    };
    $scope.previous = function() {
        $ionicSlideBoxDelegate.previous();
    };
    $scope.goToSlide = function(index) {
            $scope.modal.show();
            $timeout(function() {
                $ionicSlideBoxDelegate.slide(index);
            }, 500)
        }
        // Called each time the slide changes
    $scope.slideCurrentImageIndex = 0;
    $scope.slideChanged = function(index) {
        $scope.slideIndex = index;
        $scope.slideCurrentImageIndex = index;
    };
    $scope.currentVisitPrevSlide = function() {
        $ionicSlideBoxDelegate.previous();
    }
    $scope.currentVisitNextSlide = function() {
        $ionicSlideBoxDelegate.next();
    }
    $scope.takePicture = function() {
        var options = {
            quality: 75,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.CAMERA,
            allowEdit: true,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 700,
            targetHeight: 500,
            saveToPhotoAlbum: false
        };
        $cordovaCamera.getPicture(options).then(function(imageData) {
            imagelist.reverse();
            imagelist.push({
                Body: imageData,
                IsDirty: true
            });
            $scope.visitSummaryImages = [];
            var images = imagelist.reverse();
            for (var i = 0; i < 10; i++) {
                if (images[i] != undefined) {
                    images[i].Order = i;
                    $scope.visitSummaryImages.push(images[i]);
                }
            }
        }, function(err) {});
    }
    
    $scope.cancelVisitSummary = function() {
        $timeout(function() {
            $location.path('app/container4/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date());
        }, 500);
    }
    $scope.dateValidation = function(fieldAPI) {
        if (fieldAPI == 'Date__c' && $scope.visitSummary.Date__c != undefined && $scope.visitSummary.Date__c > new Date()) {
            Splash.ShowToast('Feedback logged date cannot be in the future.', 'long', 'bottom', function(a) {
                console.log(a)
            });
            $scope.visitSummary.Date__c = undefined;
        }
    }
    $scope.clearSubCategory = function(fieldAPI) {
        if (fieldAPI == 'Type__c') {
            $scope.visitSummary.Category__c = undefined;
            $scope.visitSummary.Sub_category__c = undefined;
        }
        if (fieldAPI == 'Category__c') {
            $scope.visitSummary.Sub_category__c = undefined;
        }
    }
    $scope.showAdd = false;
    $scope.checkValues = function() {
        $scope.showAdd = true;
        angular.forEach($scope.Fields, function(field, key) {
            if(($scope.visitSummary[field.Field_API__c]==undefined || $scope.visitSummary[field.Field_API__c]=='') && field.Type__c!='number' && field.Type__c!='text' && field.Type__c!='textarea' && field.Field_API__c!='Sub_category__c' && field.Field_API__c!='Pack_Size__c' && field.Type__c!='date' && field.Field_API__c!='Brewery__c'){
                $scope.showAdd=false;
            }
        });  
    }
    $scope.saveVisitSummary = function() {
        $timeout(function() {
            if (!jQuery.isEmptyObject($scope.visitSummary) && $scope.showAdd==true) {
                var countOfFilledFields = 0;
                for (var i = 0; i < $scope.Fields.length; i++) {
                    if ($scope.visitSummary[$scope.Fields[i].Field_API__c] != undefined) {
                        countOfFilledFields++;
                    }
                }
                if (countOfFilledFields > 1) {
                    $scope.visitSummary.External_Id__c = (new Date().getTime())+'-'+$stateParams.visitId+'-'+$window.Math.random() * 100000000;
                    $scope.visitSummary.External_Id__c = $scope.visitSummary.External_Id__c.toString();
                    $scope.visitSummary.Visit__c = $stateParams.visitId;
                    $scope.visitSummary.Account_Id__c = $stateParams.AccountId;
                    $scope.visitSummary.RecordTypeId = $scope.currentPage.sections[0].sectionDescription.Record_Type_Id__c;
                    $scope.visitSummary.IsDirty = true;
                    if ($scope.visitSummary.Date__c != undefined) {
                        $scope.visitSummary.Date__c = $filter('date')($scope.visitSummary.Date__c, 'dd/MM/yyyy');
                    } else {
                        $scope.visitSummary.Date__c = $filter('date')(new Date(), 'dd/MM/yyyy');
                    }
                    if ($scope.visitSummary.Date_of_Manufacture__c != undefined) {
                        $scope.visitSummary.Date_of_Manufacture__c = $filter('date')($scope.visitSummary.Date_of_Manufacture__c, 'dd/MM/yyyy');
                    }else {
                        $scope.visitSummary.Date_of_Manufacture__c = $filter('date')(new Date(), 'dd/MM/yyyy');
                    }
                    $scope.visitSummary.Status__c = 'Open';
                    var visitSummaryrecords = [];
                    visitSummaryrecords.push($scope.visitSummary);
                    MOBILEDATABASE_ADD($scope.currentPage.sections[0].sectionDescription.Input_Object_API__c, visitSummaryrecords, 'External_Id__c');
                    var attachments = [];
                    angular.forEach($scope.visitSummaryImages, function(record, key) {
                        attachments.push({
                            ParentId: $scope.visitSummary['External_Id__c'],
                            Name: 'image' + key,
                            Body: record.Body,
                            IsDirty: true,
                            Order: record.Order,
                            Visit: $scope.visitSummary['Visit__c'],
                            External_Id__c: $scope.visitSummary['External_Id__c'] + '--' + (new Date().getTime())+'_'+$window.Math.random() * 100000000
                        });
                    });
                    if (attachments.length > 0) {
                        MOBILEDATABASE_ADD('Db_images', attachments, 'External_Id__c');
                    }
                    $location.path('app/container4/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date());
                } else {
                    Splash.ShowToast('Please fill all the fields value.', 'long', 'bottom', function(a) {
                        console.log(a)
                    });
                    $ionicLoading.hide();
                }

            } else {
                Splash.ShowToast('Please fill all the fields value.', 'long', 'bottom', function(a) {
                    console.log(a)
                });
                $ionicLoading.hide();
            }
        }, 500);
    }
    $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('Container4NewCtrl', err.stack + '::' + err.message);
    }
}).controller('Container4ViewCtrl', function($q, $log, $ionicLoading, $scope, $rootScope, $window, $stateParams, $location, $filter, $cordovaCamera, $ionicModal, $ionicSlideBoxDelegate, $timeout, SMARTSTORE, MOBILEDATABASE_ADD,WEBSERVICE_ERROR) {
    try{
    $rootScope.backText = 'Back';
    $rootScope.showBack = true;
    $rootScope.params = $stateParams;
    $rootScope.name = "Container4ViewCtrl";
    $scope.indexValue = parseInt($stateParams.indexValue) + 1;
    $scope.outlet = SMARTSTORE.buildExactQuerySpec('Account', 'Id', $stateParams.AccountId);
    var visits = SMARTSTORE.buildExactQuerySpec('Visit__c', 'External_Id__c', $stateParams.visitId);
    var pageId = $rootScope.processFlowLineItems[$stateParams.order].Page__c;
    $scope.breadActive = $rootScope.processFlowLineItems[$stateParams.order].Name;
    $scope.currentPage = {};
    angular.forEach($rootScope.pages, function(record, key) {
        if (record.pageDescription.Id == pageId) {
            $scope.currentPage = record;
        }
    });
    $scope.Fields = [];
    if ($scope.currentPage.sections[2] != undefined && $scope.currentPage.sections[2].fields != undefined) {
        $scope.Fields = $scope.currentPage.sections[2].fields;
    }
    $scope.visitSummaryImages = [];
    $scope.currentvisitSummaryImages = [];
    $scope.visitSummary = SMARTSTORE.buildExactQuerySpec($scope.currentPage.sections[0].sectionDescription.Input_Object_API__c, 'External_Id__c', $stateParams.visitSummaryId);
    var loggedDate;
    var mfgDate;
    if ($scope.visitSummary[0] != undefined) {
        if ($scope.visitSummary[0].Date__c != undefined) {
            if ($scope.visitSummary[0].Date__c.split('-')[0].length == 4) loggedDate = new Date($scope.visitSummary[0].Date__c);
            else {
                loggedDate = new Date($scope.visitSummary[0].Date__c.split('/')[1] + '/' + $scope.visitSummary[0].Date__c.split('/')[0] + '/' + $scope.visitSummary[0].Date__c.split('/')[2]);
            }
            if($scope.visitSummary[0].Date_of_Manufacture__c!=undefined && $scope.visitSummary[0].Date_of_Manufacture__c!=null && $scope.visitSummary[0].Date_of_Manufacture__c!=''){
                if ($scope.visitSummary[0].Date_of_Manufacture__c.split('-')[0].length == 4) mfgDate = new Date($scope.visitSummary[0].Date_of_Manufacture__c);
                else {
                    mfgDate = new Date($scope.visitSummary[0].Date_of_Manufacture__c.split('/')[1] + '/' + $scope.visitSummary[0].Date_of_Manufacture__c.split('/')[0] + '/' + $scope.visitSummary[0].Date_of_Manufacture__c.split('/')[2]);
                }
            }
            $scope.visitSummary[0].Date__c = $filter('date')(loggedDate, 'dd MMM yyyy');
        }
        if ($scope.visitSummary[0].Resolution_Date__c != undefined) {
            var createdDate;
            if ($scope.visitSummary[0].Resolution_Date__c.split('-')[0].length == 4) createdDate = new Date($scope.visitSummary[0].Resolution_Date__c);
            else {
                createdDate = new Date($scope.visitSummary[0].Resolution_Date__c.split('/')[1] + '/' + $scope.visitSummary[0].Resolution_Date__c.split('/')[0] + '/' + $scope.visitSummary[0].Resolution_Date__c.split('/')[2]);
            }
            if ($scope.visitSummary[0].Status__c == 'Resolved') {
                $scope.visitSummary[0].Resolution_Date__c = $filter('date')(createdDate, 'dd MMM yyyy');
            }
        }
        if ($scope.visitSummary[0].Id != undefined) {
            $scope.visitSummaryImages = SMARTSTORE.buildExactQuerySpec('Db_images', 'ParentId', $scope.visitSummary[0].Id);
        }
        angular.forEach(SMARTSTORE.buildExactQuerySpec('Db_images', 'ParentId', $scope.visitSummary[0].External_Id__c), function(record, key) {
            if (record.Visit != undefined && record.Visit == $stateParams.visitId) {
                $scope.currentvisitSummaryImages.push(record);
            } else {
                $scope.visitSummaryImages.push(record);
            }
        });
    }
    $ionicLoading.hide();
    $scope.isFieldVisible = function(x, y) {
        if (x != undefined && y != undefined) return x.indexOf(y) == -1;
        else return false;
    }
    $scope.cancelViewVisitSummary = function() {
        $timeout(function() {
            $location.path('app/container4/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date());
        }, 500);
    }
    var currentVisitImagesSlider = $ionicSlideBoxDelegate.$getByHandle('currentImagesSlider');
    var lastVisitImagesSlider = $ionicSlideBoxDelegate.$getByHandle('lastImagesSlider');
    $ionicModal.fromTemplateUrl('current-image-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.currentImagemodal = modal;
    });
    $scope.openCurrentImageModal = function() {
        currentVisitImagesSlider.slide(0);
        $scope.currentImagemodal.show();
    };
    $scope.closeCurrentImageModal = function() {
        $scope.currentImagemodal.hide();
    };
    $scope.goToSlideCurrentImageModel = function(index) {
            $scope.currentImagemodal.show();
            $timeout(function() {
                currentVisitImagesSlider.slide(index);
            }, 500);
        }
        // Called each time the slide changes
    $scope.slideCurrentImageIndex = 0;
    $scope.slideLastVisitImageIndex = 0;
    $scope.slideChangedCurrentImageModel = function(index) {
        $scope.slideIndex1 = index;
        $scope.slideCurrentImageIndex = index;
    };
    $ionicModal.fromTemplateUrl('image-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal = modal;
    });
    $scope.openModal = function() {
        lastVisitImagesSlider.slide(0);
        $scope.modal.show();
    };
    $scope.closeModal = function() {
        $scope.modal.hide();
    };
    $scope.goToSlide = function(index) {
            $scope.modal.show();
            $timeout(function() {
                lastVisitImagesSlider.slide(index);
            }, 500);
        }
        // Called each time the slide changes
    $scope.slideChanged = function(index) {
        $scope.slideIndex = index;
        $scope.slideLastVisitImageIndex = index;
    };
    $scope.lastVisitPrevSlide = function() {
        lastVisitImagesSlider.previous();
    }
    $scope.lastVisitNextSlide = function() {
        lastVisitImagesSlider.next();
    }
    $scope.currentVisitPrevSlide = function() {
        currentVisitImagesSlider.previous();
    }
    $scope.currentVisitNextSlide = function() {
            currentVisitImagesSlider.next();
        }
        // $scope.uploadslist = $filter('orderBy')($scope.currentvisitSummaryImages, 'Order',true);
    var imagelist = [];
    $scope.currentvisitSummaryImages = $filter('orderBy')($scope.currentvisitSummaryImages, '-Order', true);
    imagelist = $scope.currentvisitSummaryImages;
    //imagelist.reverse();
    $scope.takePicture = function() {
        if ($scope.visitSummary[0].Status__c != 'Resolved') {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.CAMERA,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 700,
                targetHeight: 500,
                saveToPhotoAlbum: false
            };
            $cordovaCamera.getPicture(options).then(function(imageData) {
                imagelist.reverse();
                imagelist.push({
                    Body: imageData,
                    IsDirty: true
                });
                $scope.currentvisitSummaryImages = [];
                var images = imagelist.reverse();
                for (var i = 0; i < 10; i++) {
                    if (images[i] != undefined) {
                        images[i].Order = i;
                        $scope.currentvisitSummaryImages.push(images[i]);
                    }
                }
            }, function(err) {});
        } else {
            Splash.ShowToast('You can not take the pictures of already resolved Feedback.', 'long', 'bottom', function(a) {
                console.log(a)
            });
        }
    }
    $scope.dateValidation = function(fieldAPI) {
        if (fieldAPI == 'Resolution_Date__c' && $scope.visitSummary[0].Resolution_Date__c != undefined && $scope.visitSummary[0].Resolution_Date__c > new Date()) {
            Splash.ShowToast('Resolution date cannot be in the future.', 'long', 'bottom', function(a) {
                console.log(a)
            });
            $scope.visitSummary[0].Resolution_Date__c = undefined;
        }
    }
    $scope.saveEditVisitSummary = function() {
        $timeout(function() {
            if ($scope.visitSummary[0].Is_it_resolved__c != undefined) {
                $scope.visitSummary[0].IsDirty = true;
                if ($scope.visitSummary[0].Is_it_resolved__c == 'Yes') {
                    $scope.visitSummary[0].Status__c = 'Resolved';
                    $scope.visitSummary[0].Opened_Visit__c = $scope.visitSummary[0].Visit__c;
                    $scope.visitSummary[0].Visit__c = $stateParams.visitId;
                    if ($scope.visitSummary[0].Resolution_Date__c == undefined) {
                        $scope.visitSummary[0].Resolution_Date__c = $filter('date')(new Date(), 'dd/MM/yyyy');
                    } else {
                        $scope.visitSummary[0].Resolution_Date__c = $filter('date')($scope.visitSummary[0].Resolution_Date__c, 'dd/MM/yyyy');
                    }
                }
                $scope.visitSummary[0].Date__c = $filter('date')(loggedDate, 'dd/MM/yyyy');
                if(mfgDate!=undefined && mfgDate!=null && mfgDate!='')
                $scope.visitSummary[0].Date_of_Manufacture__c = $filter('date')(mfgDate, 'dd/MM/yyyy');
                
                MOBILEDATABASE_ADD($scope.currentPage.sections[0].sectionDescription.Input_Object_API__c, $scope.visitSummary, 'External_Id__c');
            
                var attachments = [];
                angular.forEach($scope.currentvisitSummaryImages, function(record, key) {
                    var attach = {
                        ParentId: $scope.visitSummary[0]['External_Id__c'],
                        Name: 'image' + key,
                        Body: record.Body,
                        IsDirty: true,
                        Order: record.Order,
                        Visit: $stateParams.visitId
                    };
                    if (record.External_Id__c == undefined) attach.External_Id__c = $scope.visitSummary[0]['External_Id__c'] + '--' + (new Date().getTime())+'_'+$window.Math.random() * 100000000;
                    else {
                        attach.External_Id__c = record.External_Id__c;
                    }
                    attachments.push(attach);
                });
                if (attachments.length > 0) {
                    MOBILEDATABASE_ADD('Db_images', attachments, 'External_Id__c');
                }
                $location.path('app/container4/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date());
                $ionicLoading.hide();
            }
            else{
                Splash.ShowToast('Please Specify, if it is resolved?', 'long', 'bottom', function(a) {
                    console.log(a)
                });
                $ionicLoading.hide();
            }
        }, 500);
    }
    $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('MapCtrlError', err.stack + '::' + err.message);
    }
}).controller('PRIDECall5Ctrl', function($q, $log, $ionicLoading, $scope, $rootScope, $window, $stateParams, $location, $filter, $cordovaCamera, $ionicModal, $ionicSlideBoxDelegate, $timeout, SMARTSTORE, MOBILEDATABASE_ADD, WEBSERVICE_ERROR) {
    try {
        $rootScope.params = $stateParams;
        $rootScope.name = "PRIDECall5Ctrl";
        $scope.order = $stateParams.order;
        var today = $filter('date')(new Date(), 'dd/MM/yyyy');
        var transaction = [{
            Account: $stateParams.AccountId,
            stage: $stateParams.order,
            stageValue: 'PRIDECall5Ctrl',
            Visit: $stateParams.visitId,
            status: 'pending',
            entryDate: today
        }];
        MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
        MOBILEDATABASE_ADD('Db_breadCrum', [{
            VisitedOrder: $stateParams.order,
            Name: 'PRIDECall5Ctrl'
        }], 'VisitedOrder');
        $scope.outlet = SMARTSTORE.buildExactQuerySpec('Account', 'Id', $stateParams.AccountId);
        $scope.breadActive = $rootScope.processFlowLineItems[$stateParams.order].Name;
        $scope.currentPage = {};
        angular.forEach($rootScope.pages, function(record, key) {
            if (record.pageDescription.Id == $rootScope.processFlowLineItems[$stateParams.order].Page__c) {
                $scope.currentPage = record;
            }
        });
        if ($scope.currentPage.sections.length > 1) {
            $scope.addNewSectionFields = $scope.currentPage.sections[1].fields;
        } else {
            $scope.addNewSectionFields = $scope.currentPage.sections[0].fields;
        }
        $scope.uploadslist = [];
        var queryCurrentImages = false;
        if ($rootScope.currentvisitSummaryImages[$stateParams.order] == undefined) {
            $rootScope.currentvisitSummaryImages[$stateParams.order] = [];
            queryCurrentImages = true;
        }
        //        if( $rootScope.uploadfields[$stateParams.order]==undefined){
        //                 $rootScope.uploadfields[$stateParams.order]=[];
        //        }
        $scope.visitSummaryImages = [];
        var uploads = SMARTSTORE.buildExactQuerySpec('Upload__c', 'Account__c', $stateParams.AccountId);
        var lastVisit = SMARTSTORE.buildSmartQuerySpec("SELECT {Visit__c:_soup} FROM {Visit__c} WHERE {Visit__c:Account__c}='" + $stateParams.AccountId + "' ORDER BY {Visit__c:Check_In_DateTime__c} DESC", 2);
        var d = new Date();
        var currentDate = d.getTime();
        var query = '';
        var uploadsBeforeOrder = [];
        var eDate = new Date();
        eDate.setDate(eDate.getDate() - 30);
        var sDate = new Date();
        sDate.setDate(sDate.getDate() - 14);
        var visitQuery = '';
        var uploadOldRecordWithExtIds = [];
        var uploadsNotShow = [];
        angular.forEach(uploads, function(record, key) {
            if (record.RecordTypeId == $scope.currentPage.sections[0].sectionDescription.Input_Record_Type_Id__c) {
                var endDate = new Date(record.End_Date__c);
                if (eDate < endDate && sDate >= endDate && (record.Status__c == undefined || record.Status__c.indexOf('Executed') == -1)) {
                    if (visitQuery == '') {
                        visitQuery = '{Visit_Summary__c:Upload__c}="' + record.Id + '" OR {Visit_Summary__c:Upload__c}="' + record.External_Id__c + '"';
                    } else {
                        visitQuery = visitQuery + ' OR {Visit_Summary__c:Upload__c}="' + record.Id + '" OR {Visit_Summary__c:Upload__c}="' + record.External_Id__c + '"';
                    }
                    uploadOldRecordWithExtIds.push(record.Id);
                }
            }
        });
        if (visitQuery != '') {
            var visitSummaryrecordsOld = SMARTSTORE.buildSmartQuerySpec('SELECT {Visit_Summary__c:_soup} FROM {Visit_Summary__c} WHERE ' + visitQuery, 200);
            var oldVisitIds = [];
            if (visitSummaryrecordsOld.length > 0) {
                var visitQueryForDate = ''
                var visitIds = [];
                angular.forEach(visitSummaryrecordsOld, function(record, key) {
                    if (record[0].Visit__c != undefined && visitIds.indexOf(record[0].Visit__c) == -1 && lastVisit[0][0].External_Id__c != record[0].Visit__c && lastVisit[0][0].Id != record[0].Visit__c) {
                        if (visitQueryForDate == '') {
                            visitQueryForDate = '{Visit__c:Id}="' + record[0].Visit__c + '" OR {Visit__c:External_Id__c}="' + record[0].Visit__c + '"';
                        } else {
                            visitQueryForDate = visitQueryForDate + ' OR {Visit__c:Id}="' + record[0].Visit__c + '" OR {Visit__c:External_Id__c}="' + record[0].Visit__c + '"';
                        }
                    }
                });
                if (visitQueryForDate != '') {
                    var visitrecordsOld = SMARTSTORE.buildSmartQuerySpec('SELECT {Visit__c:_soup} FROM {Visit__c} WHERE ' + visitQueryForDate, 200);
                    if (visitrecordsOld.length > 0) {
                        angular.forEach(visitrecordsOld, function(record, key) {
                            if (eDate.getTime < record[0].Check_In_DateTime__c && sDate >= record[0].Check_In_DateTime__c) {
                                if (record[0].Id != undefined) {
                                    oldVisitIds.push(record[0].Id);
                                }
                                oldVisitIds.push(record[0].External_Id__c);
                            }
                        });
                    }
                }
                angular.forEach(visitSummaryrecordsOld, function(record, key) {
                    if (oldVisitIds.indexOf(record[0].Visit__c) == -1 && lastVisit[0][0].External_Id__c != record[0].Visit__c && lastVisit[0][0].Id != record[0].Visit__c) {
                        uploadsNotShow.push(record[0].Upload__c);
                    }
                });
            }
        }
        angular.forEach(uploads, function(record, key) {
            if (record.RecordTypeId == $scope.currentPage.sections[0].sectionDescription.Input_Record_Type_Id__c && record.Start_Date__c != undefined && record.End_Date__c != undefined) {
                var startDate = new Date(record.Start_Date__c);
                startDate.setDate(startDate.getDate() - 14);
                startDate = startDate.getTime();
                var endDate = new Date(record.End_Date__c);
                endDate.setDate(endDate.getDate() + 14);
                endDate = endDate.getTime();
                if ((startDate <= currentDate && endDate >= currentDate) || (uploadOldRecordWithExtIds.indexOf(record.Id) != -1 && uploadsNotShow.indexOf(record.Id) == -1 && uploadsNotShow.indexOf(record.External_Id__c) == -1)) {
                    record.fields = $scope.currentPage.sections[0].fields;
                    if (query == '') {
                        query = '{Visit_Summary__c:Upload__c}="' + record.Id + '" OR {Visit_Summary__c:Upload__c}="' + record.External_Id__c + '"';
                    } else {
                        query = query + ' OR {Visit_Summary__c:Upload__c}="' + record.Id + '" OR {Visit_Summary__c:Upload__c}="' + record.External_Id__c + '"';
                    }
                    uploadsBeforeOrder.push(record);
                }
            }
        });
        uploadsBeforeOrder = $filter('orderBy')(uploadsBeforeOrder, '-Start_Date__c', true);
        var uploadsAfterOrder = [];
        var uploadsAfterOrderMap = [];
        angular.forEach(uploadsBeforeOrder, function(record, key) {
            record.Start_Date__c = $filter('date')(new Date(record.Start_Date__c), 'dd MMM yyyy');
            record.End_Date__c = $filter('date')(new Date(record.End_Date__c), 'dd MMM yyyy');
            uploadsAfterOrderMap[record.Id] = record;
            uploadsAfterOrder.push(record);
        });
        var img_query = '';
        if (query != '') query = ' AND (' + query + ')';
        var visitSummaryrecords = [];
        var visits = '';
        if (lastVisit[0] != undefined) {
            visits = '{Visit_Summary__c:Visit__c}="' + lastVisit[0][0].Id + '" OR {Visit_Summary__c:Visit__c}="' + lastVisit[0][0].External_Id__c + '" ';
        }
        if (lastVisit[1] != undefined) {
            if (visits != '') {
                visits = visits + ' OR {Visit_Summary__c:Visit__c}="' + lastVisit[1][0].Id + '" OR {Visit_Summary__c:Visit__c}="' + lastVisit[1][0].External_Id__c + '" ';
            } else {
                visits = '{Visit_Summary__c:Visit__c}="' + lastVisit[1][0].Id + '" OR {Visit_Summary__c:Visit__c}="' + lastVisit[1][0].External_Id__c + '" ';
            }
        }
        if (visits != '') {
            visits = '(' + visits + ')';
        }
        var currentVisitSummaries = [],
            lastVisitsummaries = [],
            currentVisitSummariesWithIds = [],
            lastVisitsummariesWithIds = [];
        visitSummaryrecords = SMARTSTORE.buildSmartQuerySpec('SELECT {Visit_Summary__c:_soup} FROM {Visit_Summary__c} WHERE {Visit_Summary__c:Account_Id__c}="' + $stateParams.AccountId + '" AND' + visits + query, 100);
        angular.forEach(visitSummaryrecords, function(record, key) {
            if (record[0].RecordTypeId == $scope.currentPage.sections[0].sectionDescription.Record_Type_Id__c) {
                if (img_query == '') {
                    if (record[0].Id != undefined) img_query = " WHERE {Db_images:ParentId}='" + record[0].External_Id__c + "' OR {Db_images:ParentId}='" + record[0].Id + "'";
                    else img_query = " WHERE {Db_images:ParentId}='" + record[0].External_Id__c + "' ";
                } else {
                    if (record[0].Id != undefined) img_query = img_query + " OR {Db_images:ParentId}='" + record[0].External_Id__c + "' OR {Db_images:ParentId}='" + record[0].Id + "'";
                    else img_query = img_query + " OR {Db_images:ParentId}='" + record[0].External_Id__c + "' ";
                }
                if (record[0].Visit__c == lastVisit[0][0].Id || record[0].Visit__c == lastVisit[0][0].External_Id__c) {
                    currentVisitSummaries.push(record[0]);
                    if (record[0].External_Id__c != undefined) currentVisitSummariesWithIds[record[0].External_Id__c] = record[0];
                    if (record[0].Id != undefined) currentVisitSummariesWithIds[record[0].Id] = record[0];
                } else {
                    if (record[0].External_Id__c != undefined) lastVisitsummariesWithIds[record[0].External_Id__c] = record[0];
                    if (record[0].Id != undefined) lastVisitsummariesWithIds[record[0].Id] = record[0];
                    var rec = angular.copy(record[0]);
                    rec.External_Id__c = undefined;
                    lastVisitsummaries.push(rec);
                }
            }
        });
        if (currentVisitSummaries.length > 0) {
            visitSummaryrecords = currentVisitSummaries;
        } else {
            visitSummaryrecords = lastVisitsummaries;
        }
        if (img_query != '') {
            var images = SMARTSTORE.buildSmartQuerySpec("SELECT {Db_images:_soup} FROM {Db_images} " + img_query, 100);
            angular.forEach(images, function(record, key) {
                if (currentVisitSummariesWithIds[record[0].ParentId] != undefined) {
                    if (queryCurrentImages == true) {
                        var uploadId = uploadsAfterOrderMap[currentVisitSummariesWithIds[record[0].ParentId].Upload__c] != undefined ? uploadsAfterOrderMap[currentVisitSummariesWithIds[record[0].ParentId].Upload__c].External_Id__c : currentVisitSummariesWithIds[record[0].ParentId].Upload__c;
                        if ($rootScope.currentvisitSummaryImages[$stateParams.order][uploadId] == undefined) $rootScope.currentvisitSummaryImages[$stateParams.order][uploadId] = [];
                        $rootScope.currentvisitSummaryImages[$stateParams.order][uploadId].push(record[0]);
                    }
                } else {
                    var uploadId = uploadsAfterOrderMap[lastVisitsummariesWithIds[record[0].ParentId].Upload__c] != undefined ? uploadsAfterOrderMap[lastVisitsummariesWithIds[record[0].ParentId].Upload__c].External_Id__c : lastVisitsummariesWithIds[record[0].ParentId].Upload__c;
                    if ($scope.visitSummaryImages[uploadId] == undefined) $scope.visitSummaryImages[uploadId] = [];
                    $scope.visitSummaryImages[uploadId].push(record[0]);
                }
            });
        }
        angular.forEach(visitSummaryrecords, function(record, key) {
            var UploadExtId = uploadsAfterOrderMap[record.Upload__c] != undefined ? uploadsAfterOrderMap[record.Upload__c].External_Id__c : record.Upload__c;
            if ($rootScope.uploadfields[$stateParams.order] == undefined) $rootScope.uploadfields[$stateParams.order] = [];
            $rootScope.uploadfields[$stateParams.order][UploadExtId] = record;
        });
        if ($rootScope.uploadfields[$stateParams.order] != undefined) {
            var isYes = [],
                isRejected = [],
                allOther = [],
                isNo = [];
            angular.forEach(uploadsAfterOrder, function(record, key) {
                if ($rootScope.currentvisitSummaryImages[$stateParams.order][record.External_Id__c] == undefined) $rootScope.currentvisitSummaryImages[$stateParams.order][record.External_Id__c] = [];
                $rootScope.currentvisitSummaryImages[$stateParams.order][record.External_Id__c] = $filter('orderBy')($rootScope.currentvisitSummaryImages[$stateParams.order][record.External_Id__c], '-Order', true);
                if ($scope.visitSummaryImages[record.External_Id__c] == undefined) $scope.visitSummaryImages[record.External_Id__c] = [];
                $scope.visitSummaryImages[record.External_Id__c] = $filter('orderBy')($scope.visitSummaryImages[record.External_Id__c], '-Order', true);
                if ($rootScope.uploadfields[$stateParams.order][record.External_Id__c] == undefined) {
                    allOther.push(record);
                } else if ($rootScope.uploadfields[$stateParams.order][record.External_Id__c].Executed__c == 'No') {
                    isNo.push(record);
                } else if ($rootScope.uploadfields[$stateParams.order][record.External_Id__c].Executed__c == 'Yes') {
                    record.Status__c = $rootScope.uploadfields[$stateParams.order][record.External_Id__c].Status__c;
                    isYes.push(record);
                } else {
                    isRejected.push(record);
                }
            });
            angular.forEach(allOther, function(record, key) {
                $scope.uploadslist.push(record);
            });
            angular.forEach(isNo, function(record, key) {
                $scope.uploadslist.push(record);
            });
            angular.forEach(isRejected, function(record, key) {
                $scope.uploadslist.push(record);
            });
            angular.forEach(isYes, function(record, key) {
                $scope.uploadslist.push(record);
            });
        } else {
            $rootScope.uploadfields[$stateParams.order] = [];
            $scope.uploadslist = uploadsAfterOrder;
        }
        $scope.titlefields = [];
        angular.forEach($scope.currentPage.sections[0].fields, function(field, key) {
            if (field.Type__c == 'title') {
                $scope.titlefields.push(field.Field_API__c);
            }
        });
        if ($rootScope.currentvisitSummaryImages[$stateParams.order] != undefined) imagelist = $rootScope.currentvisitSummaryImages[$stateParams.order];
        $scope.prevSlide = function() {
            $ionicSlideBoxDelegate.previous();
        }
        $scope.nextSlide = function() {
            $ionicSlideBoxDelegate.next();
        }
        var currentVisitImagesSlider = $ionicSlideBoxDelegate.$getByHandle('currentImagesSlider');
        var lastVisitImagesSlider = $ionicSlideBoxDelegate.$getByHandle('lastImagesSlider');
        $scope.currentImagemodal = [];
        $ionicModal.fromTemplateUrl('current-image-modal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.currentImagemodal = modal;
        });
        $scope.openCurrentImageModal = function() {
            currentVisitImagesSlider.slide(0);
            $scope.currentImagemodal.show();
        };
        $scope.closeCurrentImageModal = function() {
            $scope.currentImagemodal.hide();
        };
        $scope.goToSlideCurrentImageModel = function(uploadId, index) {
                $scope.slideActive = uploadId;
                $scope.currentImagemodal.show();
                $timeout(function() {
                    currentVisitImagesSlider.slide(index);
                }, 500);
            }
            // Called each time the slide changes
        $scope.slideCurrentImageIndex = 0;
        $scope.slideLastVisitImageIndex = 0;
        $scope.slideChangedCurrentImageModel = function(index) {
            $scope.slideIndex1 = index;
            $scope.slideCurrentImageIndex = index;
        };
        $ionicModal.fromTemplateUrl('image-modal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
        });
        $scope.openModal = function() {
            lastVisitImagesSlider.slide(0);
            $scope.modal.show();
        };
        $scope.closeModal = function() {
            $scope.modal.hide();
        };
        $scope.goToSlide = function(uploadId, index) {
                $scope.slideActive = uploadId;
                $scope.modal.show();
                $timeout(function() {
                    lastVisitImagesSlider.slide(index);
                }, 500);
            }
            // Called each time the slide changes
        $scope.slideChanged = function(index) {
            $scope.slideIndex = index;
            $scope.slideLastVisitImageIndex = index;
        };
        $scope.lastVisitPrevSlide = function() {
            lastVisitImagesSlider.previous();
        }
        $scope.lastVisitNextSlide = function() {
            lastVisitImagesSlider.next();
        }
        $scope.currentVisitPrevSlide = function() {
            currentVisitImagesSlider.previous();
        }
        $scope.currentVisitNextSlide = function() {
                currentVisitImagesSlider.next();
            }
            // $scope.isFieldHide = function(extId,fieldAPI){
            //    var status = 'Executed on ' + $filter('date')(new Date(), 'dd MMM yyyy');
            //    if (fieldAPI == 'Is_POP_Present__c' || fieldAPI == 'Status__c') {
            //        if($rootScope.uploadfields[$scope.order][extId].Status__c==undefined || status!=$rootScope.uploadfields[$scope.order][extId].Status__c){
            //            return false;
            //        }else{
            //            return true;
            //        }
            //    }else if(fieldAPI == 'Executed__c'){
            //        if($rootScope.uploadfields[$scope.order][extId].Status__c==undefined || status!=$rootScope.uploadfields[$scope.order][extId].Status__c){
            //            return true;
            //        }else{
            //            return false;
            //        }
            //    }else{
            //         return true;
            //    }
            // }
        $scope.isFieldHide = function(fieldAPI, fieldValue, DependentValue, visitId, thisValue) {
            if (fieldAPI == 'Is_POP_Present__c') {
                if (fieldValue != undefined && DependentValue.indexOf(fieldValue) == -1 && visitId != undefined && visitId != $stateParams.visitId && lastVisit[0][0].Id != visitId) {
                    return true;
                } else {
                    return false;
                }
            } else if (fieldAPI == 'Status__c') {
                if (thisValue != undefined && thisValue.indexOf('Executed') != -1 && DependentValue.indexOf(fieldValue) != -1 && visitId != undefined && visitId != $stateParams.visitId && lastVisit[0][0].Id != visitId) {
                    return true;
                } else {
                    return false;
                }
            } else if (fieldAPI == 'Executed__c') {
                if (fieldValue != undefined && DependentValue.indexOf(fieldValue) == -1 && visitId != undefined && visitId != $stateParams.visitId && lastVisit[0][0].Id != visitId) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        }
        $scope.takePicture = function(uploadId) {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.CAMERA,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 700,
                targetHeight: 500,
                saveToPhotoAlbum: false
            };
            $cordovaCamera.getPicture(options).then(function(imageData) {
                if (imagelist[uploadId] == undefined) imagelist[uploadId] = [];
                imagelist[uploadId].reverse();
                imagelist[uploadId].push({
                    Body: imageData,
                    IsDirty: true
                });
                var images = imagelist[uploadId].reverse();
                $rootScope.currentvisitSummaryImages[$stateParams.order][uploadId] = [];
                for (var i = 0; i < 10; i++) {
                    if (images[i] != undefined) {
                        images[i].Order = i;
                        $rootScope.currentvisitSummaryImages[$stateParams.order][uploadId].push(images[i]);
                    }
                }
            }, function(err) {});
        }
        $scope.goToNext = function() {
            $timeout(function() {
                var visitSummaryRecords = [];
                var attachments = [];
                angular.forEach($scope.uploadslist, function(record, key) {
                    var visitSummary = {};
                    visitSummary = $rootScope.uploadfields[$scope.order][record.External_Id__c];
                    if (visitSummary != undefined) {
                        visitSummary.IsDirty = true;
                        if (visitSummary.Executed__c == 'Yes' && (visitSummary.Status__c == undefined || visitSummary.Status__c.indexOf('Executed') == -1)) {
                            visitSummary.Opened_Visit__c = $stateParams.visitId;
                            visitSummary.Status__c = 'Executed on ' + $filter('date')(new Date(), 'dd MMM yyyy');
                        } else if (visitSummary.Executed__c == 'No') {
                            visitSummary.Status__c = 'Pending';
                        } else if (visitSummary.Executed__c == 'Rejected') {
                            visitSummary.Status__c = 'Rejected';
                        }
                        if (visitSummary.External_Id__c == undefined) {
                            visitSummary.Visit__c = $stateParams.visitId;
                            visitSummary.Account_Id__c = $stateParams.AccountId;
                            visitSummary.RecordTypeId = $scope.currentPage.sections[0].sectionDescription.Record_Type_Id__c;
                            visitSummary.External_Id__c = (new Date().getTime())+'-'+$stateParams.visitId+'-'+$window.Math.random() * 100000000;
                            visitSummary.Upload__c = record.External_Id__c;
                        }
                        $rootScope.uploadfields[$scope.order][record.External_Id__c] = visitSummary;
                        visitSummaryRecords.push(visitSummary);
                        angular.forEach($rootScope.currentvisitSummaryImages[$stateParams.order][record.External_Id__c], function(imageData, key) {
                            attachments.push({
                                ParentId: visitSummary['External_Id__c'],
                                Name: visitSummary['Upload__c'] + '.jpg',
                                Body: imageData.Body,
                                IsDirty: true,
                                Order: imageData.Order,
                                External_Id__c: 'Pridecall5--' + visitSummary['External_Id__c'] + imageData.Order
                            });
                        });
                    }
                });
                if (visitSummaryRecords.length > 0) {
                    MOBILEDATABASE_ADD($scope.currentPage.pageDescription.Master_Object__c, visitSummaryRecords, 'External_Id__c');
                }
                if (attachments.length > 0) {
                    MOBILEDATABASE_ADD('Db_images', attachments, 'External_Id__c');
                }
                var transaction = [{
                    Account: $stateParams.AccountId,
                    stage: $stateParams.order,
                    stageValue: 'pridecall',
                    Visit: $stateParams.visitId,
                    status: 'saved',
                    entryDate: today
                }];
                MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
                MOBILEDATABASE_ADD('Db_breadCrum', [{
                    VisitedOrder: $stateParams.order,
                    Name: 'PRIDECall5Ctrl'
                }], 'VisitedOrder');
                $rootScope.breadcrumClicked($stateParams.AccountId, $stateParams.visitId, $stateParams.order, parseInt($stateParams.order) + 1, 'fromCtrl');
            }, 100);
        }
        $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('PRIDECall5CtrlError', err.stack + '::' + err.message);
    }
}).controller('PRIDECall6Ctrl', function($q, $log, $ionicLoading, $scope, $rootScope, $window, $stateParams, $location, $filter, $cordovaCamera, $ionicModal, $ionicSlideBoxDelegate, $ionicScrollDelegate, $timeout, $ionicPopup, SMARTSTORE, MOBILEDATABASE_ADD,WEBSERVICE_ERROR) {
    try{
    $rootScope.params = $stateParams;
    $rootScope.name = "PRIDECall6Ctrl";
    $scope.order = $stateParams.order;
    var today = $filter('date')(new Date(), 'dd/MM/yyyy');
    var transaction = [{
        Account: $stateParams.AccountId,
        stage: $stateParams.order,
        stageValue: 'PRIDECall6Ctrl',
        Visit: $stateParams.visitId,
        status: 'pending',
        entryDate: today
    }];
    MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
    MOBILEDATABASE_ADD('Db_breadCrum', [{
        VisitedOrder: $stateParams.order,
        Name: 'PRIDECall6Ctrl'
    }], 'VisitedOrder');
    $scope.outlet = SMARTSTORE.buildExactQuerySpec('Account', 'Id', $stateParams.AccountId);
    $scope.breadActive = $rootScope.processFlowLineItems[$stateParams.order].Name;
    $scope.currentPage = {};
    angular.forEach($rootScope.pages, function(record, key) {
        if (record.pageDescription.Id == $rootScope.processFlowLineItems[$stateParams.order].Page__c) {
            $scope.currentPage = record;
        }
    });
    $scope.uploadslistAll = [];
    $scope.visiblitySlide = [];
    $scope.uploadslist = [];
    $scope.uploadslist_Merchandise = [];
    $scope.uploadslist_Competitor = [];
    var queryCurrentImages = false;
    if ($rootScope.currentvisitSummaryImages[$stateParams.order] == undefined) {
        $rootScope.currentvisitSummaryImages[$stateParams.order] = [];
        queryCurrentImages = true;
    }
    var uploads = SMARTSTORE.buildExactQuerySpec('Upload__c', 'Account__c', $stateParams.AccountId);
    var lastVisit = SMARTSTORE.buildSmartQuerySpec("SELECT {Visit__c:_soup} FROM {Visit__c} WHERE {Visit__c:Account__c}='" + $stateParams.AccountId + "' ORDER BY {Visit__c:Check_In_DateTime__c} DESC", 2);
    var uploadsListWithExtId = [];

    angular.forEach(uploads, function(record, key) {
        if ($stateParams.AccountId == record.Account__c && record.RecordTypeId == $scope.currentPage.sections[0].sectionDescription.Input_Record_Type_Id__c && record.Status__c != 'Inactive') {
            var upload = {};
            upload = record;
            if (record.Visibility_Type__c != undefined && record.Company__c != undefined) {
                upload.fields = $scope.currentPage.sections[0].fields;
                $scope.uploadslist.push(upload);
            }
            /*if (record.Visibility_Item__c == 'Signage' && record.Company__c != 'UBL') {
                upload.fields = $scope.currentPage.sections[2].fields;
                $scope.uploadslist_Competitor.push(upload);
            }*/
            $scope.uploadslistAll.push(upload);
            uploadsListWithExtId[upload.Id] = upload;
        }
    });
    if ($scope.uploadslist.length > 0) $scope.visiblitySlide.push('Signage');
    //if ($scope.uploadslist_Merchandise.length > 0) $scope.visiblitySlide.push('Merchandise');
    //if ($scope.uploadslist_Competitor.length > 0) $scope.visiblitySlide.push('Competitor');
    console.log($scope.visiblitySlide);
    console.log($scope.uploadslistAll);
    console.log($scope.uploadslist);
    var imagelist = [];
    var visits = '';
    angular.forEach(lastVisit, function(record, key) {
        if (visits != '') {
            visits = visits + 'OR {Visit_Summary__c:Visit__c}="' + record[0].Id + '" OR {Visit_Summary__c:Visit__c}="' + record[0].External_Id__c + '"';
        } else {
            visits = ' {Visit_Summary__c:Visit__c}="' + record[0].Id + '" OR {Visit_Summary__c:Visit__c}="' + record[0].External_Id__c + '"';
        }
    });
    if (visits != '') {
        visits = ' AND (' + visits + ')';
    }
    if ($rootScope.uploadfields[$scope.order] == undefined) {
        $rootScope.uploadfields[$scope.order] = [];
        var visitSummaries = SMARTSTORE.buildSmartQuerySpec('SELECT {Visit_Summary__c:_soup} FROM {Visit_Summary__c} WHERE {Visit_Summary__c:Account_Id__c}="' + $stateParams.AccountId + '"' + visits, 100);
        var img_query = '',
            currentVisitSummaries = [],
            lastVisitsummaries = [],
            currentVisitSummariesWithUpload = [],
            lastVisitsummariesWithUpload = [];
        angular.forEach(visitSummaries, function(record, key) {
            if (record[0].RecordTypeId == $scope.currentPage.sections[0].sectionDescription.Record_Type_Id__c) {
                if (img_query == '') {
                    if (record[0].Id != undefined) img_query = " WHERE {Db_images:ParentId}='" + record[0].External_Id__c + "' OR {Db_images:ParentId}='" + record[0].Id + "'";
                    else img_query = " WHERE {Db_images:ParentId}='" + record[0].External_Id__c + "' ";
                } else {
                    if (record[0].Id != undefined) img_query = img_query + " OR {Db_images:ParentId}='" + record[0].External_Id__c + "' OR {Db_images:ParentId}='" + record[0].Id + "'";
                    else img_query = img_query + " OR {Db_images:ParentId}='" + record[0].External_Id__c + "' ";
                }
                if (record[0].Visit__c == lastVisit[0][0].External_Id__c || record[0].Visit__c == lastVisit[0][0].Id) {
                    currentVisitSummaries[record[0].Id] = record[0];
                    var uExtId = record[0].Upload__c;
                    if (uploadsListWithExtId[record[0].Upload__c] != undefined) {
                        uExtId = uploadsListWithExtId[record[0].Upload__c].External_Id__c;
                    }
                    currentVisitSummariesWithUpload[uExtId] = record[0];
                    currentVisitSummaries[record[0].External_Id__c] = record[0];
                } else {
                    var uExtId = record[0].Upload__c;
                    if (uploadsListWithExtId[record[0].Upload__c] != undefined) {
                        uExtId = uploadsListWithExtId[record[0].Upload__c].External_Id__c;
                    }
                    lastVisitsummaries[record[0].Id] = record[0];
                    lastVisitsummaries[record[0].External_Id__c] = record[0];
                    var rec = angular.copy(record[0]);
                    rec.External_Id__c = undefined;
                    lastVisitsummariesWithUpload[uExtId] = rec;
                }
            }
        });
        if (currentVisitSummariesWithUpload != undefined && currentVisitSummariesWithUpload.length > 0) {
            $rootScope.uploadfields[$scope.order] = currentVisitSummariesWithUpload;
        } else {
            $rootScope.uploadfields[$scope.order] = lastVisitsummariesWithUpload;
        }
        if (img_query != '') {
            var images = SMARTSTORE.buildSmartQuerySpec("SELECT {Db_images:_soup} FROM {Db_images} " + img_query, 100);
            angular.forEach(images, function(record, key) {
                if (currentVisitSummaries[record[0].ParentId] != undefined) {
                    var uploadId = uploadsListWithExtId[currentVisitSummaries[record[0].ParentId].Upload__c] != undefined ? uploadsListWithExtId[currentVisitSummaries[record[0].ParentId].Upload__c].External_Id__c : currentVisitSummaries[record[0].ParentId].Upload__c;
                    if ($rootScope.currentvisitSummaryImages[$stateParams.order][uploadId] == undefined) $rootScope.currentvisitSummaryImages[$stateParams.order][uploadId] = [];
                    $rootScope.currentvisitSummaryImages[$stateParams.order][uploadId].push(record[0]);
                } else {
                    var uploadId = uploadsListWithExtId[lastVisitsummaries[record[0].ParentId].Upload__c] != undefined ? uploadsListWithExtId[lastVisitsummaries[record[0].ParentId].Upload__c].External_Id__c : lastVisitsummaries[record[0].ParentId].Upload__c;
                    if ($rootScope.currentvisitSummaryImages[$stateParams.order][uploadId] == undefined) $rootScope.currentvisitSummaryImages[$stateParams.order][uploadId] = [];
                    $rootScope.currentvisitSummaryImages[$stateParams.order][uploadId].push(record[0]);
                }
            });
        }
    }
    angular.forEach($scope.uploadslist, function(record, key) {
        if ($rootScope.uploadfields[$scope.order][record.External_Id__c] == undefined) {
            $rootScope.uploadfields[$scope.order][record.External_Id__c] = {};
            $rootScope.uploadfields[$scope.order][record.External_Id__c].Signage_in_right_location__c = true;
            $rootScope.uploadfields[$scope.order][record.External_Id__c].Signage_working__c = true;
        }
    });
    imagelist = $rootScope.currentvisitSummaryImages[$stateParams.order];
    $ionicLoading.hide();
    var currentVisitImagesSlider = $ionicSlideBoxDelegate.$getByHandle('currentImagesSlider');
    $scope.currentImagemodal = [];
    $ionicModal.fromTemplateUrl('current-image-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.currentImagemodal = modal;
    });
    $scope.openCurrentImageModal = function() {
        currentVisitImagesSlider.slide(0);
        $scope.currentImagemodal.show();
    };
    $scope.closeCurrentImageModal = function() {
        $scope.currentImagemodal.hide();
    };
    $scope.goToSlideCurrentImageModel = function(uploadId, index) {
            $scope.slideActive = uploadId;
            $scope.currentImagemodal.show();
            $timeout(function() {
                currentVisitImagesSlider.slide(index);
            }, 500);
        }
        // Called each time the slide changes
    $scope.slideCurrentImageIndex = 0;
    $scope.slideChangedCurrentImageModel = function(index) {
        $scope.slideIndex1 = index;
        $scope.slideCurrentImageIndex = index;
    };
    $scope.currentVisitPrevSlide = function() {
        currentVisitImagesSlider.previous();
    }
    $scope.currentVisitNextSlide = function() {
        currentVisitImagesSlider.next();
    }

    $scope.showAlert = function(order, extID, fieldType){

       if(!$rootScope.uploadfields[order][extID].Signage_working__c){
            console.log('Please raise a complaint in feedback section.');
        }
    }

    var attachmentsVis = [];
    $scope.takePicture = function(uploadId) {
        var options = {
            quality: 75,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.CAMERA,
            allowEdit: true,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 700,
            targetHeight: 500,
            saveToPhotoAlbum: false
        };

        $cordovaCamera.getPicture(options).then(function(imageData) {
            attachmentsVis.push({
                ParentId: uploadId,
                Name: 'visibilityImage@'+uploadId,
                Body: imageData,
                IsDirty: true,
                External_Id__c: uploadId +'_'+$window.Math.random() * 100000000
            });

            if (imagelist[uploadId] == undefined) imagelist[uploadId] = [];
            imagelist[uploadId].reverse();
            console.log(attachmentsVis);
            imagelist[uploadId].push({
                Body: imageData,
                IsDirty: true,
                Name:'',
                myOrder:imagelist.length
            });
            var images = imagelist[uploadId].reverse();
            $rootScope.galleryImagesVis = [];
            $rootScope.currentvisitSummaryImages[$stateParams.order][uploadId] = [];
            for (var i = 0; i < 5; i++) {
                if (images[i] != undefined) {
                    images[i].Order = i;
                    $rootScope.currentvisitSummaryImages[$stateParams.order][uploadId].push(images[i]);
                }
            }
            console.log(imagelist);
            console.log($rootScope.currentvisitSummaryImages);
            //
        }, function(err) {
            /*console.log('Visibility Image Capture Error');
            console.log(JSON.stringify(err));*/
        });
    }
    $scope.prevSlide = function() {
        $ionicSlideBoxDelegate.previous();
    }
    $scope.nextSlide = function() {
        $ionicSlideBoxDelegate.next();
    }
    $scope.restrictKeydown = function(_event, _index) {
        $log.debug(_event.keyCode);
        if (_event.which == 9) {
            document.getElementById("myinput_" + (parseInt(_index) + 1)).focus();
            _event.preventDefault();
        }
    }
    console.log('checkAvailable');
    console.log($rootScope.uploadfields);
    $scope.checkAvailable = function(uploadExtId) {
        if ($rootScope.uploadfields[$stateParams.order][uploadExtId].Available__c != undefined) {
            if ($rootScope.uploadfields[$stateParams.order][uploadExtId].Available__c == true) {
                if ($rootScope.uploadfields[$stateParams.order][uploadExtId].Quantity__c == 0) {
                    $rootScope.uploadfields[$stateParams.order][uploadExtId].Quantity__c = undefined;
                }
            } else {
                $rootScope.uploadfields[$stateParams.order][uploadExtId].Quantity__c = 0;
            }
        }
    }
    console.log('checkAvailable end');
    console.log($rootScope.uploadfields);
    $scope.remove = [];
    $scope.remove.Reason = '';
    $scope.removeUpload = function(removeUploadId, visibilityType, visibility_TypeName, companyName) {
        $scope.remove.Reason = '';
        $scope.confirmUploadDeletePopup = $ionicPopup.confirm({
            template: 'Are you sure you want to remove ' + $scope.breadActive.toLowerCase() + ' item "' + visibility_TypeName + '" for this outlet?  <br/><br/><br/><span><input placeholder="Enter the reason for removing the ' + visibility_TypeName + '." type="text" ng-change="modelDataCheck()" ng-model="remove.Reason"><div class="piller"></div><div class="piller piller_1"></div></span><br/> <div style="text-align:right; color:#38B6CB;margin-right: 10px;font-weight: bold;"><span my-touchstart="cancelConfirmUploadDelete(\'' + removeUploadId + '\')">NO</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id="coolerRemove_yes" class="coolerRemove_yes" ng-disabled="!remove.Reason" my-touchstart="confirmUploadDelete(\'' + removeUploadId + '\', \'' + visibilityType + '\', \'' + companyName + '\')">YES</span> </div>',
            cssClass: 'myConfirm',
            title: '',
            scope: $scope,
            buttons: []
        });
        $scope.confirmUploadDeletePopup.then(function(res) {
            if (res) {
                $location.path('app/removeUploadReason/' + removeUploadId + '/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order);
                console.log('You are sure');
            } else {
                console.log('You are not sure');
            }
        });
    }
    $scope.confirmUploadDelete = function(removeUploadId, visibilityType, companyName) {
        if ($scope.remove.Reason != '') {
            if (visibilityType == 'Signage' && companyName != undefined) {
                var currentList = $scope.uploadslist;
            } else if(visibilityType == 'Signage' && companyName != undefined) {
                var currentList = $scope.uploadslist_Competitor;
            }else {
                var currentList = $scope.uploadslist_Merchandise;
            }
            angular.forEach(currentList, function(record, key) {
                var uploadsToDelete = [];
                if (record.External_Id__c == removeUploadId) {
                    delIndex = key;
                    record.Status__c = 'Inactive';
                    record.Reason__c = $scope.remove.Reason;
                    record.IsDirty = true;
                    uploadsToDelete.push(record);
                    MOBILEDATABASE_ADD('Upload__c', uploadsToDelete, 'External_Id__c');
                    $location.path('app/container6/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date() + 'save');
                }
            });
            $scope.remove.Reason = '';
            $scope.confirmUploadDeletePopup.close();
        } else {
            Splash.ShowToast('Please Specify reason for removing ' + $scope.breadActive + ' ', 'long', 'bottom', function(a) {
                console.log(a)
            });
        }
    }
    $scope.cancelConfirmUploadDelete = function(removeUploadId) {
        $scope.confirmUploadDeletePopup.close();
        $scope.remove.Reason = '';
    }
    $scope.modelDataCheck = function() {
        var _myEle = document.getElementById('coolerRemove_yes');
        if ($scope.remove.Reason == '') {
            _myEle.style.color = '#aaaaaa';
        } else {
            _myEle.style.color = '#38B6CB';
        }
    }
    $scope.changeTotalStock = function(st, extId) {
        $log.debug(st + extId);
        if (st > 0 && st != '') {
            $rootScope.uploadfields[$scope.order][extId].Available__c = true;
        } else {
            $rootScope.uploadfields[$scope.order][extId].Available__c = false;
        }
    };
    $scope.focusCheckForStock = function(index, event) {
        var _currentHeight = jQuery(event.target).parents('.row').height() - 4;
        $ionicScrollDelegate.scrollTo(0, index * _currentHeight, true);
    }
    $scope.restrictNumbersLength = function(fieldAPI, fieldValue, extId, e) {     
         // var key = e.keyCode;
         // if ((key >= 48 && key <= 57) && (key != 187 && key != 189 && key != 190) ) {
         //    if(fieldValue != null){
         //     var fValue = fieldValue.toString();
         //     if (fValue.length > 3) $rootScope.uploadfields[$stateParams.order][extId][fieldAPI] = parseInt(fValue.substr(0, 3));
         //    } else{
         //        e.preventDefault();
         //    }           
         // } else {
         //    if(key == 187 || key == 189 || key == 190) {
         //        e.target.value.replace(/[^0-9 ]/g, "");       
         //        e.preventDefault();
         //    }
         // }    
         var fValue = fieldValue.toString();
            if (fValue.length > 4) $rootScope.uploadfields[$stateParams.order][extId][fieldAPI] = parseInt(fValue.substr(0, 4));         
    }
    $timeout(function() {
        if (jQuery('.lastComplaints_0 > .scroll').height() >= 315) {
            $scope.scrollHeight = false;
        } else {
            $scope.scrollHeight = true;
        }
    }, 50);
    $timeout(function() {
        if (jQuery('.lastComplaints_1 > .scroll').length > 0) {
            if (jQuery('.lastComplaints_1 > .scroll').height() >= 315) {
                $scope.scrollHeight = false;
            } else {
                $scope.scrollHeight = true;
            }
        }
    }, 50);
    $timeout(function() {
        if (jQuery('.lastComplaints_2 > .scroll').length > 0) {
            if (jQuery('.lastComplaints_2 > .scroll').height() >= 315) {
                $scope.scrollHeight = false;
            } else {
                $scope.scrollHeight = true;
            }
        }
    }, 50);
    $scope.addNew = function() {
        $location.path('app/AddNew6/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date());
    }
    $scope.goToNext = function() {

        //stopping adding image into upload, tagging  the same into Visit Summary
        if (attachmentsVis.length > 0) 
            //MOBILEDATABASE_ADD('Db_images', attachmentsVis, 'External_Id__c');
        console.log('entered');
        $timeout(function() {
            if (!jQuery('.feedbackNext').hasClass('btn-disabled')) {
                var visitSummaryRecords = [];
                var attachments = [];
                console.log($scope.uploadslistAll);
                //for # each start
                angular.forEach($scope.uploadslistAll, function(record, key) {
                    var visitSummary = {};
                    visitSummary = $rootScope.uploadfields[$stateParams.order][record.External_Id__c];
                    console.log(visitSummary);
                    if (visitSummary != undefined) {

                        //Visit_Summary__c = Visibility = 012j0000000zKrfAAE = recordtype
                        visitSummary['RecordTypeId'] = $scope.currentPage.sections[0].sectionDescription.Record_Type_Id__c;
                        
                        //if # start
                        if (record.Visibility_Type__c != undefined && visitSummary.Signage_working__c == false ) {
                            
                            var rtList = SMARTSTORE.buildSmartQuerySpec("SELECT  {DB_RecordTypes:_soup} FROM {DB_RecordTypes} WHERE {DB_RecordTypes:SobjectType}='Visit_Summary__c' AND {DB_RecordTypes:Name}='Feedback and Complaints'", 1);
                            var existComplaints = SMARTSTORE.buildExactQuerySpec('Visit_Summary__c', 'Visit__c', $stateParams.visitId);
                            console.log(existComplaints);
                            console.log(rtList + ' == 012j0000000zKraAAE');
                            var isCreateComplaint = true;
                                angular.forEach(existComplaints, function(value, key) {
                                    if (value.RecordTypeId == rtList[0][0].Id && value.Category__c == 'Visibility Element Complaint' && value.Brand__c == record.Brand__c) {
                                        isCreateComplaint = false;
                                    }
                                });
                            console.log(isCreateComplaint);
                                if (isCreateComplaint == true) {
                                    var existComplaints1 = SMARTSTORE.buildExactQuerySpec('Visit_Summary__c', 'Account_Id__c', $stateParams.AccountId);
                                    angular.forEach(existComplaints1, function(value, key) {
                                        if (value.RecordTypeId == rtList[0][0].Id && value.Category__c == 'Visibility Element Complaint' && (value.Status__c=='Open' ||value.Status__c=='open' ||value.Status__c=='Logged' ||value.Status__c=='logged') ) {
                                            isCreateComplaint = false;
                                        }
                                        console.log(isCreateComplaint);
                                    });
                                    console.log(isCreateComplaint);
                                    if (isCreateComplaint == true) {
                                        var complaint = {
                                            Source__c: 'Trade',
                                            Type__c: 'Negative',
                                            Category__c: 'Visibility Element Complaint',
                                            Sub_category__c: 'Lights not working',
                                            Notify_Supervisor__c: 'Yes',
                                            Send_Email_to_Supervisor__c: true,
                                            Signage_working__c: visitSummary.Signage_working__c,
                                            Signage_in_right_location__c: visitSummary.Signage_in_right_location__c,
                                            Brand__c: record.Brand__c,
                                            Visit__c: $stateParams.visitId,
                                            RecordTypeId: rtList[0][0].Id,
                                            Account_Id__c: $stateParams.AccountId,
                                            External_Id__c: (new Date().getTime())+'-'+$stateParams.visitId+'-'+$window.Math.random() * 100000000,
                                            Upload__c: record.External_Id__c,
                                            Date__c: $filter('date')(new Date(), 'dd/MM/yyyy'),
                                            Status__c: 'Open',
                                            IsDirty: true
                                        };
                                        console.log(complaint);
                                        visitSummaryRecords.push(complaint);
                                    }
                                }
                        }//if # end
                        console.log(visitSummary);
                        if (visitSummary.External_Id__c == undefined) {
                            visitSummary['Visit__c'] = $stateParams.visitId;
                            visitSummary['External_Id__c'] = (new Date().getTime())+'-'+$stateParams.visitId +'-'+$window.Math.random() * 10000000;
                            visitSummary['Upload__c'] = record.External_Id__c;
                            visitSummary['Brand__c'] = record.Brand__c;
                        }

                        visitSummary['IsDirty'] = true;
                        visitSummary.Account_Id__c = $stateParams.AccountId;
                        $rootScope.uploadfields[$stateParams.order][record.External_Id__c] = visitSummary;
                        visitSummaryRecords.push(visitSummary);
                        console.log($rootScope.currentvisitSummaryImages);
                        angular.forEach($rootScope.currentvisitSummaryImages[$stateParams.order][record.External_Id__c], function(imageData, keyInside) {
                            attachments.push({
                                ParentId: visitSummary['External_Id__c'],
                                Name: visitSummary['Upload__c'] + '.jpg',
                                Body: imageData.Body,
                                IsDirty: true,
                                Order: imageData.Order,
                                External_Id__c: 'Pridecall6--' + visitSummary['External_Id__c'] + imageData.Order
                            });
                        });
                    }
                });//for # each end

                if (visitSummaryRecords.length > 0) {
                    MOBILEDATABASE_ADD($scope.currentPage.pageDescription.Master_Object__c, visitSummaryRecords, 'External_Id__c');
                }
                console.log(attachments);

                if (attachments.length > 0) {
                    MOBILEDATABASE_ADD('Db_images', attachments, 'External_Id__c');
                }
                var transaction = [{
                    Account: $stateParams.AccountId,
                    stage: $stateParams.order,
                    stageValue: 'pridecall',
                    Visit: $stateParams.visitId,
                    status: 'saved',
                    entryDate: today
                }];
                MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
                MOBILEDATABASE_ADD('Db_breadCrum', [{
                    VisitedOrder: $stateParams.order,
                    Name: 'PRIDECall6Ctrl'
                }], 'VisitedOrder');
                $rootScope.breadcrumClicked($stateParams.AccountId, $stateParams.visitId, $stateParams.order, parseInt($stateParams.order) + 1, 'fromCtrl');
            } else {
                $ionicLoading.hide();
                Splash.ShowToast('Please scroll for enabling next.', 'long', 'bottom', function(a) {
                    console.log(a)
                });
            }
        }, 100);
    }
    $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('PRIDECall6Ctrl', err.stack + '::' + err.message);
    }
}).controller('AddNew6Ctrl', function($q, $log, $ionicLoading, $scope, $ionicPopup, $rootScope, $window, $stateParams, $location, $filter, $cordovaCamera, $ionicModal, $ionicSlideBoxDelegate, $timeout, SMARTSTORE, MOBILEDATABASE_ADD,WEBSERVICE_ERROR) {
    try{
    $rootScope.params = $stateParams;
    $rootScope.name = "AddNew6Ctrl";
    $scope.order = $stateParams.order;
    $scope.breadActive = $rootScope.processFlowLineItems[$stateParams.order].Name;
    $scope.currentPage = {};
    $scope.hiddenType = [];
    angular.forEach($rootScope.pages, function(record, key) {
        if (record.pageDescription.Id == $rootScope.processFlowLineItems[$stateParams.order].Page__c) {
            $scope.currentPage = record;
        }
    });
    $scope.addNewSectionFields = [];
    if ($scope.currentPage.sections.length > 3) {
        $scope.addNewSectionFields = $scope.currentPage.sections[3].fields;
    } else if ($scope.currentPage.sections.length > 2) {
        $scope.addNewSectionFields = $scope.currentPage.sections[2].fields;
    } else if ($scope.currentPage.sections.length > 1) {
        $scope.addNewSectionFields = $scope.currentPage.sections[1].fields;
    } else {
        $scope.addNewSectionFields = $scope.currentPage.sections[0].fields;
    }
    angular.forEach($scope.addNewSectionFields, function(field, key) {
        field.fieldvalue = undefined;
        field.fieldvalues = undefined;
    });
    $scope.showAdd = false;
    var uploadsList = [];
    var existingMerchandises = [];
    var uploads = SMARTSTORE.buildExactQuerySpec('Upload__c', 'Account__c', $stateParams.AccountId);
    angular.forEach(uploads, function(record, key) {
        if ($stateParams.AccountId == record.Account__c && record.RecordTypeId == $scope.currentPage.sections[0].sectionDescription.Input_Record_Type_Id__c && record.Status__c != 'Inactive' && record.Visibility_Item__c == 'Merchandise') {
            if (uploadsList[record.Brand__c] == undefined) uploadsList[record.Brand__c] = [];
            uploadsList[record.Brand__c].push(record.Visibility_Type__c);
            existingMerchandises[record.Brand__c + record.Visibility_Type__c] = record;
        }
    });
    $ionicLoading.hide();
    $scope.checkValues = function() {
        $scope.showAdd = false;
        var visibilityType;
        var visibilityBrand;
        var companyVal;
        angular.forEach($scope.addNewSectionFields, function(field, key) {
            if (field.Field_API__c == "Company__c" && field.fieldvalue!=undefined && field.fieldvalue!='') {
                visibilityType = 'Signage';
                companyVal = field.fieldvalue;
            } else if (field.Field_API__c == "Brand__c" && field.fieldvalue!=undefined && field.fieldvalue!='') {
                visibilityBrand = field.fieldvalue;
            }
        });


        if (visibilityType != undefined && visibilityBrand != undefined && visibilityBrand != '') {
            if (visibilityType == 'Signage') {
                angular.forEach($scope.addNewSectionFields, function(field, key) {
                    if (field.Field_API__c == "Visibility_Type__c" && field.Dependent_Value__c != undefined && field.fieldvalue != undefined && field.fieldvalue != '') {
                        $scope.showAdd = true;
                    }
                });
            } 
        }
    }
    $scope.isAddFieldVisible = function(x, y) {
        var count = 0;
        if (x != undefined || y != undefined) {
            angular.forEach($scope.addNewSectionFields, function(record, key) {
                if (record.Field_API__c == y && x!=undefined && record.fieldvalue!='' && x.indexOf(record.fieldvalue)>-1) {
                    count++;
                    $log.debug("1"+record.Field_API__c+"=>"+y);
                    $log.debug("1"+record.fieldvalue+"=>"+x);
                }
            });
            if (count == 0) return true;
            else return false;
        } else {
            return false;
        }
    }
    $scope.checkMerchandiseAvailibility = function(fieldAPI) {
        $scope.hiddenType = [];
        var brandName = '';
        if (fieldAPI == 'Company__c') {
            angular.forEach($scope.addNewSectionFields, function(field, key) {
                if (field.Field_API__c != "Company__c" ) {
                    field.fieldvalue = undefined;
                    field.fieldvalues = undefined
                } else {
                    brandName = field.fieldvalue;
                }
            });
        } else if (fieldAPI == 'Brand__c') {
            angular.forEach($scope.addNewSectionFields, function(field, key) {
                if (field.Field_API__c != "Brand__c" && field.Field_API__c != "Company__c" ) {
                    field.fieldvalue = undefined;
                    field.fieldvalues = undefined
                } else {
                    brandName = field.fieldvalue;
                }
            });
        } else if (fieldAPI == 'Visibility_Item__c') {
            angular.forEach($scope.addNewSectionFields, function(field, key) {
                if (field.Field_API__c != "Brand__c" && field.Field_API__c != "Company__c" && field.Field_API__c != "Visibility_Item__c") {
                    field.fieldvalue = undefined;
                    field.fieldvalues = undefined
                } else if (field.Field_API__c == "Brand__c" && field.fieldvalue!=undefined && field.fieldvalue!='') {
                    brandName = field.fieldvalue;
                }
            });
            if (uploadsList[brandName] != undefined) {
                angular.forEach(uploadsList[brandName], function(mVal, mKey) {
                    $scope.hiddenType.push(mVal);
                    angular.forEach($scope.addNewSectionFields, function(field, key) {
                        if (field.Field_API__c == "Visibility_Type__c" && field.Dependent_Value__c == "Merchandise") {
                            if (field.fieldvalues == undefined) {
                                field.fieldvalues = [];
                            }
                            field.fieldvalues[mVal] = true;
                        }
                    });
                });
            }
        }
    }
    $scope.cancelAddNew = function() {
        $location.path('app/container6/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date() + 'save');
    }
    $scope.saveNew = function() {
        if ($scope.showAdd) {
            $timeout(function() {
                var uploadsToInsert = [];
                var visibilityType;
                var visibilityBrand;
                var compValue;
                angular.forEach($scope.addNewSectionFields, function(field, key) {
                    
                    console.log('$scope.saveNew');
                    console.log(field);
                    if (field.Field_API__c == "Company__c" && field.fieldvalue!=undefined && field.fieldvalue!='') {
                        visibilityType = 'Signage';
                        compValue = field.fieldvalue;
                    } else if (field.Field_API__c == "Brand__c" && field.fieldvalue!=undefined && field.fieldvalue!='') {
                        visibilityBrand = field.fieldvalue;
                    }
                });
                if (visibilityType == 'Signage') {
                    var record = {};
                    angular.forEach($scope.addNewSectionFields, function(field, key) {
                        console.log('if (visibilityType == ');
                        console.log(field);
                        if (field.fieldvalue != '' && field.fieldvalue != undefined) {
                            record[field.Field_API__c] = field.fieldvalue;
                        }
                    });
                    record.Account__c = $stateParams.AccountId;
                    record.RecordTypeId = $scope.currentPage.sections[0].sectionDescription.Input_Record_Type_Id__c;
                    record.External_Id__c = $stateParams.visitId +(new Date().getTime())+'_'+ $window.Math.random() * 10000000;
                    record.IsDirty = true;
                    record.Visibility_Item__c = 'Signage';
                    console.log(record);
                    uploadsToInsert.push(record);
                } else {
                    var countOfselected = [];
                    angular.forEach($scope.addNewSectionFields, function(field, key) {
                        if (visibilityType == 'Merchandise' && field.Field_API__c == "Visibility_Type__c" && field.Dependent_Value__c == "Merchandise") {
                            if (visibilityType == 'Merchandise' && field.fieldvalues != undefined) {
                                angular.forEach(field.pickListValues, function(fieldValuesVal, fieldValuesKey) {
                                    if (field.fieldvalues[fieldValuesVal] == true) {
                                        countOfselected.push(fieldValuesVal);
                                    }
                                });
                            }
                        }
                    });
                    angular.forEach(countOfselected, function(field, key) {
                        if (existingMerchandises[visibilityBrand + field] == undefined) {
                            var record = {};
                            record.Brand__c = visibilityBrand;
                            record.Visibility_Item__c = visibilityType;
                            record.Visibility_Type__c = field;
                            record.Account__c = $stateParams.AccountId;
                            record.RecordTypeId = $scope.currentPage.sections[0].sectionDescription.Input_Record_Type_Id__c;
                            record.External_Id__c = $stateParams.visitId +(new Date().getTime())+'_'+ $window.Math.random() * 10000000;
                            record.IsDirty = true;
                            uploadsToInsert.push(record);
                        }
                    });
                }
                if (uploadsToInsert.length > 0) {
                    MOBILEDATABASE_ADD('Upload__c', uploadsToInsert, 'External_Id__c');
                }
                Splash.ShowToast('The ' + $scope.breadActive.toLowerCase() + ' has been added.', 'long', 'bottom', function(a) {
                    console.log(a)
                });
                $location.path('app/container6/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date() + 'save');
            }, 500);
        } else {
            Splash.ShowToast('Please fill all fields.', 'long', 'bottom', function(a) {
                console.log(a)
            });
            $ionicLoading.hide();
        }
    }
    $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('AddNew6Ctrl', err.stack + '::' + err.message);
    }
}).controller('PRIDECall7Ctrl', function($q, $log, $ionicLoading, $scope, $ionicPopup, $rootScope, $window, $stateParams, $location, $filter, $cordovaCamera, $ionicModal, $ionicSlideBoxDelegate, $timeout, SMARTSTORE, MOBILEDATABASE_ADD,WEBSERVICE_ERROR) {
    try{
    $rootScope.params = $stateParams;
    $rootScope.name = "PRIDECall7Ctrl";
    $scope.order = $stateParams.order;
    var today = $filter('date')(new Date(), 'dd/MM/yyyy');
    var transaction = [{
        Account: $stateParams.AccountId,
        stage: $stateParams.order,
        stageValue: 'pridecall',
        Visit: $stateParams.visitId,
        status: 'pending',
        entryDate: today
    }];
    MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
    MOBILEDATABASE_ADD('Db_breadCrum', [{
        VisitedOrder: $stateParams.order,
        Name: 'PRIDECall7Ctrl'
    }], 'VisitedOrder');
    $scope.outlet = SMARTSTORE.buildExactQuerySpec('Account', 'Id', $stateParams.AccountId);
    $scope.breadActive = $rootScope.processFlowLineItems[$stateParams.order].Name;
    $scope.currentPage = {};
    angular.forEach($rootScope.pages, function(record, key) {
        if (record.pageDescription.Id == $rootScope.processFlowLineItems[$stateParams.order].Page__c) {
            $scope.currentPage = record;
        }
    });
    if ($scope.currentPage.sections.length > 1) {
        $scope.addNewSectionFields = $scope.currentPage.sections[1].fields;
    } else {
        $scope.addNewSectionFields = $scope.currentPage.sections[0].fields;
    }
    $scope.uploadslist = [];
    var queryCurrentImages = false;
    if ($rootScope.currentvisitSummaryImages[$stateParams.order] == undefined) {
        $rootScope.currentvisitSummaryImages[$stateParams.order] = [];
        queryCurrentImages = true;
    }
    $scope.currentPage.sections[0].fields = $filter('orderBy')($scope.currentPage.sections[0].fields, '-Order__c', true);
    $scope.visitSummaryImages = [];
    var uploads = SMARTSTORE.buildExactQuerySpec('Upload__c', 'Account__c', $stateParams.AccountId);
    var lastVisit = SMARTSTORE.buildSmartQuerySpec("SELECT {Visit__c:_soup} FROM {Visit__c} WHERE {Visit__c:Account__c}='" + $stateParams.AccountId + "' ORDER BY {Visit__c:Check_In_DateTime__c} DESC", 2);
    var d = new Date();
    var currentDate = d.getTime();
    var query = '';
    var uploadsBeforeOrder = [];
    var currentVisitSummariesOfSalesCaptured = [];
    angular.forEach(SMARTSTORE.buildSmartQuerySpec('SELECT {Visit_Summary__c:_soup} FROM {Visit_Summary__c} WHERE {Visit_Summary__c:Visit__c}="' + lastVisit[0][0].External_Id__c + '" OR {Visit_Summary__c:Visit__c}="' + lastVisit[0][0].Id + '"', 100), function(record, key) {
        if (record[0].RecordTypeId == $scope.currentPage.sections[0].sectionDescription.Record_Type_Id__c) {
            currentVisitSummariesOfSalesCaptured[record[0].Upload__c] = record[0];
        }
    });
    angular.forEach(uploads, function(record, key) {
        if (record.RecordTypeId == $scope.currentPage.sections[0].sectionDescription.Input_Record_Type_Id__c && record.Start_Date__c != undefined && record.End_Date__c != undefined) {
            var startDate = record.Start_Date__c;
            var aStartDate = record.Start_Date__c;
            if (startDate.indexOf('/') != -1) {
                aStartDate = new Date(startDate.split('/')[1] + '/' + startDate.split('/')[0] + '/' + startDate.split('/')[2]);
                startDate = new Date(startDate.split('/')[1] + '/' + startDate.split('/')[0] + '/' + startDate.split('/')[2]);
            } else {
                aStartDate = new Date(startDate);
                startDate = new Date(startDate);
            }
            $log.debug('startDate ' + startDate);
            record.Start_Date__c = startDate.getTime();
            $log.debug(startDate.getTime());
            startDate.setDate(startDate.getDate() - 14);
            startDate = startDate.getTime();
            aStartDate = aStartDate.getTime();
            var endDate = record.End_Date__c;
            var aEndDate = record.End_Date__c;
            if (endDate.indexOf('/') != -1) {
                aEndDate = new Date(endDate.split('/')[1] + '/' + endDate.split('/')[0] + '/' + endDate.split('/')[2]);
                endDate = new Date(endDate.split('/')[1] + '/' + endDate.split('/')[0] + '/' + endDate.split('/')[2]);
            } else {
                aEndDate = new Date(endDate);
                endDate = new Date(endDate);
            }
            var endDate1 = angular.copy(endDate);
            $log.debug('End_Date__c ' + endDate);
            record.End_Date__c = endDate.getTime();
            $log.debug('End_Date__c ' + record.End_Date__c);
            endDate.setDate(endDate.getDate() - 14);
            endDate = endDate.getTime();
            aEndDate = aEndDate.getTime();
            var daysdifference = parseInt((new Date() - endDate1) / (1000 * 60 * 60 * 24));
            if ((aStartDate <= currentDate && aEndDate + (1000 * 60 * 60 * 24) >= currentDate) || (startDate <= currentDate && endDate >= currentDate) || (daysdifference > 0 && daysdifference <= 60 && (record.Sales_Data_Captured__c == false || currentVisitSummariesOfSalesCaptured[record.External_Id__c] != undefined || currentVisitSummariesOfSalesCaptured[record.Id] != undefined))) {
                record.fields = $scope.currentPage.sections[0].fields;
                if (query == '') {
                    query = '{Visit_Summary__c:Upload__c}="' + record.Id + '" OR {Visit_Summary__c:Upload__c}="' + record.External_Id__c + '"';
                } else {
                    query = query + ' OR {Visit_Summary__c:Upload__c}="' + record.Id + '" OR {Visit_Summary__c:Upload__c}="' + record.External_Id__c + '"';
                }
                uploadsBeforeOrder.push(record);
            }
        }
    });
    uploadsBeforeOrder = $filter('orderBy')(uploadsBeforeOrder, '-Start_Date__c', true);
    var uploadsAfterOrder = [];
    var uploadsAfterOrderMap = [];
    var uploadsListWithExtId = [];
    angular.forEach(uploadsBeforeOrder, function(record, key) {
        uploadsListWithExtId[record.External_Id__c] = record;
        record.Start_Date__c = $filter('date')(new Date(record.Start_Date__c), 'dd MMM yyyy');
        record.End_Date__c = $filter('date')(new Date(record.End_Date__c), 'dd MMM yyyy');
        uploadsAfterOrderMap[record.Id] = record;
        uploadsAfterOrder.push(record);
    });
    var img_query = '';
    if (query != '') query = ' AND (' + query + ')';
    var visitSummaryrecords = [];
    var visits = '';
    if (lastVisit[0] != undefined) {
        visits = '{Visit_Summary__c:Visit__c}="' + lastVisit[0][0].Id + '" OR {Visit_Summary__c:Visit__c}="' + lastVisit[0][0].External_Id__c + '" ';
    }
    if (lastVisit[1] != undefined) {
        if (visits != '') {
            visits = visits + ' OR {Visit_Summary__c:Visit__c}="' + lastVisit[1][0].Id + '" OR {Visit_Summary__c:Visit__c}="' + lastVisit[1][0].External_Id__c + '" ';
        } else {
            visits = '{Visit_Summary__c:Visit__c}="' + lastVisit[1][0].Id + '" OR {Visit_Summary__c:Visit__c}="' + lastVisit[1][0].External_Id__c + '" ';
        }
    }
    if (visits != '') {
        visits = '(' + visits + ')';
    }
    var currentVisitSummaries = [],
        lastVisitsummaries = [],
        currentVisitSummariesWithIds = [],
        lastVisitsummariesWithIds = [];
    visitSummaryrecords = SMARTSTORE.buildSmartQuerySpec('SELECT {Visit_Summary__c:_soup} FROM {Visit_Summary__c} WHERE {Visit_Summary__c:Account_Id__c}="' + $stateParams.AccountId + '" AND' + visits + query, 100);
    angular.forEach(visitSummaryrecords, function(record, key) {
        if (record[0].RecordTypeId == $scope.currentPage.sections[0].sectionDescription.Record_Type_Id__c) {
            if (img_query == '') {
                if (record[0].Id != undefined) img_query = " WHERE {Db_images:ParentId}='" + record[0].External_Id__c + "' OR {Db_images:ParentId}='" + record[0].Id + "'";
                else img_query = " WHERE {Db_images:ParentId}='" + record[0].External_Id__c + "' ";
            } else {
                if (record[0].Id != undefined) img_query = img_query + " OR {Db_images:ParentId}='" + record[0].External_Id__c + "' OR {Db_images:ParentId}='" + record[0].Id + "'";
                else img_query = img_query + " OR {Db_images:ParentId}='" + record[0].External_Id__c + "' ";
            }
            if (record[0].Visit__c == lastVisit[0][0].External_Id__c || record[0].Visit__c == lastVisit[0][0].Id) {
                currentVisitSummaries.push(record[0]);
                if (record[0].External_Id__c != undefined) currentVisitSummariesWithIds[record[0].External_Id__c] = record[0];
                if (record[0].Id != undefined) currentVisitSummariesWithIds[record[0].Id] = record[0];
            } else {
                if (record[0].External_Id__c != undefined) lastVisitsummariesWithIds[record[0].External_Id__c] = record[0];
                if (record[0].Id != undefined) lastVisitsummariesWithIds[record[0].Id] = record[0];
                var rec = angular.copy(record[0]);
                rec.External_Id__c = undefined;
                lastVisitsummaries.push(rec);
            }
        }
    });
    if (currentVisitSummaries.length > 0) {
        visitSummaryrecords = currentVisitSummaries;
    } else {
        visitSummaryrecords = lastVisitsummaries;
    }
    if (img_query != '') {
        var images = SMARTSTORE.buildSmartQuerySpec("SELECT {Db_images:_soup} FROM {Db_images} " + img_query, 100);
        angular.forEach(images, function(record, key) {
            if (currentVisitSummariesWithIds[record[0].ParentId] != undefined) {
                if (queryCurrentImages == true) {
                    var uploadId = uploadsAfterOrderMap[currentVisitSummariesWithIds[record[0].ParentId].Upload__c] != undefined ? uploadsAfterOrderMap[currentVisitSummariesWithIds[record[0].ParentId].Upload__c].External_Id__c : currentVisitSummariesWithIds[record[0].ParentId].Upload__c;
                    if ($rootScope.currentvisitSummaryImages[$stateParams.order][uploadId] == undefined) $rootScope.currentvisitSummaryImages[$stateParams.order][uploadId] = [];
                    $rootScope.currentvisitSummaryImages[$stateParams.order][uploadId].push(record[0]);
                }
            } else {
                var uploadId = uploadsAfterOrderMap[lastVisitsummariesWithIds[record[0].ParentId].Upload__c] != undefined ? uploadsAfterOrderMap[lastVisitsummariesWithIds[record[0].ParentId].Upload__c].External_Id__c : lastVisitsummariesWithIds[record[0].ParentId].Upload__c;
                if ($scope.visitSummaryImages[uploadId] == undefined) $scope.visitSummaryImages[uploadId] = [];
                $scope.visitSummaryImages[uploadId].push(record[0]);
            }
        });
    }
    angular.forEach(visitSummaryrecords, function(record, key) {
        var UploadExtId = uploadsAfterOrderMap[record.Upload__c] != undefined ? uploadsAfterOrderMap[record.Upload__c].External_Id__c : record.Upload__c;
        if ($rootScope.uploadfields[$stateParams.order] == undefined) $rootScope.uploadfields[$stateParams.order] = [];
        record.oldStatus = record.Status__c;
        $rootScope.uploadfields[$stateParams.order][UploadExtId] = record;
    });
    var isActive = [],
        isPlanned = [],
        allOther = [],
        isCompleted = [];
    angular.forEach(uploadsAfterOrder, function(record, key) {
        if ($rootScope.currentvisitSummaryImages[$stateParams.order][record.External_Id__c] == undefined) $rootScope.currentvisitSummaryImages[$stateParams.order][record.External_Id__c] = [];
        $rootScope.currentvisitSummaryImages[$stateParams.order][record.External_Id__c] = $filter('orderBy')($rootScope.currentvisitSummaryImages[$stateParams.order][record.External_Id__c], '-Order', true);
        if ($scope.visitSummaryImages[record.External_Id__c] == undefined) $scope.visitSummaryImages[record.External_Id__c] = [];
        $scope.visitSummaryImages[record.External_Id__c] = $filter('orderBy')($scope.visitSummaryImages[record.External_Id__c], '-Order', true);
        if (record.Status__c == undefined) {
            allOther.push(record);
        } else if (record.Status__c == 'Active') {
            isActive.push(record);
        } else if (record.Status__c == 'Planned') {
            isPlanned.push(record);
        } else {
            isCompleted.push(record);
        }
        console.log('isCompleted isCompleted');
        console.log(isCompleted);
    });
    angular.forEach(allOther, function(record, key) {
        $scope.uploadslist.push(record);
    });
    angular.forEach(isActive, function(record, key) {
        $scope.uploadslist.push(record);
    });
    angular.forEach(isPlanned, function(record, key) {
        $scope.uploadslist.push(record);
    });
    angular.forEach(isCompleted, function(record, key) {
        $scope.uploadslist.push(record);
    });
    if ($rootScope.uploadfields[$stateParams.order] == undefined) {
        $rootScope.uploadfields[$stateParams.order] = [];
    }
    $scope.titlefields = [];
    angular.forEach($scope.currentPage.sections[0].fields, function(field, key) {
        if (field.Type__c == 'title') {
            $scope.titlefields.push(field.Field_API__c);
        }
    });
    if ($rootScope.currentvisitSummaryImages[$stateParams.order] != undefined) imagelist = $rootScope.currentvisitSummaryImages[$stateParams.order];
    $scope.prevSlide = function() {
        $ionicSlideBoxDelegate.previous();
    }
    $scope.nextSlide = function() {
        $ionicSlideBoxDelegate.next();
    }
    var currentVisitImagesSlider = $ionicSlideBoxDelegate.$getByHandle('currentImagesSlider');
    var lastVisitImagesSlider = $ionicSlideBoxDelegate.$getByHandle('lastImagesSlider');
    $scope.currentImagemodal = [];
    $ionicModal.fromTemplateUrl('current-image-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.currentImagemodal = modal;
    });
    $scope.openCurrentImageModal = function() {
        currentVisitImagesSlider.slide(0);
        $scope.currentImagemodal.show();
    };
    $scope.closeCurrentImageModal = function() {
        $scope.currentImagemodal.hide();
    };
    $scope.goToSlideCurrentImageModel = function(uploadId, index) {
            $scope.slideActive = uploadId;
            $scope.currentImagemodal.show();
            $timeout(function() {
                currentVisitImagesSlider.slide(index);
            }, 500);
        }
        // Called each time the slide changes
    $scope.slideCurrentImageIndex = 0;
    $scope.slideLastVisitImageIndex = 0;
    $scope.slideChangedCurrentImageModel = function(index) {
        $scope.slideIndex1 = index;
        $scope.slideCurrentImageIndex = index;
    };
    $ionicModal.fromTemplateUrl('image-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal = modal;
    });
    $scope.openModal = function() {
        lastVisitImagesSlider.slide(0);
        $scope.modal.show();
    };
    $scope.closeModal = function() {
        $scope.modal.hide();
    };
    $scope.goToSlide = function(uploadId, index) {
            $scope.slideActive = uploadId;
            $scope.modal.show();
            $timeout(function() {
                lastVisitImagesSlider.slide(index);
            }, 500);
        }
        // Called each time the slide changes
    $scope.slideChanged = function(index) {
        $scope.slideIndex = index;
        $scope.slideLastVisitImageIndex = index;
    };
    $scope.lastVisitPrevSlide = function() {
        lastVisitImagesSlider.previous();
    }
    $scope.lastVisitNextSlide = function() {
        lastVisitImagesSlider.next();
    }
    $scope.currentVisitPrevSlide = function() {
        currentVisitImagesSlider.previous();
    }
    $scope.currentVisitNextSlide = function() {
        currentVisitImagesSlider.next();
    }
    $scope.showWarning = function(fieldAPI, fieldValue, statuValue, uploadId) {
        if (fieldAPI == 'Status__c') {
            if (fieldValue != 'Placed' && statuValue != undefined && statuValue == 'Completed') {
                Splash.ShowToast('Status cannot be changed for a completed promotional activity.', 'long', 'bottom', function(a) {
                    console.log(a)
                });
                if ($rootScope.uploadfields[$stateParams.order][uploadId].oldStatus != undefined) {
                    $rootScope.uploadfields[$stateParams.order][uploadId].Status__c = $rootScope.uploadfields[$stateParams.order][uploadId].oldStatus;
                } else {
                    $rootScope.uploadfields[$stateParams.order][uploadId].oldStatus = undefined;
                    $rootScope.uploadfields[$stateParams.order][uploadId].Status__c = undefined
                }
            } else {
                $rootScope.uploadfields[$stateParams.order][uploadId].oldStatus = fieldValue;
            }
        }
    }
    $scope.captureSales = function(uploadId) {
        if (uploadsListWithExtId[uploadId] != undefined) {
            var record = uploadsListWithExtId[uploadId];
            var daysDiff = parseInt((new Date() - new Date(record.End_Date__c)) / (1000 * 60 * 60 * 24));
            var endDate = new Date(record.End_Date__c);
            endDate.setDate(endDate.getDate() + 14);
            if (daysDiff > 14 && daysDiff <= 60) {
                $location.path('app/capturesales/' + record.Id + '/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order);
            } else {
                $ionicLoading.hide();
                Splash.ShowToast('Sales Cannot be captured now. You can capture after' + $filter('date')(new Date(endDate), 'dd MMM yyyy'), 'long', 'bottom', function(a) {
                    console.log(a)
                });
            }
        } else {
            $ionicLoading.hide();
        }
    }
    $scope.isFieldHide = function(fieldAPI, fieldValue, DependentValue, visitId, thisValue) {
        return true;
    }
    $scope.takePicture = function(uploadId) {
        var options = {
            quality: 75,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.CAMERA,
            allowEdit: true,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 700,
            targetHeight: 500,
            saveToPhotoAlbum: false
        };
        $cordovaCamera.getPicture(options).then(function(imageData) {
            if (imagelist[uploadId] == undefined) imagelist[uploadId] = [];
            imagelist[uploadId].reverse();
            imagelist[uploadId].push({
                Body: imageData,
                IsDirty: true
            });
            var images = imagelist[uploadId].reverse();
            $rootScope.currentvisitSummaryImages[$stateParams.order][uploadId] = [];
            for (var i = 0; i < 10; i++) {
                if (images[i] != undefined) {
                    images[i].Order = i;
                    $rootScope.currentvisitSummaryImages[$stateParams.order][uploadId].push(images[i]);
                }
            }
        }, function(err) {});
    }
    var confirmToGoNext = null;
    $scope.checkCaptureSales = function() {
        var continueSaving = true;
        for (var i = 0; i < $scope.uploadslist.length; i++) {
            var record = uploadsListWithExtId[$scope.uploadslist[i].External_Id__c];
            var daysDiff = parseInt((new Date() - new Date(record.End_Date__c)) / (1000 * 60 * 60 * 24));
            var visitSummary = $rootScope.uploadfields[$scope.order][record.External_Id__c];
            if (continueSaving == true && daysDiff > 14 && daysDiff <= 60 && (visitSummary == undefined || visitSummary.Pre_Promotion_Brand_Sale__c == undefined || visitSummary.During_Promotion_Brand_Sale__c == undefined || visitSummary.Post_Promotion_Industry_Sale__c == undefined)) {
                confirmToGoNext = $ionicPopup.confirm({
                    cssClass: 'resumeConfirm',
                    title: '',
                    scope: $scope,
                    template: '<b>No sales data is filled for ' + record.Brand__c + '. Do you wish to continue?</b><br/><br/>Ensure all fields updated and then move next. <br/><br/><br/><div style="text-align:right; color:#38B6CB; margin-right: 18px; font-size: larger; font-weight: bold;"><span my-touchstart="cancelGotoNext()" style="padding-right: 35px;">NO</span> &nbsp;<span my-touchstart="goToNext()">YES</span> </div>',
                    buttons: []
                });
                confirmToGoNext.then(function(res) {});
                continueSaving = false;
                break;
            }
        }
        if (continueSaving == true) {
            $scope.goToNext();
        } else {
            $ionicLoading.hide();
        }
    }
    $scope.cancelGotoNext = function() {
        $ionicLoading.hide();
        if (confirmToGoNext != null) {
            confirmToGoNext.close();
        }
    }
    $scope.goToNext = function() {
        $timeout(function() {
            if (confirmToGoNext != null) {
                confirmToGoNext.close();
            }
            var visitSummaryRecords = [];
            var attachments = [];
            var uploadsUpdate = [];
            angular.forEach($scope.uploadslist, function(record, key) {
                var uploadExt = uploadsListWithExtId[record.External_Id__c];
                var visitSummary = {};
                visitSummary = $rootScope.uploadfields[$scope.order][record.External_Id__c];
                if (visitSummary != undefined) {
                    visitSummary.IsDirty = true;
                    if (visitSummary.Executed__c == 'Yes') {
                        visitSummary.Status__c = 'Executed on ' + $filter('date')(new Date(), 'dd MMM yyyy');
                    } else if (visitSummary.Executed__c == 'No') {
                        visitSummary.Status__c = 'Pending';
                    } else if (visitSummary.Executed__c == 'Rejected') {
                        visitSummary.Status__c = 'Rejected';
                    }
                    if (visitSummary.External_Id__c == undefined) {
                        visitSummary.Visit__c = $stateParams.visitId;
                        visitSummary.Account_Id__c = $stateParams.AccountId;
                        visitSummary.RecordTypeId = $scope.currentPage.sections[0].sectionDescription.Record_Type_Id__c;
                        visitSummary.External_Id__c = (new Date().getTime())+'-'+$stateParams.visitId+'-'+$window.Math.random() * 100000000;
                        visitSummary.Upload__c = record.External_Id__c;
                    }
                    if (visitSummary.Pre_Promotion_Brand_Sale__c != undefined && visitSummary.During_Promotion_Brand_Sale__c != undefined && visitSummary.Post_Promotion_Industry_Sale__c != undefined) {
                        record.Sales_Data_Captured__c = true;
                        record.Start_Date__c = $filter('date')(new Date(uploadExt.Start_Date__c), 'dd/MM/yyyy');
                        record.End_Date__c = $filter('date')(new Date(uploadExt.End_Date__c), 'dd/MM/yyyy');
                        record.IsDirty = true;
                        uploadsUpdate.push(record);
                    }
                    if (visitSummary.Pre_Promotion_End_Date__c != undefined) {
                        visitSummary.Pre_Promotion_End_Date__c = $filter('date')(visitSummary.Pre_Promotion_End_Date__c, 'dd/MM/yyyy');
                    }
                    if (visitSummary.Pre_Promotion_Start_Date__c != undefined) {
                        visitSummary.Pre_Promotion_Start_Date__c = $filter('date')(visitSummary.Pre_Promotion_Start_Date__c, 'dd/MM/yyyy');
                    }
                    if (visitSummary.During_Promotion_Start_Date__c != undefined) {
                        visitSummary.During_Promotion_Start_Date__c = $filter('date')(visitSummary.During_Promotion_Start_Date__c, 'dd/MM/yyyy');
                    }
                    if (visitSummary.During_Promotion_End_Date__c != undefined) {
                        visitSummary.During_Promotion_End_Date__c = $filter('date')(visitSummary.During_Promotion_End_Date__c, 'dd/MM/yyyy');
                    }
                    if (visitSummary.Post_Promotion_Start_Date__c != undefined) {
                        visitSummary.Post_Promotion_Start_Date__c = $filter('date')(visitSummary.Post_Promotion_Start_Date__c, 'dd/MM/yyyy');
                    }
                    if (visitSummary.Post_Promotion_End_Date__c != undefined) {
                        visitSummary.Post_Promotion_End_Date__c = $filter('date')(visitSummary.Post_Promotion_End_Date__c, 'dd/MM/yyyy');
                    }
                    $rootScope.uploadfields[$scope.order][record.External_Id__c] = visitSummary;
                    visitSummaryRecords.push(visitSummary);
                    angular.forEach($rootScope.currentvisitSummaryImages[$stateParams.order][record.External_Id__c], function(imageData, key) {
                        attachments.push({
                            ParentId: visitSummary['External_Id__c'],
                            Name: visitSummary['Upload__c'] + '.jpg',
                            Body: imageData.Body,
                            IsDirty: true,
                            Order: imageData.Order,
                            External_Id__c: 'Pridecall7--' + visitSummary['External_Id__c'] + imageData.Order
                        });
                    });
                }
            });
            if (visitSummaryRecords.length > 0) {
                MOBILEDATABASE_ADD($scope.currentPage.pageDescription.Master_Object__c, visitSummaryRecords, 'External_Id__c');
            }
            if (attachments.length > 0) {
                MOBILEDATABASE_ADD('Db_images', attachments, 'External_Id__c');
            }
            if (uploadsUpdate.length > 0) {
                MOBILEDATABASE_ADD('Upload__c', uploadsUpdate, 'External_Id__c');
            }
            var transaction = [{
                Account: $stateParams.AccountId,
                stage: $stateParams.order,
                stageValue: 'pridecall',
                Visit: $stateParams.visitId,
                status: 'saved',
                entryDate: today
            }];
            MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
            MOBILEDATABASE_ADD('Db_breadCrum', [{
                VisitedOrder: $stateParams.order,
                Name: 'PRIDECall7Ctrl'
            }], 'VisitedOrder');
            $rootScope.breadcrumClicked($stateParams.AccountId, $stateParams.visitId, $stateParams.order, parseInt($stateParams.order) + 1, 'fromCtrl');
        }, 100);
    }
    $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('PRIDECall7Ctrl', err.stack + '::' + err.message);
    }
}).controller('SalesTrackingCtrl', function($q, $log, $ionicLoading, $scope, $rootScope, $window, $stateParams, $location, $filter, $cordovaCamera, $ionicModal, $ionicSlideBoxDelegate, $timeout, SMARTSTORE, MOBILEDATABASE_ADD, WEBSERVICE_ERROR) {
    try {
        $rootScope.backText = 'Back';
        $rootScope.showBack = true;
        $rootScope.params = $stateParams;
        $rootScope.name = "SalesTrackingCtrl";
        $scope.breadActive = 'Promotions';
        $scope.order = $stateParams.order;
        $scope.outlet = SMARTSTORE.buildExactQuerySpec('Account', 'Id', $stateParams.AccountId);
        $scope.upload = SMARTSTORE.buildExactQuerySpec('Upload__c', 'Id', $stateParams.uploadId);
        $scope.upload = $scope.upload[0];
        if ($rootScope.uploadfields[$stateParams.order] == undefined) {
            $rootScope.uploadfields[$stateParams.order] = [];
        }
        if ($rootScope.uploadfields[$stateParams.order][$scope.upload.External_Id__c] == undefined) {
            $rootScope.uploadfields[$stateParams.order][$scope.upload.External_Id__c] = {};
        }
        var startDate = new Date($scope.upload.Start_Date__c);
        var endDate = new Date($scope.upload.End_Date__c);
        if ($rootScope.uploadfields[$stateParams.order][$scope.upload.External_Id__c].During_Promotion_Start_Date__c == undefined) $rootScope.uploadfields[$stateParams.order][$scope.upload.External_Id__c].During_Promotion_Start_Date__c = startDate;
        if ($rootScope.uploadfields[$stateParams.order][$scope.upload.External_Id__c].During_Promotion_End_Date__c == undefined) $rootScope.uploadfields[$stateParams.order][$scope.upload.External_Id__c].During_Promotion_End_Date__c = endDate;
        var preEndDate = new Date($scope.upload.Start_Date__c);
        preEndDate.setDate(preEndDate.getDate() - 1);
        var preStartDate = new Date($scope.upload.Start_Date__c);
        preStartDate.setDate(preStartDate.getDate() - 14);
        if ($rootScope.uploadfields[$stateParams.order][$scope.upload.External_Id__c].Pre_Promotion_Start_Date__c == undefined) $rootScope.uploadfields[$stateParams.order][$scope.upload.External_Id__c].Pre_Promotion_Start_Date__c = preStartDate;
        if ($rootScope.uploadfields[$stateParams.order][$scope.upload.External_Id__c].Pre_Promotion_End_Date__c == undefined) $rootScope.uploadfields[$stateParams.order][$scope.upload.External_Id__c].Pre_Promotion_End_Date__c = preEndDate;
        var postStartDate = new Date($scope.upload.End_Date__c);
        postStartDate.setDate(postStartDate.getDate() + 1);
        var postEndDate = new Date($scope.upload.End_Date__c);
        postEndDate.setDate(postEndDate.getDate() + 14);
        if ($rootScope.uploadfields[$stateParams.order][$scope.upload.External_Id__c].Post_Promotion_Start_Date__c == undefined) $rootScope.uploadfields[$stateParams.order][$scope.upload.External_Id__c].Post_Promotion_Start_Date__c = postStartDate;
        if ($rootScope.uploadfields[$stateParams.order][$scope.upload.External_Id__c].Post_Promotion_End_Date__c == undefined) $rootScope.uploadfields[$stateParams.order][$scope.upload.External_Id__c].Post_Promotion_End_Date__c = postEndDate;
        $ionicLoading.hide();
        $scope.restrictNumbersLength = function(fieldAPI, fieldValue) {
            var fValue = fieldValue.toString();
            if (fValue.length > 4) $rootScope.uploadfields[$stateParams.order][$scope.upload.External_Id__c][fieldAPI] = parseInt(fValue.substr(0, 4));
        }
        $scope.validateSales = function(period, brandSales, industrySales) {
            if (brandSales != undefined && brandSales != null && brandSales != '' && industrySales != undefined && industrySales != null && industrySales != '' && brandSales > industrySales) {
                Splash.ShowToast(period + ': Industry sales cannot be lower than UBL sales.', 'long', 'bottom', function(a) {
                    console.log(a)
                });
                if (period == 'During Promotion') {
                    $rootScope.uploadfields[$stateParams.order][$scope.upload.External_Id__c].During_Promotion_Brand_Sale__c = undefined;
                    $rootScope.uploadfields[$stateParams.order][$scope.upload.External_Id__c].During_Promotion_Industry_Sale__c = undefined;
                } else if (period == 'Pre Promotion') {
                    $rootScope.uploadfields[$stateParams.order][$scope.upload.External_Id__c].Pre_Promotion_Brand_Sale__c = undefined;
                    $rootScope.uploadfields[$stateParams.order][$scope.upload.External_Id__c].Pre_Promotion_Industry_Sale__c = undefined;
                } else {
                    $rootScope.uploadfields[$stateParams.order][$scope.upload.External_Id__c].Post_Promotion_Brand_Sale__c = undefined;
                    $rootScope.uploadfields[$stateParams.order][$scope.upload.External_Id__c].Post_Promotion_Industry_Sale__c = undefined;
                }
            }
        }
        $scope.cancelCaptureSales = function() {
            $rootScope.breadcrumClicked($stateParams.AccountId, $stateParams.visitId, parseInt($stateParams.order) - 1, $stateParams.order, 'fromCtrl');
        }
        $scope.saveSales = function() {
            var visitSummaries = [];
            var visitSummary = $rootScope.uploadfields[$stateParams.order][$scope.upload.External_Id__c];
            if (visitSummary != undefined) {
                if (visitSummary.Pre_Promotion_Brand_Sale__c == null) {
                    visitSummary.Pre_Promotion_Brand_Sale__c = undefined;
                }
                if (visitSummary.Pre_Promotion_Industry_Sale__c == null) {
                    visitSummary.Pre_Promotion_Industry_Sale__c = undefined;
                }
                if (visitSummary.During_Promotion_Brand_Sale__c == null) {
                    visitSummary.During_Promotion_Brand_Sale__c = undefined;
                }
                if (visitSummary.During_Promotion_Industry_Sale__c == null) {
                    visitSummary.During_Promotion_Industry_Sale__c = undefined;
                }
                if (visitSummary.Post_Promotion_Brand_Sale__c == null) {
                    visitSummary.Post_Promotion_Brand_Sale__c = undefined;
                }
                if (visitSummary.Post_Promotion_Industry_Sale__c == null) {
                    visitSummary.Post_Promotion_Industry_Sale__c = undefined;
                }
                if (visitSummary.Pre_Promotion_Brand_Sale__c != undefined && visitSummary.Pre_Promotion_Industry_Sale__c != undefined && visitSummary.Pre_Promotion_Industry_Sale__c < visitSummary.Pre_Promotion_Brand_Sale__c) {
                    Splash.ShowToast('Pre Promotion: Industry sales cannot be lower than UBL sales.', 'long', 'bottom', function(a) {
                        console.log(a)
                    });
                    $ionicLoading.hide();
                    $rootScope.uploadfields[$stateParams.order][$scope.upload.External_Id__c].Pre_Promotion_Industry_Sale__c = undefined;
                } else if (visitSummary.Post_Promotion_Brand_Sale__c != undefined && visitSummary.Post_Promotion_Industry_Sale__c != undefined && visitSummary.Post_Promotion_Industry_Sale__c < visitSummary.Post_Promotion_Brand_Sale__c) {
                    Splash.ShowToast('Post Promotion: Industry sales cannot be lower than UBL sales.', 'long', 'bottom', function(a) {
                        console.log(a)
                    });
                    $ionicLoading.hide();
                    $rootScope.uploadfields[$stateParams.order][$scope.upload.External_Id__c].Post_Promotion_Industry_Sale__c = undefined;
                } else if (visitSummary.During_Promotion_Brand_Sale__c != undefined && visitSummary.During_Promotion_Industry_Sale__c != undefined && visitSummary.During_Promotion_Industry_Sale__c < visitSummary.During_Promotion_Brand_Sale__c) {
                    Splash.ShowToast('During Promotion: Industry sales cannot be lower than UBL sales.', 'long', 'bottom', function(a) {
                        console.log(a)
                    });
                    $ionicLoading.hide();
                    $rootScope.uploadfields[$stateParams.order][$scope.upload.External_Id__c].During_Promotion_Industry_Sale__c = undefined;
                } else {
                    $rootScope.uploadfields[$stateParams.order][$scope.upload.External_Id__c] = visitSummary;
                    var visitSummaryRecords=[];
                    visitSummaryRecords.push(visitSummary);
                    MOBILEDATABASE_ADD('Visit_Summary__c', visitSummaryRecords, 'External_Id__c');
                    $rootScope.breadcrumClicked($stateParams.AccountId, $stateParams.visitId, parseInt($stateParams.order) - 1, $stateParams.order, 'fromCtrl');
                }
            }
        }
        $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('SalesTrackingCtrlError', err.stack + '::' + err.message);
    }
}).controller('PRIDECall8Ctrl', function($q, $log, $ionicLoading, $scope, $rootScope, $window, $stateParams, $location, $filter, $cordovaCamera, $ionicModal, $ionicSlideBoxDelegate, $timeout, $ionicPopup, SMARTSTORE, MOBILEDATABASE_ADD,WEBSERVICE_ERROR) {
    try{
    $rootScope.params = $stateParams;
    $rootScope.name = "PRIDECall8Ctrl";
    $scope.order = $stateParams.order;
    var today = $filter('date')(new Date(), 'dd/MM/yyyy');
    var transaction = [{
        Account: $stateParams.AccountId,
        stage: $stateParams.order,
        stageValue: 'PRIDECall8Ctrl',
        Visit: $stateParams.visitId,
        status: 'pending',
        entryDate: today
    }];
    MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
    MOBILEDATABASE_ADD('Db_breadCrum', [{
        VisitedOrder: $stateParams.order,
        Name: 'PRIDECall8Ctrl'
    }], 'VisitedOrder');
    $scope.outlet = SMARTSTORE.buildExactQuerySpec('Account', 'Id', $stateParams.AccountId);
    $scope.breadActive = $rootScope.processFlowLineItems[$stateParams.order].Name;
    $scope.currentPage = {};
    angular.forEach($rootScope.pages, function(record, key) {
        if (record.pageDescription.Id == $rootScope.processFlowLineItems[$stateParams.order].Page__c) {
            $scope.currentPage = record;
        }
    });
    $scope.uploadslist = [];
    var queryCurrentImages = false;
    if ($rootScope.currentvisitSummaryImages[$stateParams.order] == undefined) {
        $rootScope.currentvisitSummaryImages[$stateParams.order] = [];
        queryCurrentImages = true;
    }
    $scope.visitSummaryImages = [];
    var uploads = SMARTSTORE.buildExactQuerySpec('Upload__c', 'Account__c', $stateParams.AccountId);
    var lastVisit = SMARTSTORE.buildSmartQuerySpec("SELECT {Visit__c:_soup} FROM {Visit__c} WHERE {Visit__c:Account__c}='" + $stateParams.AccountId + "' ORDER BY {Visit__c:Check_In_DateTime__c} DESC", 2);
    var d = new Date();
    var currentDate = d.getTime();
    var query = '';
    var uploadsBeforeOrder = [];
    angular.forEach(uploads, function(record, key) {
        if (record.RecordTypeId == $scope.currentPage.sections[0].sectionDescription.Input_Record_Type_Id__c && record.Status__c != 'Inactive') {
            record.fields = $scope.currentPage.sections[0].fields;
            if (query == '') {
                query = '{Visit_Summary__c:Upload__c}="' + record.Id + '" OR {Visit_Summary__c:Upload__c}="' + record.External_Id__c + '"';
            } else {
                query = query + ' OR {Visit_Summary__c:Upload__c}="' + record.Id + '" OR {Visit_Summary__c:Upload__c}="' + record.External_Id__c + '"';
            }
            uploadsBeforeOrder.push(record);
        }
    });
    var uploadsAfterOrder = [];
    var uploadsAfterOrderMap = [];
    angular.forEach(uploadsBeforeOrder, function(record, key) {
        uploadsAfterOrderMap[record.Id] = record;
        uploadsAfterOrder.push(record);
    });
    var img_query = '';
    if (query != '') query = ' AND (' + query + ')';
    var visitSummaryrecords = [];
    var visits = '';
    if (lastVisit[0] != undefined) {
        visits = '{Visit_Summary__c:Visit__c}="' + lastVisit[0][0].Id + '" OR {Visit_Summary__c:Visit__c}="' + lastVisit[0][0].External_Id__c + '" ';
    }
    if (lastVisit[1] != undefined) {
        if (visits != '') {
            visits = visits + ' OR {Visit_Summary__c:Visit__c}="' + lastVisit[1][0].Id + '" OR {Visit_Summary__c:Visit__c}="' + lastVisit[1][0].External_Id__c + '" ';
        } else {
            visits = '{Visit_Summary__c:Visit__c}="' + lastVisit[1][0].Id + '" OR {Visit_Summary__c:Visit__c}="' + lastVisit[1][0].External_Id__c + '" ';
        }
    }
    if (visits != '') {
        visits = '(' + visits + ')';
    }
    var currentVisitSummaries = [],
        lastVisitsummaries = [],
        currentVisitSummariesWithIds = [],
        lastVisitsummariesWithIds = [];
    visitSummaryrecords = SMARTSTORE.buildSmartQuerySpec('SELECT {Visit_Summary__c:_soup} FROM {Visit_Summary__c} WHERE {Visit_Summary__c:Account_Id__c}="' + $stateParams.AccountId + '" AND' + visits + query, 100);
    angular.forEach(visitSummaryrecords, function(record, key) {
        if (record[0].RecordTypeId == $scope.currentPage.sections[0].sectionDescription.Record_Type_Id__c) {
            if (img_query == '') {
                if (record[0].Id != undefined) img_query = " WHERE {Db_images:ParentId}='" + record[0].External_Id__c + "' OR {Db_images:ParentId}='" + record[0].Id + "'";
                else img_query = " WHERE {Db_images:ParentId}='" + record[0].External_Id__c + "' ";
            } else {
                if (record[0].Id != undefined) img_query = img_query + " OR {Db_images:ParentId}='" + record[0].External_Id__c + "' OR {Db_images:ParentId}='" + record[0].Id + "'";
                else img_query = img_query + " OR {Db_images:ParentId}='" + record[0].External_Id__c + "' ";
            }
            if (record[0].Visit__c == lastVisit[0][0].Id || record[0].Visit__c == lastVisit[0][0].External_Id__c) {
                currentVisitSummaries.push(record[0]);
                if (record[0].External_Id__c != undefined) currentVisitSummariesWithIds[record[0].External_Id__c] = record[0];
                if (record[0].Id != undefined) currentVisitSummariesWithIds[record[0].Id] = record[0];
            } else {
                if (record[0].External_Id__c != undefined) lastVisitsummariesWithIds[record[0].External_Id__c] = record[0];
                if (record[0].Id != undefined) lastVisitsummariesWithIds[record[0].Id] = record[0];
                var rec = angular.copy(record[0]);
                rec.External_Id__c = undefined;
                lastVisitsummaries.push(rec);
            }
        }
    });
    if (currentVisitSummaries.length > 0) {
        visitSummaryrecords = currentVisitSummaries;
    } else {
        visitSummaryrecords = lastVisitsummaries;
    }
    if (img_query != '') {
        var images = SMARTSTORE.buildSmartQuerySpec("SELECT {Db_images:_soup} FROM {Db_images} " + img_query, 100);
        angular.forEach(images, function(record, key) {
            if (currentVisitSummariesWithIds[record[0].ParentId] != undefined) {
                if (queryCurrentImages == true) {
                    var uploadId = uploadsAfterOrderMap[currentVisitSummariesWithIds[record[0].ParentId].Upload__c] != undefined ? uploadsAfterOrderMap[currentVisitSummariesWithIds[record[0].ParentId].Upload__c].External_Id__c : currentVisitSummariesWithIds[record[0].ParentId].Upload__c;
                    if ($rootScope.currentvisitSummaryImages[$stateParams.order][uploadId] == undefined) $rootScope.currentvisitSummaryImages[$stateParams.order][uploadId] = [];
                    $rootScope.currentvisitSummaryImages[$stateParams.order][uploadId].push(record[0]);
                }
            } else {
                var uploadId = uploadsAfterOrderMap[lastVisitsummariesWithIds[record[0].ParentId].Upload__c] != undefined ? uploadsAfterOrderMap[lastVisitsummariesWithIds[record[0].ParentId].Upload__c].External_Id__c : lastVisitsummariesWithIds[record[0].ParentId].Upload__c;
                if ($scope.visitSummaryImages[uploadId] == undefined) $scope.visitSummaryImages[uploadId] = [];
                $scope.visitSummaryImages[uploadId].push(record[0]);
            }
        });
    }
    angular.forEach(visitSummaryrecords, function(record, key) {
        var UploadExtId = uploadsAfterOrderMap[record.Upload__c] != undefined ? uploadsAfterOrderMap[record.Upload__c].External_Id__c : record.Upload__c;
        if ($rootScope.uploadfields[$stateParams.order] == undefined) $rootScope.uploadfields[$stateParams.order] = [];
        if (record.Start_date__c != undefined && record.Start_date__c != '') {
            var startDate = '';
            if (record.Start_date__c.split('-')[0].length == 4) startDate = new Date(record.Start_date__c);
            else {
                startDate = new Date(record.Start_date__c.split('/')[1] + '/' + record.Start_date__c.split('/')[0] + '/' + record.Start_date__c.split('/')[2]);
            }
            record.Start_date__c = startDate;
        }
        if (record.End_date__c != undefined && record.End_date__c != '') {
            var endDate = '';
            if (record.End_date__c.split('-')[0].length == 4) endDate = new Date(record.End_date__c);
            else {
                endDate = new Date(record.End_date__c.split('/')[1] + '/' + record.End_date__c.split('/')[0] + '/' + record.End_date__c.split('/')[2]);
            }
            record.End_date__c = endDate;
        }
        $rootScope.uploadfields[$stateParams.order][UploadExtId] = record;
    });
    if ($rootScope.uploadfields[$stateParams.order] != undefined) {
        var isYes = [],
            isRejected = [],
            allOther = [],
            isNo = [];
        angular.forEach(uploadsAfterOrder, function(record, key) {
            if ($rootScope.currentvisitSummaryImages[$stateParams.order][record.External_Id__c] == undefined) $rootScope.currentvisitSummaryImages[$stateParams.order][record.External_Id__c] = [];
            $rootScope.currentvisitSummaryImages[$stateParams.order][record.External_Id__c] = $filter('orderBy')($rootScope.currentvisitSummaryImages[$stateParams.order][record.External_Id__c], '-Order', true);
            if ($scope.visitSummaryImages[record.External_Id__c] == undefined) $scope.visitSummaryImages[record.External_Id__c] = [];
            $scope.visitSummaryImages[record.External_Id__c] = $filter('orderBy')($scope.visitSummaryImages[record.External_Id__c], '-Order', true);
            if (record.End_Date__c != undefined && record.End_Date__c != '') {
                var endDate = '';
                if (record.End_Date__c.split('-')[0].length == 4) endDate = new Date(record.End_Date__c);
                else {
                    endDate = new Date(record.End_Date__c.split('/')[1] + '/' + record.End_Date__c.split('/')[0] + '/' + record.End_Date__c.split('/')[2]);
                }
                var competitorIntelEndDate = angular.copy(endDate);
                competitorIntelEndDate.setDate(competitorIntelEndDate.getDate() + 7);
                if (new Date() <= competitorIntelEndDate) {
                    $scope.uploadslist.push(record);
                }
            } else {
                $scope.uploadslist.push(record);
            }
        });
    } else {
        $rootScope.uploadfields[$stateParams.order] = [];
        $scope.uploadslist = uploadsAfterOrder;
    }
    $scope.titlefields = [];
    angular.forEach($scope.currentPage.sections[0].fields, function(field, key) {
        if (field.Type__c == 'title') {
            $scope.titlefields.push(field.Field_API__c);
        }
    });
    if ($rootScope.currentvisitSummaryImages[$stateParams.order] != undefined) imagelist = $rootScope.currentvisitSummaryImages[$stateParams.order];
    $scope.prevSlide = function() {
        $ionicSlideBoxDelegate.previous();
    }
    $scope.nextSlide = function() {
        $ionicSlideBoxDelegate.next();
    }
    var currentVisitImagesSlider = $ionicSlideBoxDelegate.$getByHandle('currentImagesSlider');
    var lastVisitImagesSlider = $ionicSlideBoxDelegate.$getByHandle('lastImagesSlider');
    $scope.currentImagemodal = [];
    $ionicModal.fromTemplateUrl('current-image-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.currentImagemodal = modal;
    });
    $scope.openCurrentImageModal = function() {
        currentVisitImagesSlider.slide(0);
        $scope.currentImagemodal.show();
    };
    $scope.closeCurrentImageModal = function() {
        $scope.currentImagemodal.hide();
    };
    $scope.goToSlideCurrentImageModel = function(uploadId, index) {
            $scope.slideActive = uploadId;
            $scope.currentImagemodal.show();
            $timeout(function() {
                currentVisitImagesSlider.slide(index);
            }, 500);
        }
        // Called each time the slide changes
    $scope.slideCurrentImageIndex = 0;
    $scope.slideLastVisitImageIndex = 0;
    $scope.slideChangedCurrentImageModel = function(index) {
        $scope.slideIndex1 = index;
        $scope.slideCurrentImageIndex = index;
    };
    $ionicModal.fromTemplateUrl('image-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal = modal;
    });
    $scope.openModal = function() {
        lastVisitImagesSlider.slide(0);
        $scope.modal.show();
    };
    $scope.closeModal = function() {
        $scope.modal.hide();
    };
    $scope.goToSlide = function(uploadId, index) {
            $scope.slideActive = uploadId;
            $scope.modal.show();
            $timeout(function() {
                lastVisitImagesSlider.slide(index);
            }, 500);
        }
        // Called each time the slide changes
    $scope.slideChanged = function(index) {
        $scope.slideIndex = index;
        $scope.slideLastVisitImageIndex = index;
    };
    $scope.lastVisitPrevSlide = function() {
        lastVisitImagesSlider.previous();
    }
    $scope.lastVisitNextSlide = function() {
        lastVisitImagesSlider.next();
    }
    $scope.currentVisitPrevSlide = function() {
        currentVisitImagesSlider.previous();
    }
    $scope.currentVisitNextSlide = function() {
        currentVisitImagesSlider.next();
    }
    $scope.validateDates = function(startDate, endDate, ExtId) {
        if (startDate != undefined && startDate != '' && endDate != undefined && endDate != '' && startDate > endDate) {
            Splash.ShowToast('The To: date must finish after the From: date.', 'long', 'bottom', function(a) {
                console.log(a)
            });
            $rootScope.uploadfields[$scope.order][ExtId].Start_date__c = undefined;
            $rootScope.uploadfields[$scope.order][ExtId].End_date__c = undefined;
        }
    }
    $scope.isFieldVisible = function(x, y) {
        $log.debug(x + '' + y);
        if (x != undefined || y != undefined) {
            if (x == undefined || x.indexOf(y) == -1) {
                return true;
            }
        } else return false;
    }
    $scope.remove = [];
    $scope.remove.Reason = '';
    $scope.modelDataCheck = function() {
        var _myEle = document.getElementById('coolerRemove_yes');
        if ($scope.remove.Reason == '') {
            _myEle.style.color = '#aaaaaa';
        } else {
            _myEle.style.color = '#38B6CB';
        }
    }
    $scope.cancelConfirmUploadDelete = function(removeUploadId) {
        $scope.confirmUploadDeletePopup.close();
        $scope.remove.Reason = '';
    }
    $scope.confirmUploadDelete = function(removeUploadId) {
        if ($scope.remove.Reason != '') {
            angular.forEach($scope.uploadslist, function(record, key) {
                var uploadsToDelete = [];
                if (record.External_Id__c == removeUploadId) {
                    record.Status__c = 'Inactive';
                    record.Reason__c = $scope.remove.Reason;
                    record.IsDirty = true;
                    uploadsToDelete.push(record);
                    MOBILEDATABASE_ADD('Upload__c', uploadsToDelete, 'External_Id__c');
                }
            });
            $scope.remove.Reason = '';
            $scope.confirmUploadDeletePopup.close();
            $location.path('app/container8/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date() + 'save');
        } else {
            Splash.ShowToast('Please Specify reason for removing ' + $scope.currentPage.pageDescription.Add_New_Label__c + ' ', 'long', 'bottom', function(a) {
                console.log(a)
            });
        }
    }
    $scope.removeUpload = function(removeUploadId) {
        $scope.remove.Reason = '';
        $scope.confirmUploadDeletePopup = $ionicPopup.confirm({
            template: 'Are you sure you want to remove the ' + $scope.currentPage.pageDescription.Add_New_Label__c.toLowerCase() + ' information for this outlet?  <br/><br/><br/><span><input placeholder="Enter the reason for removing the ' + $scope.currentPage.pageDescription.Add_New_Label__c + '" type="text" ng-change="modelDataCheck()" ng-model="remove.Reason"><div class="piller"></div><div class="piller piller_1"></div></span><br/> <div style="text-align:right; color:#38B6CB;margin-right: 10px;font-weight: bold;"><span my-touchstart="cancelConfirmUploadDelete(\'' + removeUploadId + '\')">NO</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id="coolerRemove_yes" class="coolerRemove_yes" ng-disabled="!remove.Reason" my-touchstart="confirmUploadDelete(\'' + removeUploadId + '\')">YES</span> </div>',
            cssClass: 'myConfirm',
            title: '',
            scope: $scope,
            buttons: []
        });
        $scope.confirmUploadDeletePopup.then(function(res) {});
    }
    $scope.takePicture = function(uploadId) {
        var options = {
            quality: 75,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.CAMERA,
            allowEdit: true,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 700,
            targetHeight: 500,
            saveToPhotoAlbum: false
        };
        $cordovaCamera.getPicture(options).then(function(imageData) {
            if (imagelist[uploadId] == undefined) imagelist[uploadId] = [];
            imagelist[uploadId].reverse();
            imagelist[uploadId].push({
                Body: imageData,
                IsDirty: true
            });
            var images = imagelist[uploadId].reverse();
            $rootScope.currentvisitSummaryImages[$stateParams.order][uploadId] = [];
            for (var i = 0; i < 10; i++) {
                if (images[i] != undefined) {
                    images[i].Order = i;
                    $rootScope.currentvisitSummaryImages[$stateParams.order][uploadId].push(images[i]);
                }
            }
        }, function(err) {});
    }
    $scope.addNew = function() {
        $location.path('app/AddNew8/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date());
    }
    $scope.goToNext = function() {
        $timeout(function() {
            var visitSummaryRecords = [];
            var attachments = [];
            var uploadsToUpdate = [];
            angular.forEach($scope.uploadslist, function(record, key) {
                var visitSummary = {};
                var uploadRecord = angular.copy(record);
                visitSummary = angular.copy($rootScope.uploadfields[$scope.order][record.External_Id__c]);
                if (visitSummary != undefined) {
                    visitSummary.IsDirty = true;
                    visitSummary.Start_date__c = $filter('date')(visitSummary.Start_date__c, 'dd/MM/yyyy');
                    visitSummary.End_date__c = $filter('date')(visitSummary.End_date__c, 'dd/MM/yyyy');
                    uploadRecord.Start_Date__c = $filter('date')(visitSummary.Start_date__c, 'dd/MM/yyyy');
                    uploadRecord.End_Date__c = $filter('date')(visitSummary.End_date__c, 'dd/MM/yyyy');
                    uploadRecord.IsDirty = true;
                    uploadsToUpdate.push(uploadRecord);
                    if (visitSummary.External_Id__c == undefined) {
                        visitSummary.Visit__c = $stateParams.visitId;
                        visitSummary.Account_Id__c = $stateParams.AccountId;
                        visitSummary.RecordTypeId = $scope.currentPage.sections[0].sectionDescription.Record_Type_Id__c;
                        visitSummary.External_Id__c = (new Date().getTime())+'-'+$stateParams.visitId+'-'+$window.Math.random() * 100000000;
                        visitSummary.Upload__c = record.External_Id__c;
                    }
                    $rootScope.uploadfields[$scope.order][record.External_Id__c] = visitSummary;
                    visitSummaryRecords.push(visitSummary);
                    angular.forEach($rootScope.currentvisitSummaryImages[$stateParams.order][record.External_Id__c], function(imageData, key) {
                        //$log.debug('image parent id' + visitSummary['External_Id__c']);
                        attachments.push({
                            ParentId: visitSummary['External_Id__c'],
                            Name: visitSummary['Upload__c'] + '.jpg',
                            Body: imageData.Body,
                            IsDirty: true,
                            Order: imageData.Order,
                            External_Id__c: 'Pridecall8--' + visitSummary['External_Id__c'] + imageData.Order
                        });
                    });
                }
            });
            if (visitSummaryRecords.length > 0) {
                MOBILEDATABASE_ADD($scope.currentPage.sections[0].sectionDescription.Object_API__c, visitSummaryRecords, 'External_Id__c');
            }
            if (attachments.length > 0) {
                MOBILEDATABASE_ADD('Db_images', attachments, 'External_Id__c');
            }
            if (uploadsToUpdate.length > 0) {
                MOBILEDATABASE_ADD('Upload__c', uploadsToUpdate, 'External_Id__c');
            }
            var transaction = [{
                Account: $stateParams.AccountId,
                stage: $stateParams.order,
                stageValue: 'PRIDECall8Ctrl',
                Visit: $stateParams.visitId,
                status: 'saved',
                entryDate: today
            }];
            MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
            MOBILEDATABASE_ADD('Db_breadCrum', [{
                VisitedOrder: $stateParams.order,
                Name: 'PRIDECall8Ctrl'
            }], 'VisitedOrder');
            $rootScope.breadcrumClicked($stateParams.AccountId, $stateParams.visitId, $stateParams.order, parseInt($stateParams.order) + 1, 'fromCtrl');
        }, 100);
    }
    $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('PRIDECall8Ctrl', err.stack + '::' + err.message);
    }
}).controller('AddNew8Ctrl', function($q, $log, $ionicLoading, $scope, $ionicPopup, $rootScope, $window, $stateParams, $location, $filter, $cordovaCamera, $ionicModal, $ionicSlideBoxDelegate, $timeout, SMARTSTORE, MOBILEDATABASE_ADD,WEBSERVICE_ERROR) {
    try{
    $rootScope.params = $stateParams;
    $rootScope.name = "AddNew8Ctrl";
    $scope.order = $stateParams.order;
    $scope.outlet = SMARTSTORE.buildExactQuerySpec('Account', 'Id', $stateParams.AccountId);
    $scope.breadActive = $rootScope.processFlowLineItems[$stateParams.order].Name;
    $scope.currentPage = {};
    $scope.upload = {};
    angular.forEach($rootScope.pages, function(record, key) {
        if (record.pageDescription.Id == $rootScope.processFlowLineItems[$stateParams.order].Page__c) {
            $scope.currentPage = record;
        }
    });
    $scope.addNewSectionFields = [];
    if ($scope.currentPage.sections.length > 1) {
        $scope.addNewSectionFields = $scope.currentPage.sections[1].fields;
    } else {
        $scope.addNewSectionFields = $scope.currentPage.sections[0].fields;
    }
    angular.forEach($scope.addNewSectionFields, function(field, key) {
        if (field.Field_API__c == 'Company__c') {
            var ubindex = field.pickListValues.indexOf('UBL');
            if (ubindex != -1) field.pickListValues.splice(ubindex, 1);
            var ownerindex = field.pickListValues.indexOf('Owner');
            if (ownerindex != -1) field.pickListValues.splice(ownerindex, 1);
        }
        field.fieldvalue = undefined;
    });
    $scope.showAdd = false;
    $scope.checkPreviousField = function(currentFieldAPI, fieldAPI) {
        if (currentFieldAPI != 'Company__c' && ($scope.upload[fieldAPI] == undefined || $scope.upload[fieldAPI] == '')) {
            var fieldName = '';
            angular.forEach($scope.addNewSectionFields, function(field, key) {
                if (field.Field_API__c == fieldAPI) fieldName = field.Name;
            });
            Splash.ShowToast('Please select the ' + fieldName + ' first.', 'long', 'bottom', function(a) {
                console.log(a)
            });
        }
    }
    $scope.checkValues = function(fieldAPI) {
        $scope.showAdd = false; 
        if (fieldAPI == 'Brand__c') {
            $scope.upload['Pack_type__c']=undefined;
            $scope.upload['Type_of_Activity__c']=undefined;
        } 
        if (fieldAPI == 'Pack_type__c') {
            $scope.upload['Type_of_Activity__c']=undefined;
        } 
        if (fieldAPI == 'Company__c') {
            var company = $scope.upload[fieldAPI];
            $scope.upload = {};
            $scope.upload.Company__c = company;
        } else {
            if ($scope.upload['Company__c'] != undefined && $scope.upload['Brand__c'] != undefined && $scope.upload['Pack_type__c'] != undefined && $scope.upload['Type_of_Activity__c'] != undefined && $scope.upload['Company__c'] != '' && $scope.upload['Brand__c'] != '' && $scope.upload['Pack_type__c'] != '' && $scope.upload['Type_of_Activity__c'] != '') {
                $scope.showAdd = true;
            }
        }
    }
    $scope.cancelAddNew = function() {
        angular.forEach($scope.addNewSectionFields, function(field, key) {
            field.fieldvalue = '';
        });
        $location.path('app/container8/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date() + 'save');
    }
    $scope.isFieldVisible = function(x, y) {
        $log.debug(x + '' + y);
        if (y == '') y = undefined;
        if (x != undefined || y != undefined) {
            if (x == undefined || x.indexOf(y) == -1) {
                return true;
            }
        } else return false;
    }
    $scope.saveNew = function() {
        $timeout(function() {
            var uploadsToInsert = [];
            if ($scope.showAdd) {
                $scope.upload.Account__c = $stateParams.AccountId;
                $scope.upload.RecordTypeId = $scope.currentPage.sections[0].sectionDescription.Input_Record_Type_Id__c;
                $scope.upload.External_Id__c = $stateParams.visitId + (new Date().getTime())+'_'+ $window.Math.random() * 10000000;
                $scope.upload.IsDirty = true;
                $rootScope.Uploads.push($scope.upload);
                uploadsToInsert.push($scope.upload);
                MOBILEDATABASE_ADD('Upload__c', uploadsToInsert, 'External_Id__c');
                $ionicLoading.hide();
                Splash.ShowToast('The ' + $scope.currentPage.pageDescription.Add_New_Label__c + ' has been added.', 'long', 'bottom', function(a) {
                    console.log(a)
                });
                $rootScope.resentlyAdded = true;
                $location.path('app/container8/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date() + 'save');
            } else {
                $ionicLoading.hide();
                Splash.ShowToast('Please select all fields.', 'long', 'bottom', function(a) {
                    console.log(a)
                });
            }
        }, 100);
    }
    $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('AddNew8Ctrl', err.stack + '::' + err.message);
    }
}).controller('DepotTemplate1Ctrl', function($q, $log, $ionicLoading, $scope, $rootScope, $filter, $stateParams, $location, $timeout, SMARTSTORE, MOBILEDATABASE_ADD) {
    try{
    $rootScope.backText = 'Back';
    $rootScope.showBack = true;
    $rootScope.params = $stateParams;
    $rootScope.name = "DepotTemplate1Ctrl";
    $scope.AccountId = $stateParams.AccountId;
    $scope.visitId = $stateParams.visitId;
    $scope.order = $stateParams.order;
    var today = $filter('date')(new Date(), 'dd/MM/yyyy');
    var transaction = [{
        Account: $stateParams.AccountId,
        stage: $stateParams.order,
        stageValue: 'DepotTemplate1Ctrl',
        Visit: $stateParams.visitId,
        status: 'pending',
        entryDate: today
    }];
    MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
    MOBILEDATABASE_ADD('Db_breadCrum', [{
        VisitedOrder: $stateParams.order,
        Name: 'DepotTemplate1Ctrl'
    }], 'VisitedOrder');
    $scope.outlet = SMARTSTORE.buildExactQuerySpec('Account', 'Id', $stateParams.AccountId);
    var visits = SMARTSTORE.buildSmartQuerySpec("SELECT {Visit__c:_soup} FROM {Visit__c} WHERE {Visit__c:Account__c}='" + $stateParams.AccountId + "' ORDER BY {Visit__c:Check_In_DateTime__c} DESC", 3);
    var pageId = $rootScope.processFlowLineItems[$stateParams.order].Page__c;
    $scope.breadActive = $rootScope.processFlowLineItems[$stateParams.order].Name;
    $scope.currentPage = {};
    angular.forEach($rootScope.pages, function(record, key) {
        if (record.pageDescription.Id == pageId) {
            $scope.currentPage = record;
        }
    });
    $scope.listViewFields = [];
    if ($scope.currentPage.sections[0] != undefined && $scope.currentPage.sections[0].fields != undefined) {
        $scope.listViewFields = $scope.currentPage.sections[0].fields;
    }
    var query = "";
    for (var i = 0; i < visits.length; i++) {
        if (visits[i][0].External_Id__c != undefined) query = query + " OR {" + $scope.currentPage.sections[0].sectionDescription.Input_Object_API__c + ":Visit__c} = '" + visits[i][0].External_Id__c + "'";
        if (visits[i][0].Id != undefined) query = query + " OR {" + $scope.currentPage.sections[0].sectionDescription.Input_Object_API__c + ":Visit__c} = '" + visits[i][0].Id + "'";
    }
    $scope.visitSummaries = [];
    var visitSummaryrecordsLocal = SMARTSTORE.buildSmartQuerySpec("SELECT {" + $scope.currentPage.sections[0].sectionDescription.Input_Object_API__c + ":_soup} FROM {" + $scope.currentPage.sections[0].sectionDescription.Input_Object_API__c + "} WHERE {" + $scope.currentPage.sections[0].sectionDescription.Input_Object_API__c + ":Account_Id__c} = '" + $stateParams.AccountId + "' AND {" + $scope.currentPage.sections[0].sectionDescription.Input_Object_API__c + ":RecordTypeId} = '" + $scope.currentPage.sections[0].sectionDescription.Record_Type_Id__c + "' AND  ({" + $scope.currentPage.sections[0].sectionDescription.Input_Object_API__c + ":Status__c} = 'Open'" + query + " )", 100);
    for (var i = 0; i < visitSummaryrecordsLocal.length; i++) {
        var record = visitSummaryrecordsLocal[i][0];
        if (record.Date__c != undefined) {
            var createdDate;
            if (record.Date__c.split('-')[0].length == 4) createdDate = new Date(record.Date__c);
            else {
                createdDate = new Date(record.Date__c.split('/')[1] + '/' + record.Date__c.split('/')[0] + '/' + record.Date__c.split('/')[2]);
            }
            record.Date__c = createdDate.getTime().toString();
        }
        $scope.visitSummaries.push(record);
    }
    $ionicLoading.hide();
    $scope.viewVisitSummary = function(index, visitSummaryId) {
        $timeout(function() {
            $location.path('app/DepotTemplate1View/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + visitSummaryId + '/' + index);
        }, 500);
    }
    $scope.addNewVisitSummary = function() {
        $timeout(function() {
            $location.path('app/DepotTemplate1Add/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order+'/'+new Date());
        }, 500);
    }
    $timeout(function() {
        $scope.scrollHeight = jQuery('.lastComplaints_feedback_depot > .scroll').height();
    }, 20);
    $scope.goToNext = function() {
        MOBILEDATABASE_ADD('Db_breadCrum', [{
            VisitedOrder: $stateParams.order,
            Name: 'DepotTemplate1Ctrl'
        }], 'VisitedOrder');
        if (!jQuery('.feedbackNext').hasClass('btn-disabled')) {
            $rootScope.breadcrumClicked($stateParams.AccountId, $stateParams.visitId, $stateParams.order, parseInt($stateParams.order) + 1, 'fromCtrl');
        } else {
            Splash.ShowToast('Please scroll for enabling next.', 'long', 'bottom', function(a) {
                console.log(a)
            });
            $ionicLoading.hide();
        }
    }
    $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('DepotTemplate1Ctrl', err.stack + '::' + err.message);
    }
}).controller('DepotTemplate1AddCtrl', function($q, $log, $ionicLoading, $scope, $rootScope, $filter, $window, $stateParams, $location, $cordovaCamera, $ionicModal, $timeout, $ionicSlideBoxDelegate, SMARTSTORE, MOBILEDATABASE_ADD,WEBSERVICE_ERROR) {
   try{
   $rootScope.backText = 'Back';
    $rootScope.showBack = true;
    $rootScope.params = $stateParams;
    $rootScope.name = "DepotTemplate1AddCtrl";
    $scope.outlet = SMARTSTORE.buildExactQuerySpec('Account', 'Id', $stateParams.AccountId);
    var visits = SMARTSTORE.buildSmartQuerySpec("SELECT {Visit__c:_soup} FROM {Visit__c} WHERE {Visit__c:Account__c}='" + $stateParams.AccountId + "' ORDER BY {Visit__c:Check_In_DateTime__c} DESC", 3);
    var pageId = $rootScope.processFlowLineItems[$stateParams.order].Page__c;
    $scope.breadActive = $rootScope.processFlowLineItems[$stateParams.order].Name;
    $scope.currentPage = {};
    angular.forEach($rootScope.pages, function(record, key) {
        if (record.pageDescription.Id == pageId) {
            $scope.currentPage = record;
        }
    });
    $scope.Fields = [];
    if ($scope.currentPage.sections[1] != undefined && $scope.currentPage.sections[1].fields != undefined) {
        $scope.Fields = $scope.currentPage.sections[1].fields;
    }
    $scope.visitSummary = {};
    angular.forEach($scope.Fields, function(record, key) {
        $scope.visitSummary[record.Field_API__c]=record.Default_Value__c;
    });
    $scope.visitSummary.Date__c = new Date();
    $scope.visitSummaryImages = [];
    $ionicLoading.hide();
    var imagelist = [];
    $ionicModal.fromTemplateUrl('image-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal = modal;
    });
    $scope.openModal = function() {
        $ionicSlideBoxDelegate.slide(0);
        $scope.modal.show();
    };
    $scope.closeModal = function() {
        $scope.modal.hide();
    };
    // Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
        $scope.modal.remove();
    });
    // Execute action on hide modal
    $scope.$on('modal.hide', function() {
        // Execute action
    });
    // Execute action on remove modal
    $scope.$on('modal.removed', function() {
        // Execute action
    });
    $scope.$on('modal.shown', function() {
        console.log('Modal is shown!');
    });
    // Call this functions if you need to manually control the slides
    $scope.next = function() {
        $ionicSlideBoxDelegate.next();
    };
    $scope.previous = function() {
        $ionicSlideBoxDelegate.previous();
    };
    $scope.goToSlide = function(index) {
            $scope.modal.show();
            $timeout(function() {
                $ionicSlideBoxDelegate.slide(index);
            }, 500)
        }
        // Called each time the slide changes
    $scope.slideCurrentImageIndex = 0;
    $scope.slideChanged = function(index) {
        $scope.slideIndex = index;
        $scope.slideCurrentImageIndex = index;
    };
    $scope.currentVisitPrevSlide = function() {
        $ionicSlideBoxDelegate.previous();
    }
    $scope.currentVisitNextSlide = function() {
        $ionicSlideBoxDelegate.next();
    }
    $scope.takePicture = function() {
        var options = {
            quality: 75,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.CAMERA,
            allowEdit: true,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 700,
            targetHeight: 500,
            saveToPhotoAlbum: false
        };
        $cordovaCamera.getPicture(options).then(function(imageData) {
            imagelist.reverse();
            imagelist.push({
                Body: imageData,
                IsDirty: true
            });
            $scope.visitSummaryImages = [];
            var images = imagelist.reverse();
            for (var i = 0; i < 10; i++) {
                if (images[i] != undefined) {
                    images[i].Order = i;
                    $scope.visitSummaryImages.push(images[i]);
                }
            }
        }, function(err) {});
    }
    $scope.isFieldVisible = function(x, y) {
        $log.debug(x + '' + y);
        if (x != undefined || y != undefined) {
            if (x == undefined || x.indexOf(y) == -1) {
                return true;
            }
        } else return false;
    }
    $scope.cancelVisitSummary = function() {
        $timeout(function() {
            $location.path('app/DepotTemplate1/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date());
        }, 500);
    }
    $scope.dateValidation = function(fieldAPI) {
        if (fieldAPI == 'Date__c' && $scope.visitSummary.Date__c != undefined && $scope.visitSummary.Date__c > new Date()) {
            Splash.ShowToast('Feedback logged date cannot be in the future.', 'long', 'bottom', function(a) {
                console.log(a)
            });
            $scope.visitSummary.Date__c = undefined;
        }
    }
    $scope.clearSubCategory = function(fieldAPI) {
        if (fieldAPI == 'Type__c') {
            $scope.visitSummary.Category__c = undefined;
            $scope.visitSummary.Sub_category__c = undefined;
        }
        if (fieldAPI == 'Category__c') {
            $scope.visitSummary.Sub_category__c = undefined;
        }
    }
    $scope.showAdd = false;
    $scope.checkValues = function() {
        $scope.showAdd = true;
        angular.forEach($scope.Fields, function(field, key) {
            if(($scope.visitSummary[field.Field_API__c]==undefined || $scope.visitSummary[field.Field_API__c]=='') && field.Type__c!='text' && field.Type__c!='textarea' && field.Field_API__c!='Sub_category__c'){
                $scope.showAdd=false;
            }
        });
            
    }
    $scope.saveVisitSummary = function() {
        $timeout(function() {
            if (!jQuery.isEmptyObject($scope.visitSummary) && $scope.showAdd==true) {
                var countOfFilledFields = 0;
                for (var i = 0; i < $scope.Fields.length; i++) {
                    if ($scope.visitSummary[$scope.Fields[i].Field_API__c] != undefined) {
                        countOfFilledFields++;
                    }
                }
                if (countOfFilledFields > 1) {
                    $scope.visitSummary.External_Id__c = (new Date().getTime())+'-'+$stateParams.visitId+'-'+$window.Math.random() * 100000000;
                    $scope.visitSummary.External_Id__c = $scope.visitSummary.External_Id__c.toString();
                    $scope.visitSummary.Visit__c = $stateParams.visitId;
                    $scope.visitSummary.Account_Id__c = $stateParams.AccountId;
                    $scope.visitSummary.RecordTypeId = $scope.currentPage.sections[0].sectionDescription.Record_Type_Id__c;
                    $scope.visitSummary.IsDirty = true;
                    if ($scope.visitSummary.Date__c != undefined) {
                        $scope.visitSummary.Date__c = $filter('date')($scope.visitSummary.Date__c, 'dd/MM/yyyy');
                    } else {
                        $scope.visitSummary.Date__c = $filter('date')(new Date(), 'dd/MM/yyyy');
                    }
                    $scope.visitSummary.Status__c = 'Open';
                    var visitSummaryrecords = [];
                    visitSummaryrecords.push($scope.visitSummary);
                    MOBILEDATABASE_ADD($scope.currentPage.sections[0].sectionDescription.Input_Object_API__c, visitSummaryrecords, 'External_Id__c');
                    var attachments = [];
                    angular.forEach($scope.visitSummaryImages, function(record, key) {
                        attachments.push({
                            ParentId: $scope.visitSummary['External_Id__c'],
                            Name: 'image' + key,
                            Body: record.Body,
                            IsDirty: true,
                            Order: record.Order,
                            Visit: $scope.visitSummary['Visit__c'],
                            External_Id__c: $scope.visitSummary['External_Id__c'] + '--' +(new Date().getTime())+'_'+ $window.Math.random() * 100000000
                        });
                    });
                    if (attachments.length > 0) {
                        MOBILEDATABASE_ADD('Db_images', attachments, 'External_Id__c');
                    }
                    $location.path('app/DepotTemplate1/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date());
                } else {
                    Splash.ShowToast('Please fill all the fields value.', 'long', 'bottom', function(a) {
                        console.log(a)
                    });
                    $ionicLoading.hide();
                }
            } else {
                Splash.ShowToast('Please fill all the fields value.', 'long', 'bottom', function(a) {
                    console.log(a)
                });
                $ionicLoading.hide();
            }
        }, 500);
    }
    $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('DepotTemplate1AddCtrl', err.stack + '::' + err.message);
    }
}).controller('DepotTemplate1ViewCtrl', function($q, $log, $ionicLoading, $scope, $rootScope, $window, $stateParams, $location, $filter, $cordovaCamera, $ionicModal, $ionicSlideBoxDelegate, $timeout, SMARTSTORE, MOBILEDATABASE_ADD,WEBSERVICE_ERROR) {
    try{
    $rootScope.backText = 'Back';
    $rootScope.showBack = true;
    $rootScope.params = $stateParams;
    $rootScope.name = "DepotTemplate1ViewCtrl";
    $scope.indexValue = parseInt($stateParams.indexValue) + 1;
    $scope.outlet = SMARTSTORE.buildExactQuerySpec('Account', 'Id', $stateParams.AccountId);
    var visits = SMARTSTORE.buildExactQuerySpec('Visit__c', 'External_Id__c', $stateParams.visitId);
    var pageId = $rootScope.processFlowLineItems[$stateParams.order].Page__c;
    $scope.breadActive = $rootScope.processFlowLineItems[$stateParams.order].Name;
    $scope.currentPage = {};
    angular.forEach($rootScope.pages, function(record, key) {
        if (record.pageDescription.Id == pageId) {
            $scope.currentPage = record;
        }
    });
    $scope.Fields = [];
    if ($scope.currentPage.sections[2] != undefined && $scope.currentPage.sections[2].fields != undefined) {
        $scope.Fields = $scope.currentPage.sections[2].fields;
    }
    $scope.visitSummaryImages = [];
    $scope.currentvisitSummaryImages = [];
    $scope.visitSummary = SMARTSTORE.buildExactQuerySpec($scope.currentPage.sections[0].sectionDescription.Input_Object_API__c, 'External_Id__c', $stateParams.visitSummaryId);
    var loggedDate;
    if ($scope.visitSummary[0] != undefined) {
        if ($scope.visitSummary[0].Date__c != undefined) {
            if ($scope.visitSummary[0].Date__c.split('-')[0].length == 4) loggedDate = new Date($scope.visitSummary[0].Date__c);
            else {
                loggedDate = new Date($scope.visitSummary[0].Date__c.split('/')[1] + '/' + $scope.visitSummary[0].Date__c.split('/')[0] + '/' + $scope.visitSummary[0].Date__c.split('/')[2]);
            }
            $scope.visitSummary[0].Date__c = $filter('date')(loggedDate, 'dd MMM yyyy');
        }
        if ($scope.visitSummary[0].Resolution_Date__c != undefined) {
            var createdDate;
            if ($scope.visitSummary[0].Resolution_Date__c.split('-')[0].length == 4) createdDate = new Date($scope.visitSummary[0].Resolution_Date__c);
            else {
                createdDate = new Date($scope.visitSummary[0].Resolution_Date__c.split('/')[1] + '/' + $scope.visitSummary[0].Resolution_Date__c.split('/')[0] + '/' + $scope.visitSummary[0].Resolution_Date__c.split('/')[2]);
            }
            if ($scope.visitSummary[0].Status__c == 'Resolved') {
                $scope.visitSummary[0].Resolution_Date__c = $filter('date')(createdDate, 'dd MMM yyyy');
            }
        }
        if ($scope.visitSummary[0].Id != undefined) {
            $scope.visitSummaryImages = SMARTSTORE.buildExactQuerySpec('Db_images', 'ParentId', $scope.visitSummary[0].Id);
        }
        angular.forEach(SMARTSTORE.buildExactQuerySpec('Db_images', 'ParentId', $scope.visitSummary[0].External_Id__c), function(record, key) {
            if (record.Visit != undefined && record.Visit == $stateParams.visitId) {
                $scope.currentvisitSummaryImages.push(record);
            } else {
                $scope.visitSummaryImages.push(record);
            }
        });
    }
    $ionicLoading.hide();
    $scope.isFieldVisible = function(x, y) {
        if (x != undefined && y != undefined) return x.indexOf(y) == -1;
        else return false;
    }
    $scope.cancelViewVisitSummary = function() {
        $timeout(function() {
            $location.path('app/DepotTemplate1/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date());
        }, 500);
    }
    var currentVisitImagesSlider = $ionicSlideBoxDelegate.$getByHandle('currentImagesSlider');
    var lastVisitImagesSlider = $ionicSlideBoxDelegate.$getByHandle('lastImagesSlider');
    $ionicModal.fromTemplateUrl('current-image-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.currentImagemodal = modal;
    });
    $scope.openCurrentImageModal = function() {
        currentVisitImagesSlider.slide(0);
        $scope.currentImagemodal.show();
    };
    $scope.closeCurrentImageModal = function() {
        $scope.currentImagemodal.hide();
    };
    $scope.goToSlideCurrentImageModel = function(index) {
            $scope.currentImagemodal.show();
            $timeout(function() {
                currentVisitImagesSlider.slide(index);
            }, 500);
        }
        // Called each time the slide changes
    $scope.slideCurrentImageIndex = 0;
    $scope.slideLastVisitImageIndex = 0;
    $scope.slideChangedCurrentImageModel = function(index) {
        $scope.slideIndex1 = index;
        $scope.slideCurrentImageIndex = index;
    };
    $ionicModal.fromTemplateUrl('image-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal = modal;
    });
    $scope.openModal = function() {
        lastVisitImagesSlider.slide(0);
        $scope.modal.show();
    };
    $scope.closeModal = function() {
        $scope.modal.hide();
    };
    $scope.goToSlide = function(index) {
            $scope.modal.show();
            $timeout(function() {
                lastVisitImagesSlider.slide(index);
            }, 500);
        }
        // Called each time the slide changes
    $scope.slideChanged = function(index) {
        $scope.slideIndex = index;
        $scope.slideLastVisitImageIndex = index;
    };
    $scope.lastVisitPrevSlide = function() {
        lastVisitImagesSlider.previous();
    }
    $scope.lastVisitNextSlide = function() {
        lastVisitImagesSlider.next();
    }
    $scope.currentVisitPrevSlide = function() {
        currentVisitImagesSlider.previous();
    }
    $scope.currentVisitNextSlide = function() {
            currentVisitImagesSlider.next();
        }
        // $scope.uploadslist = $filter('orderBy')($scope.currentvisitSummaryImages, 'Order',true);
    var imagelist = [];
    $scope.currentvisitSummaryImages = $filter('orderBy')($scope.currentvisitSummaryImages, '-Order', true);
    imagelist = $scope.currentvisitSummaryImages;
    //imagelist.reverse();
    $scope.takePicture = function() {
        if ($scope.visitSummary[0].Status__c != 'Resolved') {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.CAMERA,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 700,
                targetHeight: 500,
                saveToPhotoAlbum: false
            };
            $cordovaCamera.getPicture(options).then(function(imageData) {
                imagelist.reverse();
                imagelist.push({
                    Body: imageData,
                    IsDirty: true
                });
                $scope.currentvisitSummaryImages = [];
                var images = imagelist.reverse();
                for (var i = 0; i < 10; i++) {
                    if (images[i] != undefined) {
                        images[i].Order = i;
                        $scope.currentvisitSummaryImages.push(images[i]);
                    }
                }
            }, function(err) {});
        } else {
            Splash.ShowToast('You can not take the pictures of already resolved Feedback.', 'long', 'bottom', function(a) {
                console.log(a)
            });
        }
    }
    $scope.dateValidation = function(fieldAPI) {
        if (fieldAPI == 'Resolution_Date__c' && $scope.visitSummary[0].Resolution_Date__c != undefined && $scope.visitSummary[0].Resolution_Date__c > new Date()) {
            Splash.ShowToast('Resolution date cannot be in the future.', 'long', 'bottom', function(a) {
                console.log(a)
            });
            $scope.visitSummary[0].Resolution_Date__c = undefined;
        }
    }
    $scope.saveEditVisitSummary = function() {
        $timeout(function() {
            if ($scope.visitSummary[0].Is_it_resolved__c != undefined) {
                $scope.visitSummary[0].IsDirty = true;
                if ($scope.visitSummary[0].Is_it_resolved__c == 'Yes') {
                    $scope.visitSummary[0].Status__c = 'Resolved';
                    $scope.visitSummary[0].Opened_Visit__c = $scope.visitSummary[0].Visit__c;
                    $scope.visitSummary[0].Visit__c = $stateParams.visitId;
                    if ($scope.visitSummary[0].Resolution_Date__c == undefined) {
                        $scope.visitSummary[0].Resolution_Date__c = $filter('date')(new Date(), 'dd/MM/yyyy');
                    } else {
                        $scope.visitSummary[0].Resolution_Date__c = $filter('date')($scope.visitSummary[0].Resolution_Date__c, 'dd/MM/yyyy');
                    }
                }
                $scope.visitSummary[0].Date__c = $filter('date')(loggedDate, 'dd/MM/yyyy');
                if ($scope.visitSummary[0].Resolution_Date__c == undefined) {
                    $scope.visitSummary[0].Resolution_Date__c = $filter('date')(new Date(), 'dd/MM/yyyy');
                }
                MOBILEDATABASE_ADD($scope.currentPage.sections[0].sectionDescription.Input_Object_API__c, $scope.visitSummary, 'External_Id__c');
            }
            var attachments = [];
            angular.forEach($scope.currentvisitSummaryImages, function(record, key) {
                var attach = {
                    ParentId: $scope.visitSummary[0]['External_Id__c'],
                    Name: 'image' + key,
                    Body: record.Body,
                    IsDirty: true,
                    Order: record.Order,
                    Visit: $stateParams.visitId
                };
                if (record.External_Id__c == undefined) attach.External_Id__c = $scope.visitSummary[0]['External_Id__c'] + '--' + (new Date().getTime())+'_'+$window.Math.random() * 100000000;
                else {
                    attach.External_Id__c = record.External_Id__c;
                }
                attachments.push(attach);
            });
            if (attachments.length > 0) {
                MOBILEDATABASE_ADD('Db_images', attachments, 'External_Id__c');
            }
            $location.path('app/DepotTemplate1/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date());
        }, 500);
    }
    $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('DepotTemplate1ViewCtrl', err.stack + '::' + err.message);
    }
}).controller('DepotTemplate2Ctrl', function($q, $log, $ionicLoading, $scope, $rootScope, $window, $stateParams, $location, $filter, $cordovaCamera, $ionicModal, $ionicSlideBoxDelegate, $timeout, $ionicScrollDelegate, SMARTSTORE, MOBILEDATABASE_ADD, WEBSERVICE_ERROR) {
    try{
        $rootScope.params = $stateParams;
        $rootScope.name = "DepotTemplate2Ctrl";
        $scope.order = $stateParams.order;
        var today = $filter('date')(new Date(), 'dd/MM/yyyy');
        var transaction = [{
            Account: $stateParams.AccountId,
            stage: $stateParams.order,
            stageValue: 'PRIDECall5Ctrl',
            Visit: $stateParams.visitId,
            status: 'pending',
            entryDate: today
        }];
        MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
        MOBILEDATABASE_ADD('Db_breadCrum', [{
            VisitedOrder: $stateParams.order,
            Name: 'PRIDECall5Ctrl'
        }], 'VisitedOrder');
        $scope.outlet = SMARTSTORE.buildExactQuerySpec('Account', 'Id', $stateParams.AccountId);
        $scope.breadActive = $rootScope.processFlowLineItems[$stateParams.order].Name;
        $scope.currentPage = {};
        angular.forEach($rootScope.pages, function(record, key) {
            if (record.pageDescription.Id == $rootScope.processFlowLineItems[$stateParams.order].Page__c) {
                $scope.currentPage = record;
            }
        });
        var SKURecordTypeIds = [];
        var recordTypes = SMARTSTORE.buildExactQuerySpec('DB_RecordTypes', 'SobjectType', 'Brands_and_Products__c');
        angular.forEach(recordTypes, function(record, key) {
            SKURecordTypeIds[record.Name] = record.Id;
        });
        $scope.companyList = [];
        $scope.skuList ={};
        var stateRecord = SMARTSTORE.buildExactQuerySpec("Brands_and_Products__c","RecordTypeId",SKURecordTypeIds['State']);
        var skurecords= SMARTSTORE.buildExactQuerySpec("Brands_and_Products__c","Related_to_State__c",stateRecord[0].Id);
        var depotProducts = SMARTSTORE.buildExactQuerySpec("Product_Assignment__c","Outlet_Id__c",$stateParams.AccountId);
        var skuIdsList = [];
        angular.forEach(skurecords, function(record, key){
            skuIdsList[record.Id] = record;
        });
        if(depotProducts.length>0){
            angular.forEach(depotProducts, function(record, key){
                var sku = skuIdsList[record.Product_Id__c];
                if(sku!=undefined && record.Added__c== "true"){
                    if($scope.skuList[sku.Company__c]==undefined)
                        $scope.skuList[sku.Company__c] = [];
                    $scope.skuList[sku.Company__c][record.Order__c]=sku;
                    if($scope.companyList.indexOf(sku.Company__c)==-1){
                        $scope.companyList.push(sku.Company__c);
                    }
                }
                else{
                    if($scope.companyList.indexOf(sku.Company__c)==-1){
                        $scope.companyList.push(sku.Company__c);
                    }
                }
            });
        }else{
            var productOrderList = [];
            angular.forEach(skurecords, function(record, key){
               if(record.Used_in_Chatai__c==false){
                    // productOrderList.push({
                    //     Id: ($stateParams.AccountId + record['Id']),
                    //     Outlet_Id__c: $stateParams.AccountId,
                    //     Product_Id__c: record['Id'],
                    //     Product_Name__c: record['Sku_Code__c'],
                    //     Order__c: key + '',
                    //     Added__c: 'true'
                    // });
                    if($scope.skuList[record.Company__c]==undefined)
                        $scope.skuList[record.Company__c] = [];
                    $scope.skuList[record.Company__c].push(record);
                    if($scope.companyList.indexOf(record.Company__c)==-1){
                        $scope.companyList.push(record.Company__c);
                    }
               } 
            });
            angular.forEach($scope.companyList, function(company, ckey){
                var order = 0;
                angular.forEach($scope.skuList[company], function(sku, key){
                     productOrderList.push({
                        Id: ($stateParams.AccountId + sku['Id']),
                        Outlet_Id__c: $stateParams.AccountId,
                        Product_Id__c: sku['Id'],
                        Product_Name__c: sku['Sku_Code__c'],
                        Order__c: order+'',
                        Added__c: 'true'
                    });
                     order++;
                });
            });
             MOBILEDATABASE_ADD('Product_Assignment__c', productOrderList, 'Id');
        }
        if($rootScope.uploadfields[$stateParams.order]==undefined){
            $rootScope.uploadfields[$stateParams.order] = [];
            var currentVisit = SMARTSTORE.buildExactQuerySpec('Visit__c','External_Id__c',$stateParams.visitId);
            var currentVisitSummaries = SMARTSTORE.buildExactQuerySpec('Visit_Summary__c','Visit__c',$stateParams.visitId);
            if(currentVisitSummaries.length==0 && currentVisit[0].Id!=undefined){
                currentVisitSummaries = SMARTSTORE.buildExactQuerySpec('Visit_Summary__c','Visit__c',currentVisit[0].Id);
            }
            angular.forEach(currentVisitSummaries, function(record, key){
                if($scope.currentPage.sections[0].sectionDescription.Record_Type_Id__c==record.RecordTypeId)
                    $rootScope.uploadfields[$stateParams.order][record.Product__c] = record;
            });
        }
        $timeout(function() {
            $ionicSlideBoxDelegate.slide($scope.companyList.indexOf($stateParams.company));
            $ionicLoading.hide();
        }, 500);
        
        $scope.restrictKeyup = function(_skuId,_fieldApi, _inputValue) {
            var max = 4;
            if(_inputValue!=undefined){
                var mytext = _inputValue.toString();
                if (mytext.length >= max) {
                    mytext = mytext.substr(0, max);
                    $rootScope.uploadfields[$stateParams.order][_skuId][_fieldApi]= parseInt(mytext);                
                }
            }
        }
        $scope.focusCheckForStock = function(index, event) {        
            var _currentHeight = jQuery(event.target).parents('.row').height() - 4;     
            $ionicScrollDelegate.scrollTo(0, index * _currentHeight, true);     
        }       
        $scope.restrictKeydown = function(_event, _index, _brandName) {     
            $log.debug(_event.keyCode);     
            if (_event.which == 9 && ($scope.skuList[_brandName].length == (_index+1))) {       
                // document.getElementById("myinput_" + (parseInt(_index) + 1)).focus();        
                _event.preventDefault();        
            }       
        }
        // $scope.breadCompany = 'UB';      
        $scope.gotoProductOrder = function(company) {       
            $location.path('app/productOrder/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $scope.ProductStage + '/' + $stateParams.order + '/'+ company + '/' + new Date());
            // $scope.breadCompany = company; 
        }
        $scope.nextSlide = function() {
            $ionicSlideBoxDelegate.next();
            $ionicScrollDelegate.scrollTop();
        };
        $scope.prevSlide = function() {
            $ionicSlideBoxDelegate.previous();
            $ionicScrollDelegate.scrollTop();
        };
        
         $scope.goToNext = function() {
            var stokcRecordsToInsert = [];
            angular.forEach($scope.skuList, function(brandRecord, brandKey){
                angular.forEach(brandRecord, function(record, key){
                    var stockRecord = angular.copy($rootScope.uploadfields[$stateParams.order][record.Id]);
                    if(stockRecord!=undefined){
                        stockRecord.Product__c = record.Id;
                        stockRecord.Visit__c = $stateParams.visitId
                        if(stockRecord.External_Id__c == undefined)
                        stockRecord.External_Id__c = (new Date().getTime())+'-'+$stateParams.visitId+'-'+$window.Math.random() * 100000000+'DepotTemplate3';
                        stockRecord.RecordTypeId = $scope.currentPage.sections[0].sectionDescription.Record_Type_Id__c;
                        stockRecord.Account_Id__c = $stateParams.AccountId;
                        stokcRecordsToInsert.push(stockRecord);
                        $rootScope.uploadfields[$stateParams.order][record.Id] = stockRecord;
                    }
                });
            });    
            if(stokcRecordsToInsert.length>0){
                MOBILEDATABASE_ADD('Visit_Summary__c', stokcRecordsToInsert, 'External_Id__c');
            }
            $rootScope.breadcrumClicked($stateParams.AccountId, $stateParams.visitId, $stateParams.order, parseInt($stateParams.order) + 1, 'fromCtrl');
           
        }
        $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('DepotTemplate2Ctrl', err.stack + '::' + err.message);
    }
}).controller('DepotTemplate3Ctrl', function($q, $log, $ionicLoading, $scope, $rootScope, $window, $stateParams, $location, $filter, $cordovaCamera, $ionicModal, $ionicSlideBoxDelegate, $ionicScrollDelegate, $timeout, $ionicPopup, SMARTSTORE, MOBILEDATABASE_ADD,WEBSERVICE_ERROR) {
    try{
    $rootScope.params = $stateParams;
    $rootScope.name = "DepotTemplate3Ctrl";
    $scope.order = $stateParams.order;
    var today = $filter('date')(new Date(), 'dd/MM/yyyy');
    var transaction = [{
        Account: $stateParams.AccountId,
        stage: $stateParams.order,
        stageValue: 'PRIDECall6Ctrl',
        Visit: $stateParams.visitId,
        status: 'pending',
        entryDate: today
    }];
    var date = new Date();
    var firstDay =$filter('date')(new Date(date.getFullYear(), date.getMonth(), 1), 'dd/MM/yyyy') ;
    MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
    MOBILEDATABASE_ADD('Db_breadCrum', [{
        VisitedOrder: $stateParams.order,
        Name: 'PRIDECall6Ctrl'
    }], 'VisitedOrder');
    $scope.outlet = SMARTSTORE.buildExactQuerySpec('Account', 'Id', $stateParams.AccountId);
    $scope.breadActive = $rootScope.processFlowLineItems[$stateParams.order].Name;
    $scope.currentPage = {};
    angular.forEach($rootScope.pages, function(record, key) {
        if (record.pageDescription.Id == $rootScope.processFlowLineItems[$stateParams.order].Page__c) {
            $scope.currentPage = record;
        }
    });
    var SKURecordTypeIds = [];
    var recordTypes = SMARTSTORE.buildExactQuerySpec('DB_RecordTypes', 'SobjectType', 'Brands_and_Products__c');
    angular.forEach(recordTypes, function(record, key) {
        SKURecordTypeIds[record.Name] = record.Id;
    });
    $scope.companyList = [];
    $scope.skuList ={};
    $scope.skuList['Mild'] = [];
    $scope.skuList['Strong'] = [];
    $scope.skuList['Total'] = [];
    var stateRecord = SMARTSTORE.buildExactQuerySpec("Brands_and_Products__c","RecordTypeId",SKURecordTypeIds['State']);
    var skurecords= SMARTSTORE.buildExactQuerySpec("Brands_and_Products__c","Related_to_State__c",stateRecord[0].Id)
    var depotProducts = SMARTSTORE.buildExactQuerySpec("Product_Assignment__c","Outlet_Id__c",$stateParams.AccountId);
    var productOrderIds =  [];
    angular.forEach(depotProducts, function(value, key){
        if(value.Added__c=='true') productOrderIds[value.Product_Id__c]=value.Order__c;
    });
    angular.forEach(skurecords, function(record, key){
       if(record.Used_in_Chatai__c==false && productOrderIds[record.Id]!=undefined){
            if($scope.skuList[record.Company__c]==undefined)
                $scope.skuList[record.Company__c] = [];
            // $scope.skuList[record.Company__c].push(record);
            $scope.skuList[record.Company__c][productOrderIds[record.Id]]=record;
            $scope.skuList[record.Company__c]['Mild'] = [];
            $scope.skuList[record.Company__c]['Strong'] = [];
            $scope.skuList[record.Company__c]['Total'] = [];
            $scope.skuList['Mild'] = [];
            $scope.skuList['Strong'] = [];
            $scope.skuList['Total'] = [];
            if($scope.companyList.indexOf(record.Company__c)==-1){
                $scope.companyList.push(record.Company__c);
            }
       } 
    });
    var currentVisit = SMARTSTORE.buildExactQuerySpec('Visit__c','External_Id__c',$stateParams.visitId);
    var lastVisit = SMARTSTORE.buildSmartQuerySpec("SELECT {Visit__c:_soup} FROM {Visit__c} WHERE {Visit__c:Account__c}='" + $stateParams.AccountId + "' ORDER BY {Visit__c:Check_In_DateTime__c} DESC", 2);
    var lastVisitIds=[];
    if(lastVisit[1]!=undefined){
        lastVisitIds.push(lastVisit[1][0].External_Id__c+'');
        lastVisitIds.push(lastVisit[1][0].Id+'');
    }
    var currentVisitSummaries = SMARTSTORE.buildExactQuerySpec('Visit_Summary__c','Visit__c',$stateParams.visitId);
    if(currentVisitSummaries.length==0 && currentVisit[0].Id!=undefined){
        currentVisitSummaries = SMARTSTORE.buildExactQuerySpec('Visit_Summary__c','Visit__c',currentVisit[0].Id);
    }
    $scope.todaySales={};
    angular.forEach(currentVisitSummaries, function(record, key){
        if($scope.currentPage.sections[0].sectionDescription.Record_Type_Id__c==record.RecordTypeId)
            $scope.todaySales[record.Product__c] = record.Sales__c;
    });
    $scope.lastVisits=[];
    $scope.MTD={};
    $scope.prevVisitSales={};
    var lastVisits = SMARTSTORE.buildExactQuerySpec('Visit__c','Account__c',$stateParams.AccountId);  
    angular.forEach(lastVisits, function(record, key){
            $scope.lastVisits.push(record.External_Id__c+'');
            $scope.lastVisits.push(record.Id+'');
    });
    var lastVisitSales = SMARTSTORE.buildExactQuerySpec('Visit_Summary__c','Account_Id__c',$stateParams.AccountId);  
    angular.forEach(lastVisitSales, function(record, key){
        if($scope.currentPage.sections[0].sectionDescription.Record_Type_Id__c==record.RecordTypeId && $scope.lastVisits.indexOf(record.Visit__c)>-1 && record.Sales__c!=undefined && record.Sales__c!=''){
            if($scope.MTD[record.Product__c]==undefined)
                $scope.MTD[record.Product__c] = parseInt(record.Sales__c);
            else
                $scope.MTD[record.Product__c] = parseInt($scope.MTD[record.Product__c])+parseInt(record.Sales__c);
            if(lastVisitIds.indexOf(record.Visit__c)>-1)
                $scope.prevVisitSales[record.Product__c] = parseInt(record.Sales__c);
        }
    });

    angular.forEach($scope.skuList, function(brandRecord, brandKey){
        angular.forEach(brandRecord, function(record, key){
            if($scope.MTD[record.Id]!=undefined && $scope.MTD[record.Id]!=''){
                if(record.Segment__c=='Mild'){
                    if($scope.skuList['Mild']['MTD']==undefined)
                        $scope.skuList['Mild']['MTD']=parseInt($scope.MTD[record.Id]);
                    else
                        $scope.skuList['Mild']['MTD']=parseInt($scope.skuList['Mild']['MTD'])+parseInt($scope.MTD[record.Id]);
                    
                }
                if(record.Segment__c=='Strong'){
                    if($scope.skuList['Strong']['MTD']==undefined)
                        $scope.skuList['Strong']['MTD']=parseInt($scope.MTD[record.Id]);
                    else
                        $scope.skuList['Strong']['MTD']=parseInt($scope.skuList['Strong']['MTD'])+parseInt($scope.MTD[record.Id]);
                    
                }
                if($scope.skuList['Total']['MTD']==undefined)
                    $scope.skuList['Total']['MTD']=parseInt($scope.MTD[record.Id]);
                else
                    $scope.skuList['Total']['MTD']=parseInt($scope.skuList['Total']['MTD'])+parseInt($scope.MTD[record.Id]);
                
            }
            if($scope.todaySales[record.Id]!=undefined && $scope.todaySales[record.Id]!=''){
                if(record.Segment__c=='Mild'){
                    
                    if($scope.skuList['Mild']['Today']==undefined)
                        $scope.skuList['Mild']['Today']=parseInt($scope.todaySales[record.Id]);
                    else
                        $scope.skuList['Mild']['Today']=parseInt($scope.skuList['Mild']['Today'])+parseInt($scope.todaySales[record.Id]);
                    
                }
                if(record.Segment__c=='Strong'){
                    
                    if($scope.skuList['Strong']['Today']==undefined)
                        $scope.skuList['Strong']['Today']=parseInt($scope.todaySales[record.Id]);
                    else
                        $scope.skuList['Strong']['Today']=parseInt($scope.skuList['Strong']['Today'])+parseInt($scope.todaySales[record.Id]);
                    
                }
                
                if($scope.skuList['Total']['Today']==undefined)
                    $scope.skuList['Total']['Today']=parseInt($scope.todaySales[record.Id]);
                else
                    $scope.skuList['Total']['Today']=parseInt($scope.skuList['Total']['Today'])+parseInt($scope.todaySales[record.Id]);
                
            }
            if($scope.prevVisitSales[record.Id]!=undefined && $scope.prevVisitSales[record.Id]!=''){
                if(record.Segment__c=='Mild'){
                    
                    if($scope.skuList['Mild']['Pre_day']==undefined)
                        $scope.skuList['Mild']['Pre_day']=parseInt($scope.prevVisitSales[record.Id]);
                    else
                        $scope.skuList['Mild']['Pre_day']=parseInt($scope.skuList['Mild']['Pre_day'])+parseInt($scope.prevVisitSales[record.Id]);
                }
                if(record.Segment__c=='Strong'){
                    
                    if($scope.skuList['Strong']['Pre_day']==undefined)
                        $scope.skuList['Strong']['Pre_day']=parseInt($scope.prevVisitSales[record.Id]);
                    else
                        $scope.skuList['Strong']['Pre_day']=parseInt($scope.skuList['Strong']['Pre_day'])+parseInt($scope.prevVisitSales[record.Id]);
                }
                
                if($scope.skuList['Total']['Pre_day']==undefined)
                    $scope.skuList['Total']['Pre_day']=parseInt($scope.prevVisitSales[record.Id]);
                else
                    $scope.skuList['Total']['Pre_day']=parseInt($scope.skuList['Total']['Pre_day'])+parseInt($scope.prevVisitSales[record.Id]);
            }
            // end insdustry
            if($scope.MTD[record.Id]!=undefined && $scope.MTD[record.Id]!=''){
                if(record.Segment__c=='Mild'){
                    if($scope.skuList[brandKey]['Mild']['MTD']==undefined)
                        $scope.skuList[brandKey]['Mild']['MTD']=parseInt($scope.MTD[record.Id]);
                    else
                        $scope.skuList[brandKey]['Mild']['MTD']=parseInt($scope.skuList[brandKey]['Mild']['MTD'])+parseInt($scope.MTD[record.Id]);
                    
                }
                if(record.Segment__c=='Strong'){
                    if($scope.skuList[brandKey]['Strong']['MTD']==undefined)
                        $scope.skuList[brandKey]['Strong']['MTD']=parseInt($scope.MTD[record.Id]);
                    else
                        $scope.skuList[brandKey]['Strong']['MTD']=parseInt($scope.skuList[brandKey]['Strong']['MTD'])+parseInt($scope.MTD[record.Id]);
                    
                }
                if($scope.skuList[brandKey]['Total']['MTD']==undefined)
                    $scope.skuList[brandKey]['Total']['MTD']=parseInt($scope.MTD[record.Id]);
                else
                    $scope.skuList[brandKey]['Total']['MTD']=parseInt($scope.skuList[brandKey]['Total']['MTD'])+parseInt($scope.MTD[record.Id]);
                
            }
            if($scope.todaySales[record.Id]!=undefined && $scope.todaySales[record.Id]!=''){
                if(record.Segment__c=='Mild'){
                    
                    if($scope.skuList[brandKey]['Mild']['Today']==undefined)
                        $scope.skuList[brandKey]['Mild']['Today']=parseInt($scope.todaySales[record.Id]);
                    else
                        $scope.skuList[brandKey]['Mild']['Today']=parseInt($scope.skuList[brandKey]['Mild']['Today'])+parseInt($scope.todaySales[record.Id]);
                    
                }
                if(record.Segment__c=='Strong'){
                    
                    if($scope.skuList[brandKey]['Strong']['Today']==undefined)
                        $scope.skuList[brandKey]['Strong']['Today']=parseInt($scope.todaySales[record.Id]);
                    else
                        $scope.skuList[brandKey]['Strong']['Today']=parseInt($scope.skuList[brandKey]['Strong']['Today'])+parseInt($scope.todaySales[record.Id]);
                    
                }
                
                if($scope.skuList[brandKey]['Total']['Today']==undefined)
                    $scope.skuList[brandKey]['Total']['Today']=parseInt($scope.todaySales[record.Id]);
                else
                    $scope.skuList[brandKey]['Total']['Today']=parseInt($scope.skuList[brandKey]['Total']['Today'])+parseInt($scope.todaySales[record.Id]);
                
            }
            if($scope.prevVisitSales[record.Id]!=undefined && $scope.prevVisitSales[record.Id]!=''){
                if(record.Segment__c=='Mild'){
                    
                    if($scope.skuList[brandKey]['Mild']['Pre_day']==undefined)
                        $scope.skuList[brandKey]['Mild']['Pre_day']=parseInt($scope.prevVisitSales[record.Id]);
                    else
                        $scope.skuList[brandKey]['Mild']['Pre_day']=parseInt($scope.skuList[brandKey]['Mild']['Pre_day'])+parseInt($scope.prevVisitSales[record.Id]);
                }
                if(record.Segment__c=='Strong'){
                    
                    if($scope.skuList[brandKey]['Strong']['Pre_day']==undefined)
                        $scope.skuList[brandKey]['Strong']['Pre_day']=parseInt($scope.prevVisitSales[record.Id]);
                    else
                        $scope.skuList[brandKey]['Strong']['Pre_day']=parseInt($scope.skuList[brandKey]['Strong']['Pre_day'])+parseInt($scope.prevVisitSales[record.Id]);
                }
                
                if($scope.skuList[brandKey]['Total']['Pre_day']==undefined)
                    $scope.skuList[brandKey]['Total']['Pre_day']=parseInt($scope.prevVisitSales[record.Id]);
                else
                    $scope.skuList[brandKey]['Total']['Pre_day']=parseInt($scope.skuList[brandKey]['Total']['Pre_day'])+parseInt($scope.prevVisitSales[record.Id]);
            }
        });   
    });
    $scope.isNumber = angular.isNumber;
    $scope.parseInt=function(dbl){
        return parseInt(dbl);
    }
    $scope.goToSKU=function(company){
        $timeout(function(){
            $location.path('app/DepotTemplate2/' + $stateParams.AccountId + '/' +$stateParams.visitId + '/' + (parseInt($stateParams.order) - 1) + '/'+company+'/' + new Date());
        },500);
         
    }
    $scope.prevSlide = function() {
        $ionicSlideBoxDelegate.previous();
    }
    $scope.nextSlide = function() {
        $ionicSlideBoxDelegate.next();
    }
    $scope.uploadslist = [];

    $ionicLoading.hide();
    $scope.goToNext = function() {
        $timeout(function() {           
            $rootScope.breadcrumClicked($stateParams.AccountId, $stateParams.visitId, $stateParams.order, parseInt($stateParams.order) + 1, 'fromCtrl');
        }, 100);
    }
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('DepotTemplate3Ctrl', err.stack + '::' + err.message);
    }
}).controller('WholeSalerTemplate1Ctrl', function($q, $log, $ionicLoading, $scope, $rootScope, $filter, $stateParams, $location, $timeout, SMARTSTORE, MOBILEDATABASE_ADD) {
    try{
    $rootScope.backText = 'Back';
    $rootScope.showBack = true;
    $rootScope.params = $stateParams;
    $rootScope.name = "WholeSalerTemplate1Ctrl";
    $scope.AccountId = $stateParams.AccountId;
    $scope.visitId = $stateParams.visitId;
    $scope.order = $stateParams.order;
    var today = $filter('date')(new Date(), 'dd/MM/yyyy');
    var transaction = [{
        Account: $stateParams.AccountId,
        stage: $stateParams.order,
        stageValue: 'WholeSalerTemplate1Ctrl',
        Visit: $stateParams.visitId,
        status: 'pending',
        entryDate: today
    }];
    MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
    MOBILEDATABASE_ADD('Db_breadCrum', [{
        VisitedOrder: $stateParams.order,
        Name: 'WholeSalerTemplate1Ctrl'
    }], 'VisitedOrder');
    $scope.outlet = SMARTSTORE.buildExactQuerySpec('Account', 'Id', $stateParams.AccountId);
    var visits = SMARTSTORE.buildSmartQuerySpec("SELECT {Visit__c:_soup} FROM {Visit__c} WHERE {Visit__c:Account__c}='" + $stateParams.AccountId + "' ORDER BY {Visit__c:Check_In_DateTime__c} DESC", 3);
    var pageId = $rootScope.processFlowLineItems[$stateParams.order].Page__c;
    $scope.breadActive = $rootScope.processFlowLineItems[$stateParams.order].Name;
    $scope.currentPage = {};
    angular.forEach($rootScope.pages, function(record, key) {
        if (record.pageDescription.Id == pageId) {
            $scope.currentPage = record;
        }
    });
    $scope.listViewFields = [];
    if ($scope.currentPage.sections[0] != undefined && $scope.currentPage.sections[0].fields != undefined) {
        $scope.listViewFields = $scope.currentPage.sections[0].fields;
    }
    var query = "";
    for (var i = 0; i < visits.length; i++) {
        if (visits[i][0].External_Id__c != undefined) query = query + " OR {" + $scope.currentPage.sections[0].sectionDescription.Input_Object_API__c + ":Visit__c} = '" + visits[i][0].External_Id__c + "'";
        if (visits[i][0].Id != undefined) query = query + " OR {" + $scope.currentPage.sections[0].sectionDescription.Input_Object_API__c + ":Visit__c} = '" + visits[i][0].Id + "'";
    }
    $scope.visitSummaries = [];
    var visitSummaryrecordsLocal = SMARTSTORE.buildSmartQuerySpec("SELECT {" + $scope.currentPage.sections[0].sectionDescription.Input_Object_API__c + ":_soup} FROM {" + $scope.currentPage.sections[0].sectionDescription.Input_Object_API__c + "} WHERE {" + $scope.currentPage.sections[0].sectionDescription.Input_Object_API__c + ":Account_Id__c} = '" + $stateParams.AccountId + "' AND {" + $scope.currentPage.sections[0].sectionDescription.Input_Object_API__c + ":RecordTypeId} = '" + $scope.currentPage.sections[0].sectionDescription.Record_Type_Id__c + "' AND  ({" + $scope.currentPage.sections[0].sectionDescription.Input_Object_API__c + ":Status__c} = 'Open'" + query + " )", 100);
    for (var i = 0; i < visitSummaryrecordsLocal.length; i++) {
        var record = visitSummaryrecordsLocal[i][0];
        if (record.Date__c != undefined) {
            var createdDate;
            if (record.Date__c.split('-')[0].length == 4) createdDate = new Date(record.Date__c);
            else {
                createdDate = new Date(record.Date__c.split('/')[1] + '/' + record.Date__c.split('/')[0] + '/' + record.Date__c.split('/')[2]);
            }
            record.Date__c = createdDate.getTime().toString();
        }
        $scope.visitSummaries.push(record);
    }
    $ionicLoading.hide();
    $scope.viewVisitSummary = function(index, visitSummaryId) {
        $timeout(function() {
            $location.path('app/WholeSalerTemplate1View/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + visitSummaryId + '/' + index);
        }, 500);
    }
    $scope.addNewVisitSummary = function() {
        $timeout(function() {
            $location.path('app/WholeSalerTemplate1Add/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order+'/'+new Date());
        }, 500);
    }
    $timeout(function() {
        $scope.scrollHeight = jQuery('.lastComplaints_feedback_wholeSale > .scroll').height();
    }, 20);
    $scope.goToNext = function() {
        
        if (!jQuery('.feedbackNext').hasClass('btn-disabled')) {
            $rootScope.breadcrumClicked($stateParams.AccountId, $stateParams.visitId, $stateParams.order, parseInt($stateParams.order) + 1, 'fromCtrl');
        } else {
            Splash.ShowToast('Please scroll for enabling next.', 'long', 'bottom', function(a) {
                console.log(a)
            });
        }
    }
    $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('WholeSalerTemplate1Ctrl', err.stack + '::' + err.message);
    }
}).controller('WholeSalerTemplate1AddCtrl', function($q, $log, $ionicLoading, $scope, $rootScope, $filter, $window, $stateParams, $location, $cordovaCamera, $ionicModal, $timeout, $ionicSlideBoxDelegate, SMARTSTORE, MOBILEDATABASE_ADD,WEBSERVICE_ERROR) {
   try{
   $rootScope.backText = 'Back';
    $rootScope.showBack = true;
    $rootScope.params = $stateParams;
    $rootScope.name = "WholeSalerTemplate1AddCtrl";
    $scope.outlet = SMARTSTORE.buildExactQuerySpec('Account', 'Id', $stateParams.AccountId);
    var visits = SMARTSTORE.buildSmartQuerySpec("SELECT {Visit__c:_soup} FROM {Visit__c} WHERE {Visit__c:Account__c}='" + $stateParams.AccountId + "' ORDER BY {Visit__c:Check_In_DateTime__c} DESC", 3);
    var pageId = $rootScope.processFlowLineItems[$stateParams.order].Page__c;
    $scope.breadActive = $rootScope.processFlowLineItems[$stateParams.order].Name;
    $scope.currentPage = {};
    angular.forEach($rootScope.pages, function(record, key) {
        if (record.pageDescription.Id == pageId) {
            $scope.currentPage = record;
        }
    });
    $scope.Fields = [];
    if ($scope.currentPage.sections[1] != undefined && $scope.currentPage.sections[1].fields != undefined) {
        $scope.Fields = $scope.currentPage.sections[1].fields;
    }
    $scope.visitSummary = {};
    angular.forEach($scope.Fields, function(record, key) {
        $scope.visitSummary[record.Field_API__c]=record.Default_Value__c;
    });
    $scope.visitSummary.Date__c = new Date();
    $scope.visitSummaryImages = [];
    $ionicLoading.hide();
    var imagelist = [];
    $ionicModal.fromTemplateUrl('image-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal = modal;
    });
    $scope.openModal = function() {
        $ionicSlideBoxDelegate.slide(0);
        $scope.modal.show();
    };
    $scope.closeModal = function() {
        $scope.modal.hide();
    };
    // Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
        $scope.modal.remove();
    });
    // Execute action on hide modal
    $scope.$on('modal.hide', function() {
        // Execute action
    });
    // Execute action on remove modal
    $scope.$on('modal.removed', function() {
        // Execute action
    });
    $scope.$on('modal.shown', function() {
        console.log('Modal is shown!');
    });
    // Call this functions if you need to manually control the slides
    $scope.next = function() {
        $ionicSlideBoxDelegate.next();
    };
    $scope.previous = function() {
        $ionicSlideBoxDelegate.previous();
    };
    $scope.goToSlide = function(index) {
            $scope.modal.show();
            $timeout(function() {
                $ionicSlideBoxDelegate.slide(index);
            }, 500)
        }
        // Called each time the slide changes
    $scope.slideCurrentImageIndex = 0;
    $scope.slideChanged = function(index) {
        $scope.slideIndex = index;
        $scope.slideCurrentImageIndex = index;
    };
    $scope.currentVisitPrevSlide = function() {
        $ionicSlideBoxDelegate.previous();
    }
    $scope.currentVisitNextSlide = function() {
        $ionicSlideBoxDelegate.next();
    }
    $scope.takePicture = function() {
        var options = {
            quality: 75,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.CAMERA,
            allowEdit: true,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 700,
            targetHeight: 500,
            saveToPhotoAlbum: false
        };
        $cordovaCamera.getPicture(options).then(function(imageData) {
            imagelist.reverse();
            imagelist.push({
                Body: imageData,
                IsDirty: true
            });
            $scope.visitSummaryImages = [];
            var images = imagelist.reverse();
            for (var i = 0; i < 10; i++) {
                if (images[i] != undefined) {
                    images[i].Order = i;
                    $scope.visitSummaryImages.push(images[i]);
                }
            }
        }, function(err) {});
    }
    $scope.isFieldVisible = function(x, y) {
        $log.debug(x + '' + y);
        if (x != undefined || y != undefined) {
            if (x == undefined || x.indexOf(y) == -1) {
                return true;
            }
        } else return false;
    }
    $scope.cancelVisitSummary = function() {
        $timeout(function() {
            $location.path('app/WholeSalerTemplate1/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date());
        }, 500);
    }
    $scope.dateValidation = function(fieldAPI) {
        if (fieldAPI == 'Date__c' && $scope.visitSummary.Date__c != undefined && $scope.visitSummary.Date__c > new Date()) {
            Splash.ShowToast('Feedback logged date cannot be in the future.', 'long', 'bottom', function(a) {
                console.log(a)
            });
            $scope.visitSummary.Date__c = undefined;
        }
    }
    $scope.clearSubCategory = function(fieldAPI) {
        if (fieldAPI == 'Type__c') {
            $scope.visitSummary.Category__c = undefined;
            $scope.visitSummary.Sub_category__c = undefined;
        }
        if (fieldAPI == 'Category__c') {
            $scope.visitSummary.Sub_category__c = undefined;
        }
    }
    $scope.showAdd = false;
    $scope.checkValues = function() {
        $scope.showAdd = true;
        angular.forEach($scope.Fields, function(field, key) {
            if(($scope.visitSummary[field.Field_API__c]==undefined || $scope.visitSummary[field.Field_API__c]=='') && field.Type__c!='text' && field.Type__c!='textarea' && field.Field_API__c!='Sub_category__c'){
                $scope.showAdd=false;
            }
        });
            
    }
    $scope.saveVisitSummary = function() {
        $timeout(function() {
            if (!jQuery.isEmptyObject($scope.visitSummary) && $scope.showAdd==true) {
                var countOfFilledFields = 0;
                for (var i = 0; i < $scope.Fields.length; i++) {
                    if ($scope.visitSummary[$scope.Fields[i].Field_API__c] != undefined) {
                        countOfFilledFields++;
                    }
                }
                if (countOfFilledFields > 1) {
                    $scope.visitSummary.External_Id__c =(new Date().getTime())+'-'+$stateParams.visitId+'-'+$window.Math.random() * 100000000;
                    $scope.visitSummary.External_Id__c = $scope.visitSummary.External_Id__c.toString();
                    $scope.visitSummary.Visit__c = $stateParams.visitId;
                    $scope.visitSummary.Account_Id__c = $stateParams.AccountId;
                    $scope.visitSummary.RecordTypeId = $scope.currentPage.sections[0].sectionDescription.Record_Type_Id__c;
                    $scope.visitSummary.IsDirty = true;
                    if ($scope.visitSummary.Date__c != undefined) {
                        $scope.visitSummary.Date__c = $filter('date')($scope.visitSummary.Date__c, 'dd/MM/yyyy');
                    } else {
                        $scope.visitSummary.Date__c = $filter('date')(new Date(), 'dd/MM/yyyy');
                    }
                    $scope.visitSummary.Status__c = 'Open';
                    var visitSummaryrecords = [];
                    visitSummaryrecords.push($scope.visitSummary);
                    MOBILEDATABASE_ADD($scope.currentPage.sections[0].sectionDescription.Input_Object_API__c, visitSummaryrecords, 'External_Id__c');
                    var attachments = [];
                    angular.forEach($scope.visitSummaryImages, function(record, key) {
                        attachments.push({
                            ParentId: $scope.visitSummary['External_Id__c'],
                            Name: 'image' + key,
                            Body: record.Body,
                            IsDirty: true,
                            Order: record.Order,
                            Visit: $scope.visitSummary['Visit__c'],
                            External_Id__c: $scope.visitSummary['External_Id__c'] + '--' + (new Date().getTime())+'_'+$window.Math.random() * 100000000
                        });
                    });
                    if (attachments.length > 0) {
                        MOBILEDATABASE_ADD('Db_images', attachments, 'External_Id__c');
                    }
                    $location.path('app/WholeSalerTemplate1/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date());
                } else {
                    Splash.ShowToast('Please fill all the fields value.', 'long', 'bottom', function(a) {
                        console.log(a)
                    });
                    $ionicLoading.hide();
                }
            } else {
                Splash.ShowToast('Please fill all the fields value.', 'long', 'bottom', function(a) {
                    console.log(a)
                });
                $ionicLoading.hide();
            }
        }, 500);
    }
    $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('WholeSalerTemplate1AddCtrl', err.stack + '::' + err.message);
    }
}).controller('WholeSalerTemplate1ViewCtrl', function($q, $log, $ionicLoading, $scope, $rootScope, $window, $stateParams, $location, $filter, $cordovaCamera, $ionicModal, $ionicSlideBoxDelegate, $timeout, SMARTSTORE, MOBILEDATABASE_ADD,WEBSERVICE_ERROR) {
    try{
    $rootScope.backText = 'Back';
    $rootScope.showBack = true;
    $rootScope.params = $stateParams;
    $rootScope.name = "DepotTemplate1ViewCtrl";
    $scope.indexValue = parseInt($stateParams.indexValue) + 1;
    $scope.outlet = SMARTSTORE.buildExactQuerySpec('Account', 'Id', $stateParams.AccountId);
    var visits = SMARTSTORE.buildExactQuerySpec('Visit__c', 'External_Id__c', $stateParams.visitId);
    var pageId = $rootScope.processFlowLineItems[$stateParams.order].Page__c;
    $scope.breadActive = $rootScope.processFlowLineItems[$stateParams.order].Name;
    $scope.currentPage = {};
    angular.forEach($rootScope.pages, function(record, key) {
        if (record.pageDescription.Id == pageId) {
            $scope.currentPage = record;
        }
    });
    $scope.Fields = [];
    if ($scope.currentPage.sections[2] != undefined && $scope.currentPage.sections[2].fields != undefined) {
        $scope.Fields = $scope.currentPage.sections[2].fields;
    }
    $scope.visitSummaryImages = [];
    $scope.currentvisitSummaryImages = [];
    $scope.visitSummary = SMARTSTORE.buildExactQuerySpec($scope.currentPage.sections[0].sectionDescription.Input_Object_API__c, 'External_Id__c', $stateParams.visitSummaryId);
    var loggedDate;
    if ($scope.visitSummary[0] != undefined) {
        if ($scope.visitSummary[0].Date__c != undefined) {
            if ($scope.visitSummary[0].Date__c.split('-')[0].length == 4) loggedDate = new Date($scope.visitSummary[0].Date__c);
            else {
                loggedDate = new Date($scope.visitSummary[0].Date__c.split('/')[1] + '/' + $scope.visitSummary[0].Date__c.split('/')[0] + '/' + $scope.visitSummary[0].Date__c.split('/')[2]);
            }
            $scope.visitSummary[0].Date__c = $filter('date')(loggedDate, 'dd MMM yyyy');
        }
        if ($scope.visitSummary[0].Resolution_Date__c != undefined) {
            var createdDate;
            if ($scope.visitSummary[0].Resolution_Date__c.split('-')[0].length == 4) createdDate = new Date($scope.visitSummary[0].Resolution_Date__c);
            else {
                createdDate = new Date($scope.visitSummary[0].Resolution_Date__c.split('/')[1] + '/' + $scope.visitSummary[0].Resolution_Date__c.split('/')[0] + '/' + $scope.visitSummary[0].Resolution_Date__c.split('/')[2]);
            }
            if ($scope.visitSummary[0].Status__c == 'Resolved') {
                $scope.visitSummary[0].Resolution_Date__c = $filter('date')(createdDate, 'dd MMM yyyy');
            }
        }
        if ($scope.visitSummary[0].Id != undefined) {
            $scope.visitSummaryImages = SMARTSTORE.buildExactQuerySpec('Db_images', 'ParentId', $scope.visitSummary[0].Id);
        }
        angular.forEach(SMARTSTORE.buildExactQuerySpec('Db_images', 'ParentId', $scope.visitSummary[0].External_Id__c), function(record, key) {
            if (record.Visit != undefined && record.Visit == $stateParams.visitId) {
                $scope.currentvisitSummaryImages.push(record);
            } else {
                $scope.visitSummaryImages.push(record);
            }
        });
    }
    $ionicLoading.hide();
    $scope.isFieldVisible = function(x, y) {
        if (x != undefined && y != undefined) return x.indexOf(y) == -1;
        else return false;
    }
    $scope.cancelViewVisitSummary = function() {
        $timeout(function() {
            $location.path('app/WholeSalerTemplate1/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date());
        }, 500);
    }
    var currentVisitImagesSlider = $ionicSlideBoxDelegate.$getByHandle('currentImagesSlider');
    var lastVisitImagesSlider = $ionicSlideBoxDelegate.$getByHandle('lastImagesSlider');
    $ionicModal.fromTemplateUrl('current-image-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.currentImagemodal = modal;
    });
    $scope.openCurrentImageModal = function() {
        currentVisitImagesSlider.slide(0);
        $scope.currentImagemodal.show();
    };
    $scope.closeCurrentImageModal = function() {
        $scope.currentImagemodal.hide();
    };
    $scope.goToSlideCurrentImageModel = function(index) {
            $scope.currentImagemodal.show();
            $timeout(function() {
                currentVisitImagesSlider.slide(index);
            }, 500);
        }
        // Called each time the slide changes
    $scope.slideCurrentImageIndex = 0;
    $scope.slideLastVisitImageIndex = 0;
    $scope.slideChangedCurrentImageModel = function(index) {
        $scope.slideIndex1 = index;
        $scope.slideCurrentImageIndex = index;
    };
    $ionicModal.fromTemplateUrl('image-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal = modal;
    });
    $scope.openModal = function() {
        lastVisitImagesSlider.slide(0);
        $scope.modal.show();
    };
    $scope.closeModal = function() {
        $scope.modal.hide();
    };
    $scope.goToSlide = function(index) {
            $scope.modal.show();
            $timeout(function() {
                lastVisitImagesSlider.slide(index);
            }, 500);
        }
        // Called each time the slide changes
    $scope.slideChanged = function(index) {
        $scope.slideIndex = index;
        $scope.slideLastVisitImageIndex = index;
    };
    $scope.lastVisitPrevSlide = function() {
        lastVisitImagesSlider.previous();
    }
    $scope.lastVisitNextSlide = function() {
        lastVisitImagesSlider.next();
    }
    $scope.currentVisitPrevSlide = function() {
        currentVisitImagesSlider.previous();
    }
    $scope.currentVisitNextSlide = function() {
            currentVisitImagesSlider.next();
        }
        // $scope.uploadslist = $filter('orderBy')($scope.currentvisitSummaryImages, 'Order',true);
    var imagelist = [];
    $scope.currentvisitSummaryImages = $filter('orderBy')($scope.currentvisitSummaryImages, '-Order', true);
    imagelist = $scope.currentvisitSummaryImages;
    //imagelist.reverse();
    $scope.takePicture = function() {
        if ($scope.visitSummary[0].Status__c != 'Resolved') {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.CAMERA,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 700,
                targetHeight: 500,
                saveToPhotoAlbum: false
            };
            $cordovaCamera.getPicture(options).then(function(imageData) {
                imagelist.reverse();
                imagelist.push({
                    Body: imageData,
                    IsDirty: true
                });
                $scope.currentvisitSummaryImages = [];
                var images = imagelist.reverse();
                for (var i = 0; i < 10; i++) {
                    if (images[i] != undefined) {
                        images[i].Order = i;
                        $scope.currentvisitSummaryImages.push(images[i]);
                    }
                }
            }, function(err) {});
        } else {
            Splash.ShowToast('You can not take the pictures of already resolved Feedback.', 'long', 'bottom', function(a) {
                console.log(a)
            });
        }
    }
    $scope.dateValidation = function(fieldAPI) {
        if (fieldAPI == 'Resolution_Date__c' && $scope.visitSummary[0].Resolution_Date__c != undefined && $scope.visitSummary[0].Resolution_Date__c > new Date()) {
            Splash.ShowToast('Resolution date cannot be in the future.', 'long', 'bottom', function(a) {
                console.log(a)
            });
            $scope.visitSummary[0].Resolution_Date__c = undefined;
        }
    }
    $scope.saveEditVisitSummary = function() {
        $timeout(function() {
            if ($scope.visitSummary[0].Is_it_resolved__c != undefined) {
                $scope.visitSummary[0].IsDirty = true;
                if ($scope.visitSummary[0].Is_it_resolved__c == 'Yes') {
                    $scope.visitSummary[0].Status__c = 'Resolved';
                    $scope.visitSummary[0].Opened_Visit__c = $scope.visitSummary[0].Visit__c;
                    $scope.visitSummary[0].Visit__c = $stateParams.visitId;
                    if ($scope.visitSummary[0].Resolution_Date__c == undefined) {
                        $scope.visitSummary[0].Resolution_Date__c = $filter('date')(new Date(), 'dd/MM/yyyy');
                    } else {
                        $scope.visitSummary[0].Resolution_Date__c = $filter('date')($scope.visitSummary[0].Resolution_Date__c, 'dd/MM/yyyy');
                    }
                }
                $scope.visitSummary[0].Date__c = $filter('date')(loggedDate, 'dd/MM/yyyy');
                if ($scope.visitSummary[0].Resolution_Date__c == undefined) {
                    $scope.visitSummary[0].Resolution_Date__c = $filter('date')(new Date(), 'dd/MM/yyyy');
                }
                MOBILEDATABASE_ADD($scope.currentPage.sections[0].sectionDescription.Input_Object_API__c, $scope.visitSummary, 'External_Id__c');
            }
            var attachments = [];
            angular.forEach($scope.currentvisitSummaryImages, function(record, key) {
                var attach = {
                    ParentId: $scope.visitSummary[0]['External_Id__c'],
                    Name: 'image' + key,
                    Body: record.Body,
                    IsDirty: true,
                    Order: record.Order,
                    Visit: $stateParams.visitId
                };
                if (record.External_Id__c == undefined) attach.External_Id__c = $scope.visitSummary[0]['External_Id__c'] + '--' + (new Date().getTime())+'_'+$window.Math.random() * 100000000;
                else {
                    attach.External_Id__c = record.External_Id__c;
                }
                attachments.push(attach);
            });
            if (attachments.length > 0) {
                MOBILEDATABASE_ADD('Db_images', attachments, 'External_Id__c');
            }
            $location.path('app/WholeSalerTemplate1/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date());
        }, 500);
    }
    $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('WholeSalerTemplate1ViewCtrl', err.stack + '::' + err.message);
    }
}).controller('WholeSalerTemplate2Ctrl', function($scope, $stateParams, $ionicScrollDelegate, $rootScope, $window, $state, $location, $q, $ionicModal, $timeout, $interval, $ionicLoading, $log, $filter, SalesforceSession, FETCH_DATA_LOCAL, SOUP_EXISTS, FETCH_DAILY_PLAN_ACCOUNTS, MOBILEDATABASE_ADD, FETCH_DATA, WEBSERVICE_ERROR) {
    try {
        $ionicLoading.show({
            template: '<img src="./img/loader.gif" style="height:100px; width:100px;" /><br/> '
        });
        $rootScope.backText = 'Back';
        var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
        $rootScope.params = $stateParams;
        $rootScope.name = "WholeSalerTemplate2Ctrl";
        var oldvalues = [];
        angular.forEach($rootScope.StockAndSalesList, function(record, key) {
            oldvalues[record.Product__c] = record;
        });
        if (oldvalues != undefined && oldvalues.length == 0) {
            var visitRecord = FETCH_DATA.querySoup('Visit__c', 'External_Id__c', $stateParams.visitId);
            var query = "{Visit_Summary__c:Visit__c} = '" + $stateParams.visitId + "'";
            if (visitRecord.currentPageOrderedEntries != undefined && visitRecord.currentPageOrderedEntries[0] != undefined && visitRecord.currentPageOrderedEntries[0].Id != undefined) {
                query = query + " OR {Visit_Summary__c:Visit__c} = '" + visitRecord.currentPageOrderedEntries[0].Id + "'";
            }
            var visitSummaryrecords = FETCH_DATA_LOCAL('Visit_Summary__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Visit_Summary__c:_soup} FROM {Visit_Summary__c} WHERE " + query, 100000));
            if (visitSummaryrecords.currentPageOrderedEntries != undefined && visitSummaryrecords.currentPageOrderedEntries.length > 0) {
                angular.forEach(visitSummaryrecords.currentPageOrderedEntries, function(record, key) {
                    if (record[0].Product__c != undefined) {
                        oldvalues[record[0].Product__c] = record[0];
                    }
                });
            }
        }
        var today = $filter('date')(new Date(), 'dd/MM/yyyy');
        var transaction = [{
            Account: $stateParams.AccountId,
            stage: $stateParams.order,
            stageValue: 'WholeSalerTemplate2Ctrl',
            Visit: $stateParams.visitId,
            status: 'pending',
            entryDate: today
        }];
        MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
        MOBILEDATABASE_ADD('Db_breadCrum', [{
            VisitedOrder: $stateParams.order,
            Name: 'WholeSalerTemplate2Ctrl'
        }], 'VisitedOrder');
        if ($rootScope.StockAndSalesList.length < 1) $rootScope.StockAndSalesList = [];
        $scope.auditTypeValues = [];
        $scope.showAuditType = false;
        if ($rootScope.processFlow.Retail__c == true) {
            $scope.showAuditType = true;
            $scope.auditTypeValues.push('Audit');
        }
        if ($rootScope.processFlow.Chatai__c == true) {
            $scope.showAuditType = true;
            $scope.auditTypeValues.push('Chatai');
        }
        var currentAccountLocal = FETCH_DATA.querySoup('Account', 'Id', $stateParams.AccountId);
        if (currentAccountLocal != undefined && currentAccountLocal.currentPageOrderedEntries != undefined && currentAccountLocal.currentPageOrderedEntries.length > 0) {
            $scope.currentOutlet = currentAccountLocal.currentPageOrderedEntries[0];
            var SKURecordTypes = FETCH_DATA.querySoup('DB_RecordTypes', 'SobjectType', 'Brands_and_Products__c');
            var SKURecordTypeIds = [];
            angular.forEach(SKURecordTypes.currentPageOrderedEntries, function(record, key) {
                SKURecordTypeIds[record.Name] = record.Id;
            });
            var visitSummaryRtListLocal = FETCH_DATA.querySoup('DB_RecordTypes', 'SobjectType', 'Visit_Summary__c');
            var visitSummaryRtList = [];
            angular.forEach(visitSummaryRtListLocal.currentPageOrderedEntries, function(record, key) {
                visitSummaryRtList[record.Name] = record.Id;
            });
            var outletProducts = FETCH_DATA_LOCAL('Product_Assignment__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Product_Assignment__c:_soup} FROM {Product_Assignment__c} WHERE  {Product_Assignment__c:Outlet_Id__c}='" + $stateParams.AccountId + "' order by {Product_Assignment__c:Order__c} ", 100000));
            if (outletProducts == undefined || outletProducts.currentPageOrderedEntries == undefined || outletProducts.currentPageOrderedEntries.length == 0 || outletProducts.currentPageOrderedEntries[0][0] == undefined) {
                var stateRecord = FETCH_DATA_LOCAL('Brands_and_Products__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Brands_and_Products__c:_soup} FROM {Brands_and_Products__c} WHERE {Brands_and_Products__c:RecordTypeId}='" + SKURecordTypeIds['State'] + "' ", 1));
                if (stateRecord != undefined && stateRecord.currentPageOrderedEntries != undefined && stateRecord.currentPageOrderedEntries.length > 0) {
                    var stateActiveProducts = FETCH_DATA_LOCAL('Brands_and_Products__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Brands_and_Products__c:_soup} FROM {Brands_and_Products__c} WHERE   {Brands_and_Products__c:Related_to_State__c}='" + stateRecord.currentPageOrderedEntries[0][0].Id + "' AND {Brands_and_Products__c:RecordTypeId}='" + SKURecordTypeIds['Product'] + "' and {Brands_and_Products__c:Competitor_Product__c}='false' and {Brands_and_Products__c:Used_in_Chatai__c}='false' and {Brands_and_Products__c:Removed__c}='false'", 100000));
                    if (stateActiveProducts != undefined && stateActiveProducts.currentPageOrderedEntries != undefined && stateActiveProducts.currentPageOrderedEntries.length > 0) {
                        var productOrderList = [];
                        angular.forEach(stateActiveProducts.currentPageOrderedEntries, function(record, key) {
                            productOrderList.push({
                                Id: ($stateParams.AccountId + record[0]['Id']),
                                Outlet_Id__c: $stateParams.AccountId,
                                Product_Id__c: record[0]['Id'],
                                Product_Name__c: record[0]['Sku_Code__c'],
                                Order__c: key + '',
                                Added__c: 'true'
                            });
                            $rootScope.StockAndSalesList[key] = {
                                ProductName: record[0]['Sku_Code__c'],
                                Product__c: record[0]['Id'],
                                Visit__c: $stateParams.visitId,
                                External_Id__c: (new Date().getTime())+'-'+$stateParams.visitId+'-'+$window.Math.random() * 100000000,
                                RecordTypeId: visitSummaryRtList['Stock and Sales'],
                                IsDirty: true
                            };
                        });
                        MOBILEDATABASE_ADD('Product_Assignment__c', productOrderList, 'Id');
                    }
                }
            } else {
                angular.forEach(outletProducts.currentPageOrderedEntries, function(record, key) {
                    if (record[0].Added__c == 'true') {
                        var stock = {
                            ProductName: record[0].Product_Name__c,
                            Product__c: record[0].Product_Id__c,
                            Visit__c: $stateParams.visitId,
                            RecordTypeId: visitSummaryRtList['Stock and Sales'],
                            IsDirty: true
                        };
                        var productOldvalues = oldvalues[record[0]['Product_Id__c']];
                        if (productOldvalues != undefined) {
                            stock.Total_Stock__c = productOldvalues.Total_Stock__c;
                            stock.Stock_Available__c = productOldvalues.Stock_Available__c;
                            stock.Chilled_Stock__c = productOldvalues.Chilled_Stock__c;
                            stock.Sales__c = productOldvalues.Sales__c;
                            stock.Stock_Ageing__c = productOldvalues.Stock_Ageing__c;
                            stock.External_Id__c = productOldvalues.External_Id__c;
                        } else {
                            stock.External_Id__c =(new Date().getTime())+'-'+$stateParams.visitId+'-'+$window.Math.random() * 100000000;
                        }
                        $rootScope.StockAndSalesList[key] = stock;
                    }
                });
            }
        }
        if ($rootScope.StockAndSalesList.length == 0) {
            Splash.ShowToast('No SKU added to this outlet. Please click on product to add products.', 'long', 'bottom', function(a) {
                console.log(a)
            });
        }
        $ionicLoading.hide();
        $scope.selectRow = function(kk) {
                // $rootScope.StockAndSalesList
                $log.debug(kk + JSON.stringify($rootScope.StockAndSalesList));
                angular.forEach($rootScope.StockAndSalesList, function(record, key) {
                    if (key == kk) {
                        record.clicked = true;
                    } else {
                        record.clicked = false;
                    }
                });
            }
            // var scrollPosition = 0;
        $scope.focusCheckForStock = function(index) {
            // scrollPosition =scrollPosition+43;
            // $scope.selectRow(index);
            if (index % 8 == 0) {
                $ionicScrollDelegate.scrollTo(0, index * 43, true);
            }
            for (var i = 0; i < $rootScope.StockAndSalesList.length; i++) {
                document.getElementById('mySelect_' + (i)).style.opacity = '0';
                document.getElementById('mySelect1_' + (i)).style.opacity = '0';
            }
            document.getElementById('mySelect_' + (index)).style.opacity = '1';
            document.getElementById('mySelect1_' + (index)).style.opacity = '1';
        }
        $scope.changeStock = function(st, ind) {
            if (st == false) {
                $rootScope.StockAndSalesList[ind].Total_Stock__c = 0;
                $scope.selectAll = false;
            } else if ($rootScope.StockAndSalesList[ind].Total_Stock__c == '0') {
                $rootScope.StockAndSalesList[ind].Total_Stock__c = '';
                $rootScope.StockAndSalesList[ind].Stock_Available__c = true;
            }
            if (st) {
                var noOfAvailables = 0;
                for (var i = 0; i < $rootScope.StockAndSalesList.length; i++) {
                    if ($rootScope.StockAndSalesList[i].Stock_Available__c != undefined || $rootScope.StockAndSalesList[i].Stock_Available__c == false) break;
                    noOfAvailables++
                };
                if (noOfAvailables == $rootScope.StockAndSalesList.length) {
                    $scope.selectAll = true;
                }
            }
        };
        $scope.checkedStock = function(st, ind) {
            //$log.debug(st + ind);
            if (st == true) document.getElementById("myinput_" + ind).focus();
        };
        $scope.selectRow_display = function(kk, event, index) {
            for (var i = 0; i < $rootScope.StockAndSalesList.length; i++) {
                document.getElementById('mySelect_' + (i)).style.opacity = '0';
                document.getElementById('mySelect1_' + (i)).style.opacity = '0';
            }
            document.getElementById('mySelect_' + (index)).style.opacity = '1';
            document.getElementById('mySelect1_' + (index)).style.opacity = '1';
        }
        $scope.restrictKeyup = function(_event, _index) {
            $log.debug(_event.keyCode);
            var max = 4;
            var mytext = document.getElementById("myinput_" + (parseInt(_index))).value;
            if (mytext.length >= max) {
                mytext = mytext.substr(0, max);
                document.getElementById("myinput_" + (parseInt(_index))).value = parseInt(mytext);
                $rootScope.StockAndSalesList[_index].Total_Stock__c = parseInt(mytext);
                $scope.changeTotalStock(parseInt(mytext), _index);
            }
        }
        $scope.restrictKeydown = function(_event, _index) {
            $log.debug(_event.keyCode);
            if (_event.which == 9) {
                document.getElementById("myinput_" + (parseInt(_index) + 1)).focus();
                _event.preventDefault();
            }
        }
        $scope.changeTotalStock = function(st, ind) {
            $log.debug(st + ind);
            if (st > 0 && st != '') $rootScope.StockAndSalesList[ind].Stock_Available__c = true;
            else $rootScope.StockAndSalesList[ind].Stock_Available__c = false;
        };
        $scope.gotoProductOrder = function() {
            $location.path('app/productOrder/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $scope.ProductStage + '/' + $stateParams.order + '/UB/' + new Date());
        }
        $scope.selectAll = false;
        $scope.isRunning = false;
        $scope.selectAllAsAvailable = function() {
            $timeout(function() {
                if (!$scope.isRunning) {
                    $scope.isRunning = true;
                    $log.debug('$scope.selectAll' + $scope.selectAll + 'select');
                    $scope.selectAll = !$scope.selectAll;
                    angular.forEach($rootScope.StockAndSalesList, function(record, key) {
                        $log.debug('$scope.selectAll' + $scope.selectAll + 'record.Total_Stock__c' + record.Total_Stock__c);
                        if ($scope.selectAll == false && (record.Total_Stock__c == undefined || record.Total_Stock__c == 0)) {
                            record.Stock_Available__c = false;
                        } else {
                            record.Stock_Available__c = true;
                        }
                        if ($rootScope.StockAndSalesList.length == key + 1) {
                            $timeout(function() {
                                $scope.isRunning = false;
                            }, 2000);
                        }
                    });
                }
                $ionicLoading.hide();
            }, 250);
        }
        $scope.justSelectAllAsAvailable = function() {
            if (!$scope.isRunning) {
                $scope.isRunning = true;
                $log.debug('$scope.selectAll' + $scope.selectAll + 'justSelect');
                angular.forEach($rootScope.StockAndSalesList, function(record, key) {
                    $log.debug('$scope.selectAll' + $scope.selectAll + 'record.Total_Stock__c' + record.Total_Stock__c);
                    if ($scope.selectAll == false && (record.Total_Stock__c == undefined || record.Total_Stock__c == 0)) {
                        record.Stock_Available__c = false;
                    } else {
                        record.Stock_Available__c = true;
                    }
                    if ($rootScope.StockAndSalesList.length == key + 1) {
                        $timeout(function() {
                            $scope.isRunning = false;
                        }, 2000);
                    }
                });
            }
        }
        $scope.SaveStocks = function() {
            $timeout(function() {
                var stcokRecords = [];
                angular.forEach($rootScope.StockAndSalesList, function(record, key) {
                    if (record.Stock_Available__c != undefined || record.Total_Stock__c != undefined || record.Sales__c != undefined || record.Chilled_Stock__c != undefined || record.Stock_Ageing__c != undefined) {
                        var stock = record;
                        stock.Account_Id__c = $stateParams.AccountId;
                        stcokRecords.push(stock);
                    }
                });
                if (stcokRecords.length > 0) {
                    MOBILEDATABASE_ADD('Visit_Summary__c', stcokRecords, 'External_Id__c');
                }
                var transaction = [{
                    Account: $stateParams.AccountId,
                    stage: $stateParams.order,
                    stageValue: 'WholeSalerTemplate2Ctrl',
                    Visit: $stateParams.visitId,
                    status: 'saved',
                    entryDate: today
                }];
                MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
                MOBILEDATABASE_ADD('Db_breadCrum', [{
                    VisitedOrder: $stateParams.order,
                    Name: 'WholeSalerTemplate2Ctrl'
                }], 'VisitedOrder');
                if ($rootScope.audit != undefined && $rootScope.audit.visitType != undefined) {
                    $ionicLoading.hide();
                    if ($rootScope.audit.visitType == 'Chatai') {
                        $location.path('app/chataiSales/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date());
                    } else if ($rootScope.audit.visitType == 'Audit') {
                        $location.path('app/retailSales/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date());
                    }
                } else {
                    $rootScope.breadcrumClicked($stateParams.AccountId, $stateParams.visitId, $stateParams.order, parseInt($stateParams.order) + 1, 'fromCtrl');
                }
                //User log
                var today_now = $filter('date')(new Date(), 'dd/MM/yyyy hh:mm a');
                var userOprObj=[{
                    ObjectData:stcokRecords,
                    time_stamp:today_now,
                    comment:'From Stock Clicked next'
                }];
                MOBILEDATABASE_ADD('Db_User_Logs', userOprObj, '_soupEntryId');
            }, 100);
        }
        $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('WholeSalerTemplate2Ctrl', err.stack + '::' + err.message);
    }
}).controller('WholeSalerTemplate3Ctrl', function($q, $log, $ionicLoading, $scope, $rootScope, $window, $stateParams, $location, $filter, $cordovaCamera, $ionicModal, $ionicSlideBoxDelegate, $ionicScrollDelegate, $timeout, $ionicPopup, SMARTSTORE, MOBILEDATABASE_ADD,WEBSERVICE_ERROR) {
    try{
    $rootScope.params = $stateParams;
    $rootScope.name = "WholeSalerTemplate3Ctrl";
    $scope.order = $stateParams.order;
    var today = $filter('date')(new Date(), 'dd/MM/yyyy');
    var transaction = [{
        Account: $stateParams.AccountId,
        stage: $stateParams.order,
        stageValue: 'WholeSalerTemplate3Ctrl',
        Visit: $stateParams.visitId,
        status: 'pending',
        entryDate: today
    }];
    MOBILEDATABASE_ADD('Db_transaction', transaction, 'Account');
    MOBILEDATABASE_ADD('Db_breadCrum', [{
        VisitedOrder: $stateParams.order,
        Name: 'WholeSalerTemplate3Ctrl'
    }], 'VisitedOrder');
    $scope.outlet = SMARTSTORE.buildExactQuerySpec('Account', 'Id', $stateParams.AccountId);
    $scope.breadActive = $rootScope.processFlowLineItems[$stateParams.order].Name;
    $scope.currentPage = {};
    angular.forEach($rootScope.pages, function(record, key) {
        if (record.pageDescription.Id == $rootScope.processFlowLineItems[$stateParams.order].Page__c) {
            $scope.currentPage = record;
        }
    });
    $scope.currentPage.sections[0].fields = $filter('orderBy')($scope.currentPage.sections[0].fields, '-Order__c', true);
    $scope.permitslist = [];
    $ionicLoading.hide();
    $scope.visitSummaryImages = [];
    // var uploads = SMARTSTORE.buildExactQuerySpec('Upload__c', 'Account__c', $stateParams.AccountId);
    var lastVisit = SMARTSTORE.buildSmartQuerySpec("SELECT {Visit__c:_soup} FROM {Visit__c} WHERE {Visit__c:Account__c}='" + $stateParams.AccountId + "' ORDER BY {Visit__c:Check_In_DateTime__c} DESC", 100);
    var permits = [],localPermits =  SMARTSTORE.buildSmartQuerySpec("SELECT {"+$scope.currentPage.sections[0].sectionDescription.Object_API__c+":_soup} FROM {"+$scope.currentPage.sections[0].sectionDescription.Object_API__c+"} WHERE {"+$scope.currentPage.sections[0].sectionDescription.Object_API__c+":Account__c}='" + $stateParams.AccountId + "' AND {Order__c:Order_stage__c}!='Inactive'  AND {"+$scope.currentPage.sections[0].sectionDescription.Object_API__c+":RecordTypeId}='" + $scope.currentPage.sections[0].sectionDescription.Input_Record_Type_Id__c + "' ", 100);
    $scope.TotalOrders = [];
    var d = new Date();
    var currentDate = d.getTime();
    var permitsWithExtIds = [],permitsWithIds = [];
    var imagesQuery = '';
    var currentVisitOrders = [],lastVisitOrders = [];
    angular.forEach(localPermits, function(record, key) {
        if(new Date(record[0].Permit_expiry_Date__c)>=d && (record.Visit_ExtId__c==undefined ||record.Visit_ExtId__c==$stateParams.visitId )){
            permits.push(record);
            record[0].fields = $scope.currentPage.sections[0].fields;
            $scope.permitslist.push(record[0]);
            permitsWithExtIds[record[0].External_Id__c] = record[0];
            currentVisitOrders[record[0].External_Id__c] = record[0];
            lastVisitOrders[record[0].External_Id__c] = record[0];
            permitsWithIds[record[0].Id] = record[0].External_Id__c;
            if(imagesQuery==''){
                imagesQuery = imagesQuery +'{Db_images:ParentId}="'+record[0].External_Id__c +'" OR {Db_images:ParentId}="'+record[0].Id+'"';
            }else{
                imagesQuery = imagesQuery +' OR {Db_images:ParentId}="'+record[0].External_Id__c +'" OR {Db_images:ParentId}="'+record[0].Id+'"';
            }
        }
    });
    $scope.titlefields = [];
    angular.forEach($scope.currentPage.sections[0].fields, function(field, key) {
        if (field.Type__c == 'title') {
            $scope.titlefields.push(field);
        }
    });
   
    var query ='';
    angular.forEach(lastVisit,function(record,key){
        if(query==''){
            query=query+'{Order__c:Visit__c}="'+record[0].External_Id__c+'" OR {Order__c:Visit__c}="'+record[0].Id+'"';
        }else{
            query=query+' OR {Order__c:Visit__c}="'+record[0].External_Id__c+'" OR {Order__c:Visit__c}="'+record[0].Id+'"';
        }
    });
    if(query!=''){
        query = ' AND ('+query+')';
    }
    var visitOrders = SMARTSTORE.buildSmartQuerySpec("SELECT  {Order__c:_soup} FROM {Order__c} WHERE {Order__c:RecordTypeId}='"+$scope.currentPage.sections[0].sectionDescription.Record_Type_Id__c+"'"+query,100);
    var currentImages = [];
    if(imagesQuery!=''){
        var orderImages = SMARTSTORE.buildSmartQuerySpec("SELECT  {Db_images:_soup} FROM {Db_images} WHERE "+imagesQuery,100);        
        angular.forEach(orderImages,function(record,key){
            if(record[0].External_Id__c!=undefined){
                var visitExtId = record[0].External_Id__c.split('--')[1];
                var orderExtId = permitsWithIds[record [0].ParentId]!=undefined?permitsWithIds[record [0].ParentId]:record [0].ParentId;
                if(visitExtId==lastVisit[0][0].External_Id__c || visitExtId==lastVisit[0][0].Id){
                    if(currentImages[orderExtId]==undefined){
                        currentImages[orderExtId] = [];
                    }
                    currentImages[orderExtId].push(record[0]); 
                }else if(visitExtId==lastVisit[1][0].External_Id__c || visitExtId==lastVisit[1][0].Id){
                    if($scope.visitSummaryImages[orderExtId]==undefined){
                        $scope.visitSummaryImages[orderExtId] = [];
                    }
                    $scope.visitSummaryImages[orderExtId].push(record[0]);
                }
            }
        });
    }
    if($rootScope.currentvisitSummaryImages[$stateParams.order]==undefined){
        $rootScope.currentvisitSummaryImages[$stateParams.order] =[];
        angular.forEach(permits, function(record, key) {
            if(currentImages[record[0].External_Id__c]!=undefined){
                currentImages[record[0].External_Id__c] =$filter('orderBy')(currentImages[record[0].External_Id__c], '-Order', true);
            }
            if($scope.visitSummaryImages[record[0].External_Id__c]!=undefined){
                $scope.visitSummaryImages[record[0].External_Id__c] =$filter('orderBy')($scope.visitSummaryImages[record[0].External_Id__c], '-Order', true);
            }
        });
        $rootScope.currentvisitSummaryImages[$stateParams.order] = currentImages;
    }
    var recordTypes = SMARTSTORE.buildSmartQuerySpec("SELECT  {DB_RecordTypes:_soup} FROM {DB_RecordTypes} WHERE  {DB_RecordTypes:SobjectType}='Brands_and_Products__c' OR {DB_RecordTypes:SobjectType} = 'Visit_Summary__c'", 20);
    var recordTypeIds = [];
    angular.forEach(recordTypes,function(record,key){
        recordTypeIds[record[0].Name] =  record[0].Id;
    });

    var outletProducts = SMARTSTORE.buildSmartQuerySpec("SELECT  {Product_Assignment__c:_soup} FROM {Product_Assignment__c} WHERE  {Product_Assignment__c:Outlet_Id__c}='" + $stateParams.AccountId + "' order by {Product_Assignment__c:Order__c} ", 500);
    var skuRecords  =  SMARTSTORE.buildExactQuerySpec("Brands_and_Products__c","RecordTypeId", recordTypeIds['Product']);
    var skuRecordsWithIds = [];
    angular.forEach(skuRecords,function(record,key){
        skuRecordsWithIds[record.Id] = record;
    });
    $scope.skuList = [];
    if(outletProducts.length>0){
        angular.forEach(outletProducts,function(record,key){
            if(skuRecordsWithIds[record[0].Product_Id__c]!=undefined && record[0].Added__c == "true"){
                $scope.skuList.push(skuRecordsWithIds[record[0].Product_Id__c]);
            }
        });
    }else{
        angular.forEach(outletProducts,function(record,key){
            if(record.Company__c =='UB' || record.Company__c =='UBL' ){
                $scope.skuList.push(record);
            }
        });
    }
    angular.forEach(visitOrders,function(record,key){
        if(record[0].Visit__c==$stateParams.visitId || record[0].Visit__c==lastVisit[0][0].Id){
            var currentOrder = angular.copy(record[0]);
            currentOrder.Order__c = permitsWithIds[record[0].Order__c]!=undefined?permitsWithIds[record[0].Order__c]:record[0].Order__c;
            if(currentVisitOrders[currentOrder.Order__c]['Orders']==undefined){
                currentVisitOrders[currentOrder.Order__c]['Orders'] = [];
            }
            currentVisitOrders[currentOrder.Order__c]['Orders'][currentOrder.Sku__c] = currentOrder; 
        }else{
            var lastOrder = angular.copy(record[0]);
            lastOrder.External_Id__c=undefined;
            lastOrder.Order__c = permitsWithIds[record[0].Order__c]!=undefined?permitsWithIds[record[0].Order__c]:record[0].Order__c;
            if(lastVisitOrders[lastOrder.Order__c]!=undefined && lastVisitOrders[lastOrder.Order__c]['Orders']==undefined){
                lastVisitOrders[lastOrder.Order__c]['Orders'] = {};
            }
            if(lastVisitOrders[lastOrder.Order__c]!=undefined)
            lastVisitOrders[lastOrder.Order__c]['Orders'][lastOrder.Sku__c] = lastOrder; 
        }
    });
    if($rootScope.uploadfields[$scope.order]==undefined){
        $rootScope.uploadfields[$scope.order] = [];
        if(currentVisitOrders.length!=0){
            $rootScope.uploadfields[$scope.order]=currentVisitOrders;
        }else{
            $rootScope.uploadfields[$scope.order]= lastVisitOrders;
        }
    }
    angular.forEach(permits, function(record, key) {        
        $scope.TotalOrders[record[0].External_Id__c] = 0;
        if( $rootScope.uploadfields[$scope.order][record[0].External_Id__c]!=undefined){
            angular.forEach($rootScope.uploadfields[$scope.order][record[0].External_Id__c].Orders,function(order,oKey){
                $scope.TotalOrders[record[0].External_Id__c] = $scope.TotalOrders[record[0].External_Id__c]+order.Volume__c;
            });
        }
    });
    $ionicLoading.hide();
    var imagelist = [];
    if ($rootScope.currentvisitSummaryImages[$stateParams.order] != undefined) imagelist = $rootScope.currentvisitSummaryImages[$stateParams.order];
     $scope.prevSlide = function(indentObtained,bulkLitters,totalOrders, extId) {
        if((permitsWithExtIds[extId]['Indent_obtained__c']==true || permitsWithExtIds[extId]['Indent_obtained__c'] == undefined) && bulkLitters!=totalOrders){
            Splash.ShowToast('Indent details do not match permissible bulk liters.', 'long', 'bottom', function(a) {
                console.log(a)
            });
        }
        $ionicSlideBoxDelegate.previous();
    }
    $scope.nextSlide = function(indentObtained,bulkLitters,totalOrders, extId) {
        if((permitsWithExtIds[extId]['Indent_obtained__c']==true || permitsWithExtIds[extId]['Indent_obtained__c'] == undefined) && bulkLitters!=totalOrders){
            Splash.ShowToast('Indent details do not match permissible bulk liters.', 'long', 'bottom', function(a) {
                console.log(a)
            });
        }
        $ionicSlideBoxDelegate.next();
    }
    $scope.slideHasChanged = function(index){
        console.log(index)
    }
    var currentVisitImagesSlider = $ionicSlideBoxDelegate.$getByHandle('currentImagesSlider');
    var lastVisitImagesSlider = $ionicSlideBoxDelegate.$getByHandle('lastImagesSlider');
    $scope.currentImagemodal = [];
    $ionicModal.fromTemplateUrl('current-image-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.currentImagemodal = modal;
    });
    $scope.openCurrentImageModal = function() {
        currentVisitImagesSlider.slide(0);
        $scope.currentImagemodal.show();
    };
    $scope.closeCurrentImageModal = function() {
        $scope.currentImagemodal.hide();
    };
    $scope.goToSlideCurrentImageModel = function(uploadId, index) {
            $scope.slideActive = uploadId;
            $scope.currentImagemodal.show();
            $timeout(function() {
                currentVisitImagesSlider.slide(index);
            }, 500);
        }
        // Called each time the slide changes
    $scope.slideCurrentImageIndex = 0;
    $scope.slideLastVisitImageIndex = 0;
    $scope.slideChangedCurrentImageModel = function(index) {
        $scope.slideIndex1 = index;
        $scope.slideCurrentImageIndex = index;
    };
    $ionicModal.fromTemplateUrl('image-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal = modal;
    });
    $scope.openModal = function() {
        lastVisitImagesSlider.slide(0);
        $scope.modal.show();
    };
    $scope.closeModal = function() {
        $scope.modal.hide();
    };
    $scope.goToSlide = function(uploadId, index) {
            $scope.slideActive = uploadId;
            $scope.modal.show();
            $timeout(function() {
                lastVisitImagesSlider.slide(index);
            }, 500);
        }
        // Called each time the slide changes
    $scope.slideChanged = function(index) {
        $scope.slideIndex = index;
        $scope.slideLastVisitImageIndex = index;
    };
    $scope.lastVisitPrevSlide = function() {
        lastVisitImagesSlider.previous();
    }
    $scope.lastVisitNextSlide = function() {
        lastVisitImagesSlider.next();
    }
    $scope.currentVisitPrevSlide = function() {
        currentVisitImagesSlider.previous();
    }
    $scope.currentVisitNextSlide = function() {
        currentVisitImagesSlider.next();
    }
    // $scope.validateDates = function(startDate, endDate, ExtId) {
    //     if (startDate != undefined && startDate != '' && endDate != undefined && endDate != '' && startDate > endDate) {
    //         Splash.ShowToast('The To: date must finish after the From: date.', 'long', 'bottom', function(a) {
    //             console.log(a)
    //         });
    //        // $rootScope.uploadfields[$scope.order][ExtId].Start_date__c = undefined;
    //         $rootScope.uploadfields[$scope.order][ExtId].End_date__c = undefined;
    //     }
    // }
    $scope.isFieldVisible = function(extId, field) {
        if(field.Dependent_Field__c != undefined){
            var dependentvalue,dependentvalue1 =  $rootScope.uploadfields[$scope.order][extId];
            if(dependentvalue1!=undefined) dependentvalue = dependentvalue1[field.Dependent_Field__c]!=undefined?dependentvalue1[field.Dependent_Field__c].toString():'';
            if(permitsWithExtIds[extId][field.Dependent_Field__c]==field.Dependent_Value__c || field.Dependent_Value__c ==dependentvalue ){
            return false;
            }else
            return true;
        }else return false;
    }
    $scope.calculateTotal = function(extId){
        $scope.TotalOrders[extId] = 0;
        angular.forEach($rootScope.uploadfields[$scope.order][extId].Orders,function(order,oKey){
            if(order.Volume__c!=undefined && order.Volume__c!=null)
                $scope.TotalOrders[extId] = parseInt($scope.TotalOrders[extId])+parseInt(order.Volume__c);
        });
    }
    $scope.remove = [];
    $scope.remove.Reason = '';
    $scope.modelDataCheck = function() {
        var _myEle = document.getElementById('coolerRemove_yes');
        if ($scope.remove.Reason == '') {
            _myEle.style.color = '#aaaaaa';
        } else {
            _myEle.style.color = '#38B6CB';
        }
    }
    $scope.cancelConfirmUploadDelete = function(removeUploadId) {
        $scope.confirmUploadDeletePopup.close();
        $scope.remove.Reason = '';
    }
     $scope.confirmUploadDelete = function(removeUploadId) {
        if ($scope.remove.Reason != '') {
            angular.forEach($scope.permitslist, function(record, key) {
                var uploadsToDelete = [];
                if (record.External_Id__c == removeUploadId) {
                    record.Order_stage__c = 'Inactive';
                    record.Reason__c = $scope.remove.Reason;
                    record.IsDirty = true;
                    uploadsToDelete.push(record);
                    MOBILEDATABASE_ADD('Order__c', uploadsToDelete, 'External_Id__c');
                }
            });
            $scope.remove.Reason = '';
            $scope.confirmUploadDeletePopup.close();
            $location.path('app/WholeSalerTemplate3/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date() + 'save');
        } else {
            Splash.ShowToast('Please Specify reason for removing ' + $scope.currentPage.pageDescription.Add_New_Label__c + ' ', 'long', 'bottom', function(a) {
                console.log(a)
            });
        }
    }
    $scope.removeUpload = function(removeUploadId) {
        $scope.remove.Reason = '';
        $scope.confirmUploadDeletePopup = $ionicPopup.confirm({
            template: 'Are you sure you want to remove the ' + $scope.currentPage.pageDescription.Add_New_Label__c.toLowerCase() + ' information for this outlet?  <br/><br/><br/><span><input placeholder="Enter the reason for removing the ' + $scope.currentPage.pageDescription.Add_New_Label__c + '" type="text" ng-change="modelDataCheck()" ng-model="remove.Reason"><div class="piller"></div><div class="piller piller_1"></div></span><br/> <div style="text-align:right; color:#38B6CB;margin-right: 10px;font-weight: bold;"><span my-touchstart="cancelConfirmUploadDelete(\'' + removeUploadId + '\')">NO</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id="coolerRemove_yes" class="coolerRemove_yes" ng-disabled="!remove.Reason" my-touchstart="confirmUploadDelete(\'' + removeUploadId + '\')">YES</span> </div>',
            cssClass: 'myConfirm',
            title: '',
            scope: $scope,
            buttons: []
        });
        $scope.confirmUploadDeletePopup.then(function(res) {});
    }

    $scope.restrictKeyup = function(_event, _index, slideNumber, extId, skuId) {
        $log.debug(_event.keyCode);
        var max = 4;
        var mytext = document.querySelector('#myPermit_' + slideNumber + " #myinput_" + (parseInt(_index))).value;
        if (mytext.length >= max) {
            mytext = mytext.substr(0, max);
            document.querySelector('#myPermit_' + slideNumber + " #myinput_" + (parseInt(_index))).value = parseInt(mytext);
            $rootScope.uploadfields[$stateParams.order][extId]['Orders'][skuId].Volume__c = mytext;
            //$rootScope.StockAndSalesList[_index].Total_Stock__c = parseInt(mytext);
            //$scope.changeTotalStock(parseInt(mytext), _index);
            $scope.calculateTotal(extId);
        }
    }

    $scope.takePicture = function(uploadId) {
        var options = {
            quality: 75,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.CAMERA,
            allowEdit: true,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 700,
            targetHeight: 500,
            saveToPhotoAlbum: false
        };
        $cordovaCamera.getPicture(options).then(function(imageData) {
            if (imagelist[uploadId] == undefined) imagelist[uploadId] = [];
            imagelist[uploadId].reverse();
            imagelist[uploadId].push({
                Body: imageData,
                IsDirty: true
            });
            var images = imagelist[uploadId].reverse();
            $rootScope.currentvisitSummaryImages[$stateParams.order][uploadId] = [];
            for (var i = 0; i < 10; i++) {
                if (images[i] != undefined) {
                    images[i].Order = i;
                    $rootScope.currentvisitSummaryImages[$stateParams.order][uploadId].push(images[i]);
                }
            }
        }, function(err) {});
    }
    $scope.addNew = function() {
        $location.path('app/AddNewWholeSalerTemplate3/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date());
    }
    $scope.dependentValueCheck = function(fieldAPI, indentCheck, extId){
        if(fieldAPI=='Indent_obtained__c' && indentCheck==false){
            $rootScope.uploadfields[$stateParams.order][extId]['Payment_obtained__c'] = false;
        }
         if(fieldAPI=='Indent_obtained__c'){
            permitsWithExtIds[extId]['Indent_obtained__c'] = indentCheck;
        }        
    }
    $scope.goToNext = function() {
        $timeout(function() {
            var ordersList = [];
            var attachments = [];
            var showAlert=false;
            if($scope.permitslist.length>0){
            //angular.forEach($scope.permitslist,function(record,key){
                if(($scope.permitslist[$scope.permitslist.length-1]['Indent_obtained__c']==true || $scope.permitslist[$scope.permitslist.length-1]['Indent_obtained__c'] == undefined) && $scope.permitslist[$scope.permitslist.length-1]['Volume__c']!=$scope.TotalOrders[$scope.permitslist[$scope.permitslist.length-1]['External_Id__c']]){
                    showAlert=true;
                }
            //});
            }
            if(showAlert){
                Splash.ShowToast('Indent details do not match permissible bulk liters.', 'long', 'bottom', function(a) {
                    console.log(a)
                });
            }
            angular.forEach($scope.permitslist,function(record,key){
                var inputRecord = angular.copy($rootScope.uploadfields[$stateParams.order][record.External_Id__c]);
                if(inputRecord!=undefined){
                    record.Indent_obtained__c = inputRecord.Indent_obtained__c;
                    record.Payment_obtained__c = inputRecord.Payment_obtained__c;
                    record.Comments__c = inputRecord.Comments__c;
                    if(inputRecord.Payment_obtained__c==true){
                        if(record.Visit_ExtId__c==undefined) 
                            record.Visit_ExtId__c = $stateParams.visitId;
                        record.Order_stage__c='Payment obtained';
                    }else if(inputRecord.Indent_obtained__c==true){
                        record.Order_stage__c='Indent obtained';
                    } else {
                        record.Order_stage__c='Permit obtained';
                    }

                    record.IsDirty = true;
                    record.Payment_mode__c = inputRecord.Payment_mode__c;
                    ordersList.push(angular.copy(record));
                    if(inputRecord.Indent_obtained__c==true && inputRecord.Orders != undefined){
                        angular.forEach($scope.skuList,function(sku,skey){
                            if(inputRecord.Orders[sku.Id]!=undefined){
                                var order = {};
                                order.Sku__c = sku.Id;
                                order.Order__c = record.External_Id__c;
                                order.Volume__c = inputRecord.Orders[sku.Id].Volume__c;
                                order.Indent_obtained__c = inputRecord.Indent_obtained__c;
                                order.Payment_obtained__c = inputRecord.Payment_obtained__c;
                                order.Visit__c = $stateParams.visitId;
                                order.Account__c = $stateParams.AccountId;
                                order.RecordTypeId =  $scope.currentPage.sections[0].sectionDescription.Record_Type_Id__c;
                                if(order.External_Id__c==undefined){
                                    order.External_Id__c = record.External_Id__c+(new Date().getTime())+'_'+$window.Math.random() * 10000000;
                                }
                                order.IsDirty = true;
                                ordersList.push(order);
                            }
                            
                        });                            
                    }
                    angular.forEach($rootScope.currentvisitSummaryImages[$stateParams.order][record.External_Id__c], function(imageData, key) {
                        attachments.push({
                            ParentId: record.External_Id__c,
                            Name: record.External_Id__c + '.jpg',
                            Body: imageData.Body,
                            IsDirty: true,
                            Order: imageData.Order,
                            External_Id__c: 'WholeSalerTemplate3--' + $stateParams.visitId+'--' + record.External_Id__c + imageData.Order
                        });
                    });
                }
            });
            
            if (ordersList.length > 0) {
                 MOBILEDATABASE_ADD($scope.currentPage.sections[0].sectionDescription.Object_API__c, ordersList, 'External_Id__c');
             }
             if (attachments.length > 0) {
                 MOBILEDATABASE_ADD('Db_images', attachments, 'External_Id__c');
             }
             
            
            MOBILEDATABASE_ADD('Db_transaction',  [{
                Account: $stateParams.AccountId,
                 stage: $stateParams.order,
                 stageValue: 'PRIDECall8Ctrl',
                 Visit: $stateParams.visitId,
                status: 'saved',
                 entryDate: today
            }], 'Account');
             MOBILEDATABASE_ADD('Db_breadCrum', [{
                VisitedOrder: $stateParams.order,
                 Name: 'PRIDECall8Ctrl'
             }], 'VisitedOrder');
            $rootScope.breadcrumClicked($stateParams.AccountId, $stateParams.visitId, $stateParams.order, parseInt($stateParams.order) + 1, 'fromCtrl');
        }, 100);
    }
    $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('WholeSalerTemplate3Ctrl', err.stack + '::' + err.message);
    }
}).controller('AddNewWholeSalerTemplate3Ctrl', function($q, $log, $ionicLoading, $scope, $ionicPopup, $rootScope, $window, $stateParams, $location, $filter, $cordovaCamera, $ionicModal, $ionicSlideBoxDelegate, $timeout, SMARTSTORE, MOBILEDATABASE_ADD,WEBSERVICE_ERROR) {
    try{
    $rootScope.params = $stateParams;
    $rootScope.name = "AddNewWholeSalerTemplate3Ctrl";
    $scope.order = $stateParams.order;
    $scope.outlet = SMARTSTORE.buildExactQuerySpec('Account', 'Id', $stateParams.AccountId);
    $scope.breadActive = $rootScope.processFlowLineItems[$stateParams.order].Name;
    $scope.currentPage = {};
    $scope.permit = {};
    angular.forEach($rootScope.pages, function(record, key) {
        if (record.pageDescription.Id == $rootScope.processFlowLineItems[$stateParams.order].Page__c) {
            $scope.currentPage = record;
        }
    });
    $scope.addNewSectionFields = [];
    if ($scope.currentPage.sections.length > 1) {
        $scope.addNewSectionFields = $scope.currentPage.sections[1].fields;
    } else {
        $scope.addNewSectionFields = $scope.currentPage.sections[0].fields;
    }
    
    $scope.showAdd = false;
    
    $scope.checkValues = function(fieldAPI) {
        $scope.showAdd = true;
        if(fieldAPI=='Permit_expiry_Date__c' || fieldAPI=='Permit_issue_Date__c'){
            if($scope.permit['Permit_expiry_Date__c']!=undefined && $scope.permit['Permit_issue_Date__c'] !=undefined && $scope.permit['Permit_expiry_Date__c']<$scope.permit['Permit_issue_Date__c']){
                Splash.ShowToast('Expiry date must be after the Issue date.', 'long', 'bottom', function(a) {
                    console.log(a)
                });
               $scope.permit['Permit_expiry_Date__c'] = undefined;
            }
        }
        angular.forEach($scope.addNewSectionFields, function(field, key) {
            if($scope.permit[field.Field_API__c]==undefined){
                $scope.showAdd = false;
            }
        });
    }
    $scope.cancelAddNew = function() {
        angular.forEach($scope.addNewSectionFields, function(field, key) {
            field.fieldvalue = '';
        });
        $location.path('app/WholeSalerTemplate3/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date() + 'save');
    }
    
    $scope.saveNew = function() {
        $timeout(function() {
            var permitsToInsert = [];
            if ($scope.showAdd) {
                if($scope.permit.Permit_issue_Date__c.toString() != $scope.permit.Permit_expiry_Date__c.toString()){
                    $scope.permit.Account__c = $stateParams.AccountId;
                    $scope.permit.RecordTypeId = $scope.currentPage.sections[0].sectionDescription.Input_Record_Type_Id__c;
                    $scope.permit.External_Id__c =  'Permit'+ $stateParams.visitId + (new Date().getTime())+'_'+$window.Math.random() * 10000000;
                    $scope.permit.Order_stage__c = 'Permit obtained';
                    $scope.permit.IsDirty = true;
                    permitsToInsert.push($scope.permit);
                    MOBILEDATABASE_ADD($scope.currentPage.sections[1].sectionDescription.Object_API__c, permitsToInsert, 'External_Id__c');                
                    Splash.ShowToast('The ' + $scope.currentPage.pageDescription.Add_New_Label__c + ' has been added.', 'long', 'bottom', function(a) {
                        console.log(a)
                    });                
                    $location.path('app/WholeSalerTemplate3/' + $stateParams.AccountId + '/' + $stateParams.visitId + '/' + $stateParams.order + '/' + new Date() + 'save');
                } else {
                    $ionicLoading.hide();
                    Splash.ShowToast('Expiry date must be after the Issue date', 'long', 'bottom', function(a) {
                        console.log(a)
                    });
                }
                
            } else {
                $ionicLoading.hide();
                Splash.ShowToast('Please complete all fields before you Add permit', 'long', 'bottom', function(a) {
                    console.log(a)
                });
            }
        }, 100);
    }
    $ionicLoading.hide();
    } catch (err) {
        $ionicLoading.hide();
        WEBSERVICE_ERROR('AddNewWholeSalerTemplate3Ctrl', err.stack + '::' + err.message);
    }
}).controller('webSerCtrl',function($scope,FETCH_DATA_LOCAL,$log,WRITE_TO_FILE){
   
    

    var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
    var dbObjects = FETCH_DATA_LOCAL('Db_All_Logs', sfSmartstore.buildSmartQuerySpec("SELECT  {Db_All_Logs:_soup} FROM {Db_All_Logs}", 1000));
    $scope.allLogsArr=dbObjects.currentPageOrderedEntries;
    $scope.allLogsArr.reverse();

    var errorLog = FETCH_DATA_LOCAL('Db_webservice_error_log', sfSmartstore.buildSmartQuerySpec("SELECT  {Db_webservice_error_log:_soup} FROM {Db_webservice_error_log}", 1000));
    var userLogs = FETCH_DATA_LOCAL('Db_User_Logs', sfSmartstore.buildSmartQuerySpec("SELECT  {Db_User_Logs:_soup} FROM {Db_User_Logs}", 1000));
    WRITE_TO_FILE.write({data:$scope.allLogsArr},{data:errorLog.currentPageOrderedEntries.reverse()},{data:userLogs.currentPageOrderedEntries.reverse()});
    console.log('---> '+$scope.allLogsArr);


}).controller('errCtrl',function($scope,FETCH_DATA_LOCAL,$log){
    
    var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
    var dbObjects = FETCH_DATA_LOCAL('Db_webservice_error_log', sfSmartstore.buildSmartQuerySpec("SELECT  {Db_webservice_error_log:_soup} FROM {Db_webservice_error_log}", 1000));
    $scope.allErrArr=dbObjects.currentPageOrderedEntries;    
    $scope.allErrArr.reverse();
    console.log('$scope.allErrArr-->');
    console.log($scope.allErrArr);

}).controller('userLogs',function($scope,FETCH_DATA_LOCAL,$log){



      var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
      var dbObjects = FETCH_DATA_LOCAL('Db_User_Logs', sfSmartstore.buildSmartQuerySpec("SELECT  {Db_User_Logs:_soup} FROM {Db_User_Logs}", 1000));
      $scope.allUserLog=dbObjects.currentPageOrderedEntries;
      $scope.allUserLog.reverse();

      console.log('---> '+$scope.allUserLog);

});