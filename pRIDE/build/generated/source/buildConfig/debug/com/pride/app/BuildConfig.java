/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.pride.app;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.pride.app";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 233;
  public static final String VERSION_NAME = "2.46";
}
