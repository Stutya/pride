package com.pride.app;

import com.pride.app.R;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.Animator.AnimatorListener;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.animation.BounceInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SplashDialog extends Dialog {
	private Context context = null;
	ImageView animationView;
	ProgressBar progressBar;
	AnimationDrawable frameAnimation;
	TextView textViewProgress;
	float scale;
	int progressValue, progress = 0;
	int topMargin = 0;
	RelativeLayout.LayoutParams params = null;
	final int progressTop = -200;
	final int progressBottom = 50;
	int lastMoveBy = 0;
	int lastProgress = 0;
	ProgressTask task = null;

	public SplashDialog(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		this.context = context;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// must not do anything on back press
	}

	@Override
	protected void onStop() {
		animationView = null;
		progressBar = null;
		frameAnimation = null;
		textViewProgress = null;
		context = null;

		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_splash);
		getWindow().setLayout(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT);

		scale = this.context.getResources().getDisplayMetrics().density;

		final TextView textViewHeader = (TextView) findViewById(R.id.textViewHeader);
		progressBar = (ProgressBar) findViewById(R.id.progressBar1);
		textViewProgress = (TextView) findViewById(R.id.textViewProgress);
		textViewHeader.setTextColor(Color.parseColor("#ffffff"));
		textViewProgress.setTextColor(Color.parseColor("#000000"));
		ObjectAnimator textViewHeaderAnimator = ObjectAnimator.ofFloat(
				textViewHeader, "translationY", -100f, 0f);
		textViewHeaderAnimator.setDuration(1000);// 1sec
		textViewHeaderAnimator.setStartDelay(500);
		textViewHeaderAnimator.setInterpolator(new BounceInterpolator());
		textViewHeaderAnimator.setRepeatCount(0);
		textViewHeaderAnimator.start();

		task = new ProgressTask();

		setOnDismissListener(new OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialog) {
				// TODO Auto-generated method stub
				task.cancel(true);
			}
		});

		textViewHeaderAnimator.addListener(new AnimatorListener() {
			@Override
			public void onAnimationStart(Animator animation) {
				// TODO Auto-generated method stub
				textViewHeader.setText("Welcome...");
			}

			@Override
			public void onAnimationRepeat(Animator animation) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onAnimationEnd(Animator animation) {
				// TODO Auto-generated method stub
				task.execute();
			}

			@Override
			public void onAnimationCancel(Animator animation) {
				// TODO Auto-generated method stub
			}
		});
	}

	public void setHeader(String name) {
		TextView textViewHeader = (TextView) findViewById(R.id.textViewHeader);
		textViewHeader.setText("Welcome " + name);
	}
	public void setProgressText(int progress,String progressText) {
		setProgressLevel(progress);
		textViewProgress.setText(progressText);
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		// TODO Auto-generated method stub
		super.onWindowFocusChanged(hasFocus);
		animationView = (ImageView) findViewById(R.id.imageViewBeer);
		animationView.setBackgroundResource(R.drawable.beer_animation_01);
		frameAnimation = (AnimationDrawable) animationView.getBackground();
		frameAnimation.setOneShot(false);
		frameAnimation.start();

		params = (RelativeLayout.LayoutParams) animationView.getLayoutParams();
		topMargin = params.topMargin;
	}

	public class ProgressTask extends AsyncTask<Void, Integer, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			try {
				int sleep = 2000;

				publishProgress(5);
				Thread.sleep(sleep);

				/*publishProgress(30);
				Thread.sleep(sleep);

				sleep = 3000;
				publishProgress(50);
				Thread.sleep(sleep);

				sleep = 3000;
				publishProgress(75);
				Thread.sleep(sleep);

				sleep = 5000;
				publishProgress(100);
				Thread.sleep(sleep);*/
			} catch (InterruptedException e) {

			}

			return null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			// super.onPostExecute( result );
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			// TODO Auto-generated method stub
			setProgressLevel(values[0]);
		}
	}

	private void setProgressLevel(int progress)
	{
		double percentage = (double) progress / 100;
		double diff = (progressBottom - progressTop) * scale;
		final int moveBy = (int) (diff * percentage);

		try
		{
			textViewProgress.setText("Initializing...");
			/*switch (progress) {
			case 0:
				textViewProgress.setText("Initializing...");
				break;
			case 30:
				textViewProgress.setText("Downloading Data...");
				break;
			case 50:
				textViewProgress.setText("Saving Data...");
				break;
			case 75:
				textViewProgress.setText("Configuring Data...");
				break;
			case 100:
				textViewProgress.setText("Launching Application...");
				break;
			default:
				textViewProgress.setText("Initializing...");
				break;
			}*/
	
			final int prgrs = progress;
	
			final ObjectAnimator animator = ObjectAnimator.ofFloat(animationView,
					"translationY", lastMoveBy, -moveBy);
			animator.setDuration(400);
	
			final ObjectAnimator animator2 = ObjectAnimator.ofInt(progressBar,
					"progress", lastProgress, progress);
			animator2.setDuration(300);
	
			Handler h = new Handler(Looper.getMainLooper());
			h.post(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					animator.start();
					animator2.start();
				}
			});
	
			animator.addListener(new AnimatorListener() {
	
				@Override
				public void onAnimationStart(Animator animation) {
					// TODO Auto-generated method stub
				}
	
				@Override
				public void onAnimationRepeat(Animator animation) {
					// TODO Auto-generated method stub
				}
	
				@Override
				public void onAnimationEnd(Animator animation) {
					// TODO Auto-generated method stub
					lastMoveBy = -moveBy;
					lastProgress = prgrs;
				}
	
				@Override
				public void onAnimationCancel(Animator animation) {
					// TODO Auto-generated method stub
				}
			});
		}
		catch(Exception e)
		{
			Log.e("SEVERE", "Must not handle generic exception");
		}
	}
}
