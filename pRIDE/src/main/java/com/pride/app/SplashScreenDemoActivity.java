package com.pride.app;

import org.apache.cordova.CordovaWebView;
import org.apache.cordova.api.CallbackContext;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import com.salesforce.androidsdk.ui.sfhybrid.SalesforceDroidGapActivity;

public class SplashScreenDemoActivity extends SalesforceDroidGapActivity {
	private SplashDialog splash;
	private static CordovaWebView m_appView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		//Log.i("SalesforceDroidGapActivity.onCreate", "onCreate called");
		super.onCreate(savedInstanceState);
	}

//	@Override
//	public void onResume() {
//		super.onResume();
//		// TODO Auto-generated method stub
//		if (m_appView != null) {
//			m_appView.addJavascriptInterface(this, "Splash");
//			splash = new SplashDialog(getActivity());
//			splash.setCancelable(false);
//			splash.show();
//		}
//	}
//
//	@Override
//	public void onPause() {
//		super.onPause();
//		// TODO Auto-generated method stub
//		if (m_appView != null) {
//			m_appView.removeJavascriptInterface("Splash");
//		}
//	}

	@Override
	public void loadUrl(final String url) {
		super.loadUrl(url, 15000 /*
								 * time for which splash screen should be
								 * displayed
								 */);
		m_appView = super.appView;
		m_appView.addJavascriptInterface(this, "Splash");
		splash = new SplashDialog(getActivity());
		splash.setCancelable(false);
		splash.show();

	}

	@JavascriptInterface
	public void SetUserName(final String name) {
		SplashScreenDemoActivity.this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				splash.setHeader(name);
			}
		});
	}
	@JavascriptInterface
	public void setProgressText(final int progress,final String progressText) {
		SplashScreenDemoActivity.this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				splash.setProgressText(progress,progressText);
			}
		});
	}

	@JavascriptInterface
	public void Complete() {
		Log.d("CALLBACK", "SUCCESS");
		splash.dismiss();
	}

	@JavascriptInterface
	public int isActive() {
		Log.d("isActive", "success==" + splash.isShowing());
		return splash.isShowing() ? 1 : 0;
	}

	@JavascriptInterface
	public void ShowToast(final String message, final String duration,
			final String position, final CallbackContext callbackContext) {
		try {
			SplashScreenDemoActivity.this.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					android.widget.Toast toast = android.widget.Toast.makeText(
							SplashScreenDemoActivity.this.getActivity(),
							message, Toast.LENGTH_SHORT);

					if ("top".equals(position)) {
						toast.setGravity(Gravity.TOP
								| Gravity.CENTER_HORIZONTAL, 0, 20);
					} else if ("bottom".equals(position)) {
						toast.setGravity(Gravity.BOTTOM
								| Gravity.CENTER_HORIZONTAL, 0, 20);
					} else if ("center".equals(position)) {
						toast.setGravity(Gravity.CENTER_VERTICAL
								| Gravity.CENTER_HORIZONTAL, 0, 0);
					} else {
						callbackContext
								.error("invalid position. valid options are 'top', 'center' and 'bottom'");
						return;
					}

					if ("short".equals(duration)) {
						toast.setDuration(Toast.LENGTH_SHORT);
					} else if ("long".equals(duration)) {
						toast.setDuration(Toast.LENGTH_LONG);
					} else {
						callbackContext
								.error("invalid duration. valid options are 'short' and 'long'");
						return;
					}

					toast.show();
					if (callbackContext != null)
						callbackContext.success();
				}
			});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}