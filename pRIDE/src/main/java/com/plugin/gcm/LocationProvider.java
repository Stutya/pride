package com.plugin.gcm;

import org.apache.cordova.api.CallbackContext;
import org.apache.cordova.api.CordovaPlugin;
import org.apache.cordova.api.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.location.LocationManager;

public class LocationProvider extends CordovaPlugin {

	LocationManager mLocationManager;
	double lat,longi ;
	public static String TAG ="Location";
	
	// GPSTracker class
	GPSTracker gps;
	
	String result;
	@Override
	public boolean execute(String action, JSONArray args, CallbackContext callbackContext) {
		if (action.equals("get") || action.equals("getNew")) {
			
			gps = new GPSTracker(this.cordova.getActivity());

			// check if GPS enabled     
			if(gps.canGetLocation()){

				lat   = gps.getLatitude();
				longi = gps.getLongitude();

			}else{
				// can't get location
				// GPS or Network is not enabled
				// Ask user to enable GPS/network in settings
				if(action.equals("getNew")){
					//to clear the previous location details I have set it to 0.0
					lat = 0.0;
					longi = 0.0;
					//gps.showSettingsAlert();

				}
			}

			result = lat+" , "+longi;
			JSONObject temp = new JSONObject();
			String latname = "latitude";
			String longName = "longitude";
			try {
				temp.put(latname,lat);
				temp.put(longName,longi);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (result != null) {
				callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, temp));
				return true;
			}else{
				callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR));
			}

		}
		callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR));
		return false;
	}
}