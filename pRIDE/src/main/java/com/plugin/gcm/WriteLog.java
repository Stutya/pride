package com.plugin.gcm;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import org.apache.cordova.api.CallbackContext;
import org.apache.cordova.api.CordovaPlugin;
import org.apache.cordova.api.LOG;
import org.apache.cordova.api.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * Created by Admin on 27-03-2017.
 */

public class WriteLog extends CordovaPlugin {


    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) {
        if (action.equalsIgnoreCase("write")) {

            try{
                writeToFile(args.getJSONObject(0).toString(),"pride_service_log.txt");
                writeToFile(args.getJSONObject(1).toString(),"pride_error_log.txt");
                writeToFile(args.getJSONObject(2).toString(),"pride_user_log.txt");
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            LOG.v("--->",args.toString());
            Toast.makeText(this.cordova.getActivity(),"Log Files Created!",Toast.LENGTH_SHORT).show();
            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK));
            return true;
        }
        callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR));
        return false;
    }


    private void writeToFile(String data,String fileName) {
        try {
            File myDirectory = new File(Environment.getExternalStorageDirectory(), "pride_log");
            if(!myDirectory.exists()) {
                myDirectory.mkdir();
                Log.v("--->","Created For first time directory ");
            }
            File file = new File(myDirectory, fileName);
            FileOutputStream stream = new FileOutputStream(file);
            try {
                stream.write(data.getBytes());
            } finally {
                stream.close();
            }
        }
        catch (Exception e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }
}
