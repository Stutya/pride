cordova.define("cordova/plugin/writetofile",
  function(require, exports, module) {
    var exec = require("cordova/exec");
    var WriteToFile = function () {};

    var WriteToFileError = function(code, message) {
        this.code = code || null;
        this.message = message || '';
    };



    WriteToFile.prototype.write = function(serviceTxt,errorTxt,userlogText,success,fail) {
        exec(success,fail,"WriteToFile",
            "write",[serviceTxt,errorTxt,userlogText]);
    };


    var writetofile = new WriteToFile();
    module.exports = writetofile;
});

if(!window.plugins) {
    window.plugins = {};
}
if (!window.plugins.WriteToFile) {
    window.plugins.telephoneNumber = cordova.require("cordova/plugin/writetofile");
}