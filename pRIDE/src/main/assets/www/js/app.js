// Ionic Starter App
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'UB_PRIDE_APP' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'UB_PRIDE_APP.controllers' is found in controllers.js
angular.module('UB_PRIDE_APP', ['ionic', 'UB_PRIDE_APP.controllers', 'UB_PRIDE_APP.services', 'UB_PRIDE_APP.SFDC', 'ngSanitize', 'uiGmapgoogle-maps', 'ngCordova'])

.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        /* if (window.cordova && window.cordova.plugins.Keyboard) {
             cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
         }*/
         if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
})

.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
    .state('app', {
        url: "/app",
        abstract: true,
        templateUrl: "templates/menu.html",
        controller: 'AppCtrl'
    })

    .state('app.today', {
        url: "/today/:stage",
        views: {
            'menuContent': {
                templateUrl: "templates/today.html",
                controller: 'TodayCtrl'
            }
        }
    })
    .state('app.map', {
        url: "/map/:accountId",
        views: {
            'menuContent': {
                templateUrl: "templates/map.html",
                controller: 'MapCtrl'

            }
        }
    })
    .state('app.products', {
        url: "/products",
        views: {
            'menuContent': {
                templateUrl: "templates/products.html"
            }
        }
    })

    .state('app.reports', {
        url: "/reports",
        views: {
            'menuContent': {
                templateUrl: "templates/reports.html"
            }
        }
    })
    .state('app.chatter', {
        url: "/chatter",
        views: {
            'menuContent': {
                templateUrl: "templates/chatter.html"

            }
        }
    })
    .state('app.overview', {
        url: "/overview/:accountId",
        views: {
            'menuContent': {
                templateUrl: "templates/overview.html",
                controller: "OverviewCtrl"
            }
        }
    })
    .state('app.depotOverview', {
        url: "/depotOverview/:accountId",
        views: {
            'menuContent': {
                templateUrl: "templates/depotOverview.html",
                controller: "depotOverviewCtrl"
            }
        }
    })
    .state('app.wholesaleOverview', {
        url: "/wholesaleOverview/:accountId",
        views: {
            'menuContent': {
                templateUrl: "templates/wholesaleOverview.html",
                controller: "wholesaleOverviewCtrl"
            }
        }
    })
    
    .state('app.container2', {
        url: "/container2/:AccountId/:visitId/:order/:refresh",
        views: {
            'menuContent': {
                templateUrl: "templates/containerForTemplate2.html",
                controller: "PRIDECallCtrl"
            }
        }
    })
    .state('app.container3', {
        url: "/container3/:AccountId/:visitId/:order/:refresh",
        views: {
            'menuContent': {
                templateUrl: "templates/containerForTemplate3.html",
                controller: "PRIDECallCtrl2"
            }
        }
    })
    .state('app.container4', {
        url: "/container4/:AccountId/:visitId/:order/:refresh",
        views: {
            'menuContent': {
                templateUrl: "templates/containerForTemplate4.html",
                controller: "Container4Ctrl"
            }
        }
    })
    .state('app.container5', {
        url: "/container5/:AccountId/:visitId/:order/:refresh",
        views: {
            'menuContent': {
                templateUrl: "templates/containerForTemplate5.html",
                controller: "PRIDECall5Ctrl"
            }
        }
    })
    .state('app.container6', {
        url: "/container6/:AccountId/:visitId/:order/:refresh",
        views: {
            'menuContent': {
                templateUrl: "templates/containerForTemplate6.html",
                controller: "PRIDECall6Ctrl"
            }
        }
    })
    .state('app.AddNew6', {
        url: "/AddNew6/:AccountId/:visitId/:order/:refresh",
        views: {
            'menuContent': {
                templateUrl: "templates/AddNew6.html",
                controller: 'AddNew6Ctrl'
            }
        }
    })
    .state('app.container7', {
        url: "/container7/:AccountId/:visitId/:order/:refresh",
        views: {
            'menuContent': {
                templateUrl: "templates/containerForTemplate7.html",
                controller: "PRIDECall7Ctrl"
            }
        }
    })
    .state('app.container8', {
        url: "/container8/:AccountId/:visitId/:order/:refresh",
        views: {
            'menuContent': {
                templateUrl: "templates/containerForTemplate8.html",
                controller: "PRIDECall8Ctrl"
            }
        }
    })
    .state('app.container4New', {
        url: "/container4New/:AccountId/:visitId/:order",
        views: {
            'menuContent': {
                templateUrl: "templates/containerForTemplate4-addNew.html",
                controller: "Container4NewCtrl"
            }
        }
    })
    .state('app.container4View', {
        url: "/container4View/:AccountId/:visitId/:order/:visitSummaryId/:indexValue",
        views: {
            'menuContent': {
                templateUrl: "templates/containerForTemplate4-view.html",
                controller: "Container4ViewCtrl"
            }
        }
    })
    .state('app.capturesales', {
        url: "/capturesales/:uploadId/:AccountId/:visitId/:order",
        views: {
            'menuContent': {
                templateUrl: "templates/captureSales.html",
                controller: "SalesTrackingCtrl"
            }
        }
    })
    .state('app.stocks', {
        url: "/stocks/:AccountId/:visitId/:order/:refresh",
        views: {
            'menuContent': {
                templateUrl: "templates/Stock.html",
                controller: "StockCtrl"
            }
        }
    })
    .state('app.chataiSales', {
        url: "/chataiSales/:AccountId/:visitId/:order/:refresh",
        views: {
            'menuContent': {
                templateUrl: "templates/ChataiSales.html",
                controller: "ChataiCtrl"
            }
        }
    })
    .state('app.retailSales', {
        url: "/retailSales/:AccountId/:visitId/:order/:refresh",
        views: {
            'menuContent': {
                templateUrl: "templates/RetailSales.html",
                controller: "RetailCtrl"
            }
        }
    })
    .state('app.stockDetails', {
        url: "/stockDetails/:AccountId/:visitId/:order/:refresh",
        views: {
            'menuContent': {
                templateUrl: "templates/StockDetails.html",
                controller: "StockDetailsCtrl"
            }
        }
    })
    
    .state('app.DepotTemplate1', {
        url: "/DepotTemplate1/:AccountId/:visitId/:order/:refresh",
        views: {
            'menuContent': {
                templateUrl: "templates/DepotContainer1.html",
                controller: "DepotTemplate1Ctrl"
            }
        }
    })
    .state('app.DepotTemplate2', {
        url: "/DepotTemplate2/:AccountId/:visitId/:order/:company/:refresh",
        views: {
            'menuContent': {
                templateUrl: "templates/DepotContainer2.html",
                controller: "DepotTemplate2Ctrl"
            }
        }
    })
    .state('app.DepotTemplate3', {
        url: "/DepotTemplate3/:AccountId/:visitId/:order/:refresh",
        views: {
            'menuContent': {
                templateUrl: "templates/DepotContainer3.html",
                controller: "DepotTemplate3Ctrl"
            }
        }
    })
    .state('app.DepotTemplate1Add', {
        url: "/DepotTemplate1Add/:AccountId/:visitId/:order/:refresh",
        views: {
            'menuContent': {
                templateUrl: "templates/DepotContainer1Add.html",
                controller: "DepotTemplate1AddCtrl"
            }
        }
    })
    .state('app.DepotTemplate1View', {
        url: "/DepotTemplate1View/:AccountId/:visitId/:order/:visitSummaryId/:indexValue",
        views: {
            'menuContent': {
                templateUrl: "templates/DepotContainer1View.html",
                controller: "DepotTemplate1ViewCtrl"
            }
        }
    })
    .state('app.WholeSalerTemplate1', {
        url: "/WholeSalerTemplate1/:AccountId/:visitId/:order/:refresh",
        views: {
            'menuContent': {
                templateUrl: "templates/WholeSalerContainer1.html",
                controller: "WholeSalerTemplate1Ctrl"
            }
        }
    })
    .state('app.WholeSalerTemplate2', {
        url: "/WholeSalerTemplate2/:AccountId/:visitId/:order/:refresh",
        views: {
            'menuContent': {
                templateUrl: "templates/WholeSalerContainer2.html",
                controller: "WholeSalerTemplate2Ctrl"
            }
        }
    })
    .state('app.WholeSalerTemplate3', {
        url: "/WholeSalerTemplate3/:AccountId/:visitId/:order/:refresh",
        views: {
            'menuContent': {
                templateUrl: "templates/WholeSalerContainer3.html",
                controller: "WholeSalerTemplate3Ctrl"
            }
        }
    })
    .state('app.AddNewWholeSalerTemplate3', {
        url: "/AddNewWholeSalerTemplate3/:AccountId/:visitId/:order/:refresh",
        views: {
            'menuContent': {
                templateUrl: "templates/AddNewWholeSalerTemplate3.html",
                controller: 'AddNewWholeSalerTemplate3Ctrl'
            }
        }
    })
    .state('app.WholeSalerTemplate1Add', {
        url: "/WholeSalerTemplate1Add/:AccountId/:visitId/:order/:refresh",
        views: {
            'menuContent': {
                templateUrl: "templates/WholeSalerContainer1Add.html",
                controller: "WholeSalerTemplate1AddCtrl"
            }
        }
    })
    .state('app.WholeSalerTemplate1View', {
        url: "/WholeSalerTemplate1View/:AccountId/:visitId/:order/:visitSummaryId/:indexValue",
        views: {
            'menuContent': {
                templateUrl: "templates/WholeSalerContainer1View.html",
                controller: "WholeSalerTemplate1ViewCtrl"
            }
        }
    })

    .state('app.lastVistSummary', {
        url: "/lastVistSummary/:accountId",
        views: {
            'menuContent': {
                templateUrl: "templates/lastVistSummary.html",
                controller: 'LastVisitSummaryCtrl'

            }
        }
    })
    .state('app.wholesalerlastVistSummary', {
        url: "/wholesalerlastVistSummary/:accountId",
        views: {
            'menuContent': {
                templateUrl: "templates/wLastVistSummary.html",
                controller: 'WholeSalerLastVisitSummaryCtrl'

            }
        }
    })
    .state('app.Objectives', {
        url: "/Objectives/:accountId",
        views: {
            'menuContent': {
                templateUrl: "templates/Objectives.html",
                controller: 'BusinessObjectivesCtrl'

            }
        }
    })
    .state('app.MarketShare', {
        url: "/MarketShare/:accountId",
        views: {
            'menuContent': {
                templateUrl: "templates/marketShare.html",
                controller: 'MarketShareCtrl'

            }
        }
    })
    .state('app.AddOutlets', {
        url: "/AddOutlets/:refresh",
        views: {
            'menuContent': {
                templateUrl: "templates/AddOutlets.html",
                controller: 'AddNewCtrl'
            }
        }
    })
    .state('app.reason', {
        url: "/reason/:refresh",
        views: {
            'menuContent': {
                templateUrl: "templates/Reason.html",
                controller: 'ReasonCtrl'
            }
        }
    })
    .state('app.removeReason', {
        url: "/removeReason/:accountId",
        views: {
            'menuContent': {
                templateUrl: "templates/removeOutletReason.html",
                controller: 'RemoveReasonCtrl'
            }
        }
    })
    .state('app.AddNew', {
        url: "/AddNew/:AccountId/:visitId/:order",
        views: {
            'menuContent': {
                templateUrl: "templates/AddNew.html",
                controller: 'PRIDECallNewCtrl'
            }
        }
    })
    .state('app.AddNew8', {
        url: "/AddNew8/:AccountId/:visitId/:order/:refresh",
        views: {
            'menuContent': {
                templateUrl: "templates/AddNew8.html",
                controller: 'AddNew8Ctrl'
            }
        }
    })
    .state('app.checkout', {
        url: "/checkout/:AccountId/:visitId",
        views: {
            'menuContent': {
                templateUrl: "templates/Checkout.html",
                controller: "CheckoutCtrl"
            }
        }
    })

    .state('app.productOrder', {
        url: "/productOrder/:AccountId/:visitId/:stage/:order/:company/:refresh",
        views: {
            'menuContent': {
                templateUrl: "templates/productOrder.html",
                controller: "productOrderCtrl"
            }
        }
    })
    .state('app.removeUploadReason', {
        url: "/removeUploadReason/:uploadId/:AccountId/:visitId/:order",
        views: {
            'menuContent': {
                templateUrl: "templates/removeUploadReason.html",
                controller: 'PRIDECallCtrl'
            }
        }
    })
    .state('app.showWebServices', {
        url: "/showWebServices",
        views: {
            'menuContent': {
                templateUrl: "templates/webSerLog.html",
                controller: 'webSerCtrl'
            }
        }
    })
    .state('app.showErrors', {
        url: "/showErrors",
        views: {
            'menuContent': {
                templateUrl: "templates/errorLog.html",
                controller: 'errCtrl'
            }
        }
    })
    .state('app.showUserLogs', {
        url: "/showUserLogs",
        views: {
            'menuContent': {
                templateUrl: "templates/userLogs.html",
                controller: 'userLogs'
            }
        }
    })
        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/app/today/bb');
    });