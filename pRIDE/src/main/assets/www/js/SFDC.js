angular.module('UB_PRIDE_APP.SFDC', []).factory('SalesforceSession', ['$rootScope', 'cordovaReady', function($rootScope, cordovaReady) {
        return {
            refreshSalesforceSession: cordovaReady(function(onSuccess, onError) {
                cordova.require("salesforce/plugin/oauth").getAuthCredentials(function() {
                    var that = this,
                        args = arguments;
                    if (onSuccess) {
                        $rootScope.$apply(function() {
                            onSuccess.apply(that, args);
                        });
                    }
                }, function() {
                    var that = this,
                        args = arguments;
                    if (onError) {
                        $rootScope.$apply(function() {
                            onError.apply(that, args);
                        });
                    }
                });
            })
        };
    }])
    // LOCAL DB SOUPS
    //This is a service used to perform soup operations like registerSoup,remove soup,existssoup
    .factory('SOUP', ['$q', '$log', function($q, $log) {
        var soupOperations = function(tableName, indexSpec, operation) {
            var result;
            var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
            if (operation === 'exits') {
                sfSmartstore.soupExists(tableName, function(response) {
                    result = response;
                }, function(error) {
                    result = error;
                });
            } else if (operation === 'register') {
                sfSmartstore.registerSoup(tableName, indexSpec, function(response) {
                    result = response;
                }, function(error) {
                    result = error;
                });
            } else {
                sfSmartstore.removeSoup(tableName, function(response) {
                    result = response;
                }, function(error) {
                    result = error;
                });
            }
            return result;
        }
        return {
            isExists: function(tableName) {
                return soupOperations(tableName, null, 'exits');
            },
            register: function(tableName, indexSpec) {
                return soupOperations(tableName, indexSpec, 'register');
            },
            remove: function(tableName) {
                return soupOperations(tableName, null, 'remove');
            }
        }
    }])
    //Service to add local database
    // .factory("MOBILEDATABASE_ADD", ['$q', '$log', function($q, $log) {
    //     return function(tableName, datalist, indexSpec) {
    //         var defer = $q.defer();
    //         var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
    //         sfSmartstore.upsertSoupEntriesWithExternalId(tableName, datalist, indexSpec, function(response) {
    //             //$log.debug(tableName + ' records are inserted locally');
    //             defer.resolve(response);
    //         }, function(error) {
    //             //$log.debug("insert error" + JSON.stringify(error));
    //             defer.reject(error);
    //         });
    //         return defer.promise;
    //     }
    // }])
    //service to fetch local data using smartquery and extact query and all records from specified table(local DB)
    .factory("SMARTSTORE", ['$q', '$log', function($q, $log) {
        var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
        var buildAllQuerySpecFunc = function(tableName, indexField) {
                var result = [];
                sfSmartstore.querySoup(tableName, sfSmartstore.buildAllQuerySpec(indexField, null), function(response) {
                    if (response.currentPageOrderedEntries != undefined) result = response.currentPageOrderedEntries;
                }, function(error) {
                    result = error;
                });
                return result;
            },
            buildExactQuerySpecFunc = function(tableName, indexField, indexValue) {
                var result = [];
                sfSmartstore.querySoup(tableName, sfSmartstore.buildExactQuerySpec(indexField, indexValue), function(response) {
                    if (response.currentPageOrderedEntries != undefined) result = response.currentPageOrderedEntries;
                }, function(error) {
                    result = error;
                });
                return result;
            },
            buildSmartQuerySpecFunc = function(query, pages) {
                var result = [];
                sfSmartstore.runSmartQuery(sfSmartstore.buildSmartQuerySpec(query, pages), function(response) {
                    if (response.currentPageOrderedEntries != undefined) result = response.currentPageOrderedEntries;
                }, function(error) {
                    result = error;
                });
                return result;
            },
            removeFromSoupFunc = function(tableName, entryIds) {
                var result;
                sfSmartstore.removeFromSoup(tableName, entryIds, function(response) {
                    result = response;
                }, function(error) {
                    result = error;
                });
            }
        return {
            buildExactQuerySpec: function(tableName, indexField, indexValue) {
                return buildExactQuerySpecFunc(tableName, indexField, indexValue);
            },
            buildAllQuerySpec: function(tableName, indexField) {
                return buildAllQuerySpecFunc(tableName, indexField);
            },
            buildSmartQuerySpec: function(query, pages) {
                return buildSmartQuerySpecFunc(query, pages);
            },
            removeFromSoup: function(tableName, entryIds) {
                return removeFromSoupFunc(tableName, entryIds);
            }
        }
    }]).factory('SOUP_REGISTER', ['$q', '$log', function($q, $log) {
        return function(tableName, indexSpec) {
            var defer = $q.defer();
            var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
            var querySpec;
            sfSmartstore.registerSoup(tableName, indexSpec, function(response) {
                // //$log.debug(tableName + ' Soup created');
                defer.resolve(response);
            }, function(error) {
                defer.reject(error);
            });
            return defer.promise;
        }
    }]).factory('REST_API', ['$q', '$log', 'WEBSERVICE_ERROR', function($q, $log, WEBSERVICE_ERROR) {
        return function(path, method, payload, headerParams) {
            var defer = $q.defer();
            var cordovaRe = cordova.require("salesforce/plugin/oauth");
            cordovaRe.getAuthCredentials(function(creds) {
                var apiVersion = "v29.0";
                var credsData = creds;
                if (creds.data) // Event sets the data object with the auth data.
                    credsData = creds.data;
                var forcetkClient = new forcetk.Client(credsData.clientId, credsData.loginUrl, null, cordova.require("salesforce/plugin/oauth").forcetkRefresh);
                forcetkClient.setSessionToken(credsData.accessToken, apiVersion, credsData.instanceUrl);
                forcetkClient.setRefreshToken(credsData.refreshToken);
                forcetkClient.setUserAgentString(credsData.userAgent);
                forcetkClient.apexrest(path, method, payload, headerParams, function(response) {
                    // //$log.debug('onSuccess'+response);
                    defer.resolve(response);
                }, function(error) {
                    // //$log.debug('onError' + error);
                    WEBSERVICE_ERROR('WebserviceCall:Auth', JSON.stringify(error) + '');
                    defer.resolve(error);
                });
            });
            return defer.promise;
        }
    }]).factory('FETCH_OBJECT_RECORDS', ['$q', '$log', 'REST_API', 'SOUP_EXISTS', 'REMOVE_SOUP', 'SOUP_REGISTER', '$filter', 'MOBILEDATABASE_ADD', 'WEBSERVICE', '$interval', 'WEBSERVICE_ERROR', '$rootScope', function($q, $log, REST_API, SOUP_EXISTS, REMOVE_SOUP, SOUP_REGISTER, $filter, MOBILEDATABASE_ADD, WEBSERVICE, $interval, WEBSERVICE_ERROR, $rootScope) {
        return function(pageCount) {
            var defer = $q.defer();
            var isRunning = true;
            var recc = function(pageCount) {
                // //$log.debug('kunalFETCH_OBJECT_RECORDS');
                //if(pageCount==null || pageCount=='')
                // //$log.debug('page is done');
                var mobileData = [];
                if (pageCount == null || pageCount == '') mobileData.push(WEBSERVICE('FetchObjectsSoupsAndRecordsServiceNewAPI'));
                else mobileData.push(WEBSERVICE('FetchObjectsSoupsAndRecordsServiceNewAPI', 'Name'));
                //mobileData[1]['page']='0';
                // //$log.debug('kunal'+JSON.stringify(mobileData));
                console.log('FetchObjectsSoupsAndRecordsServiceNewAPI called');
                console.log(angular.toJson(mobileData));
                //db
                var startTime = Date.now();
                //
                $q.all({
                    OBJECTS_RECORDS: REST_API("/FetchObjectsSoupsAndRecordsServiceNewAPI", 'POST', angular.toJson(mobileData), null)
                }).then(function(response) {
                    //db
                    var elapsedTime=Date.now() - startTime;
                    //
                    //db
                    var today_now = $filter('date')(new Date(), 'dd/MM/yyyy hh:mm a');
                    var logData=[{
                        W_Service_Used : 'FetchObjectsSoupsAndRecordsServiceNewAPI',
                        Time_Consumed : elapsedTime,
                        User : $rootScope.UserId,
                        Date_Of_Cal : today_now
                        //NetWork_Type : $rootScope.getConnectionType()
                    }];
                    MOBILEDATABASE_ADD('Db_All_Logs', logData, '_soupEntryId');
                    //
                    console.log(response);
                    if (typeof Splash !== "undefined" && Splash != undefined && Splash.isActive() == 1) {
                        if($rootScope.progressBarCount<85)
                        $rootScope.progressBarCount = $rootScope.progressBarCount + 15;
                        Splash.setProgressText($rootScope.progressBarCount, 'Saving Data...');
                    }
                    // //$log.debug('dadafasdsad'+JSON.stringify(response))
                    angular.forEach(response.OBJECTS_RECORDS, function(record, key) {
                        var flag = true
                        if (record.ObjectName != undefined) flag = SOUP_EXISTS.soupExists(record.ObjectName);
                        if (flag == false && record.Fields != undefined && record.Fields != null) {
                            record.Fields.push({
                                "path": "IsDirty",
                                "type": "string"
                            });
                            SOUP_REGISTER(record.ObjectName, record.Fields);
                        }
                        if (record.Records != null && record.Records.length > 0)
                            if (record.ObjectName == 'Visit__c') {
                                var visitRecords = [];
                                if (record.Records != null) {
                                    var transactions = [];
                                    angular.forEach(record.Records, function(visit, vk) {
                                        var vrecord = visit;
                                        if (vrecord.Check_Out_DateTime__c != undefined) {
                                            var checkOutDate = new Date(vrecord.Check_Out_DateTime__c);
                                            var checkOutDateNew = $filter("date")(checkOutDate, "dd/MM/yyyy");
                                            var transaction = {
                                                Account: vrecord.Account__c,
                                                stage: 'checkout',
                                                Visit: vrecord.External_Id__c,
                                                status: 'saved',
                                                entryDate: checkOutDateNew
                                            };
                                            transactions.push(transaction);
                                        } else if (vrecord.Check_In_DateTime__c != undefined) {
                                            var checkinDate = new Date(vrecord.Check_In_DateTime__c);
                                            var checkinDateNew = $filter("date")(checkinDate, "dd/MM/yyyy");
                                            var transaction = {
                                                Account: vrecord.Account__c,
                                                stage: '0',
                                                stageValue: 'Container4Ctrl',
                                                Visit: vrecord.External_Id__c,
                                                status: 'pending',
                                                entryDate: checkinDateNew
                                            };
                                            transactions.push(transaction);
                                        }
                                        if (vrecord.Check_In_DateTime__c != undefined) {
                                            var checkinDate = new Date(vrecord.Check_In_DateTime__c);
                                            vrecord.Check_In_DateTime__c = checkinDate.getTime().toString();
                                        }
                                        visitRecords.push(vrecord);
                                    });
                                    MOBILEDATABASE_ADD('Db_transaction', transactions, 'Visit');
                                    MOBILEDATABASE_ADD(record.ObjectName, visitRecords, 'Id');
                                }
                            } else if (record.ObjectName == 'Order__c') {
                            var orderList = [];
                            angular.forEach(record.Records, function(order, vk) {
                                if (order.Permit_issue_Date__c || order.Permit_expiry_Date__c) {
                                    order.Permit_expiry_Date__c = new Date(order.Permit_expiry_Date__c);
                                }
                                orderList.push(order);
                            });
                            MOBILEDATABASE_ADD(record.ObjectName, orderList, 'External_Id__c');
                        } else if (record.ObjectName != undefined && record.ObjectName != '') {
                            if (record.Records != null) {
                                MOBILEDATABASE_ADD(record.ObjectName, record.Records, 'Id');
                            }
                        }

                        if (record.ObjectName == 'Upload__c' && parseInt(record.RecordsCount) != record.Records.length) {
                            ////$log.debug('kunal Upload__c and '+record.Records.length+' still loading data'+record.RecordsCount);
                            angular.forEach(record.Records, function(upload, uploadkey) {
                                if (uploadkey == record.Records.length - 1) {
                                    var lastName = upload.Name;
                                    var today_now = $filter('date')(new Date(), 'dd/MM/yyyy hh:mm a');
                                    var webservice = [{
                                        Webservice: 'FetchObjectsSoupsAndRecordsServiceNewAPI',
                                        name: lastName,
                                        lastSynced: mobileData.lastSynced,
                                    }];
                                    MOBILEDATABASE_ADD('Db_webservice', webservice, 'Webservice');
                                     //db
                                        /*var logData=[{
                                            W_Service_Used : 'FetchObjectsSoupsAndRecordsServiceNewAPI',
                                            Time_Consumed : '-',
                                            User : $rootScope.UserId,
                                            Date_Of_Cal : today_now
                                        }];
                                        MOBILEDATABASE_ADD('Db_All_Logs', logData, '_soupEntryId');*/
                                     //
                                    isRunning = true;
                                }
                            });
                        } else if (record.ObjectName == 'Upload__c' && parseInt(record.RecordsCount) == record.Records.length) {
                            isRunning = false;
                        }
                        if (response.OBJECTS_RECORDS.length == key + 1 && isRunning == false) {
                            var today_now = $filter('date')(new Date(), 'dd/MM/yyyy hh:mm a');
                            var webservice = [{
                                Webservice: 'FetchObjectsSoupsAndRecordsServiceNewAPI',
                                lastSynced: today_now
                            }];
                            MOBILEDATABASE_ADD('Db_webservice', webservice, 'Webservice');
                            //db
                               /* var logData=[{
                                    W_Service_Used : 'FetchObjectsSoupsAndRecordsServiceNewAPI',
                                    Time_Consumed : '-',
                                    User : $rootScope.UserId,
                                    Date_Of_Cal : today_now
                                }];
                                MOBILEDATABASE_ADD('Db_All_Logs', logData, '_soupEntryId');*/
                            //
                            isRunning = false;
                        }
                        if (response.OBJECTS_RECORDS.length == key + 1 && isRunning == true) {
                            recc('1');
                        }
                    });
                    if (!isRunning) defer.resolve('Success');
                }, function(error) {
                    // //$log.debug('FETCH_OBJECT_RECORDS Error' + JSON.stringify(error));
                    WEBSERVICE_ERROR('FetchObjectsSoupsAndRecordsServiceNewAPI', JSON.stringify(error) + '');
                    defer.resolve('Failure');
                });
            }
            recc(pageCount);
            return defer.promise;
        }
    }]).factory('FETCH_MOBILE_SETTINGS', ['$q', '$log', 'REST_API', 'SOUP_EXISTS', 'REMOVE_SOUP', 'SOUP_REGISTER', 'MOBILEDATABASE_ADD', 'WEBSERVICE_ERROR', 'WEBSERVICE', '$filter', 'FETCH_DATA_LOCAL', '$rootScope', function($q, $log, REST_API, SOUP_EXISTS, REMOVE_SOUP, SOUP_REGISTER, MOBILEDATABASE_ADD, WEBSERVICE_ERROR, WEBSERVICE, $filter, FETCH_DATA_LOCAL, $rootScope) {
        return function() {
            var DB_PAGE = "DB_page";
            var DB_SECTION = "DB_section";
            var DB_FIELDS = "DB_fields";
            var DB_PROCESS_FLOW = "Db_process_flow";
            var DB_PROCESS_FLOW_LINE_ITEM = "Db_process_flow_line_item";
            var DB_PLANOGRAM = "Db_planogram";
            var USER_SOUP_NAME = "User";
            var last_sync = null;
            //----- Query Last Sync time from DB
            var mobileSettingsWebService = [];
            var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
            mobileSettingsWebService = FETCH_DATA_LOCAL('Db_webservice', sfSmartstore.buildSmartQuerySpec("SELECT  {Db_webservice:_soup} FROM {Db_webservice} WHERE {Db_webservice:Webservice}='MobileSettingsWebService' ORDER BY {Db_webservice:lastSynced} desc", 1));
            //$log.debug('FetchMobileSettingsWebService ' + JSON.stringify(mobileSettingsWebService));
            if (mobileSettingsWebService != undefined && mobileSettingsWebService != null) {
                angular.forEach(mobileSettingsWebService.currentPageOrderedEntries, function(record, k) {
                    if (record[0].lastSynced != undefined && record[0].Webservice != undefined) {
                        last_sync = record[0].lastSynced;
                    }
                });
            }
            //changes
                var INDEXES_ALL_LOGS = [
                {"path":"W_Service_Used","type":"string"},
                {"path":"Time_Consumed","type":"string"},
                {"path":"User","type":"string"},
                {"path":"Date_Of_Cal","type":"string"},
                {"path":"NetWork_Type","type":"string"}
                ];

                var INDEX_USER_ACTIONS = [
                {"path":"ObjectData","type":"string"},
                {"path":"time_stamp","type":"string"},
                {"path":"comment","type":"string"}];
            //
            var INDEXES_PAGE = [{
                "path": "Is_Planogram_Show__c",
                "type": "string"
            }, {
                "path": "Add_New__c",
                "type": "string"
            }, {
                "path": "Add_New_Label__c",
                "type": "string"
            }, {
                "path": "Id",
                "type": "string"
            }, {
                "path": "Master_Object__c",
                "type": "string"
            }, {
                "path": "Title__c",
                "type": "string"
            }, {
                "path": "Template__c",
                "type": "string"
            }, {
                "path": "Template_Name__c",
                "type": "string"
            }, {
                "path": "Removed__c",
                "type": "string"
            }];
            var INDEXES_SECTIION = [{
                "path": "Id",
                "type": "string"
            }, {
                "path": "Object_API__c",
                "type": "string"
            }, {
                "path": "Order__c",
                "type": "string"
            }, {
                "path": "Page__c",
                "type": "string"
            }, {
                "path": "Record_Type_Id__c",
                "type": "string"
            }, {
                "path": "Record_Type_Name__c",
                "type": "string"
            }, {
                "path": "Title__c",
                "type": "string"
            }, {
                "path": "Type__c",
                "type": "string"
            }, {
                "path": "Input_Object_API__c",
                "type": "string"
            }, {
                "path": "Input_Record_Type_Id__c",
                "type": "string"
            }, {
                "path": "Removed__c",
                "type": "string"
            }];
            var INDEXES_FIELDS = [{
                "path": "Id",
                "type": "string"
            }, {
                "path": "Name",
                "type": "string"
            }, {
                "path": "Field_API__c",
                "type": "string"
            }, {
                "path": "Object__c",
                "type": "string"
            }, {
                "path": "Object_API__c",
                "type": "string"
            }, {
                "path": "Order__c",
                "type": "integer"
            }, {
                "path": "Picklist_Values__c",
                "type": "string"
            }, {
                "path": "Record_Type_Id__c",
                "type": "string"
            }, {
                "path": "Record_Type_Name__c",
                "type": "string"
            }, {
                "path": "Section__c",
                "type": "string"
            }, {
                "path": "Type__c",
                "type": "string"
            }, {
                "path": "Dependent_Field__c",
                "type": "string"
            }, {
                "path": "Dependent_Picklist_Values__c",
                "type": "string"
            }, {
                "path": "Dependent_Value__c",
                "type": "string"
            }, {
                "path": "Process_Flow__c",
                "type": "string"
            }, {
                "path": "Default_Value__c",
                "type": "string"
            }, {
                "path": "Removed__c",
                "type": "string"
            }];
            var INDEXES_FLOW = [{
                "path": "Id",
                "type": "string"
            }, {
                "path": "Chatai__c",
                "type": "string"
            }, {
                "path": "Smile_Market__c",
                "type": "string"
            }, {
                "path": "Retail__c",
                "type": "string"
            }, {
                "path": "Outlet_Type__c",
                "type": "string"
            }, {
                "path": "State__c",
                "type": "string"
            }, {
                "path": "Removed__c",
                "type": "string"
            }];
            var INDEXES_FLOW_ITEMS = [{
                "path": "Id",
                "type": "string"
            }, {
                "path": "Order__c",
                "type": "string"
            }, {
                "path": "Page__c",
                "type": "string"
            }, {
                "path": "Process_Flow__c",
                "type": "string"
            }, {
                "path": "Removed__c",
                "type": "string"
            }];
            var INDEXES_PLANOGRAM = [{
                "path": "Id",
                "type": "string"
            }, {
                "path": "Region__c",
                "type": "string"
            }, {
                "path": "State_Code__c",
                "type": "string"
            }, {
                "path": "Type_of_cooler__c",
                "type": "string"
            }, {
                "path": "Category__c",
                "type": "string"
            }];
            var INDEXES_OUTLET_PRODUCTS = [{
                "path": "Id",
                "type": "string"
            }, {
                "path": "Outlet_Id__c",
                "type": "string"
            }, {
                "path": "Product_Id__c",
                "type": "string"
            }, {
                "path": "Order__c",
                "type": "string"
            }, {
                "path": "Added__c",
                "type": "string"
            }];
            var INDEXES_USER = [{
                "path": "Id",
                "type": "string"
            }, {
                "path": "Name",
                "type": "string"
            }, {
                "path": "Username",
                "type": "string"
            }, {
                "path": "User_ID__c",
                "type": "string"
            }];

            if (SOUP_EXISTS.soupExists(USER_SOUP_NAME) == false) {
                SOUP_REGISTER(USER_SOUP_NAME, INDEXES_USER);
            }

            //changes
            if (SOUP_EXISTS.soupExists('Db_All_Logs') == false) {
                SOUP_REGISTER('Db_All_Logs', INDEXES_ALL_LOGS);
            }
            if (SOUP_EXISTS.soupExists('Db_User_Logs') == false) {
                SOUP_REGISTER('Db_User_Logs', INDEX_USER_ACTIONS);
            }
            //
            if (SOUP_EXISTS.soupExists('Product_Assignment__c') == false) {
                SOUP_REGISTER('Product_Assignment__c', INDEXES_OUTLET_PRODUCTS);
            }
            if (SOUP_EXISTS.soupExists(DB_PROCESS_FLOW) == false) {
                SOUP_REGISTER(DB_PROCESS_FLOW, INDEXES_FLOW);
            }
            if (SOUP_EXISTS.soupExists(DB_PROCESS_FLOW_LINE_ITEM) == false) {
                SOUP_REGISTER(DB_PROCESS_FLOW_LINE_ITEM, INDEXES_FLOW_ITEMS);
            }
            if (SOUP_EXISTS.soupExists(DB_PAGE) == false) {
                SOUP_REGISTER(DB_PAGE, INDEXES_PAGE);
            }
            if (SOUP_EXISTS.soupExists(DB_SECTION) == false) {
                SOUP_REGISTER(DB_SECTION, INDEXES_SECTIION);
            }
            if (SOUP_EXISTS.soupExists(DB_FIELDS) == false) {
                SOUP_REGISTER(DB_FIELDS, INDEXES_FIELDS);
            }
            if (SOUP_EXISTS.soupExists(DB_PLANOGRAM) == false) {
                SOUP_REGISTER(DB_PLANOGRAM, INDEXES_PLANOGRAM);
            }
            
            var defer = $q.defer();
            console.log('MobileConfigurationServiceNewAPI?Time_Stamp= called');
            //db
             var startTime = Date.now();
             //
            $q.all({
                MOBILE_SETTINGS_MAP: REST_API("/MobileConfigurationServiceNewAPI?Time_Stamp=" + last_sync, 'GET', null, null)
            }).then(function(resp) {
                var today_now = $filter('date')(new Date(), 'dd/MM/yyyy hh:mm a');
                //db
                var elapsedTime=Date.now() - startTime;
                //
                //db
                var logData=[{
                    W_Service_Used : 'MobileConfigurationServiceNewAPI',
                    Time_Consumed : elapsedTime,
                    User : $rootScope.UserId,
                    Date_Of_Cal : today_now
                    //NetWork_Type : $rootScope.getConnectionType()
                }];
                MOBILEDATABASE_ADD('Db_All_Logs', logData, '_soupEntryId');
                //
                if (typeof Splash !== "undefined" && Splash != undefined && Splash.isActive() == 1) {
                    if($rootScope.progressBarCount<85)
                    $rootScope.progressBarCount = $rootScope.progressBarCount + 15;
                    Splash.setProgressText($rootScope.progressBarCount, 'Configuring Data...');
                }
                var mobileSettings = resp.MOBILE_SETTINGS_MAP;
                if (mobileSettings.MobilePageRecords != undefined && mobileSettings.MobilePageRecords.length > 0) MOBILEDATABASE_ADD(DB_PAGE, mobileSettings.MobilePageRecords, 'Id');
                if (mobileSettings.MobilePageSectionRecords != undefined && mobileSettings.MobilePageSectionRecords.length > 0) MOBILEDATABASE_ADD(DB_SECTION, mobileSettings.MobilePageSectionRecords, 'Id');
                if (mobileSettings.MobilePageSectionFieldsRecords != undefined && mobileSettings.MobilePageSectionFieldsRecords.length > 0) MOBILEDATABASE_ADD(DB_FIELDS, mobileSettings.MobilePageSectionFieldsRecords, 'Id');
                if (mobileSettings.ProcessFlowRecords != undefined && mobileSettings.ProcessFlowRecords.length > 0) MOBILEDATABASE_ADD(DB_PROCESS_FLOW, mobileSettings.ProcessFlowRecords, 'Id');
                if (mobileSettings.ProcessFlowLineItemRecords != undefined && mobileSettings.ProcessFlowLineItemRecords.length > 0) MOBILEDATABASE_ADD(DB_PROCESS_FLOW_LINE_ITEM, mobileSettings.ProcessFlowLineItemRecords, 'Id');
                if (mobileSettings.MobilePlanogramRecords != undefined && mobileSettings.MobilePlanogramRecords.length > 0) MOBILEDATABASE_ADD(DB_PLANOGRAM, mobileSettings.MobilePlanogramRecords, 'Id');
                //$log.debug(mobileSettings.MobilePlanogramRecords);
                if (mobileSettings.MobilePlanogramImageRecords != undefined && mobileSettings.MobilePlanogramImageRecords.length > 0) MOBILEDATABASE_ADD('Db_images', mobileSettings.MobilePlanogramImageRecords, 'Id');
                //$log.debug(mobileSettings.MobilePlanogramImageRecords);
                defer.resolve('Success');
                //----- Update time stamp in the soup

                var webservice = [{
                    Webservice: 'MobileSettingsWebService',
                    lastSynced: today_now
                }];
                MOBILEDATABASE_ADD('Db_webservice', webservice, 'Webservice');



            }, function(error) {
                WEBSERVICE_ERROR('MobileConfigurationServiceNewAPI', JSON.stringify(error) + '');
                //$log.debug('FETCH_MOBILE_SETTINGS on error' + JSON.stringify(error));
                defer.resolve('Failure');
            });
            var cordovaRe = cordova.require("salesforce/plugin/oauth");
            cordovaRe.getAuthCredentials(function(creds) {
                var apiVersion = "v29.0";
                var credsData = creds;
                if (creds.data) // Event sets the data object with the auth data.
                    credsData = creds.data;
                var forcetkClient = new forcetk.Client(credsData.clientId, credsData.loginUrl, null, cordova.require("salesforce/plugin/oauth").forcetkRefresh);
                forcetkClient.setSessionToken(credsData.accessToken, apiVersion, credsData.instanceUrl);
                forcetkClient.setRefreshToken(credsData.refreshToken);
                forcetkClient.setUserAgentString(credsData.userAgent);
                credsData.userId;
                var userName = null;
                forcetkClient.query("SELECT Id,Name,Username,User_ID__c FROM User where Id='" + credsData.userId + "'", function(response) {
                    var USER_SOUP_NAME = "User";
                    var INDEXES_USER = [{
                        "path": "Id",
                        "type": "string"
                    }, {
                        "path": "Name",
                        "type": "string"
                    }, {
                        "path": "Username",
                        "type": "string"
                    }, {
                        "path": "User_ID__c",
                        "type": "string"
                    }];
                    if (SOUP_EXISTS.soupExists(USER_SOUP_NAME) == false) {
                        SOUP_REGISTER(USER_SOUP_NAME, INDEXES_USER);
                    }
                    var USER_DATA = [{
                        Id: credsData.userId,
                        Name: response.records[0].Name,
                        Username: response.records[0].Username,
                        User_ID__c: response.records[0].User_ID__c
                    }];
                    MOBILEDATABASE_ADD('User', USER_DATA, 'Id');
                    // Splash.SetUserName(response.records[0].Name!=undefined ? ' '+response.records[0].Name+'...' : ' Guest...');
                }, function(error) {
                    //$log.debug('myerror' + error);
                });
            });
            return defer.promise;
        };
    }]).factory('FETCH_DATA_LOCAL', ['$q', '$log','SOUP_EXISTS', function($q, $log,SOUP_EXISTS) {
        return function(tableName, querySpec) {
            var result;
            if(SOUP_EXISTS.soupExists(tableName)){
                var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
                sfSmartstore.runSmartQuery(querySpec, function(response) {
                    result = response;
                }, function(error) {
                    //$log.debug('error' + error);
                    result = error;
                });
            }else{
                $log.debug('Soup '+tableName+' does not exists');
            }
            return result;
        }
    }]).factory("MOBILEDATABASE_ADD", ['$q', '$log', 'SMARTSTORE', function($q, $log, SMARTSTORE) {
        return function(tableName, datalist, indexSpec) {
            var defer = $q.defer();
            try{
                var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
                sfSmartstore.upsertSoupEntriesWithExternalId(tableName, datalist, indexSpec, 
                function(response) {
                    //$log.debug(tableName + ' records are inserted locally one shot');
                    defer.resolve(response);
                }, function(error) {

                    var soupEntryId = [];
                    for (var i = 0; i < datalist.length; i++) {
                        var records = SMARTSTORE.buildExactQuerySpec(tableName, indexSpec, datalist[i][indexSpec]);
                        angular.forEach(records, function(record, key) {
                            if (record._soupEntryId != undefined) soupEntryId.push(record._soupEntryId);
                        });
                    }
                    var result = SMARTSTORE.removeFromSoup(tableName, soupEntryId);
                    sfSmartstore.upsertSoupEntriesWithExternalId(tableName, datalist, indexSpec, function(response) {
                        //$log.debug(tableName + ' records are inserted locally loop');
                    defer.resolve(response);
                    }, function(error1) {

                    });
                });
            }catch (err) {
                defer.resolve(err);
            }
            return defer.promise;

        }
    }])
    // REMOVE SOUPS
    .factory('REMOVE_SOUP', ['$q', '$log', function($q, $log) {
        var handleRequest = function(SOUP_NAME) {
            var defer = $q.defer();
            var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
            sfSmartstore.removeSoup(SOUP_NAME, function(response) {
                //   //$log.debug('on Success'+JSON.stringify(response));
                defer.resolve(response);
            }, function(error) {
                //$log.debug('on error' + JSON.stringify(error));
                defer.reject(error);
            });
            return defer.promise;
        }
        return {
            removeSoup: function(SOUP_NAME) {
                return handleRequest(SOUP_NAME);
            }
        }
    }]).factory('SOUP_EXISTS', ['$q', '$log', function($q, $log) {
        var handleRequest = function(SOUP_NAME) {
            var defer = $q.defer();
            var flag = false;
            var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
            sfSmartstore.soupExists(SOUP_NAME, function(response) {
                if (response == true) {
                    flag = true;
                }
            }, function(error) {
                //$log.debug(SOUP_NAME + 'on error' + JSON.stringify(error));
            });
            return flag;
        }
        return {
            soupExists: function(SOUP_NAME) {
                return handleRequest(SOUP_NAME);
            }
        }
    }]).factory('FETCH_DAILY_PLAN_ACCOUNTS', ['$q', '$log', '$filter', 'FETCH_DATA_LOCAL', 'SOUP_EXISTS', 'FETCH_DATA', function($q, $log, $filter, FETCH_DATA_LOCAL, SOUP_EXISTS, FETCH_DATA) {
        return function(dateParam) {
            var TodaysPlan = [];
            var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
            var newDate;
            if (dateParam == 'today') {
                newDate = new Date();
            } else if (dateParam == 'tomorrow') {
                var newDate = new Date();
                newDate.setDate(newDate.getDate() + 1);
            } else {
                var newDate = new Date();
                newDate.setDate(newDate.getDate() + 2);
            }
            var today = $filter('date')(newDate, 'yyyy-MM-dd');
            var queryVariable = '';
            var routePlanMap = [];
            var ROUTE_PLAN = FETCH_DATA.querySoup('Route_Plan__c', 'Planned_Date__c', today);
            angular.forEach(ROUTE_PLAN.currentPageOrderedEntries, function(record, key) {
                if (record.Route_Outlet__c != undefined && record.Status__c != 'Removed') {
                    routePlanMap[record.Route_Outlet__c] = record;
                    if (queryVariable == '') {
                        queryVariable = queryVariable + "{Route_Assignment__c:Id} = '" + record.Route_Outlet__c + "'";
                    } else {
                        queryVariable = queryVariable + " OR {Route_Assignment__c:Id} = '" + record.Route_Outlet__c + "'";
                    }
                }
            });
            if (queryVariable == '') queryVariable = ' 1==0 ';
            // //$log.debug('Routeplan'+JSON.stringify(ROUTE_PLAN.currentPageOrderedEntries));
            ////$log.debug('Routeplan length'+ROUTE_PLAN.currentPageOrderedEntries.length);
            var routeAssignmentMap = [];
            if (ROUTE_PLAN.currentPageOrderedEntries != undefined && ROUTE_PLAN.currentPageOrderedEntries.length != 0) {
                var routeAssignment = FETCH_DATA_LOCAL('Route_Assignment__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Route_Assignment__c:_soup} FROM {Route_Assignment__c} WHERE (" + queryVariable+") and {Route_Assignment__c:Removed__c}='false'", 100000));
                queryVariable = '';
                angular.forEach(routeAssignment.currentPageOrderedEntries, function(record, key) {
                    if (queryVariable == '') {
                        queryVariable = queryVariable + "{Account:Id} = '" + record[0].Account__c + "'";
                    } else {
                        queryVariable = queryVariable + " OR {Account:Id} = '" + record[0].Account__c + "'";
                    }
                });
                //$log.debug(JSON.stringify(queryVariable));
                if (queryVariable != '') {
                    var order = 1;
                    var todayAccounts = FETCH_DATA_LOCAL('Account', sfSmartstore.buildSmartQuerySpec("SELECT  {Account:_soup} FROM {Account} WHERE " + queryVariable, 100000));
                    angular.forEach(todayAccounts.currentPageOrderedEntries, function(record, key) {
                        //$log.debug('MYaccount ' + JSON.stringify(record[0]));
                        var rec = record[0];
                        rec.VisitType = '';
                        angular.forEach(routeAssignment.currentPageOrderedEntries, function(RA, K) {
                            //  //$log.debug(record[0].Id + "=======" + RA[0].Account__c);
                            if (record[0].Id == RA[0].Account__c && routePlanMap[RA[0].Id] != undefined) {
                                //$log.debug('VisitType' + JSON.stringify(routePlanMap[RA[0].Id]));
                                rec.VisitType = routePlanMap[RA[0].Id].Visit_Type__c;
                                rec.RoutePlanId = routePlanMap[RA[0].Id].Id;
                                rec.RoutePlan = routePlanMap[RA[0].Id];
                                if (routePlanMap[RA[0].Id].Order__c != undefined && routePlanMap[RA[0].Id].Order__c != '') order = routePlanMap[RA[0].Id].Order__c;
                                else {
                                    routePlanMap[RA[0].Id].Order__c = order;
                                    order++;
                                }
                            }
                        });
                        TodaysPlan[order - 1] = rec;
                    });
                }
            }
            return TodaysPlan;
        };
    }]).factory('fetchRecordTypes', ['$log', '$q', 'MOBILEDATABASE_ADD', function($log, $q, MOBILEDATABASE_ADD) {
        return function() {
            var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
            var INDEXES_RT = [{
                'path': 'Id',
                'type': 'string'
            }, {
                'path': 'Name',
                'type': 'string'
            }, {
                'path': 'SobjectType',
                'type': 'string'
            }];
            var result;
            var flag = false
            var SOUP_NAME = 'DB_RecordTypes';
            sfSmartstore.soupExists(SOUP_NAME, function(response) {
                if (response == true) {
                    flag = true;
                }
            }, function(error) {});
            if (flag == false) {
                sfSmartstore.registerSoup(SOUP_NAME, INDEXES_RT, function() {
                    //$log.debug('DB_RecordTypes SOUP SUCCESS');
                }, function() {
                    //$log.debug('DB_RecordTypes SOUP ERROR');
                });
            }
            var cordovaRe = cordova.require("salesforce/plugin/oauth");
            cordovaRe.getAuthCredentials(function(creds) {
                var apiVersion = "v29.0";
                var credsData = creds;
                if (creds.data) // Event sets the data object with the auth data.
                    credsData = creds.data;
                var forcetkClient = new forcetk.Client(credsData.clientId, credsData.loginUrl, null, cordova.require("salesforce/plugin/oauth").forcetkRefresh);
                forcetkClient.setSessionToken(credsData.accessToken, apiVersion, credsData.instanceUrl);
                forcetkClient.setRefreshToken(credsData.refreshToken);
                forcetkClient.setUserAgentString(credsData.userAgent);
                forcetkClient.query("SELECT Id,Name,SobjectType FROM RecordType", function(response) {
                    MOBILEDATABASE_ADD(SOUP_NAME, response.records, 'Id');
                    result = 'Success';
                }, function(error) {
                    result = 'Failure';
                });
            });
            return result;
        };
    }]).factory('WEBSERVICE', ['$q', '$log', 'FETCH_DATA_LOCAL', function($q, $log, FETCH_DATA_LOCAL) {
        return function(webserviceName, callType) {
            var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
            var webserviceList = null;
            if (callType == 'Name') webserviceList = FETCH_DATA_LOCAL('Db_webservice', sfSmartstore.buildSmartQuerySpec("SELECT  {Db_webservice:_soup} FROM {Db_webservice} WHERE {Db_webservice:Webservice}='" + webserviceName + "' ORDER BY {Db_webservice:lastSynced} desc", 1));
            else webserviceList = FETCH_DATA_LOCAL('Db_webservice', sfSmartstore.buildSmartQuerySpec("SELECT  {Db_webservice:_soup} FROM {Db_webservice} WHERE {Db_webservice:Webservice}='" + webserviceName + "' ORDER BY {Db_webservice:name} desc", 1));
            var recordWebservice = null;
            angular.forEach(webserviceList.currentPageOrderedEntries, function(record, key) {
                recordWebservice = record[0];
            });
            return recordWebservice;
        }
    }]).factory('WEBSERVICE_ERROR', ['$q', '$log', '$filter','$window', 'FETCH_DATA_LOCAL', 'MOBILEDATABASE_ADD', '$rootScope', '$timeout', function($q, $log, $filter,$window, FETCH_DATA_LOCAL, MOBILEDATABASE_ADD, $rootScope, $timeout) {
        return function(webserviceName, webserviceError) {
            var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
            webserviceErrorList = FETCH_DATA_LOCAL('Db_webservice_error_log', sfSmartstore.buildSmartQuerySpec("SELECT  {Db_webservice_error_log:_soup} FROM {Db_webservice_error_log}", 100000));
            //$log.debug('ErrorLogs' + JSON.stringify(webserviceErrorList));
            if ($rootScope.toastDone != true && webserviceName.indexOf('Activity') == -1) {
                $rootScope.toastDone = true;
                $log.debug('Something went wrong error');
                //console.log(webserviceError);
                Splash.ShowToast('Something went wrong. Please try again later', 'long', 'bottom', function(a) {
                    $log.debug(a);
                });
                $timeout(function() {
                    $rootScope.toastDone = false;
                }, 5000);
            }
            var today_now = $filter('date')(new Date(), 'dd/MM/yyyy hh:mm a');
            var webserviceErrorData = [{
                Webservice__c: webserviceName,
                Error_Date__c: today_now,
                External_Id__c: (new Date().getTime())+'_'+$window.Math.random() * 10000000,
                User__c: $rootScope.UserId,
                Version__c: $rootScope.appVersion,
                Error_Details__c: webserviceError,
                IsDirty: true
            }];
            MOBILEDATABASE_ADD('Db_webservice_error_log', webserviceErrorData, 'Webservice');
            return 'Logged';
        }
    }]).factory('PUSH_TO_SFDC', ['$rootScope','$q', '$log', '$filter', 'FETCH_DATA', 'REST_API', 'MOBILEDATABASE_ADD', 'FETCH_DATA_LOCAL', 'WEBSERVICE_ERROR', function($rootScope,$q, $log, $filter, FETCH_DATA, REST_API, MOBILEDATABASE_ADD, FETCH_DATA_LOCAL, WEBSERVICE_ERROR) {
        var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
        //used for Visit summary looping and error handling
        //$rootScope.errorArray = [];
        $rootScope.errorAccount = [];
        $rootScope.errorRoutPlan = [];
        $rootScope.errorVisit = [];
        $rootScope.errorVisitSummary = [];
        $rootScope.errorQuerySummary = [];
        $rootScope.errorUpload = [];
        $rootScope.errorAttachment = [];
        $rootScope.errorOrder = [];
        $rootScope.errorException = [];
        $rootScope.looping = 1;
        //console.log($rootScope.errorArray);
        //used for attachemnt lopping
        var exceptionAtt = true;
        var attCount = 2;
        var isAccountsPushed = function() {
            var isSuccess = true,
                defer = $q.defer(),
                accountstoUpdate = [],
                accounts = FETCH_DATA_LOCAL('Account', sfSmartstore.buildSmartQuerySpec("SELECT  {Account:_soup} FROM {Account} WHERE {Account:IsDirty} ='true'" , 100));
                //accounts = FETCH_DATA.querySoup('Account', 'IsDirty', true);
            if (accounts != undefined && accounts.currentPageOrderedEntries != undefined && accounts.currentPageOrderedEntries.length > 0) {
                console.log('Accounts Record Dirty');
                console.log(accounts);
                var ccno=0;
                angular.forEach(accounts.currentPageOrderedEntries, function(record1, key) {
                    var record=record1[0];
                    if(ccno<100){
                        if (record.Location__Latitude__s != undefined && record.Location__Longitude__s != undefined) {
                            var accountRecord = {
                                Id: record.Id,
                                Location__Latitude__s: record.Location__Latitude__s,
                                Location__Longitude__s: record.Location__Longitude__s
                            };
                            accountstoUpdate.push(accountRecord);
                        }
                        ccno++;
                    }
                });
                if (accountstoUpdate.length > 0) {
                    var accountRecords = {};
                    accountRecords.ObjectName = 'Account';
                    accountRecords.Records = accountstoUpdate;
                    var mobileData = [];
                    mobileData.push(accountRecords);
                    //db
                    var startTime = Date.now();
                    //
                    console.log('Accounts Record Push');
                    console.log(accountstoUpdate);
                    $q.all({
                        returnAccounts: REST_API("/CreateRecordsFromMobileApp_POST", 'POST', angular.toJson(mobileData), null)
                    }).then(function(response) {
                        console.log(response);
                        //db
                        var today_now = $filter('date')(new Date(), 'dd/MM/yyyy hh:mm a');
                        var elapsedTime=Date.now() - startTime;
                        //
                        //db
                        var logData=[{
                            W_Service_Used : 'CreateRecordsFromMobileApp_POST',
                            Time_Consumed : elapsedTime,
                            User : $rootScope.UserId,
                            Date_Of_Cal : today_now
                            //NetWork_Type : $rootScope.getConnectionType()
                        }];
                        MOBILEDATABASE_ADD('Db_All_Logs', logData, '_soupEntryId');
                        //
                        if (response != undefined && response.returnAccounts != undefined) {
                            $rootScope.errorAccount = [];
                            isSuccess = true;
                            defer.resolve('SUCCESS');
                        }else{
                            $rootScope.errorAccount['Error'] = 'Account';
                        }
                        
                    }, function(error) {
                        $rootScope.errorAccount['Error'] = 'Account';
                        WEBSERVICE_ERROR('CreateRecordsFromMobileApp_POST:Account', JSON.stringify(error) + '');
                        //isSuccess = false;
                        //defer.reject('Failure');
                        //console.log('CreateRecordsFromMobileApp_POST/Account');
                        isSuccess = true;
                        defer.resolve('SUCCESS');
                    });
                }
                else {
                    defer.resolve('SUCCESS');
                }
            } else {
                $rootScope.errorAccount = [];
                defer.resolve('SUCCESS');
            }
            //return isSuccess;
            return defer.promise;
        }
        var isRoutePlansPushed = function() {
            var isSuccess = true,
                defer = $q.defer(),
                routePlanToPush = [],
                routePlans = FETCH_DATA_LOCAL('Route_Plan__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Route_Plan__c:_soup} FROM {Route_Plan__c} WHERE {Route_Plan__c:IsDirty} ='true'" , 100));
                //routePlans = FETCH_DATA.querySoup('Route_Plan__c', 'IsDirty', true);
            if (routePlans != undefined && routePlans.currentPageOrderedEntries != undefined && routePlans.currentPageOrderedEntries.length > 0) {
                var ccno=0;
                angular.forEach(routePlans.currentPageOrderedEntries, function(record1, key) {
                    var record=record1[0];
                    if(ccno<100){
                        routePlanToPush.push({
                            Route_Outlet__c: record.Route_Outlet__c,
                            Route_Plan__c: record.Route_Plan__c,
                            Status__c: record.Status__c,
                            Planned_Date__c: $filter('date')(record.Planned_Date__c, 'dd/MM/yyyy'),
                            Visit_Type__c: 'Market Visit',
                            Reason__c: record.Reason__c,
                            RecordTypeId: record.RecordTypeId,
                            External_Id__c: record.External_Id__c.toString(),
                            Order__c: record.Order__c
                        });
                        ccno++;
                    }
                });
                if (routePlanToPush.length > 0) {
                    var routePlanRecord = {};
                    routePlanRecord.ObjectName = 'Route_Plan__c';
                    routePlanRecord.Records = routePlanToPush;
                    var mobileData = [];
                    mobileData.push(routePlanRecord);
                    console.log('CreateRecordsFromMobileApp_POST/RoutePlan');
                    //db
                     var startTime = Date.now();
                     //
                    $q.all({
                        MOBILE_SETTINGS_MAP: REST_API("/CreateRecordsFromMobileApp_POST", 'POST', angular.toJson(mobileData), null)
                    }).then(function(resp) {
                        //db
                        var elapsedTime=Date.now() - startTime;
                        var today_now = $filter('date')(new Date(), 'dd/MM/yyyy hh:mm a');
                        //
                        //db
                        var logData=[{
                            W_Service_Used : 'CreateRecordsFromMobileApp_POST',
                            Time_Consumed : elapsedTime,
                            User : $rootScope.UserId,
                            Date_Of_Cal : today_now
                            //NetWork_Type : $rootScope.getConnectionType()
                        }];
                        MOBILEDATABASE_ADD('Db_All_Logs', logData, '_soupEntryId');
                        //
                        var RoutePlanResponseRecords = [];
                        angular.forEach(resp.MOBILE_SETTINGS_MAP[0].Records, function(record, key) {
                            if (record.Id != undefined) {
                                var routePlan = record;
                                routePlan.IsDirty = false;
                                RoutePlanResponseRecords.push(routePlan);
                            }
                        });
                        if (RoutePlanResponseRecords.length > 0) {
                            MOBILEDATABASE_ADD('Route_Plan__c', RoutePlanResponseRecords, 'External_Id__c');
                        }
                        $rootScope.errorRoutPlan=[];
                        isSuccess = true;
                        defer.resolve('SUCCESS');
                    }, function(error) {
                        $rootScope.errorRoutPlan['Error'] = 'RoutePlan';
                        WEBSERVICE_ERROR('CreateRecordsFromMobileApp_POST:RoutePlan', JSON.stringify(error) + '');
                        //isSuccess = false;
                        //defer.reject('Failure');
                        //console.log('CreateRecordsFromMobileApp_POST calles 966');
                        isSuccess = true;
                        defer.resolve('SUCCESS');
                    });
                }
                else {
                    defer.resolve('SUCCESS');
                }
            } else {
                $rootScope.errorRoutPlan = [];
                defer.resolve('SUCCESS');
            }
            //return isSuccess;
            return defer.promise;
        }
        var isVisitsPushed = function() {
            var isSuccess = true,
                defer = $q.defer(),
                recordtoUpdate = [],
                visitRecords = FETCH_DATA_LOCAL('Visit__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Visit__c:_soup} FROM {Visit__c} WHERE {Visit__c:IsDirty} ='true'" , 100));
                //visitRecords = FETCH_DATA.querySoup('Visit__c', 'IsDirty', true);
            //$log.debug(visitRecords);
            if (visitRecords != undefined && visitRecords.currentPageOrderedEntries != undefined && visitRecords.currentPageOrderedEntries.length > 0) {
                var query = '';
                angular.forEach(visitRecords.currentPageOrderedEntries, function(record1, key) {
                    var record=record1[0];
                    if (query != '') {
                        query = query + " OR {Route_Plan__c:Id} = '" + record.Route_Plan__c + "' OR {Route_Plan__c:External_Id__c}='" + record.Route_Plan__c + "'";
                    } else {
                        query = " {Route_Plan__c:Id} = '" + record.Route_Plan__c + "' OR {Route_Plan__c:External_Id__c}='" + record.Route_Plan__c + "'";
                    }
                });
                var routePlans = FETCH_DATA_LOCAL('Route_Plan__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Route_Plan__c:_soup} FROM {Route_Plan__c} WHERE " + query, 100000));
                var routePlanIds = [];
                if (routePlans != undefined && routePlans.currentPageOrderedEntries != undefined && routePlans.currentPageOrderedEntries.length > 0) {
                    angular.forEach(routePlans.currentPageOrderedEntries, function(record, key) {
                        if (record[0].Id != undefined && record[0].External_Id__c != undefined) {
                            routePlanIds[record[0].External_Id__c] = record[0].Id;
                        }
                        if (record[0].Id != undefined) {
                            routePlanIds[record[0].Id] = record[0].Id;
                        }
                    });
                }
                var ccno=0;
                angular.forEach(visitRecords.currentPageOrderedEntries, function(record1, key) {
                    var record=record1[0];
                    if(ccno<100){
                        if (routePlanIds[record.Route_Plan__c] != undefined) {
                            var visitRecord = {};
                            angular.forEach(record, function(v, k) {
                                var str = k.toString();
                                if (str.indexOf('__c') != -1 || str.indexOf('__s') != -1 || k == 'RecordTypeId' || k == 'Id') {
                                    visitRecord[k] = v;
                                    if (k == 'External_Id__c') {
                                        visitRecord[k] = v.toString();
                                    } else if (k == 'Check_In_DateTime__c') {
                                        visitRecord[k] = $filter('date')(v, 'dd/MM/yyyy hh:mm a')
                                    }
                                    if (k == 'Route_Plan__c') visitRecord[k] = routePlanIds[record.Route_Plan__c];
                                }
                            });
                            recordtoUpdate.push(visitRecord);
                        } else if (record.Id != undefined) {
                            var visitRecord = {};
                            angular.forEach(record, function(v, k) {
                                var str = k.toString();
                                if (str.indexOf('__c') != -1 || str.indexOf('__s') != -1 || k == 'RecordTypeId' || k == 'Id') {
                                    visitRecord[k] = v;
                                    if (k == 'External_Id__c') {
                                        visitRecord[k] = v.toString();
                                    } else if (k == 'Check_In_DateTime__c') {
                                        visitRecord[k] = $filter('date')(v, 'dd/MM/yyyy hh:mm a')
                                    }
                                    if (k == 'Route_Plan__c') visitRecord[k] = routePlanIds[record.Route_Plan__c];
                                }
                            });
                            recordtoUpdate.push(visitRecord);
                        }
                        ccno++;
                    }
                });
                if (recordtoUpdate.length > 0) {
                    var visitRecords = {
                        ObjectName: 'Visit__c',
                        Records: recordtoUpdate
                    };
                    var mobileData = [];
                    mobileData.push(visitRecords);
                    var visitResponseRecords = [];
                    console.log('CreateRecordsFromMobileApp_POST/Visit');
                    //db
                     var startTime = Date.now();
                     //
                    $q.all({
                        MOBILE_SETTINGS_MAP: REST_API("/CreateRecordsFromMobileApp_POST", 'POST', angular.toJson(mobileData), null)
                    }).then(function(resp) {
                        //db
                        var elapsedTime=Date.now() - startTime;
                        var today_now = $filter('date')(new Date(), 'dd/MM/yyyy hh:mm a');
                        //
                        //db
                        var logData=[{
                            W_Service_Used : 'CreateRecordsFromMobileApp_POST',
                            Time_Consumed : elapsedTime,
                            User : $rootScope.UserId,
                            Date_Of_Cal : today_now
                            //NetWork_Type : $rootScope.getConnectionType()
                        }];
                        MOBILEDATABASE_ADD('Db_All_Logs', logData, '_soupEntryId');
                        //
                        angular.forEach(resp.MOBILE_SETTINGS_MAP[0].Records, function(record, key) {
                            if (record.Id != undefined) {
                                var visit = record;
                                if (visit.Check_In_DateTime__c != undefined) {
                                    var checkinDate = new Date(visit.Check_In_DateTime__c);
                                    visit.Check_In_DateTime__c = checkinDate.getTime().toString();
                                }
                                visit.IsDirty = false;
                                visitResponseRecords.push(visit);
                            }
                        });
                        if (visitResponseRecords.length) {
                            MOBILEDATABASE_ADD('Visit__c', visitResponseRecords, 'External_Id__c');
                        }
                        $rootScope.errorVisit=[];
                        isSuccess = true;
                        defer.resolve('SUCCESS');
                    }, function(error) {

                        $rootScope.errorVisit['Error'] = 'Visit';
                        WEBSERVICE_ERROR('CreateRecordsFromMobileApp_POST:Visit', JSON.stringify(error) + '');
                        //isSuccess = false;
                        //defer.reject('Failure');
                        isSuccess = true;
                        defer.resolve('SUCCESS');
                    });
                }
                else {
                    defer.resolve('SUCCESS');
                }
            } else {
                $rootScope.errorVisit=[];
                defer.resolve('SUCCESS');
            }

            //try catch block added to handle the system error "Error calling method on NPObject"
            //when the query failed to retireve the records from TAB. when it count more that 20K.
            try{

                var dataSize;
                var visitsSummaryRecordAll =[];
                visitsSummaryRecordAll = FETCH_DATA_LOCAL('Visit_Summary__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Visit_Summary__c:_soup} FROM {Visit_Summary__c} WHERE {Visit_Summary__c:IsDirty} ='true'" , 10000));
                //console.log(visitsSummaryRecordAll);
                if (visitsSummaryRecordAll != undefined && visitsSummaryRecordAll.currentPageOrderedEntries != undefined && visitsSummaryRecordAll.currentPageOrderedEntries.length > 0) {
                    dataSize = visitsSummaryRecordAll.currentPageOrderedEntries.length;
                    dataSize = dataSize/100;
                    $rootScope.looping = Math.round(dataSize);
                    if($rootScope.looping != 0)
                        $rootScope.looping = Math.round(dataSize) + 1;
                }
                console.log(dataSize +' =#Visit#= '+ $rootScope.looping);

            }catch(err){
                $rootScope.errorQuerySummary['Error'] = 'Query Failure';
                WEBSERVICE_ERROR('VisitSummary Data Query Error/POST', JSON.stringify(err) + '');
            }
            
            //$rootScope.looping = 3;
            return defer.promise;
        }

        var isVisitSummaryPushed = function() {

            console.log(' =#VisitSummary#= '+ $rootScope.looping);

            var isSuccess = true,
                defer = $q.defer(),
                visitsSummariesToPush = [],
                visitsSummaryRecords = FETCH_DATA_LOCAL('Visit_Summary__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Visit_Summary__c:_soup} FROM {Visit_Summary__c} WHERE {Visit_Summary__c:IsDirty} ='true'" , 100));
                //visitsSummaryRecords = FETCH_DATA.querySoup('Visit_Summary__c', 'IsDirty', true);
            if (visitsSummaryRecords != undefined && visitsSummaryRecords.currentPageOrderedEntries != undefined && visitsSummaryRecords.currentPageOrderedEntries.length > 0) {
                var query = '';
                var queryForUploads = '';
                angular.forEach(visitsSummaryRecords.currentPageOrderedEntries, function(record1, key) {
                    var record=record1[0];
                    if (query != '') {
                        query = query + " OR {Visit__c:Id} = '" + record.Visit__c + "' OR {Visit__c:External_Id__c}='" + record.Visit__c + "'";
                        if (record.Opened_Visit__c != undefined) {
                            query = query + " OR {Visit__c:Id} = '" + record.Opened_Visit__c + "' OR {Visit__c:External_Id__c}='" + record.Opened_Visit__c + "'";
                        }
                        queryForUploads = queryForUploads + " OR {Upload__c:Id} = '" + record.Upload__c + "' OR {Upload__c:External_Id__c}='" + record.Upload__c + "'";
                    } else {
                        query = " {Visit__c:Id} = '" + record.Visit__c + "' OR {Visit__c:External_Id__c}='" + record.Visit__c + "'";
                        if (record.Opened_Visit__c != undefined) {
                            query = query + " OR {Visit__c:Id} = '" + record.Opened_Visit__c + "' OR {Visit__c:External_Id__c}='" + record.Opened_Visit__c + "'";
                        }
                        queryForUploads = " {Upload__c:Id} = '" + record.Upload__c + "' OR {Upload__c:External_Id__c}='" + record.Upload__c + "'";
                    }
                });
                if(query!='')
                    query='WHERE '+query;
                var visits = FETCH_DATA_LOCAL('Visit__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Visit__c:_soup} FROM {Visit__c} " + query, 100000));
                var visitIds = [];
                if (visits != undefined && visits.currentPageOrderedEntries != undefined && visits.currentPageOrderedEntries.length > 0) {
                    angular.forEach(visits.currentPageOrderedEntries, function(record, key) {
                        if (record[0].Id != undefined && record[0].External_Id__c != undefined) {
                            visitIds[record[0].External_Id__c] = record[0].Id;
                        }
                        if (record[0].Id != undefined) {
                            visitIds[record[0].Id] = record[0].Id;
                        }
                    });
                }
                if(queryForUploads!='')
                    queryForUploads='WHERE '+queryForUploads;
                var uploads = FETCH_DATA_LOCAL('Upload__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Upload__c:_soup} FROM {Upload__c} " + queryForUploads, 100000));
                var uploadIds = [];
                if (uploads != undefined && uploads.currentPageOrderedEntries != undefined && uploads.currentPageOrderedEntries.length > 0) {
                    angular.forEach(uploads.currentPageOrderedEntries, function(record, key) {
                        if (record[0].Id != undefined && record[0].External_Id__c != undefined) {
                            uploadIds[record[0].External_Id__c] = record[0].Id;
                        }
                        if (record[0].Id != undefined) {
                            uploadIds[record[0].Id] = record[0].Id;
                        }
                    });
                }
                var ccno=0;
                angular.forEach(visitsSummaryRecords.currentPageOrderedEntries, function(record1, key) {
                    var record=record1[0];
                    if(ccno<100){
                        if (visitIds[record.Visit__c] != undefined) {
                            var flag = true;
                            if (record.Upload__c != undefined) {
                                if (uploadIds[record.Upload__c] != undefined) 
                                    record.Upload__c = uploadIds[record.Upload__c];
                                else {
                                    flag = false;
                                }
                            }
                            if (flag == true) {
                                var visitSummaryRecord = {};
                                record.Visit__c = visitIds[record.Visit__c];
                                if (record.Opened_Visit__c != undefined) {
                                    record.Opened_Visit__c = visitIds[record.Opened_Visit__c];
                                }
                                angular.forEach(record, function(v, k) {
                                    var str = k.toString();
                                    if (str.indexOf('__c') != -1 || k == 'RecordTypeId') {
                                        if (k != 'AccountId__c') {
                                            visitSummaryRecord[k] = v;
                                            if (k == 'External_Id__c') {
                                                visitSummaryRecord[k] = v.toString();
                                            }
                                        }
                                    }
                                });
                                visitsSummariesToPush.push(visitSummaryRecord);
                            }
                        }
                        ccno++;
                    }
                });
                if (visitsSummariesToPush.length > 0) {
                    var VisitSummaryRecords = {};
                    VisitSummaryRecords.ObjectName = 'Visit_Summary__c';
                    VisitSummaryRecords.Records = visitsSummariesToPush;
                    var mobileData = [];
                    mobileData.push(VisitSummaryRecords);
                    console.log('CreateRecordsFromMobileApp_POST/Visit Summary');
                    //console.log(VisitSummaryRecords);
                    //db
                     var startTime = Date.now();
                     //
                    $q.all({
                        MOBILE_SETTINGS_MAP: REST_API("/CreateRecordsFromMobileApp_POST", 'POST', angular.toJson(mobileData), null)
                    }).then(function(resp) {
                        console.log(resp);
                        //db
                        var today_now = $filter('date')(new Date(), 'dd/MM/yyyy hh:mm a');
                        var elapsedTime=Date.now() - startTime;
                        //
                        //db
                        var logData=[{
                            W_Service_Used : 'CreateRecordsFromMobileApp_POST',
                            Time_Consumed : elapsedTime,
                            User : $rootScope.UserId,
                            Date_Of_Cal : today_now
                            //NetWork_Type : $rootScope.getConnectionType()
                        }];
                        MOBILEDATABASE_ADD('Db_All_Logs', logData, '_soupEntryId');
                        //
                        var visitSummaryResponse = [];
                        angular.forEach(resp.MOBILE_SETTINGS_MAP[0].Records, function(record, key) {
                            if (record.Id != undefined) {
                                var visitSummary = record;
                                visitSummary.IsDirty = false;
                                visitSummaryResponse.push(visitSummary);
                            }
                        });
                        if (visitSummaryResponse.length > 0) {
                            MOBILEDATABASE_ADD('Visit_Summary__c', visitSummaryResponse, 'External_Id__c');

                            if($rootScope.looping != 0){
                                $rootScope.looping--;
                                //call the same until all dirty error free visit summary get synched 
                                isVisitSummaryPushed();
                            }
                        }
                        $rootScope.errorVisitSummary = [];
                        isSuccess = true;
                        defer.resolve('SUCCESS');
                    }, function(error) {
                        
                        $rootScope.errorVisitSummary['Error'] = 'VisitSummary';
                        WEBSERVICE_ERROR('CreateRecordsFromMobileApp_POST:VisitSummary', JSON.stringify(error) + '');
                        //isSuccess = false;Db_webservice_error_log
                        //defer.reject('Failure');
                        isSuccess = true;
                        defer.resolve('SUCCESS');
                    });
                }
                else {
                    defer.resolve('SUCCESS');
                }
            } else {
                $rootScope.errorVisitSummary = [];
                defer.resolve('SUCCESS');
            }

            return defer.promise;
        }

        var isImagesPushed = function() {

            var isSuccess = true,
                defer = $q.defer(),
                attachments;
            if (exceptionAtt) {
                attachments = FETCH_DATA_LOCAL('Db_images', sfSmartstore.buildSmartQuerySpec("SELECT  {Db_images:_soup} FROM {Db_images} WHERE {Db_images:IsDirty} ='true'" , 100));
            } else {
                attachments = FETCH_DATA_LOCAL('Db_images', sfSmartstore.buildSmartQuerySpec("SELECT  {Db_images:_soup} FROM {Db_images} WHERE {Db_images:IsDirty} ='true'" , 50));
            }
            
            console.log(attachments.currentPageOrderedEntries.length + 'images to be pushed --->');
            //console.log(attachments);
                //attachments = FETCH_DATA.querySoup('Db_images', 'IsDirty', true);
            if (attachments.currentPageOrderedEntries != undefined && attachments.currentPageOrderedEntries.length > 0) {
                var queryVariable = '';
                var queryVariableForAccount = '';
                var queryVariableForVisit = '';
                var queryVariableforOrder = '';
                var queryVariableforUpload = '';
                angular.forEach(attachments.currentPageOrderedEntries, function(record1, key) {
                    var record=record1[0];
                    if (queryVariable == '') {
                        queryVariable = queryVariable + "{Visit_Summary__c:External_Id__c} = '" + record.ParentId + "'";
                        queryVariableForAccount = queryVariableForAccount + "{Account:Id} = '" + record.ParentId + "'";
                        queryVariableForVisit = queryVariableForVisit + "{Visit__c:External_Id__c} = '" + record.ParentId + "'";
                        queryVariableforOrder = queryVariableforOrder + "{Order__c:External_Id__c} = '" + record.ParentId + "'";
                        queryVariableforUpload = queryVariableforUpload + "{Upload__c:External_Id__c} = '" + record.ParentId + "'";
                    } else {
                        queryVariable = queryVariable + " OR {Visit_Summary__c:External_Id__c} = '" + record.ParentId + "'";
                        queryVariableForAccount = queryVariableForAccount + " OR {Account:Id} = '" + record.ParentId + "'";
                        queryVariableForVisit = queryVariableForVisit + " OR {Visit__c:External_Id__c} = '" + record.ParentId + "'";
                        queryVariableforOrder = queryVariableforOrder + " OR {Order__c:External_Id__c} = '" + record.ParentId + "'";
                        queryVariableforUpload = queryVariableforUpload + " OR {Upload__c:External_Id__c} = '" + record.ParentId + "'";
                    }
                });
                var visitLocalRecords = FETCH_DATA_LOCAL('Visit_Summary__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Visit_Summary__c:_soup} FROM {Visit_Summary__c} WHERE " + queryVariable, 100000));
                var accountsForImages = FETCH_DATA_LOCAL('Account', sfSmartstore.buildSmartQuerySpec("SELECT  {Account:_soup} FROM {Account} WHERE " + queryVariableForAccount, 100000));
                var visitsForImages = FETCH_DATA_LOCAL('Visit__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Visit__c:_soup} FROM {Visit__c} WHERE " + queryVariableForVisit, 100000));
                var ordersForImages = FETCH_DATA_LOCAL('Order__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Order__c:_soup} FROM {Order__c} WHERE " + queryVariableforOrder, 100000));
                var uploadForImages = FETCH_DATA_LOCAL('Upload__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Upload__c:_soup} FROM {Upload__c} WHERE " + queryVariableforUpload, 100000));


                var visitSummaryIds = [];
                angular.forEach(visitLocalRecords.currentPageOrderedEntries, function(record, key) {
                    if (record[0].Id != undefined) {
                        visitSummaryIds[record[0].External_Id__c] = record[0].Id;
                    }
                });
                angular.forEach(accountsForImages.currentPageOrderedEntries, function(record, key) {
                    if (record[0].Id != undefined) {
                        visitSummaryIds[record[0].Id] = record[0].Id;
                    }
                });
                angular.forEach(visitsForImages.currentPageOrderedEntries, function(record, key) {
                    if (record[0].Id != undefined) {
                        visitSummaryIds[record[0].External_Id__c] = record[0].Id;
                    }
                });
                angular.forEach(ordersForImages.currentPageOrderedEntries, function(record, key) {
                    if (record[0].Id != undefined) {
                        visitSummaryIds[record[0].External_Id__c] = record[0].Id;
                    }
                });
                angular.forEach(uploadForImages.currentPageOrderedEntries, function(record, key) {
                    if (record[0].Id != undefined) {
                        visitSummaryIds[record[0].External_Id__c] = record[0].Id;
                    }
                });
                var attachmentsPushLocal = [];
                var ccno=0;
                angular.forEach(attachments.currentPageOrderedEntries, function(attach1, key) {
                    var attach=attach1[0];
                    // if (visitSummaryIds[attach.ParentId] != undefined && (attach['Name'].indexOf('gallery-')>-1)) {
                    //     attachmentsPushLocal.push({
                    //         Name: attach.Name + '.png',
                    //         ParentId: visitSummaryIds[attach.ParentId],
                    //         Body: attach.Body
                    //     });
                    // }else
                    if(ccno<100){
                        if (visitSummaryIds[attach.ParentId] != undefined) {
                            attachmentsPushLocal.push({
                                Name: attach.External_Id__c + ';' + attach.Name + '.png',
                                ParentId: visitSummaryIds[attach.ParentId],
                                Body: attach.Body
                            });
                        }
                        ccno++;
                    }
                });
                if (attachmentsPushLocal.length > 0) {
                    var attachmentsRecords = {};
                    attachmentsRecords.ObjectName = 'Attachment';
                    attachmentsRecords.Records = attachmentsPushLocal;
                    var mobileData = [];
                    mobileData.push(attachmentsRecords);
                    console.log('CreateRecordsFromMobileApp_POST/Attachment');
                    //db
                    var startTime = Date.now();
                    //
                    $q.all({
                        ATTACHMENT_MAP: REST_API("/CreateRecordsFromMobileApp_POST", 'POST', angular.toJson(mobileData), null)
                    }).then(function(resp) {
                        //db
                        var today_now = $filter('date')(new Date(), 'dd/MM/yyyy hh:mm a');
                        var elapsedTime=Date.now() - startTime;
                        //
                        //db
                        var logData=[{
                            W_Service_Used : 'CreateRecordsFromMobileApp_POST',
                            Time_Consumed : elapsedTime,
                            User : $rootScope.UserId,
                            Date_Of_Cal : today_now
                            //NetWork_Type : $rootScope.getConnectionType()
                        }];
                        MOBILEDATABASE_ADD('Db_All_Logs', logData, '_soupEntryId');
                        //
                        var attachmentsToAddLocal = [];
                        angular.forEach(resp.ATTACHMENT_MAP[0].Records, function(attachment, key) {
                            if (attachment.Id != undefined) {
                                var externalId = attachment.Name;
                                var attachmentrecord = FETCH_DATA.querySoup('Db_images', 'External_Id__c', externalId.split(';')[0]);
                                attachmentsToAddLocal.push({
                                    Id: attachment.Id,
                                    ParentId: attachment.ParentId,
                                    Name:attachment.Name,
                                    External_Id__c: externalId.split(';')[0],
                                    Body: attachmentrecord.currentPageOrderedEntries[0].Body,
                                    Order: attachmentrecord.currentPageOrderedEntries[0].Order,
                                    IsDirty: false
                                });
                            }
                        });
                        if (attachmentsToAddLocal.length > 0) {
                            MOBILEDATABASE_ADD('Db_images', attachmentsToAddLocal, 'External_Id__c');
                        }
                        //this condition is only for if exception occured and we are using count to only have 2 loops 50+50
                        if (exceptionAtt == false && attCount != 0) {
                            attCount--;
                            isImagesPushed();
                        }
                        $rootScope.errorAttachment=[];
                        isSuccess = true;
                        defer.resolve('SUCCESS');
                    }, function(error) {
                        $rootScope.errorAttachment['Error'] = 'Attachment';
                        WEBSERVICE_ERROR('CreateRecordsFromMobileApp_POST:Attachment', JSON.stringify(error) + '');
                        //String errorStr = JSON.stringify(error);
                        //calling image post method with limit 50 as to avoid string length 6million
                        exceptionAtt = false;
                        if (exceptionAtt == false && attCount != 0) {
                            attCount--;
                            isImagesPushed();
                        }
                        //isSuccess = false;
                        //defer.reject('Failure');
                        isSuccess = true;
                        defer.resolve('SUCCESS');
                    });
                } else {
                    defer.resolve('SUCCESS');
                }
            } else {
                $rootScope.errorAttachment=[];
                defer.resolve('SUCCESS');
            }
            return defer.promise;
        }
        var isUploadsPushed = function() {
            var isSuccess = true,
                defer = $q.defer(),
                uploadslist = FETCH_DATA_LOCAL('Upload__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Upload__c:_soup} FROM {Upload__c} WHERE {Upload__c:IsDirty} ='true'" , 100));
                //uploadslist = FETCH_DATA.querySoup('Upload__c', 'IsDirty', true);
            if (uploadslist.currentPageOrderedEntries != undefined && uploadslist.currentPageOrderedEntries.length != 0) {
                var uploadRecordsToPush = [];
                var ccno=0;
                angular.forEach(uploadslist.currentPageOrderedEntries, function(record1, key) {
                    var record=record1[0];
                    if(ccno<100){
                        var uploadRecord = {};
                        angular.forEach(record, function(v, k) {
                            //$log.debug(k + "-----" + v);
                            var str = k.toString();
                            if (str.indexOf('__c') != -1 || k == 'RecordTypeId' || k == 'Id') {
                                if (k != 'Last_MS__c' && k != 'Current_MS__c' && k != 'Sales_Month__c' && k != 'Category__c') {
                                    uploadRecord[k] = v;
                                    if (k == 'External_Id__c') {
                                        uploadRecord[k] = v.toString();
                                    }
                                }
                            }
                        });
                        uploadRecordsToPush.push(uploadRecord);
                        ccno++;
                    }
                });
                if (uploadRecordsToPush.length > 0) {
                    var uploadRecords = {};
                    uploadRecords.ObjectName = 'Upload__c';
                    uploadRecords.Records = uploadRecordsToPush;
                    var mobileData = [];
                    mobileData.push(uploadRecords);
                    //db
                     var startTime = Date.now();
                     //
                    $q.all({
                        MOBILE_SETTINGS_MAP: REST_API("/CreateRecordsFromMobileApp_POST", 'POST', angular.toJson(mobileData), null)
                    }).then(function(resp) {
                        //db
                        var elapsedTime=Date.now() - startTime;
                        var today_now = $filter('date')(new Date(), 'dd/MM/yyyy hh:mm a');
                        //
                        //db
                        var logData=[{
                            W_Service_Used : 'CreateRecordsFromMobileApp_POST',
                            Time_Consumed : elapsedTime,
                            User : $rootScope.UserId,
                            Date_Of_Cal : today_now
                            //NetWork_Type : $rootScope.getConnectionType()
                        }];
                        MOBILEDATABASE_ADD('Db_All_Logs', logData, '_soupEntryId');
                        //
                        var uploadResponse = [];
                        angular.forEach(resp.MOBILE_SETTINGS_MAP[0].Records, function(record, key) {
                            if (record.Id != undefined) {
                                var upload = record;
                                upload.IsDirty = false;
                                uploadResponse.push(upload);
                            }
                        });
                        if (uploadResponse.length > 0) {
                            MOBILEDATABASE_ADD('Upload__c', uploadResponse, 'External_Id__c');
                        }
                        $rootScope.errorUpload=[];
                        isSuccess = true;
                        defer.resolve('SUCCESS');
                    }, function(error) {
                        $rootScope.errorUpload['Error'] = 'Upload';
                        WEBSERVICE_ERROR('CreateRecordsFromMobileApp_POST:Upload', JSON.stringify(error) + '');
                        //isSuccess = false;
                        //defer.reject('Failure');
                        isSuccess = true;
                        defer.resolve('SUCCESS');
                    });
                }
                else {
                    defer.resolve('SUCCESS');
                }
            } else {
                $rootScope.errorUpload=[];
                defer.resolve('SUCCESS');
            }
            return defer.promise;
        }
        var isOrdersPushed = function() {
            var isSuccess = true,
                defer = $q.defer(),
                orderslist = FETCH_DATA_LOCAL('Order__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Order__c:_soup} FROM {Order__c} WHERE {Order__c:IsDirty} ='true'" , 100));
                //orderslist = FETCH_DATA.querySoup('Order__c', 'IsDirty', true);
            if (orderslist.currentPageOrderedEntries != undefined && orderslist.currentPageOrderedEntries.length != 0) {
                var ordersToPush = [],
                    visitQuery = '',
                    orderQuery = '',
                    visitIds = [],
                    orderIds = [];
                angular.forEach(orderslist.currentPageOrderedEntries, function(record1, key) {
                    var record=record1[0];
                    if (record.Account__c != undefined && record.Visit__c == undefined) {
                        var orderRecord = {};
                        angular.forEach(record, function(v, k) {
                            var str = k.toString();
                            if (str.indexOf('__c') != -1 || k == 'RecordTypeId') {
                                orderRecord[k] = v;
                                if (k == 'External_Id__c') {
                                    orderRecord[k] = v.toString();
                                } else if (k == 'Permit_expiry_Date__c' || k == 'Permit_issue_Date__c') {
                                    orderRecord[k] = $filter('date')(new Date(v), 'dd/MM/yyyy');
                                }
                            }
                        });
                        ordersToPush.push(orderRecord);
                    } else {
                        if (visitQuery == '') {
                            visitQuery = '{Visit__c:External_Id__c} ="' + record.Visit__c + '" OR {Visit__c:Id}="' + record.Visit__c + '"';
                        } else {
                            visitQuery = visitQuery + ' OR {Visit__c:External_Id__c} ="' + record.Visit__c + '" OR {Visit__c:Id}="' + record.Visit__c + '"';
                        }
                        if (orderQuery == '') {
                            orderQuery = '{Order__c:External_Id__c} ="' + record.Order__c + '" OR {Order__c:Id}="' + record.Order__c + '"';
                        } else {
                            orderQuery = orderQuery + ' OR {Order__c:External_Id__c} ="' + record.Order__c + '" OR {Order__c:Id}="' + record.Order__c + '"';
                        }
                    }
                });
                if (visitQuery != '') {
                    var visits = FETCH_DATA_LOCAL('Visit__c', sfSmartstore.buildSmartQuerySpec('SELECT  {Visit__c:_soup} FROM {Visit__c} WHERE ' + visitQuery, 100000));
                    angular.forEach(visits.currentPageOrderedEntries, function(visit, key) {
                        visitIds[visit[0].External_Id__c] = visit[0].Id;
                        visitIds[visit[0].Id] = visit[0].Id;
                    });
                }
                if (orderQuery != '') {
                    var orders = FETCH_DATA_LOCAL('Order__c', sfSmartstore.buildSmartQuerySpec('SELECT  {Order__c:_soup} FROM {Order__c} WHERE ' + orderQuery, 100000));
                    angular.forEach(orders.currentPageOrderedEntries, function(order, key) {
                        orderIds[order[0].External_Id__c] = order[0].Id;
                        orderIds[order[0].Id] = order[0].Id;
                    });
                }
                var ccno=0;
                angular.forEach(orderslist.currentPageOrderedEntries, function(record, key) {
                    if(ccno<100){
                        if (record.Visit__c != undefined && record.Order__c != undefined && orderIds[record.Order__c] != undefined && visitIds[record.Visit__c] != undefined) {
                            var orderRecord = {};
                            angular.forEach(record, function(v, k) {
                                var str = k.toString();
                                if (str.indexOf('__c') != -1 || k == 'RecordTypeId') {
                                    orderRecord[k] = v;
                                    if (k == 'External_Id__c') {
                                        orderRecord[k] = v.toString();
                                    } else if (k == 'Permit_expiry_Date__c' || k == 'Permit_issue_Date__c') {
                                        orderRecord[k] = $filter('date')(new Date(v), 'dd/MM/yyyy');
                                    }
                                }
                            });
                            orderRecord.Visit__c = visitIds[record.Visit__c];
                            orderRecord.Order__c = orderIds[record.Order__c];
                            ordersToPush.push(orderRecord);
                        }
                        ccno++;
                    }
                });
                if (ordersToPush.length > 0) {
                    var orderRecords = {};
                    orderRecords.ObjectName = 'Order__c';
                    orderRecords.Records = ordersToPush;
                    var mobileData = [];
                    mobileData.push(orderRecords);
                    //db
                    var startTime = Date.now();
                    //
                    $q.all({
                        MOBILE_SETTINGS_MAP: REST_API("/CreateRecordsFromMobileApp_POST", 'POST', angular.toJson(mobileData), null)
                    }).then(function(resp) {
                        //db
                        var today_now = $filter('date')(new Date(), 'dd/MM/yyyy hh:mm a');
                        var elapsedTime=Date.now() - startTime;
                        //
                        //db
                        var logData=[{
                            W_Service_Used : 'CreateRecordsFromMobileApp_POST',
                            Time_Consumed : elapsedTime,
                            User : $rootScope.UserId,
                            Date_Of_Cal : today_now
                            //NetWork_Type : $rootScope.getConnectionType()
                        }];
                        MOBILEDATABASE_ADD('Db_All_Logs', logData, '_soupEntryId');
                        //
                        var orderResponse = [];
                        angular.forEach(resp.MOBILE_SETTINGS_MAP[0].Records, function(record, key) {
                            if (record.Id != undefined) {
                                var order = record;
                                if (order.Permit_issue_Date__c != undefined) order.Permit_issue_Date__c = new Date(order.Permit_issue_Date__c);
                                if (order.Permit_expiry_Date__c != undefined) order.Permit_expiry_Date__c = new Date(order.Permit_expiry_Date__c);
                                order.IsDirty = false;
                                orderResponse.push(order);
                            }
                        });
                        if (orderResponse.length > 0) {
                            MOBILEDATABASE_ADD('Order__c', orderResponse, 'External_Id__c');
                        }
                        $rootScope.errorOrder=[];
                        isSuccess = true;
                        defer.resolve('SUCCESS');
                    }, function(error) {
                        $rootScope.errorOrder['Error'] = 'Order';
                        WEBSERVICE_ERROR('CreateRecordsFromMobileApp_POST:Order', JSON.stringify(error) + '');
                        //isSuccess = false;
                        //defer.reject('Failure');
                        isSuccess = true;
                        defer.resolve('SUCCESS');
                    });
                }
                else {
                    defer.resolve('SUCCESS');
                }
            } else {
                $rootScope.errorOrder=[];
                defer.resolve('SUCCESS');
            }
            return defer.promise;
        }
        var isExceptionsPushed = function() {
            var isSuccess = true,
                defer = $q.defer(),
                exceptionList = FETCH_DATA_LOCAL('Db_webservice_error_log', sfSmartstore.buildSmartQuerySpec("SELECT  {Db_webservice_error_log:_soup} FROM {Db_webservice_error_log} WHERE {Db_webservice_error_log:IsDirty} ='true'" , 100));
                //exceptionList = FETCH_DATA.querySoup('Db_webservice_error_log', 'IsDirty', true);
            if (exceptionList.currentPageOrderedEntries != undefined && exceptionList.currentPageOrderedEntries.length != 0) {
                var exceptionToPush = [],
                    exceptionQuery = '';
                var ccno=0;
                angular.forEach(exceptionList.currentPageOrderedEntries, function(record1, key) {
                    var record=record1[0];
                    if(ccno<100){
                        exceptionToPush.push(record);
                        ccno++;
                    }
                });
                if (exceptionToPush.length > 0) {
                    var exceptionRecords = {};
                    exceptionRecords.ObjectName = 'Mobile_Exception__c';
                    exceptionRecords.Records = exceptionToPush;
                    var mobileData = [];
                    mobileData.push(exceptionRecords);
                    //db
                    var startTime = Date.now();
                    //
                    $q.all({
                        MOBILE_SETTINGS_MAP: REST_API("/CreateRecordsFromMobileApp_POST", 'POST', angular.toJson(mobileData), null)
                    }).then(function(resp) {
                        //db
                        var elapsedTime=Date.now() - startTime;
                        var today_now = $filter('date')(new Date(), 'dd/MM/yyyy hh:mm a');
                        //
                        //db
                        var logData=[{
                            W_Service_Used : 'CreateRecordsFromMobileApp_POST',
                            Time_Consumed : elapsedTime,
                            User : $rootScope.UserId,
                            Date_Of_Cal : today_now
                            //NetWork_Type : $rootScope.getConnectionType()
                        }];
                        MOBILEDATABASE_ADD('Db_All_Logs', logData, '_soupEntryId');
                        //
                        var exceptionResponse = [];
                        angular.forEach(resp.MOBILE_SETTINGS_MAP[0].Records, function(record, key) {
                            if (record.Id != undefined) {
                                var exceptionRecord = record;
                                exceptionRecord.IsDirty = false;
                                exceptionResponse.push(exceptionRecord);
                            }
                        });
                        if (exceptionResponse.length > 0) {
                            MOBILEDATABASE_ADD('Db_webservice_error_log', exceptionResponse, 'External_Id__c');
                        }
                        $rootScope.errorException=[];
                        isSuccess = true;
                        defer.resolve('SUCCESS');
                    }, function(error) {
                        $rootScope.errorException['Error'] = 'MobileException';
                        WEBSERVICE_ERROR('CreateRecordsFromMobileApp_POST:MobileException', JSON.stringify(error) + '');
                        //isSuccess = false;
                        //defer.reject('Failure');
                        isSuccess = true;
                        defer.resolve('SUCCESS');
                    });
                }
                else {
                    defer.resolve('SUCCESS');
                }
            } else {
                $rootScope.errorException=[];
                defer.resolve('SUCCESS');
            }
            return defer.promise;
        }
        return {
            accounts: function() {
                return isAccountsPushed();
            },
            routePlans: function() {
                return isRoutePlansPushed();
            },
            visits: function() {
                return isVisitsPushed();
            },
            visitSummaries: function() {
                return isVisitSummaryPushed();
            },
            attachments: function() {
                return isImagesPushed();
            },
            uploads: function() {
                return isUploadsPushed();
            },
            orders: function() {
                return isOrdersPushed();
            },
            exceptions: function() {
                return isExceptionsPushed();
            }
        }
    }]).factory('SYNC_SFDC', ['$q', '$log', 'PUSH_TO_SFDC', function($q, $log, PUSH_TO_SFDC) {
        return function() {
            var defer = $q.defer();
            $q.all({
                EXCEPTIONS: PUSH_TO_SFDC.exceptions()
            }).then(function(exceptionsResponse) {
                $log.debug('exceptions pushed');
            },function(exceptionsError) {
                $log.debug('exceptions not pushed');
            });
            $q.all({
                ACCOUNTS: PUSH_TO_SFDC.accounts()
            }).then(function(accountsResponse) {
                ////$log.debug('accountsResponse' + JSON.stringify(accountsResponse));
                if (accountsResponse.ACCOUNTS != undefined) {
                    $q.all({
                        UPLOADS: PUSH_TO_SFDC.uploads()
                    }).then(function(uploadsResponse) {
                        if (uploadsResponse.UPLOADS != undefined) {
                            $q.all({
                                ROUTEPLANS: PUSH_TO_SFDC.routePlans()
                            }).then(function(routePlansresponse) {
                                if (routePlansresponse.ROUTEPLANS != undefined) {
                                    $q.all({
                                        VISITS: PUSH_TO_SFDC.visits()
                                    }).then(function(visitsResponse) {
                                        if (visitsResponse.VISITS != undefined) {
                                            $q.all({
                                                VISITSUMMARIES: PUSH_TO_SFDC.visitSummaries()
                                            }).then(function(visitSummariesResponse) {
                                                if (visitSummariesResponse.VISITSUMMARIES != undefined) {
                                                    $q.all({
                                                        ORDERS: PUSH_TO_SFDC.orders()
                                                    }).then(function(ordersResponse) {
                                                            if (ordersResponse.ORDERS != undefined) {
                                                                $q.all({
                                                                    ORDER_LINE_ITEM: PUSH_TO_SFDC.orders()
                                                                }).then(function(orderLineItemsResponse) {
                                                                        if (orderLineItemsResponse.ORDER_LINE_ITEM != undefined) {
                                                                                $q.all({
                                                                                    ATTACHMENTS: PUSH_TO_SFDC.attachments()
                                                                                }).then(function(attachmentsResponse) {
                                                                                        if (attachmentsResponse.ATTACHMENTS != undefined) {
                                                                                            defer.resolve('Success');
                                                                                        }
                                                                                },function(attachmentsError) {
                                                                                    defer.reject('Failure');
                                                                                });
                                                                            }
                                                                        },
                                                                        function(orderLineItemsErrors) {
                                                                            $log.debug(orderLineItemsErrors);
                                                                            defer.reject('Failure');
                                                                        });
                                                                }
                                                            },
                                                            function(orderErrors) {
                                                                $log.debug(orderErrors);
                                                                defer.reject('Failure');
                                                            });
                                                    }
                                            }, function(visitSummaryError) {
                                                defer.reject('Failure');
                                            });
                                        }
                                    }, function(VisitError) {
                                        defer.reject('Failure');
                                    });
                                }
                            }, function(routePlanError) {
                                defer.reject('Failure');
                            });
                        }
                    }, function(uploaError) {
                        defer.reject('Failure');
                    });
                }
            }, function(accountError) {
                $log.debug(accountError);
                $q.all({
                    UPLOADS: PUSH_TO_SFDC.uploads()
                }).then(function(uploadsResponse) {
                    //$log.debug('uploadsResponse' + JSON.stringify(uploadsResponse));
                    if (uploadsResponse.UPLOADS != undefined) {
                        $q.all({
                            ROUTEPLANS: PUSH_TO_SFDC.routePlans()
                        }).then(function(routePlansresponse) {
                            //$log.debug('routePlansresponse' + JSON.stringify(routePlansresponse));
                            if (routePlansresponse.ROUTEPLANS != undefined) {
                                $q.all({
                                    VISITS: PUSH_TO_SFDC.visits()
                                }).then(function(visitsResponse) {
                                    //$log.debug('visitsResponse' + JSON.stringify(visitsResponse));
                                    if (visitsResponse.VISITS != undefined) {
                                        $q.all({
                                            VISITSUMMARIES: PUSH_TO_SFDC.visitSummaries()
                                        }).then(function(visitSummariesResponse) {
                                            //$log.debug('visitSummariesResponse' + JSON.stringify(visitSummariesResponse));
                                            if (visitSummariesResponse.VISITSUMMARIES != undefined) {
                                                $q.all({
                                                    ATTACHMENTS: PUSH_TO_SFDC.attachments()
                                                }).then(function(attachmentsResponse) {
                                                    //$log.debug('attachmentsResponse' + JSON.stringify(attachmentsResponse));
                                                    if (attachmentsResponse.ATTACHMENTS != undefined) {
                                                        defer.resolve('Success');
                                                    }
                                                }, function(attachmentsError) {
                                                    defer.reject('Failure');
                                                });
                                            }
                                        }, function(visitSummaryError) {
                                            defer.reject('Failure');
                                        });
                                    }
                                }, function(VisitError) {
                                    defer.reject('Failure');
                                });
                            }
                        }, function(routePlanError) {
                            defer.reject('Failure');
                        });
                    }
                }, function(uploaError) {
                    defer.reject('Failure');
                });
            });
            return defer.promise;
        }
    }]).factory('FETCH_DATA', ['$q', '$log', function($q, $log) {
        var handleRequest = function(tableName, indexSpec, indexValue) {
            var result;
            var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
            var querySpec = sfSmartstore.buildAllQuerySpec(indexSpec, indexValue);
            if (indexValue != null) {
                querySpec = sfSmartstore.buildExactQuerySpec(indexSpec, indexValue);
            }
            sfSmartstore.querySoup(tableName, querySpec, function(response) {
                result = response;
            }, function(error) {
                result = error;
            });
            return result;
        }
        return {
            querySoup: function(tableName, indexSpec, indexValue) {
                return handleRequest(tableName, indexSpec, indexValue);
            }
        }
    }]).factory('FETCH_LOCAL_DATA', ['$q', '$log', function($q, $log) {
        return function(tableName, indexSpec, indexValue) {
            var result;
            var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
            var querySpec = sfSmartstore.buildAllQuerySpec(indexSpec, indexValue);
            if (indexValue != null) {
                querySpec = sfSmartstore.buildExactQuerySpec(indexSpec, indexValue);
            }
            sfSmartstore.querySoup(tableName, querySpec, function(response) {
                if (response.currentPageOrderedEntries != undefined) {
                    result = response.currentPageOrderedEntries;
                }
            }, function(error) {
                result = error;
            });
            return result;
        }
    }]).factory('IMG_SOUP', ['$q', '$log', 'SOUP_EXISTS', 'SOUP_REGISTER', function($q, $log, SOUP_EXISTS, SOUP_REGISTER) {
        return function() {
            var SOUP_NAME = 'Db_images';
            var flag = SOUP_EXISTS.soupExists(SOUP_NAME);
            var TRANSACTION_SOUP_NAME = 'Db_transaction';
            var flag1 = SOUP_EXISTS.soupExists(TRANSACTION_SOUP_NAME);
            if (flag === false) {
                var INDEXES_IMG = [{
                    "path": "Id",
                    "type": "string"
                }, {
                    "path": "ParentId",
                    "type": "string"
                }, {
                    "path": "Name",
                    "type": "string"
                }, {
                    "path": "Body",
                    "type": "string"
                }, {
                    "path": "IsDirty",
                    "type": "string"
                }, {
                    "path": "External_Id__c",
                    "type": "string"
                }];
                SOUP_REGISTER(SOUP_NAME, INDEXES_IMG);
            }
            if (flag1 === false) {
                var INDEXES_TRANSACTION = [{
                    "path": "Account",
                    "type": "string"
                }, {
                    "path": "Visit",
                    "type": "string"
                }, {
                    "path": "stage",
                    "type": "string"
                }, {
                    "path": "stageValue",
                    "type": "string"
                }, {
                    "path": "status",
                    "type": "string"
                }, {
                    "path": "entryDate",
                    "type": "string"
                }];
                SOUP_REGISTER(TRANSACTION_SOUP_NAME, INDEXES_TRANSACTION);
            }
            if (SOUP_EXISTS.soupExists('Db_webservice') == false) {
                var INDEXES_WEBSERVICE = [{
                    "path": "Webservice",
                    "type": "string"
                }, {
                    "path": "lastSynced",
                    "type": "string"
                }, {
                    "path": "name",
                    "type": "string"
                }];
                SOUP_REGISTER('Db_webservice', INDEXES_WEBSERVICE);
            }
            if (SOUP_EXISTS.soupExists('Db_webservice_error_log') == false) {
                var INDEXES_WEBSERVICE = [{
                    "path": "Webservice__c",
                    "type": "string"
                }, {
                    "path": "Error_Date__c",
                    "type": "string"
                }, {
                    "path": "Error_Details__c",
                    "type": "string"
                }, {
                    "path": "IsDirty",
                    "type": "string"
                }, {
                    "path": "External_Id__c",
                    "type": "string"
                }, {
                    "path": "User__c",
                    "type": "string"
                }, {
                    "path": "Version__c",
                    "type": "string"
                }];
                SOUP_REGISTER('Db_webservice_error_log', INDEXES_WEBSERVICE);
            }
        };
    }]).factory('FETCH_REMOTE_DATA', ['$q', '$log', 'FETCH_MOBILE_SETTINGS', 'FETCH_OBJECT_RECORDS', 'fetchRecordTypes', 'IMG_SOUP', 'FETCH_MOBILE_ATTACHMENTS', '$timeout', '$rootScope', function($q, $log, FETCH_MOBILE_SETTINGS, FETCH_OBJECT_RECORDS, fetchRecordTypes, IMG_SOUP, FETCH_MOBILE_ATTACHMENTS, $timeout, $rootScope) {
        return function() {
            var defer = $q.defer();
            IMG_SOUP();
            // //$log.debug('FETCH_REMOTE_DATA');
            if (typeof Splash !== "undefined" && Splash != undefined && Splash.isActive() == 1) {
                if($rootScope.progressBarCount<85)
                $rootScope.progressBarCount = $rootScope.progressBarCount + 15;
                Splash.setProgressText($rootScope.progressBarCount, 'Downloading Data...');
            }
            $q.all({
                OBJECT_STATUS: FETCH_OBJECT_RECORDS(),
                SETTINGS_STATUS: FETCH_MOBILE_SETTINGS(),
                RT_STATUS: fetchRecordTypes(),
                ATTACHMENT_STATUS: FETCH_MOBILE_ATTACHMENTS()
            }).then(function(response) {
                $log.debug("response :: "+JSON.stringify(response));
                defer.resolve(response);
            }, function(error) {
                defer.reject(error);
                $log.debug("Exception :: "+JSON.stringify(error));
            });
            return defer.promise;
        }
    }]).factory('GEO_LOCATION', ['$q', '$log', function($q, $log) {
        var handleRequest = function() {
            var defer = $q.defer();
            var telephoneNumber = cordova.require("cordova/plugin/telephonenumber");
            telephoneNumber.get(function(result) {
                $log.info('FROM FACTORY' + JSON.stringify(result));
                defer.resolve(result);
            }, function(error1) {
                var error = "Unable to capture LOCATION";
                $log.info(error + JSON.stringify(error1));
                defer.reject(error);
            });
            return defer.promise;
        };
        return {
            getCurrentPosition: function() {
                return handleRequest();
            }
        };
    }]).factory('GEO_LOCATION_ACCOUNT', ['$q', '$log', function($q, $log) {
        var handleRequest = function() {
            var defer = $q.defer();
            var telephoneNumber = cordova.require("cordova/plugin/telephonenumber");
            telephoneNumber.getNew(function(result) {
                $log.info('FROM FACTORY' + JSON.stringify(result));
                defer.resolve(result);
            }, function(error1) {
                var error = "Unable to capture LOCATION";
                $log.info(error + JSON.stringify(error1));
                defer.reject(error);
            });
            return defer.promise;
        };
        return {
            getCurrentPosition: function() {
                return handleRequest();
            }
        };
    }]).factory('WRITE_TO_FILE', ['$q', '$log', function($q, $log) {
        var handleRequest = function(error,service,userLog) {
            var defer = $q.defer();
            var writeToLog = cordova.require("cordova/plugin/writetofile");
            writeToLog.write(error,service,userLog,function(result) {
                $log.info('FROM FACTORY' + JSON.stringify(result));
                defer.resolve(result);
            }, function(error1) {
                var error = "Unable to Write";
                $log.info(error + JSON.stringify(error1));
                defer.reject(error);
            });
            return defer.promise;
        };
        return {
            write: function(error,service,userLog) {
                return handleRequest(error,service,userLog);
            }
        };
    }]).factory('FETCH_MOBILE_ATTACHMENTS', ['$rootScope','$q', '$log', 'REST_API', 'SOUP_EXISTS', 'REMOVE_SOUP', 'SOUP_REGISTER', 'MOBILEDATABASE_ADD', 'WEBSERVICE_ERROR', 'WEBSERVICE', '$filter', 'FETCH_DATA_LOCAL', function($rootScope,$q, $log, REST_API, SOUP_EXISTS, REMOVE_SOUP, SOUP_REGISTER, MOBILEDATABASE_ADD, WEBSERVICE_ERROR, WEBSERVICE, $filter, FETCH_DATA_LOCAL) {
        return function() {

            var defer = $q.defer();
            var isRunning = true;
            var recc = function() {
                var last_sync,last_name = null;
                //----- Query Last Sync time from DB
                var attachmentWebService = [];
                var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
                attachmentWebService = FETCH_DATA_LOCAL('Db_webservice', sfSmartstore.buildSmartQuerySpec("SELECT  {Db_webservice:_soup} FROM {Db_webservice} WHERE {Db_webservice:Webservice}='FetchVisitSummaryAttachments' ORDER BY {Db_webservice:lastSynced} desc,{Db_webservice:name} desc", 1));
                //attachmentWebService = WEBSERVICE('FetchVisitSummaryAttachments');
                //$log.debug('FetchVisitSummaryAttachments AttachmentWebService ' + JSON.stringify(attachmentWebService));
                //var attachmentWebServiceAll =FETCH_DATA_LOCAL('Db_webservice', sfSmartstore.buildSmartQuerySpec("SELECT  {Db_webservice:_soup} FROM {Db_webservice} ", 100));
                ////$log.debug('FetchVisitSummaryAttachments All ' + attachmentWebServiceAll);
                if (attachmentWebService != undefined && attachmentWebService != null) {
                    angular.forEach(attachmentWebService.currentPageOrderedEntries, function(record, k) {
                        if (record[0].Webservice != undefined) {
                            last_sync = record[0].lastSynced;
                            last_name = record[0].name;
                        }
                    });
                }
                if(last_sync==undefined)
                    last_sync='';
                if(last_name==undefined)
                    last_name='';
                //$log.debug('FetchVisitSummaryAttachments Last Sync ' + last_sync);
                //db
                var startTime = Date.now();
                //
                $q.all({
                    MOBILE_SETTINGS_MAP: REST_API("/FetchVisitSummaryAttachments?Time_Stamp=" + last_sync+"&Name="+last_name, 'GET', null, null)
                }).then(function(resp) {
                    //db
                    var elapsedTime=Date.now() - startTime;
                    var today_now = $filter('date')(new Date(), 'dd/MM/yyyy hh:mm a');
                    //
                    //db
                    var logData=[{
                        W_Service_Used : 'FetchVisitSummaryAttachments',
                        Time_Consumed : elapsedTime,
                        User : $rootScope.UserId,
                        Date_Of_Cal : today_now
                        //NetWork_Type : $rootScope.getConnectionType()
                    }];
                    MOBILEDATABASE_ADD('Db_All_Logs', logData, '_soupEntryId');
                    //
                    var mobileSettings = resp.MOBILE_SETTINGS_MAP;
                    var imageCountArr=mobileSettings.shift();

                    //$log.debug('FETCH_MOBILE_ATTACHMENTS on Success' + JSON.stringify(resp));
                    MOBILEDATABASE_ADD('Db_images', mobileSettings, 'Id');



                    if(parseInt(imageCountArr.Name)>mobileSettings.length){
                        var webservice = [{
                            Webservice: 'FetchVisitSummaryAttachments',
                            name: mobileSettings[mobileSettings.length-1].Id,
                            lastSynced: last_sync
                        }];
                        isRunning = true;
                    }else{
                        var webservice = [{
                            Webservice: 'FetchVisitSummaryAttachments',
                            lastSynced: today_now
                        }];
                        isRunning = false;

                    }

                    MOBILEDATABASE_ADD('Db_webservice', webservice, 'Webservice');


                    if (isRunning == true) {
                        recc();
                    }
                    if (!isRunning) defer.resolve('Success');
                    //$log.debug('FetchVisitSummaryAttachments MOBILEDATABASE_ADD' + JSON.stringify(webservice));
                }, function(error) {
                    WEBSERVICE_ERROR('FetchVisitSummaryAttachments', JSON.stringify(error) + '');
                    //$log.debug('FETCH_MOBILE_ATTACHMENTS on error' + JSON.stringify(error));
                    defer.resolve('Failure');
                });
            }
            recc();
            return defer.promise;
        };
    }]).factory('OUTLET_IMAGES', ['$log', 'SOUP', 'SMARTSTORE', 'MOBILEDATABASE_ADD', function($log, SOUP, SMARTSTORE, MOBILEDATABASE_ADD) {
        return function() {
            if (SOUP.isExists('Db_webservice')) {
                var isMakeOutletImagesDirty = SMARTSTORE.buildExactQuerySpec('Db_webservice', 'name', 'MakeOutletImagesDirty');
                if (isMakeOutletImagesDirty != undefined && isMakeOutletImagesDirty[0] == undefined) {
                    var imagesToMakeDirty = [];
                    angular.forEach(SMARTSTORE.buildAllQuerySpec('Db_images', 'ParentId'), function(record, key) {
                        if (record.IsDirty == false && record.External_Id__c == undefined) {
                            record.External_Id__c = record.ParentId;
                            record.IsDirty = true;
                            imagesToMakeDirty.push(record);
                        }
                    });
                    if (imagesToMakeDirty.length > 0) {
                        MOBILEDATABASE_ADD('Db_images', imagesToMakeDirty, 'ParentId');
                    }
                }
                MOBILEDATABASE_ADD('Db_webservice', [{
                    name: 'MakeOutletImagesDirty',
                    Webservice: 'outletImages'
                }], 'name');
            }
        };
    }]).factory('PURGE_DATA', ['$q', '$log', 'SOUP', 'SMARTSTORE','FETCH_DATA_LOCAL',  function($q, $log, SOUP, SMARTSTORE, FETCH_DATA_LOCAL) {
        return function(tableName,deleteQuery) {
            if (SOUP.isExists(tableName)) {
                var defer = $q.defer();
                var datalist=[];
                if(deleteQuery=="")
                    deleteQuery="SELECT  {"+tableName+":_soup} FROM {"+tableName+"} ";
                var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
                var delecteDatalist = FETCH_DATA_LOCAL(tableName, sfSmartstore.buildSmartQuerySpec(deleteQuery, 100));
                angular.forEach(delecteDatalist.currentPageOrderedEntries, function(record, key) {
                    datalist.push(record[0]._soupEntryId);
                });
                sfSmartstore.removeFromSoup(tableName, datalist, null, function(response) {
                    $log.debug("insert success" + JSON.stringify(response));
                    defer.resolve(response);

                }, function(error) {
                    $log.error("insert error" + JSON.stringify(error));
                    defer.reject(error);
                });
                return defer.promise;
            }
        };
    }]);