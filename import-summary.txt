ECLIPSE ANDROID PROJECT IMPORT SUMMARY
======================================

Risky Project Location:
-----------------------
The tools *should* handle project locations in any directory. However,
due to bugs, placing projects in directories containing spaces in the
path, or characters like ", ' and &, have had issues. We're working to
eliminate these bugs, but to save yourself headaches you may want to
move your project to a location where this is not a problem.
C:\Users\Anshuman Stutya\Desktop\Dazeworks\PRIDE
                 -                              

Ignored Files:
--------------
The following files were *not* copied into the new Gradle project; you
should evaluate whether these are still needed in your project and if
so manually move them:

From PRIDE:
* build.xml
* ic_launcher-web.png
* proguard.cfg
From SalesforceSDK:
* ant.properties
* build.xml
* proguard-project.txt
* proguard.cfg
From SmartStore:
* ant.properties
* build.xml
* proguard.cfg

Replaced Jars with Dependencies:
--------------------------------
The importer recognized the following .jar files as third party
libraries and replaced them with Gradle dependencies instead. This has
the advantage that more explicit version information is known, and the
libraries can be updated automatically. However, it is possible that
the .jar file in your project was of an older version than the
dependency we picked, which could render the project not compileable.
You can disable the jar replacement in the import wizard and try again:

guava-r09.jar => com.google.guava:guava:18.0

Moved Files:
------------
Android Gradle projects use a different directory structure than ADT
Eclipse projects. Here's how the projects were restructured:

In SalesforceSDK:
* AndroidManifest.xml => salesforceSDK\src\main\AndroidManifest.xml
* libs\android-junit-report-1.2.6.jar => salesforceSDK\libs\android-junit-report-1.2.6.jar
* libs\apache-mime4j-0.6.jar => salesforceSDK\libs\apache-mime4j-0.6.jar
* libs\cordova-2.3.0.jar => salesforceSDK\libs\cordova-2.3.0.jar
* libs\httpmime-4.0.3.jar => salesforceSDK\libs\httpmime-4.0.3.jar
* libs\volley_android-4.3_r2.1.jar => salesforceSDK\libs\volley_android-4.3_r2.1.jar
* res\ => salesforceSDK\src\main\res\
* src\ => salesforceSDK\src\main\java\
In SmartStore:
* AndroidManifest.xml => smartStore\src\main\AndroidManifest.xml
* assets\ => smartStore\src\main\assets\
* libs\armeabi\libdatabase_sqlcipher.so => smartStore\src\main\jniLibs\armeabi\libdatabase_sqlcipher.so
* libs\armeabi\libsqlcipher_android.so => smartStore\src\main\jniLibs\armeabi\libsqlcipher_android.so
* libs\armeabi\libstlport_shared.so => smartStore\src\main\jniLibs\armeabi\libstlport_shared.so
* libs\commons-codec.jar => smartStore\libs\commons-codec.jar
* libs\sqlcipher.jar => smartStore\libs\sqlcipher.jar
* libs\x86\libdatabase_sqlcipher.so => smartStore\src\main\jniLibs\x86\libdatabase_sqlcipher.so
* libs\x86\libsqlcipher_android.so => smartStore\src\main\jniLibs\x86\libsqlcipher_android.so
* libs\x86\libstlport_shared.so => smartStore\src\main\jniLibs\x86\libstlport_shared.so
* res\ => smartStore\src\main\res\
* src\ => smartStore\src\main\java\
In PRIDE:
* AndroidManifest.xml => pRIDE\src\main\AndroidManifest.xml
* assets\ => pRIDE\src\main\assets\
* lint.xml => pRIDE\lint.xml
* res\ => pRIDE\src\main\res\
* src\ => pRIDE\src\main\java\

Next Steps:
-----------
You can now build the project. The Gradle project needs network
connectivity to download dependencies.

Bugs:
-----
If for some reason your project does not build, and you determine that
it is due to a bug or limitation of the Eclipse to Gradle importer,
please file a bug at http://b.android.com with category
Component-Tools.

(This import summary is for your information only, and can be deleted
after import once you are satisfied with the results.)
